--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Types.Memory_Region_Table
with Abstract_State => null
is

   type Cell_Type
     (Used             : Boolean := False;
      Has_Base_Address : Boolean := False)
   is record
      case Used is
         when True =>
           Region        : Root_Range;
           Owner         : Root_Range;
           Offset        : Page_Count_Type;  --  Offset in the MR
           Length        : Page_Count_Type;
           Mapped_Pages  : Page_Count_Type;  --  <= Length
           Writable      : Boolean;
           Executable    : Boolean;

           case Has_Base_Address is
              when True =>
                 --  Address the MR is mapped to
                 Base_Address  : Virtual_Address_Type;
                 --  # pages which are not mapped to their natural position
                 Shifted_Pages : Page_Count_Type;
              when False =>
                 null;
           end case;
         when False =>
            null;
      end case;
   end record
   with Dynamic_Predicate =>
     (if Used then
       Mapped_Pages <= Length and
       (if Has_Base_Address then Shifted_Pages <= Mapped_Pages));

   Null_Cell : constant Cell_Type := Cell_Type'
     (Used             => False,
      Has_Base_Address => False);

   type Memory_Region_Table_Type is
     array (Memory_Region_Table_Range) of Cell_Type;

end Tau0.Types.Memory_Region_Table;
