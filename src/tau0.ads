--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

limited with Tau0.Data.Devices;

package Tau0
with Abstract_State => State
is

   pragma Annotate (GNATprove, Terminating, Tau0);

   type Tau0_State_Type is range 0 .. 2;

   Setup   : constant Tau0_State_Type := 0;
   Running : constant Tau0_State_Type := 1;
   Failure : constant Tau0_State_Type := 2;

   subtype Valid_State_Type is Tau0_State_Type range Setup .. Running;

   function Tau0_State return Tau0_State_Type
   with Global => State;

   function Valid_Transition (Old_State, New_State : Tau0_State_Type) return Boolean
   is (Old_State = Setup or
      (Old_State = Running and New_State /= Setup))
   with Global => null;

   function Setup_Phase_Complete return Boolean
   with Global => Data.Devices.State;

   procedure Set_Tau0_State (State : Tau0_State_Type)
   with
     Global => (Proof_In => Data.Devices.State,
                In_Out   => Tau0.State),
     Pre    => Valid_Transition (Old_State => Tau0_State,
                                 New_State => State) and
               Setup_Phase_Complete,
     Post   => Tau0_State = State;

end Tau0;
