--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Processors;
with Tau0.Types.Device;

package Tau0.Commands.Devices
is

   procedure Create_Legacy_Device
     (Device :     Types.Device.Device_Range;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input    => Tau0.State,
                In_Out   => Data.Devices.State),
     Pre    => Data.Roots.Invariant,
     Post   => Data.Roots.Invariant;

   procedure Create_PCI_Device
     (Device      :     Types.Device.Device_Range;
      PCI_Bus     :     Types.Device.PCI_Bus_Range;
      PCI_Dev     :     Types.Device.PCI_Device_Range;
      PCI_Func    :     Types.Device.PCI_Function_Range;
      IOMMU_Group :     Types.Device.IOMMU_Group_Type;
      Uses_MSI    :     Boolean;
      Status      : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input  => Tau0.State,
                In_Out => Data.Devices.State),
     Pre    => Data.Roots.Invariant,
     Post   => Data.Roots.Invariant;

   procedure Add_IO_Port_Range
     (Device :     Types.Device.Device_Range;
      From   :     Types.IO_Port_Type;
      To     :     Types.IO_Port_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input  => Tau0.State,
                In_Out => Data.Devices.State),
     Pre    => Data.Roots.Invariant,
     Post   => Data.Roots.Invariant;

   procedure Add_IRQ
     (Device :     Types.Device.Device_Range;
      IRQ    :     Types.Device.HW_IRQ_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input  => Tau0.State,
                In_Out => Data.Devices.State),
     Pre    => Data.Roots.Invariant,
     Post   => Data.Roots.Invariant;

   procedure Add_Memory
     (Device  :     Types.Device.Device_Range;
      Address :     Types.Physical_Address_Type;
      Size    :     Types.Physical_Page_Count_Type;
      Caching :     Types.Caching_Type;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input    => Tau0.State,
                In_Out   => (Data.Devices.State,
                             Data.Memory.Typization_State)),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant;

   procedure Activate
     (Device :     Types.Device.Device_Range;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input  => Tau0.State,
                In_Out => Data.Devices.State),
     Pre    => Data.Roots.Invariant,
     Post   => Data.Roots.Invariant;

end Tau0.Commands.Devices;
