--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.IO_Bitmap;
with Tau0.Types.MSR_Bitmap;

--  Modify subject MSR and IO bitmaps
package Tau0.Data.Memory.Bitmaps
is

   --  Set permissions in an IO bitmap page tuple
   procedure Set_IO_Port_Range
     (IO_Bitmap : Types.Physical_Address_Type;
      From      : Types.IO_Port_Type;
      To        : Types.IO_Port_Type;
      Mode      : Types.IO_Bitmap.Mode_Type)
   with
     Global => (Proof_In => Typization_State,
                In_Out   => Memory_State),
     Pre  => Invariant and then
             Types.Aligned_Page (IO_Bitmap) and then
             Types.Successor_Page (IO_Bitmap) in Types.Physical_Address_Type and then
             Get_Full_Typization (IO_Bitmap) = Types.Typization.IO_Bitmap_Low and then
             Get_Full_Typization (Types.Successor_Page (IO_Bitmap))
               = Types.Typization.IO_Bitmap_High and then
             Accessible (IO_Bitmap) and then
             Accessible (Types.Successor_Page (IO_Bitmap)),
     Post => Invariant and
             (for all M in Memory_Range =>
             (if M < To_Range (IO_Bitmap) or
                 M >= To_Range (IO_Bitmap + 2 * Config.Page_Size)
              then Get_Memory_Model (M) = Get_Memory_Model'Old (M)));

   --  Set permissions in an MSR bitmap page
   procedure Set_MSR_Range
     (MSR_Bitmap : Types.Physical_Address_Type;
      From       : Types.MSR_Bitmap.MSR_Range;
      To         : Types.MSR_Bitmap.MSR_Range;
      Mode       : Types.MSR_Bitmap.Mode_Type)
   with
     Global => (Proof_In => Typization_State,
                In_Out   => Memory_State),
     Pre  => Invariant and then
             Types.Aligned_Page (MSR_Bitmap) and then
             Types.MSR_Bitmap.Same_Subrange (From, To) and then
             Get_Full_Typization (MSR_Bitmap) = Types.Typization.MSR_Bitmap and then
             Accessible (MSR_Bitmap),
     Post => Invariant and
             (for all M in Memory_Range =>
             (if M < To_Range (MSR_Bitmap) or
                 M >= To_Range (MSR_Bitmap + Config.Page_Size)
              then Get_Memory_Model (M) = Get_Memory_Model'Old (M)));

end Tau0.Data.Memory.Bitmaps;
