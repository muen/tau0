--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Types.Page_Table.MPT
with Abstract_State => null
is

   pragma Warnings (Off, "unused variable ""Level""",
                    Reason => "Consistency with other functions To_/From_Entry");

   function To_Entry (Word : Word64; Level : Page_Level_Type) return Entry_Type;

   function From_Entry (Entr : Entry_Type) return Word64;

   pragma Warnings (On, "unused variable ""Level""");

end Tau0.Types.Page_Table.MPT;
