--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Tau0.Types.Command;
with Tau0.Types.Device;
with Tau0.Types.Parameters;
with Tau0.Types.VTd_Tables;

package body Tau0.Command_Stream.XML.Device_Domains
with SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   function Activate_Device_Domain
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain : constant String := Get (Attrs => Attrs, Attr => domain_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Device_Domain (Domain => " & Domain & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Activate_Device_Domain,
         Sequence_Number               => 0,
         Activate_Device_Domain_Params =>
           Types.Parameters.Activate_Device_Domain_Type'
             (Domain => Types.Root_Range'Value (Domain)));
   end Activate_Device_Domain;

   -----------------------------------------------------------------------------

   function Activate_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain          : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Level           : constant String := Get (Attrs => Attrs, Attr => level_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Page_Table" & ASCII.LF &
            "  (Domain          => " & Domain          & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Level           => " & Level           & ")"));

      return Types.Command.Command_Type'
        (ID                             =>
           Types.Command.Activate_Page_Table_Device_Domain,
         Sequence_Number                => 0,
         Activate_Page_Table_Device_Domain_Params =>
           Types.Parameters.Activate_Page_Table_Device_Domain_Type'
             (Domain          => Types.Root_Range'Value (Domain),
              Level           => Types.Page_Level_Type'Value (Level),
              Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address)));
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   function Add_Device
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Device : constant String := Get (Attrs => Attrs, Attr => device_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Add_Device" & ASCII.LF &
            "  (Domain => " & Domain & "," & ASCII.LF &
            "   Device => " & Device & ")"));

      return Types.Command.Command_Type'
        (ID                                 =>
           Types.Command.Add_Device_To_Device_Domain,
         Sequence_Number                    => 0,
         Add_Device_To_Device_Domain_Params =>
           Types.Parameters.Add_Device_To_Device_Domain_Type'
           (Domain => Types.Root_Range'Value (Domain),
            Device => Types.Device.Device_Range'Value (Device)));
   end Add_Device;

   -----------------------------------------------------------------------------

   function Attach_Memory_Region
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain       : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Region       : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Base_Address : constant String := Get (Attrs => Attrs, Attr => baseAddress_attr);
      Offset       : constant String := Get (Attrs => Attrs, Attr => offset_attr);
      Length       : constant String := Get (Attrs => Attrs, Attr => length_attr);
      Index        : constant String := Get (Attrs => Attrs, Attr => tableIndex_attr);
      Writable     : constant String := Get (Attrs => Attrs, Attr => writable_attr);
      Executable   : constant String := Get (Attrs => Attrs, Attr => executable_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Attach_Memory_Region" & ASCII.LF &
            "  (Domain       => " & Domain       & "," & ASCII.LF &
            "   Region       => " & Region       & "," & ASCII.LF &
            "   Base_Address => " & Base_Address & "," & ASCII.LF &
            "   Offset       => " & Offset       & "," & ASCII.LF &
            "   Length       => " & Length       & "," & ASCII.LF &
            "   Index        => " & Index        & "," & ASCII.LF &
            "   Writable     => " & Writable     & "," & ASCII.LF &
            "   Executable   => " & Executable   & ")"));

      return Types.Command.Command_Type'
        (ID                                 =>
           Types.Command.Attach_Memory_Region_Domain,
         Sequence_Number                    => 0,
         Attach_Memory_Region_Domain_Params =>
           Types.Parameters.Attach_Memory_Region_Domain_Type'
            (Domain           => Types.Root_Range'Value (Domain),
             Region           => Types.Root_Range'Value (Region),
             Use_Base_Address => Base_Address /= "",
             Base_Address     => (if Base_Address /= ""
                                  then Types.Virtual_Address_Type'Value (Base_Address)
                                  else 0),
             Offset           => Types.Page_Count_Type'Value (Offset),
             Length           => Types.Page_Count_Type'Value (Length),
             Index            => Types.Memory_Region_Table_Range'Value (Index),
             Writable         => Boolean'Value (Writable),
             Executable       => Boolean'Value (Executable)));
   end Attach_Memory_Region;

   -----------------------------------------------------------------------------

   function Create_Device_Domain
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Level  : constant String := Get (Attrs => Attrs, Attr => level_attr);
      ID     : constant String := Get (Attrs => Attrs, Attr => id_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_Device_Domain" & ASCII.LF &
              "  (Domain => " & Domain & "," & ASCII.LF &
              "   Level  => " & Level  & "," & ASCII.LF &
              "   ID     => " & ID     & ")"));

      return Types.Command.Command_Type'
        (ID                          => Types.Command.Create_Device_Domain,
         Sequence_Number             => 0,
         Create_Device_Domain_Params => Types.Parameters.Create_Device_Domain_Type'
           (Domain => Types.Root_Range'Value (Domain),
            Level  => Types.Nonleaf_Level_Type'Value (Level),
            ID     => Types.VTd_Tables.Domain_Identifier_Type'Value (ID)));
   end Create_Device_Domain;

   -----------------------------------------------------------------------------

   function Create_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page            : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Domain          : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Level           : constant String := Get (Attrs => Attrs, Attr => level_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
      Readable        : constant String := Get (Attrs => Attrs, Attr => readable_attr);
      Writable        : constant String := Get (Attrs => Attrs, Attr => writable_attr);
      Executable      : constant String := Get (Attrs => Attrs, Attr => executable_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_Page_Table" & ASCII.LF &
            "  (Page            => " & Page            & "," & ASCII.LF &
            "   Domain          => " & Domain          & "," & ASCII.LF &
            "   Level           => " & Level           & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Readable        => " & Readable        & "," & ASCII.LF &
            "   Writable        => " & Writable        & "," & ASCII.LF &
            "   Executable      => " & Executable      & ")"));

      return Types.Command.Command_Type'
        (ID                                     =>
           Types.Command.Create_Page_Table_Device_Domain,
         Sequence_Number                        => 0,
         Create_Page_Table_Device_Domain_Params =>
           Types.Parameters.Create_Page_Table_Device_Domain_Type'
           (Page            => Types.Physical_Address_Type'Value (Page),
            Domain          => Types.Root_Range'Value (Domain),
            Level           => Types.Page_Level_Type'Value (Level),
            Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address),
            Readable        => Boolean'Value (Readable),
            Writable        => Boolean'Value (Writable),
            Executable      => Boolean'Value (Executable)));
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   function Lock_Device_Domain
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain : constant String := Get (Attrs => Attrs, Attr => domain_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Lock_Device_Domain (Domain => " & Domain & ")"));

      return Types.Command.Command_Type'
        (ID                        => Types.Command.Lock_Device_Domain,
         Sequence_Number           => 0,
         Lock_Device_Domain_Params => Types.Parameters.Lock_Device_Domain_Type'
           (Domain => Types.Root_Range'Value (Domain)));
   end Lock_Device_Domain;

   -----------------------------------------------------------------------------

   function Map_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Domain          : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
      Table_Index     : constant String := Get (Attrs => Attrs, Attr => tableIndex_attr);
      Offset          : constant String := Get (Attrs => Attrs, Attr => offset_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Map_Page" & ASCII.LF &
            "  (Domain          => " & Domain          & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Table_Index     => " & Table_Index     & "," & ASCII.LF &
            "   Offset          => " & Offset          & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Map_Page_Device_Domain,
         Sequence_Number               => 0,
         Map_Page_Device_Domain_Params => Types.Parameters.Map_Page_Device_Domain_Type'
           (Domain          => Types.Root_Range'Value (Domain),
            Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address),
            Table_Index     => Types.Memory_Region_Table_Range'Value (Table_Index),
            Offset          => Types.Page_Count_Type'Value (Offset)));
   end Map_Page;

   -----------------------------------------------------------------------------

   function Map_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Domain          : constant String := Get (Attrs => Attrs, Attr => domain_attr);
      Table_Index     : constant String := Get (Attrs => Attrs,
                                                Attr  => tableIndex_attr);
      Str_Base_Offset : constant String := Get (Attrs => Attrs,
                                                Attr  => baseOffset_attr);

      Base_Address : constant Virtual_Address_Type
        := Virtual_Address_Type'Value (Str_Base);
      Base_Offset  : constant Page_Count_Type
        := Page_Count_Type'Value (Str_Base_Offset);
      Offset_Pages : constant Page_Count_Type      := Page_Count_Type (Index);
      Offset       : constant Virtual_Address_Type := From_Page_Count (Offset_Pages);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Map_Pages_Device_Domain" & ASCII.LF &
            "  (Domain               => " & Domain          & "," & ASCII.LF &
            "   Base_Virtual_Address => " & Str_Base        & "," & ASCII.LF &
            "   Table_Index          => " & Table_Index     & "," & ASCII.LF &
            "   Base_Offset          => " & Str_Base_Offset & "," & ASCII.LF &
            "   Index                =>"  & Index'Img       & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Map_Page_Device_Domain,
         Sequence_Number               => 0,
         Map_Page_Device_Domain_Params => Types.Parameters.Map_Page_Device_Domain_Type'
           (Domain          => Types.Root_Range'Value (Domain),
            Virtual_Address => Base_Address + Offset,
            Table_Index     => Types.Memory_Region_Table_Range'Value (Table_Index),
            Offset          => Base_Offset + Offset_Pages));
   end Map_Pages;

end Tau0.Command_Stream.XML.Device_Domains;
