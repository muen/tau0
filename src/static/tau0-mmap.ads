with Tau0.Config;
with Tau0.Types;

package Tau0.Mmap
with Abstract_State => null
is

   use type Types.Word64;

   Mmap_Error : exception;

   subtype Filename_Type is String;
   type File_Size_Type is range 0 .. 2 ** 63 - 1;

   --  Memory map an anonymous region of Size bytes to Map_Address.  Initially
   --  the region contains only zeroes.
   procedure Mmap_Anon
     (Map_Address : Types.Word64;
      Size        : File_Size_Type)
   with
     Global   => null,
     Pre      => Map_Address mod Config.Page_Size = 0,
     Annotate => (GNATprove, Terminating);

   procedure Mremap_Fixed
     (Map_Address : Types.Word64;
      Old_Size    : File_Size_Type;
      New_Size    : File_Size_Type)
   with
     Global   => null,
     Pre      => Map_Address mod Config.Page_Size = 0,
     Annotate => (GNATprove, Terminating);

   --  Unmap a mapped memory region.
   procedure Munmap
     (Map_Address : Types.Word64;
      Size        : File_Size_Type)
   with
     Global   => null,
     Annotate => (GNATprove, Terminating);

end Tau0.Mmap;
