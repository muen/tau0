--
--  Copyright (C) 2023  secunet Security Networks AG
--  Copyright (C) 2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Sax.Readers;
with Sax.Symbols;
with Sax.Utils;
with Schema.Readers;

with Tau0.Types;

package Tau0.Command_Stream.XML.Reader
with Abstract_State => null,
     SPARK_Mode     => Off              --  FIXME: Use?
is

   Processing_Error : exception;

   type Reader_Type is new Schema.Readers.Validating_Reader with private;

   --  SAX callback executed when the parser encounters the start of an XML
   --  element. Implements command processing logic.
   overriding
   procedure Start_Element
     (Handler    : in out Reader_Type;
      NS         :        Sax.Utils.XML_NS;
      Local_Name :        Sax.Symbols.Symbol;
      Atts       :        Sax.Readers.Sax_Attribute_List);

   --  Returns the current sequence number value of the given reader.
   function Get_Sequence_Number (Reader : Reader_Type) return Types.Word64;

private

   type Reader_Type is new Schema.Readers.Validating_Reader with record
      --  Set to True if command processing is enabled.
      Do_Process_Commands : Boolean      := False;
      --  Current command sequence number.
      Sequence_Number     : Types.Word64 := 0;
   end record;

   function Get_Sequence_Number (Reader : Reader_Type) return Types.Word64
   is (Reader.Sequence_Number);

end Tau0.Command_Stream.XML.Reader;
