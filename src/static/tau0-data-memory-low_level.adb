--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Ada.Sequential_IO;
with Tau0.Config;
with Tau0.Mmap;
with Tau0.Types.CSL;

separate (Tau0.Data.Memory)
package body Low_Level
with Refined_State => (State => (Memory, First_Written, Last_Written, Num_Alloc))
is

   First_Written : Types.Physical_Page_Count_Type
     := Types.Physical_Page_Count_Type'Last;
   Last_Written  : Types.Physical_Page_Count_Type
     := Types.Physical_Page_Count_Type'First;
   Num_Alloc     : Types.Page_Count_Type := 0;

   Word64s_Per_Page : constant := Config.Page_Size / 8;

   --  Check whether Address is backed by mmaped region
   function Allocated (Address : Types.Physical_Address_Type) return Boolean;

   --  Extend the mapping
   procedure Allocate (Address : Types.Physical_Address_Type)
   with Pre => not Allocated (Address);

   --  Update {First,Last}_Written
   procedure Update_Written (Address : Types.Tau0_Address_Type);

   --  CSL check message as array of 64-bit words.
   type Check_Msg_Array is array (0 .. Types.CSL.Max_Check_String / 8 - 1)
     of Types.Word64;

   --  Convert given check message to word representation.
   function To_Words (Msg : Types.CSL.Check_Msg_Type) return Check_Msg_Array;

   --  Returns Success iff the content of the given page can be constructed
   --  using a CSL fill command. On Success, the pattern is returned as well.
   procedure Is_Fill
     (Page    :     Types.Physical_Page_Count_Type;
      Success : out Boolean;
      Pattern : out Types.Byte);

   --  Look ahead from given Page_Start index to find biggest consecutive fill
   --  region which can be reconstructed with the given pattern.
   procedure Fill_Look_Ahead
     (Page_Start   :     Types.Physical_Page_Count_Type;
      Page_Type    :     Types.Typization.Typization_Type;
      Page_Fill    :     Boolean;
      Page_Pattern :     Types.Byte;
      Page_End     : out Types.Physical_Page_Count_Type);

   -------------------------------------------------------------------------

   procedure Allocate (Address : Types.Physical_Address_Type)
   is
      use type Mmap.File_Size_Type;

      function To_File_Size
        (Count : Types.Page_Count_Type)
         return Mmap.File_Size_Type
      is (Mmap.File_Size_Type (Count) * Config.Page_Size);

      --  Add room for growth, but don't grow too big
      New_Alloc : constant Types.Page_Count_Type
        := Types.Page_Count_Type'Min
          (Types.To_Page_Count (Address + 100 * 2 ** 20),
           Types.Physical_Page_Count_Type'Last);
   begin
      if Num_Alloc = 0 then  --  Establish initial mapping
         Mmap.Mmap_Anon
           (Map_Address => Config.Memory_Map_Base,
            Size        => To_File_Size (New_Alloc));
      else  -- Make mapping larger
         Mmap.Mremap_Fixed
           (Map_Address => Config.Memory_Map_Base,
            Old_Size    => To_File_Size (Num_Alloc),
            New_Size    => To_File_Size (New_Alloc));
      end if;

      Num_Alloc := New_Alloc;
   end Allocate;

   -------------------------------------------------------------------------

   function Allocated (Address : Types.Physical_Address_Type) return Boolean
   is (Types.To_Page_Count (Address) < Num_Alloc);

   -------------------------------------------------------------------------

   procedure Fill_Look_Ahead
     (Page_Start   :     Types.Physical_Page_Count_Type;
      Page_Type    :     Types.Typization.Typization_Type;
      Page_Fill    :     Boolean;
      Page_Pattern :     Types.Byte;
      Page_End     : out Types.Physical_Page_Count_Type)
   is
      use Types;
   begin
      Page_End := Page_Start;

      for I in Page_Start .. Physical_Page_Count_Type'Last loop
         declare
            Next_Type    : constant Types.Typization.Typization_Type
              := Get_Typization
                (Address => From_Page_Count (I));
            Next_Fill    : Boolean;
            Next_Pattern : Byte;
         begin
            exit when Next_Type /= Page_Type
              or else not Typization.Has_Content (Page => I);

            Is_Fill (Page    => I,
                     Success => Next_Fill,
                     Pattern => Next_Pattern);

            exit when Page_Fill /= Next_Fill
              or else Page_Pattern /= Next_Pattern;

            Page_End := I;

            exit when I = Last_Written;
         end;
      end loop;
   end Fill_Look_Ahead;

   -------------------------------------------------------------------------

   --  REVIEW: Is defaulting to zero safe?
   function Get (Address : Types.Tau0_Address_Type) return Types.Word64
   is (if (Address in Types.Physical_Address_Type and then Allocated (Address)) or
         Address in Types.Tau0_Private_Address_Type
       then Memory (To_Range (Address))
       else 0);

   -------------------------------------------------------------------------

   function Get (Address : Types.Tau0_Address_Type) return Types.Word128
   is (Types.Word128'(0 => Get (Address), 1 => Get (Address + 8)));

   -------------------------------------------------------------------

   procedure Is_Fill
     (Page    :     Types.Physical_Page_Count_Type;
      Success : out Boolean;
      Pattern : out Types.Byte)
   is
      use Types;

      Addr         : constant Tau0_Address_Type := From_Page_Count (Page);
      This, Next   : Word64;
      Byte_Pattern : Byte;
   begin
      Pattern := 0;

      for Word in Tau0_Address_Type range 0 .. Word64s_Per_Page - 2 loop
         This := Memory (To_Range (Addr + Word * 8));
         Next := Memory (To_Range (Addr + (Word + 1) * 8));

         if This /= Next then
            Success := False;
            return;
         else
            declare
               Tmp : Word64 := 0;
            begin

               --  Check that qword contains identical bytes.

               Byte_Pattern := Byte (Next and 16#ff#);

               for B in Natural range 0 .. 7 loop
                  Tmp := Tmp + Word64 (Byte_Pattern) * 2 ** (8 * B);
               end loop;

               if Tmp /= Next then
                  Success := False;
                  return;
               end if;
            end;
         end if;
      end loop;

      Pattern := Byte_Pattern;
      Success := True;
   end Is_Fill;

   -------------------------------------------------------------------------

   procedure Set (Address : Types.Tau0_Address_Type; Value : Types.Word64)
   is
   begin
      if Address in Types.Physical_Address_Type and then
        not Allocated (Address => Address)
      then
         Allocate (Address => Address);
      end if;

      Update_Written (Address => Address);

      Memory (To_Range (Address)) := Value;
      Accessible_Lemma (Address);
   end Set;

   -------------------------------------------------------------------------

   procedure Set (Address : Types.Tau0_Address_Type; Value : Types.Word128)
   is
   begin
      Accessible_Lemma (Address);

      Set (Address => Address, Value => Value (0));
      Set (Address => Address + 8, Value => Value (1));
   end Set;

   -------------------------------------------------------------------------

   function To_Words (Msg : Types.CSL.Check_Msg_Type) return Check_Msg_Array
   is
      Words : Check_Msg_Array;
   begin
      for I in Words'Range loop
         declare
            W : Types.Word64 := 0;
         begin
            for J in 0 .. 7 loop
               W := W + (Character'Pos (Msg (I * 8 + Msg'First + J))
                         * 2 ** (J * 8));
            end loop;
            Words (I) := W;
         end;
      end loop;

      return Words;
   end To_Words;

   -------------------------------------------------------------------------

   procedure Update_Written (Address : Types.Tau0_Address_Type)
   is
      Count : constant Types.Page_Count_Type := Types.To_Page_Count (Address);
   begin
      if Address in Types.Physical_Address_Type then
         First_Written := Types.Page_Count_Type'Min (First_Written, Count);
         Last_Written  := Types.Page_Count_Type'Max (Last_Written, Count);
      end if;
   end Update_Written;

   -------------------------------------------------------------------------

   procedure Write_Image_Commands
     (Filename    : String;
      Entry_Point : Types.Tau0_Address_Type)
   with SPARK_Mode => Off
   is
      use Types;

      package IO64 is new Ada.Sequential_IO (Element_Type => Word64);

      --  Write check commands for image command stream loader (CSL).
      procedure Write_Check_Commands (Fd : IO64.File_Type);

      --  Write content commands (fill|write) for image command stream loader
      --  (CSL).
      procedure Write_Content_Commands (Fd : IO64.File_Type);

      --  Write set entry point command to given file.
      procedure Write_Entry_Point_Command
        (Fd          : IO64.File_Type;
         Entry_Point : Tau0_Address_Type);

      ----------------------------------------------------------------------

      procedure Write_Check_Commands (Fd : IO64.File_Type)
      is
         --  CPUID checks.
         CPUID_Checks : constant array
           (Positive range <>) of CSL.CPUID_Check_Type
           := ((EAX        => 16#8000_0007#,
                ECX        => 0,
                Result_Reg => CSL.Reg_EDX,
                Mask       => 16#0100#,
                Value      => 16#0100#,
                Msg        => CSL.To_Msg ("Check for invariant TSC")),
               (EAX        => 1,
                ECX        => 0,
                Result_Reg => CSL.Reg_EDX,
                Mask       => 16#0200#,
                Value      => 16#0200#,
                Msg        => CSL.To_Msg ("Check local APIC support")),
               (EAX        => 1,
                ECX        => 0,
                Result_Reg => CSL.Reg_ECX,
                Mask       => 16#0020_0000#,
                Value      => 16#0020_0000#,
                Msg        => CSL.To_Msg ("Check for x2APIC support")),
               (EAX        => 1,
                ECX        => 0,
                Result_Reg => CSL.Reg_ECX,
                Mask       => 16#0020#,
                Value      => 16#0020#,
                Msg        => CSL.To_Msg ("Check for VMX support"))
              );
      begin
         for Check of CPUID_Checks loop
            IO64.Write
              (File => Fd,
               Item => Word64 (CSL.Cmd_Check_CPUID));
            IO64.Write
              (File => Fd,
               Item => CSL.Cmd_Check_CPUID_Data_Len);
            IO64.Write
              (File => Fd,
               Item => Word64 (Check.EAX) * 2 ** 32 + Word64 (Check.ECX));
            IO64.Write
              (File => Fd,
               Item => Word64 (Check.Mask) * 2 ** 32 + Word64 (Check.Value));
            IO64.Write
              (File => Fd,
               Item => Word64 (CSL.CPU_Register_Type'Pos (Check.Result_Reg)));

            for W of To_Words (Msg => Check.Msg) loop
               IO64.Write
                 (File => Fd,
                  Item => W);
            end loop;
         end loop;
      end Write_Check_Commands;

      ----------------------------------------------------------------------

      procedure Write_Content_Commands (Fd : IO64.File_Type)
      is
         Base_Address : Tau0_Address_Type;
         Curr_Start   : Physical_Page_Count_Type := First_Written;
         Curr_End     : Physical_Page_Count_Type;
         Curr_Type    : Types.Typization.Typization_Type;
         Curr_Fill    : Boolean;
         Curr_Pattern : Byte;
      begin
         while Curr_Start <= Last_Written loop
            Base_Address := From_Page_Count (Curr_Start);
            Curr_Type    := Get_Typization (Address => Base_Address);

            if Curr_Type = Types.Typization.Used
              and then Typization.Has_Content (Page => Curr_Start)
            then
               Is_Fill (Page    => Curr_Start,
                        Success => Curr_Fill,
                        Pattern => Curr_Pattern);

               Fill_Look_Ahead (Page_Start   => Curr_Start,
                                Page_Type    => Curr_Type,
                                Page_Fill    => Curr_Fill,
                                Page_Pattern => Curr_Pattern,
                                Page_End     => Curr_End);

               declare
                  use Tau0.Types.CSL;

                  Cmd : constant Image_Cmd_ID_Type
                    := (if Curr_Fill then Cmd_Fill_Pattern else Cmd_Write_Data);
                  Len : constant Word64
                    := Word64 (Curr_End - Curr_Start + 1) * Config.Page_Size;

                  --  Write memory content of region designated by page start,
                  --  end count to file.
                  procedure Write_Memory_Content
                    (Page_Start : Physical_Page_Count_Type;
                     Page_End   : Physical_Page_Count_Type);

                  ----------------------------------------------------------

                  procedure Write_Memory_Content
                    (Page_Start : Physical_Page_Count_Type;
                     Page_End   : Physical_Page_Count_Type)
                  is
                  begin
                     for Page in Physical_Page_Count_Type range
                       Page_Start .. Page_End
                     loop
                        for Word in Tau0_Address_Type range
                          0 .. Word64s_Per_Page - 1
                        loop
                           IO64.Write
                             (File => Fd,
                              Item => Memory (To_Range
                                (From_Page_Count (Page) + Word * 8)));
                        end loop;
                     end loop;
                  end Write_Memory_Content;
               begin
                  IO64.Write
                    (File => Fd,
                     Item => Word64 (Cmd));
                  IO64.Write
                    (File => Fd,
                     Item => (if Curr_Fill
                              then Cmd_Fill_Pattern_Data_Len
                              else Len + Cmd_Write_Address_Len));
                  IO64.Write
                    (File => Fd,
                     Item => Word64 (Base_Address));

                  if Curr_Fill then
                     IO64.Write
                       (File => Fd,
                        Item => Len);
                     IO64.Write
                       (File => Fd,
                        Item => Word64 (Curr_Pattern));
                  else
                     Write_Memory_Content (Page_Start => Curr_Start,
                                           Page_End   => Curr_End);
                  end if;
               end;

               exit when Curr_End = Physical_Page_Count_Type'Last;
               Curr_Start := Curr_End + 1;
            else
               Curr_Start := Curr_Start + 1;
            end if;
         end loop;
      end Write_Content_Commands;

      ----------------------------------------------------------------------

      procedure Write_Entry_Point_Command
        (Fd          : IO64.File_Type;
         Entry_Point : Tau0_Address_Type)
      is
      begin
         IO64.Write
           (File => Fd,
            Item => Word64 (CSL.Cmd_Set_Entry_Point));
         IO64.Write
           (File => Fd,
            Item => CSL.Cmd_Set_Entry_Point_Data_Len);
         IO64.Write
           (File => Fd,
            Item => Word64 (Entry_Point));
      end Write_Entry_Point_Command;

      File : IO64.File_Type;
   begin
      IO64.Create
        (File => File,
         Name => Filename);
      IO64.Write
        (File => File,
         Item => CSL.Cmd_Stream_Magic);

      Write_Check_Commands
        (Fd => File);
      Write_Content_Commands
        (Fd => File);
      Write_Entry_Point_Command
        (Fd          => File,
         Entry_Point => Entry_Point);

      IO64.Close (File => File);
   end Write_Image_Commands;

begin

   declare
      use Types;
      Offset : constant Word64 := Word64 (Types.Tau0_Private_Address_Type'First);
   begin
      pragma Debug (Debug.Put_Line (Item  => "Mapping private memory backing...",
                                    Level => Debug.Notice));

      --  Establish backing for private memory
      Mmap.Mmap_Anon
        (Map_Address => Config.Memory_Map_Base + Offset,
         Size        => Mmap.File_Size_Type (Config.Tau0_Private_Memory_Size));
   end;

end Low_Level;
