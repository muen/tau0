--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Tau0.Types.Command;
with Tau0.Types.Device;
with Tau0.Types.Parameters;

package body Tau0.Command_Stream.XML.Setup
with SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   function Activate_Tau0
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      pragma Unreferenced (Attrs);
   begin
      pragma Debug (Debug.Put_Line ("Activate_Tau0"));

      return Types.Command.Command_Type'
        (ID              => Types.Command.Activate_Tau0,
         Sequence_Number => 0);
   end Activate_Tau0;

   -----------------------------------------------------------------------------

   function Add_Ioapic
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      SID : constant String := Get (Attrs => Attrs, Attr => sid_attr);
   begin
      pragma Debug (Debug.Put_Line ("Add_Ioapic (Source_ID => " & SID & ")"));

      return Types.Command.Command_Type'
        (ID                => Types.Command.Add_Ioapic,
         Sequence_Number   => 0,
         Add_Ioapic_Params =>
           Types.Parameters.Add_Ioapic_Type'
             (Source_ID => Types.Source_ID_Type'Value (SID)));
   end Add_Ioapic;

   -----------------------------------------------------------------------------

   function Add_Memory_Block
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Address : constant String := Get (Attrs => Attrs, Attr => address_attr);
      Size    : constant String := Get (Attrs => Attrs, Attr => size_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Add_Memory_Block" & ASCII.LF &
           "  (Address => " & Address & "," & ASCII.LF &
           "   Size    => " & Size    & ")"));

      return Types.Command.Command_Type'
        (ID                              => Types.Command.Add_Memory_Block,
         Sequence_Number                 => 0,
         Add_Memory_Block_Params =>
           Types.Parameters.Add_Memory_Block_Type'
             (Address => Types.Tau0_Address_Type'Value (Address),
              Size    => Types.Page_Count_Type'Value (Size)));
   end Add_Memory_Block;

   -----------------------------------------------------------------------------

   function Add_Processor
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      ID      : constant String := Get (Attrs => Attrs, Attr => id_attr);
      APIC_ID : constant String := Get (Attrs => Attrs, Attr => apicId_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Add_Processor"    & ASCII.LF &
           "  (ID      => " & ID      & "," & ASCII.LF &
           "   APIC_ID => " & APIC_ID & ")"));

      return Types.Command.Command_Type'
        (ID                              => Types.Command.Add_Processor,
         Sequence_Number                 => 0,
         Add_Processor_Params =>
           Types.Parameters.Add_Processor_Type'
             (ID      => Types.CPU_Range'Value (ID),
              APIC_ID => Types.APIC_ID_Type'Value (APIC_ID)));
   end Add_Processor;

   -----------------------------------------------------------------------------

   function Create_VTd_Context_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Bus  : constant String := Get (Attrs => Attrs, Attr => bus_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Create_VTd_Context_Table" & ASCII.LF &
           "  (Page => " & Page & "," & ASCII.LF &
           "   Bus  => " & Bus  & ")"));

      return Types.Command.Command_Type'
        (ID                              => Types.Command.Create_VTd_Context_Table,
         Sequence_Number                 => 0,
         Create_VTd_Context_Table_Params =>
           Types.Parameters.Create_VTd_Context_Table_Type'
             (Page => Types.Tau0_Address_Type'Value (Page),
              Bus  => Types.Device.PCI_Bus_Range'Value (Bus)));
   end Create_VTd_Context_Table;

   -----------------------------------------------------------------------------

   function Create_VTd_IRQ_Remap_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page : constant String := Get (Attrs => Attrs, Attr => page_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Create_VTd_IRQ_Remap_Table (Page => " & Page & ")"));

      return Types.Command.Command_Type'
        (ID                                =>
           Types.Command.Create_VTd_IRQ_Remap_Table,
         Sequence_Number                   => 0,
         Create_VTd_IRQ_Remap_Table_Params =>
           Types.Parameters.Create_VTd_IRQ_Remap_Table_Type'
             (Page => Types.Tau0_Address_Type'Value (Page)));
   end Create_VTd_IRQ_Remap_Table;

   -----------------------------------------------------------------------------

   function Create_VTd_Root_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page : constant String := Get (Attrs => Attrs, Attr => page_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Create_VTd_Root_Table (Page => " & Page & ")"));

      return Types.Command.Command_Type'
        (ID                           => Types.Command.Create_VTd_Root_Table,
         Sequence_Number              => 0,
         Create_VTd_Root_Table_Params =>
           Types.Parameters.Create_VTd_Root_Table_Type'
           (Page => Types.Tau0_Address_Type'Value (Page)));
   end Create_VTd_Root_Table;

end Tau0.Command_Stream.XML.Setup;
