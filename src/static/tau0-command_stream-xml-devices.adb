--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Tau0.Types.Device;
with Tau0.Types.Parameters;

package body Tau0.Command_Stream.XML.Devices
with SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   function Activate
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Device : constant String := Get (Attrs => Attrs, Attr => device_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Activate (Device => " & Device & ")"));

      return Types.Command.Command_Type'
        (ID                     => Types.Command.Activate_Device,
         Sequence_Number        => 0,
         Activate_Device_Params => Types.Parameters.Activate_Device_Type'
           (Device => Types.Device.Device_Range'Value (Device)));
   end Activate;

   -----------------------------------------------------------------------------

   function Add_IO_Port_Range
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Device : constant String := Get (Attrs => Attrs, Attr => device_attr);
      From   : constant String := Get (Attrs => Attrs, Attr => from_attr);
      To     : constant String := Get (Attrs => Attrs, Attr => to_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Add_IO_Port_Range" & ASCII.LF &
            "  (Device => " & Device & "," & ASCII.LF &
            "   From   => " & From   & "," & ASCII.LF &
            "   To     => " & To     & ")"));

      return Types.Command.Command_Type'
        (ID                              =>
           Types.Command.Add_IO_Port_Range_Device,
         Sequence_Number                 => 0,
         Add_IO_Port_Range_Device_Params =>
           Types.Parameters.Add_IO_Port_Range_Device_Type'
             (Device => Types.Device.Device_Range'Value (Device),
              From   => Types.IO_Port_Type'Value (From),
              To     => Types.IO_Port_Type'Value (To)));
   end Add_IO_Port_Range;

   -----------------------------------------------------------------------------

   function Add_IRQ
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Device : constant String := Get (Attrs => Attrs, Attr => device_attr);
      IRQ    : constant String := Get (Attrs => Attrs, Attr => irq_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Add_IRQ" & ASCII.LF &
            "  (Device => " & Device & "," & ASCII.LF &
            "   IRQ    => " & IRQ    & ")"));

      return Types.Command.Command_Type'
        (ID                    => Types.Command.Add_IRQ_Device,
         Sequence_Number       => 0,
         Add_IRQ_Device_Params => Types.Parameters.Add_IRQ_Device_Type'
           (Device => Types.Device.Device_Range'Value (Device),
            IRQ    => Types.Device.IRQ_Type'Value (IRQ)));
   end Add_IRQ;

   -----------------------------------------------------------------------------

   function Add_Memory
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Device  : constant String := Get (Attrs => Attrs, Attr => device_attr);
      Address : constant String := Get (Attrs => Attrs, Attr => address_attr);
      Size    : constant String := Get (Attrs => Attrs, Attr => size_attr);
      Caching : constant String := Get (Attrs => Attrs, Attr => caching_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Add_Memory" & ASCII.LF &
            "  (Device  => " & Device  & "," & ASCII.LF &
            "   Address => " & Address & "," & ASCII.LF &
            "   Size    => " & Size    & "," & ASCII.LF &
            "   Caching => " & Caching & ")"));

      return Types.Command.Command_Type'
        (ID                       => Types.Command.Add_Memory_Device,
         Sequence_Number          => 0,
         Add_Memory_Device_Params => Types.Parameters.Add_Memory_Device_Type'
           (Device  => Types.Device.Device_Range'Value (Device),
            Address => Types.Physical_Address_Type'Value (Address),
            Size    => Types.Physical_Page_Count_Type'Value (Size),
            Caching => Parse_Caching (Caching)));
   end Add_Memory;

   -----------------------------------------------------------------------------

   function Create_Legacy_Device
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Device : constant String := Get (Attrs => Attrs, Attr => device_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Create_Legacy_Device (Device => " & Device & ")"));

      return Types.Command.Command_Type'
        (ID                          => Types.Command.Create_Legacy_Device,
         Sequence_Number             => 0,
         Create_Legacy_Device_Params => Types.Parameters.Create_Legacy_Device_Type'
           (Device => Types.Device.Device_Range'Value (Device)));
   end Create_Legacy_Device;

   -----------------------------------------------------------------------------

   function Create_PCI_Device
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Device      : constant String := Get (Attrs => Attrs, Attr => device_attr);
      PCI_Bus     : constant String := Get (Attrs => Attrs, Attr => bus_attr);
      PCI_Dev     : constant String := Get (Attrs => Attrs, Attr => dev_attr);
      PCI_Func    : constant String := Get (Attrs => Attrs, Attr => func_attr);
      IOMMU_Group : constant String := Get (Attrs => Attrs, Attr => iommuGroup_attr);
      Uses_MSI    : constant String := Get (Attrs => Attrs, Attr => usesMSI_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_PCI_Device" & ASCII.LF &
            "  (Device      => " & Device      & "," & ASCII.LF &
            "   PCI_Bus     => " & PCI_Bus     & "," & ASCII.LF &
            "   PCI_Dev     => " & PCI_Dev     & "," & ASCII.LF &
            "   PCI_Func    => " & PCI_Func    & "," & ASCII.LF &
            "   IOMMU_Group => " & IOMMU_Group & "," & ASCII.LF &
            "   Uses_MSI    => " & Uses_MSI    & ")"));

      return Types.Command.Command_Type'
        (ID                       => Types.Command.Create_PCI_Device,
         Sequence_Number          => 0,
         Create_PCI_Device_Params => Types.Parameters.Create_PCI_Device_Type'
           (Device      => Types.Device.Device_Range'Value (Device),
            PCI_Bus     => Types.Device.PCI_Bus_Range'Value (PCI_Bus),
            PCI_Dev     => Types.Device.PCI_Device_Range'Value (PCI_Dev),
            PCI_Func    => Types.Device.PCI_Function_Range'Value (PCI_Func),
            IOMMU_Group => (if IOMMU_Group /= ""
                            then Types.Device.IOMMU_Group_Type'Value (IOMMU_Group)
                            else Types.Device.No_IOMMU_Group),
            Uses_MSI    => Boolean'Value (Uses_MSI)));
   end Create_PCI_Device;

end Tau0.Command_Stream.XML.Devices;
