--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Tau0.Types.Command;
with Tau0.Types.Device;
with Tau0.Types.IO_Bitmap;
with Tau0.Types.MSR_Bitmap;
with Tau0.Types.Parameters;
with Tau0.Types.Root;

package body Tau0.Command_Stream.XML.Subjects
with SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   function Activate_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject         : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Level           : constant String := Get (Attrs => Attrs, Attr => level_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Page_Table_Subject" & ASCII.LF &
            "  (Subject         => " & Subject         & "," & ASCII.LF &
            "   Level           => " & Level           & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & ")"));

      return Types.Command.Command_Type'
        (ID                                 =>
           Types.Command.Activate_Page_Table_Subject,
         Sequence_Number                    => 0,
         Activate_Page_Table_Subject_Params =>
           Types.Parameters.Activate_Page_Table_Subject_Type'
             (Subject         => Types.Root_Range'Value (Subject),
              Level           => Types.Page_Level_Type'Value (Level),
              Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address)));
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   function Activate_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject : constant String := Get (Attrs => Attrs, Attr => subject_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Subject (Subject => " & Subject & ")"));

      return Types.Command.Command_Type'
        (ID                      => Types.Command.Activate_Subject,
         Sequence_Number         => 0,
         Activate_Subject_Params => Types.Parameters.Activate_Subject_Type'
           (Subject => Types.Root_Range'Value (Subject)));
   end Activate_Subject;

   -----------------------------------------------------------------------------

   function Assign_Device_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Device  : constant String := Get (Attrs => Attrs, Attr => device_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Assign_Device_Subject" & ASCII.LF &
            "  (Subject => " & Subject & "," & ASCII.LF &
            "   Device  => " & Device  & ")"));

      return Types.Command.Command_Type'
        (ID                           => Types.Command.Assign_Device_Subject,
         Sequence_Number              => 0,
         Assign_Device_Subject_Params => Types.Parameters.Assign_Device_Subject_Type'
           (Subject => Types.Root_Range'Value (Subject),
            Device  => Types.Device.Device_Range'Value (Device)));
   end Assign_Device_Subject;

   -----------------------------------------------------------------------------

   function Assign_IRQ_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Device  : constant String := Get (Attrs => Attrs, Attr => device_attr);
      IRQ     : constant String := Get (Attrs => Attrs, Attr => irq_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Assign_IRQ_Subject" & ASCII.LF &
            "  (Subject => " & Subject & "," & ASCII.LF &
            "   Device  => " & Device  & "," & ASCII.LF &
            "   IRQ     => " & IRQ     & ")"));

      return Types.Command.Command_Type'
        (ID                        => Types.Command.Assign_IRQ_Subject,
         Sequence_Number           => 0,
         Assign_IRQ_Subject_Params => Types.Parameters.Assign_IRQ_Subject_Type'
           (Subject => Types.Root_Range'Value (Subject),
            Device  => Types.Device.Device_Range'Value (Device),
            IRQ     => Types.Device.HW_IRQ_Type'Value (IRQ)));
   end Assign_IRQ_Subject;

   -----------------------------------------------------------------------------

   function Attach_Memory_Region
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject      : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Region       : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Base_Address : constant String := Get (Attrs => Attrs, Attr => baseAddress_attr);
      Offset       : constant String := Get (Attrs => Attrs, Attr => offset_attr);
      Length       : constant String := Get (Attrs => Attrs, Attr => length_attr);
      Index        : constant String := Get (Attrs => Attrs, Attr => tableIndex_attr);
      Writable     : constant String := Get (Attrs => Attrs, Attr => writable_attr);
      Executable   : constant String := Get (Attrs => Attrs, Attr => executable_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Attach_Memory_Region" & ASCII.LF &
            "  (Subject      => " & Subject      & "," & ASCII.LF &
            "   Region       => " & Region       & "," & ASCII.LF &
            "   Base_Address => " & Base_Address & "," & ASCII.LF &
            "   Offset       => " & Offset       & "," & ASCII.LF &
            "   Index        => " & Index        & "," & ASCII.LF &
            "   Length       => " & Length       & ")"));

      return Types.Command.Command_Type'
        (ID                                  =>
           Types.Command.Attach_Memory_Region_Subject,
         Sequence_Number                     => 0,
         Attach_Memory_Region_Subject_Params =>
           Types.Parameters.Attach_Memory_Region_Subject_Type'
             (Subject          => Types.Root_Range'Value (Subject),
              Region           => Types.Root_Range'Value (Region),
              Use_Base_Address => Base_Address /= "",
              Base_Address     => (if Base_Address /= ""
                                   then Types.Virtual_Address_Type'Value (Base_Address)
                                   else 0),
              Offset           => Types.Page_Count_Type'Value (Offset),
              Length           => Types.Page_Count_Type'Value (Length),
              Index            => Types.Memory_Region_Table_Range'Value (Index),
              Writable         => Boolean'Value (Writable),
              Executable       => Boolean'Value (Executable)));
   end Attach_Memory_Region;

   -----------------------------------------------------------------------------

   function Create_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page            : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Subject         : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Level           : constant String := Get (Attrs => Attrs, Attr => level_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
      Readable        : constant String := Get (Attrs => Attrs, Attr => readable_attr);
      Writable        : constant String := Get (Attrs => Attrs, Attr => writable_attr);
      Executable      : constant String := Get (Attrs => Attrs, Attr => executable_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_Page_Table_Subject" & ASCII.LF &
            "  (Page            => " & Page            & "," & ASCII.LF &
            "   Subject         => " & Subject         & "," & ASCII.LF &
            "   Level           => " & Level           & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Readable        => " & Readable        & "," & ASCII.LF &
            "   Writable        => " & Writable        & "," & ASCII.LF &
            "   Executable      => " & Executable      & ")"));

      return Types.Command.Command_Type'
        (ID                               =>
           Types.Command.Create_Page_Table_Subject,
         Sequence_Number                  => 0,
         Create_Page_Table_Subject_Params =>
           Types.Parameters.Create_Page_Table_Subject_Type'
             (Page            => Types.Physical_Address_Type'Value (Page),
              Subject         => Types.Root_Range'Value (Subject),
              Level           => Types.Page_Level_Type'Value (Level),
              Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address),
              Readable        => Boolean'Value (Readable),
              Writable        => Boolean'Value (Writable),
              Executable      => Boolean'Value (Executable)));
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   function Create_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject    : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Paging     : constant String := Get (Attrs => Attrs, Attr => paging_attr);
      IO_Bitmap  : constant String := Get (Attrs => Attrs, Attr => ioBitmap_attr);
      MSR_Bitmap : constant String := Get (Attrs => Attrs, Attr => msrBitmap_attr);
      CPU        : constant String := Get (Attrs => Attrs, Attr => cpu_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_Subject" & ASCII.LF &
            "  (Subject    => " & Subject    & "," & ASCII.LF &
            "   Paging     => " & Paging     & "," & ASCII.LF &
            "   IO_Bitmap  => " & IO_Bitmap  & "," & ASCII.LF &
            "   MSR_Bitmap => " & MSR_Bitmap & ")"));

      return Types.Command.Command_Type'
        (ID                    => Types.Command.Create_Subject,
         Sequence_Number       => 0,
         Create_Subject_Params => Types.Parameters.Create_Subject_Type'
           (Subject    => Types.Root_Range'Value (Subject),
            Paging     => Types.Root.Subject_Paging_Type'Value (Paging),
            IO_Bitmap  => Types.Physical_Address_Type'Value (IO_Bitmap),
            MSR_Bitmap => Types.Physical_Address_Type'Value (MSR_Bitmap),
            CPU        => Types.CPU_Range'Value (CPU)));
   end Create_Subject;

   -----------------------------------------------------------------------------

   function Lock_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject : constant String := Get (Attrs => Attrs, Attr => subject_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Lock_Subject (Subject => " & Subject & ")"));

      return Types.Command.Command_Type'
        (ID                  => Types.Command.Lock_Subject,
         Sequence_Number     => 0,
         Lock_Subject_Params => Types.Parameters.Lock_Subject_Type'
           (Subject => Types.Root_Range'Value (Subject)));
   end Lock_Subject;

   -----------------------------------------------------------------------------

   function Map_Device_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject         : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Device          : constant String := Get (Attrs => Attrs, Attr => device_attr);
      Page            : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
      Writable        : constant String := Get (Attrs => Attrs, Attr => writable_attr);
      Executable      : constant String := Get (Attrs => Attrs, Attr => executable_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Map_Device_Page_Subject" & ASCII.LF &
            "  (Subject         => " & Subject         & "," & ASCII.LF &
            "   Device          => " & Device          & "," & ASCII.LF &
            "   Page            => " & Page            & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Writable        => " & Writable        & "," & ASCII.LF &
            "   Executable      => " & Executable      & ")"));

      return Types.Command.Command_Type'
        (ID                             => Types.Command.Map_Device_Page_Subject,
         Sequence_Number                => 0,
         Map_Device_Page_Subject_Params =>
           Types.Parameters.Map_Device_Page_Subject_Type'
             (Subject         => Types.Root_Range'Value (Subject),
              Device          => Types.Device.Device_Range'Value (Device),
              Page            => Types.Physical_Address_Type'Value (Page),
              Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address),
              Writable        => Boolean'Value (Writable),
              Executable      => Boolean'Value (Executable)));
   end Map_Device_Page;

   -----------------------------------------------------------------------------

   function Map_Device_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Subject          : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Device           : constant String := Get (Attrs => Attrs, Attr => device_attr);
      Writable         : constant String := Get (Attrs => Attrs, Attr => writable_attr);
      Executable       : constant String := Get (Attrs => Attrs,
                                                 Attr  => executable_attr);
      Str_Base_Address : constant String
        := Get (Attrs => Attrs, Attr => baseVirtualAddress_attr);

      Base_Page : constant Physical_Address_Type
        := Physical_Address_Type'Value (Str_Base);
      Base_Address : constant Virtual_Address_Type
        := Virtual_Address_Type'Value (Str_Base_Address);

      Offset_Pages : constant Page_Count_Type       := Page_Count_Type (Index);
      Virt_Offset  : constant Virtual_Address_Type  := From_Page_Count (Offset_Pages);
      Phys_Offset  : constant Physical_Address_Type := From_Page_Count (Offset_Pages);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Map_Device_Pages_Subject" & ASCII.LF &
            "  (Subject              => " & Subject          & "," & ASCII.LF &
            "   Device               => " & Device           & "," & ASCII.LF &
            "   Base_Page            => " & Str_Base         & "," & ASCII.LF &
            "   Base_Virtual_Address => " & Str_Base_Address & "," & ASCII.LF &
            "   Index                =>"  & Index'Img        & "," & ASCII.LF &
            "   Writable             => " & Writable         & "," & ASCII.LF &
            "   Executable           => " & Executable       & ")"));

      return Types.Command.Command_Type'
        (ID                             => Types.Command.Map_Device_Page_Subject,
         Sequence_Number                => 0,
         Map_Device_Page_Subject_Params =>
           Types.Parameters.Map_Device_Page_Subject_Type'
           (Subject         => Types.Root_Range'Value (Subject),
            Device          => Types.Device.Device_Range'Value (Device),
            Page            => Base_Page + Phys_Offset,
            Virtual_Address => Base_Address + Virt_Offset,
            Writable        => Boolean'Value (Writable),
            Executable      => Boolean'Value (Executable)));
   end Map_Device_Pages;

   -----------------------------------------------------------------------------

   function Map_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject         : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
      Table_Index     : constant String := Get (Attrs => Attrs, Attr => tableIndex_attr);
      Offset          : constant String := Get (Attrs => Attrs, Attr => offset_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Map_Page_Subject" & ASCII.LF &
            "  (Subject         => " & Subject         & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Table_Index     => " & Table_Index     & "," & ASCII.LF &
            "   Offset          => " & Offset          & ")"));

      return Types.Command.Command_Type'
        (ID                      => Types.Command.Map_Page_Subject,
         Sequence_Number         => 0,
         Map_Page_Subject_Params => Types.Parameters.Map_Page_Subject_Type'
           (Subject         => Types.Root_Range'Value (Subject),
            Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address),
            Table_Index     => Types.Memory_Region_Table_Range'Value (Table_Index),
            Offset          => Types.Page_Count_Type'Value (Offset)));
   end Map_Page;

   -----------------------------------------------------------------------------

   function Map_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Subject         : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Table_Index     : constant String := Get (Attrs => Attrs, Attr => tableIndex_attr);
      Str_Base_Offset : constant String := Get (Attrs => Attrs, Attr => baseOffset_attr);

      Canonical_Base_Addr : constant Word64 := Word64'Value (Str_Base);
      Base_Address        : constant Virtual_Address_Type
        := Virtual_Address_Type
          (Canonical_Base_Addr and Word64 (Virtual_Address_Type'Last));

      Base_Offset  : constant Page_Count_Type
        := Page_Count_Type'Value (Str_Base_Offset);
      Offset_Pages : constant Page_Count_Type      := Page_Count_Type (Index);
      Offset       : constant Virtual_Address_Type := From_Page_Count (Offset_Pages);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Map_Pages_Subject" & ASCII.LF &
            "  (Subject              => " & Subject         & "," & ASCII.LF &
            "   Base_Virtual_Address => " & Str_Base        & "," & ASCII.LF &
            "   Table_Index          => " & Table_Index     & "," & ASCII.LF &
            "   Base_Offset          => " & Str_Base_Offset & "," & ASCII.LF &
            "   Index                =>"  & Index'Img       & ")"));

      return Types.Command.Command_Type'
        (ID                      => Types.Command.Map_Page_Subject,
         Sequence_Number         => 0,
         Map_Page_Subject_Params => Types.Parameters.Map_Page_Subject_Type'
           (Subject         => Types.Root_Range'Value (Subject),
            Virtual_Address => Base_Address + Offset,
            Table_Index     => Types.Memory_Region_Table_Range'Value (Table_Index),
            Offset          => Base_Offset + Offset_Pages));
   end Map_Pages;

   -----------------------------------------------------------------------------

   function Set_IO_Port_Range
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      Device  : constant String := Get (Attrs => Attrs, Attr => device_attr);
      From    : constant String := Get (Attrs => Attrs, Attr => from_attr);
      To      : constant String := Get (Attrs => Attrs, Attr => to_attr);
      Mode    : constant String := Get (Attrs => Attrs, Attr => mode_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Set_IO_Port_Range" & ASCII.LF &
            "  (Subject => " & Subject & "," & ASCII.LF &
            "   Device  => " & Device  & "," & ASCII.LF &
            "   From    => " & From    & "," & ASCII.LF &
            "   To      => " & To      & "," & ASCII.LF &
            "   Mode    => " & Mode    & ")"));

      return Types.Command.Command_Type'
        (ID                       => Types.Command.Set_IO_Port_Range,
         Sequence_Number          => 0,
         Set_IO_Port_Range_Params => Types.Parameters.Set_IO_Port_Range_Type'
           (Subject => Types.Root_Range'Value (Subject),
            Device  => Types.Device.Device_Range'Value (Device),
            From    => Types.IO_Port_Type'Value (From),
            To      => Types.IO_Port_Type'Value (To),
            Mode    => (if Mode = "allowed" then Types.IO_Bitmap.Allowed
                                            else Types.IO_Bitmap.Denied)));
   end Set_IO_Port_Range;

   -----------------------------------------------------------------------------

   function Set_MSR_Range
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Subject : constant String := Get (Attrs => Attrs, Attr => subject_attr);
      From    : constant String := Get (Attrs => Attrs, Attr => from_attr);
      To      : constant String := Get (Attrs => Attrs, Attr => to_attr);
      Mode    : constant String := Get (Attrs => Attrs, Attr => mode_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Set_MSR_Range" & ASCII.LF &
            "  (Subject => " & Subject & "," & ASCII.LF &
            "   From    => " & From    & "," & ASCII.LF &
            "   To      => " & To      & "," & ASCII.LF &
            "   Mode    => " & Mode    & ")"));

      return Types.Command.Command_Type'
        (ID                   => Types.Command.Set_MSR_Range,
         Sequence_Number      => 0,
         Set_MSR_Range_Params => Types.Parameters.Set_MSR_Range_Type'
           (Subject => Types.Root_Range'Value (Subject),
            From    => Types.MSR_Bitmap.MSR_Range'Value (From),
            To      => Types.MSR_Bitmap.MSR_Range'Value (To),
            Mode    => (if    Mode = "r"  then Types.MSR_Bitmap.R
                        elsif Mode = "w"  then Types.MSR_Bitmap.W
                        elsif Mode = "rw" then Types.MSR_Bitmap.RW
                        else Types.MSR_Bitmap.Denied)));
   end Set_MSR_Range;

end Tau0.Command_Stream.XML.Subjects;
