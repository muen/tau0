--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;
with Ada.Exceptions;

with Input_Sources.File;
with Input_Sources.Strings;
with Sax.Readers;
with Schema.Readers;
with Schema.Schema_Readers;
with Schema.Validators;
with Unicode.CES.Utf8;

with Debug;
with Tau0.Command_Line;
with Tau0.Command_Stream.XML.Reader;
with Tau0.command_stream_schema;

--  Implementation for static Tau0, using an XML parser.
package body Tau0.Command_Stream
with SPARK_Mode => Off
is

   function Get_Grammar return Schema.Validators.XML_Grammar;

   -----------------------------------------------------------------------------

   function Get_Grammar return Schema.Validators.XML_Grammar
   is
      String_Input  : Input_Sources.Strings.String_Input;
      Schema_Reader : Schema.Schema_Readers.Schema_Reader;
   begin
      Input_Sources.Strings.Open
        (Str      => command_stream_schema.Data,
         Encoding => Unicode.CES.Utf8.Utf8_Encoding,
         Input    => String_Input);

      begin
         Schema.Schema_Readers.Parse
           (Parser => Schema_Reader,
            Input  => String_Input);
      exception
         when others =>
            Input_Sources.Strings.Close (Input => String_Input);
            Schema_Reader.Free;
            raise;
      end;

      Input_Sources.Strings.Close (Input => String_Input);

      return Schema_Reader.Get_Grammar;
   end Get_Grammar;

   -----------------------------------------------------------------------------

   procedure Process
   is
      use type Debug.Level_Type;
      use Sax.Readers;

      File_Input : Input_Sources.File.File_Input;
      Reader     : Command_Stream.XML.Reader.Reader_Type;
   begin
      Input_Sources.File.Open (Filename => Command_Line.Get_Command_Filename,
                               Input    => File_Input);

      Reader.Set_Grammar (Grammar => Get_Grammar);
      Reader.Set_Feature (Name  => Sax.Readers.Schema_Validation_Feature,
                          Value => True);

      XML.Set (Symbols => Schema.Readers.Validating_Reader (Reader));
      begin
         Reader.Parse (Input => File_Input);
      exception
         when Schema.Validators.XML_Validation_Error =>
            File_Input.Close;
            raise XML.Parse_Error with Reader.Get_Error_Message;
         when Command_Stream.XML.Reader.Processing_Error =>
            File_Input.Close;

            --  For processing errors, error messages have already been printed
            --  and the return code has also been set to indicate failure. Thus
            --  simply stop further processing by returning to the caller.

            return;
         when others =>
            File_Input.Close;
            raise;
      end;

      File_Input.Close;

      pragma Debug (Debug.Put_Line
                    (Item  => "Successfully processed"
                     & XML.Reader.Get_Sequence_Number (Reader => Reader)'Img
                     & " commands",
                     Level => Debug.Notice));

   exception
      when E : others =>
         Debug.Put_Line
           (Item  => "Error: " & Ada.Exceptions.Exception_Name (X => E),
            Level => Debug.Error);
         Debug.Put_Line
           (Item  => Ada.Exceptions.Exception_Message (X => E),
            Level => Debug.Error);

         if Tau0.Command_Line.Get_Log_Level /= Debug.Level_Type'First then
            Debug.Put_Line
              (Item  => "Re-run command with " & Debug.Level_Type'First'Img
               & " loglevel to see more context",
               Level => Debug.Error);
         end if;
         Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
   end Process;

end Tau0.Command_Stream;
