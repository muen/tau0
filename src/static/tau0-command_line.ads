--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;

package Tau0.Command_Line
with
  Abstract_State => State,
  Initializes    => State
is

   Invalid_Cmd_Line : exception;

   function Get_Command_Filename return String
   with
     Global   => (Input => State),
     Annotate => (GNATprove, Terminating);

   function Get_Image_Filename return String
   with
     Global   => (Input => State),
     Annotate => (GNATprove, Terminating);

   function Get_Input_Directory return String
   with
     Global   => (Input => State),
     Annotate => (GNATprove, Terminating);

   function Get_Output_Directory return String
   with
     Global   => (Input => State),
     Annotate => (GNATprove, Terminating);

   function Get_Log_Level return Debug.Level_Type
   with
     Global   => (Input => State),
     Annotate => (GNATprove, Terminating);

   function Get_Skip_Hashing return Boolean
   with
     Global   => (Input => State),
     Annotate => (GNATprove, Terminating);

end Tau0.Command_Line;
