--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Page_Intervals;
with System;
with Tau0.Config;
with Tau0.Mmap;

separate (Tau0.Data.Memory)
package body Typization
with Refined_State => (State => (Typizations, Memory_Blocks, Device_Blocks,
                                 Content_Regions))
is

   subtype Nonreserved_Type is T.Typization_Type range T.Undefined .. T.Used;

   subtype Typization_Range is Types.Tau0_Page_Count_Type;

   --  Workaround: A bit packed array type may not have more than Integer'Last+1
   --  elements.  We store the typizations in subarrays of size 1B.
   type Typization_Byte_Range is range 0 .. Typizations_Per_Byte - 1;
   type Typization_Page_Range is
     range 0 .. Typization_Range'Last / Typizations_Per_Byte;

   pragma Warnings (Off, "component size clause forces biased representation");
   type Internal_Subarray is array (Typization_Byte_Range) of Nonreserved_Type
   with Component_Size => 2;
   pragma Warnings (On, "component size clause forces biased representation");

   type Internal_Typization_Array is array (Typization_Page_Range) of Internal_Subarray
   with Component_Size => 8;

   pragma Warnings
     (GNATprove, Off,
      "indirect writes to * through a potential alias are ignored",
      Reason => "Non-overlap with Memory array is checked by "
      & "Config.Initial_Condition");
   pragma Warnings
     (GNATprove, Off,
      "writing * is assumed to have no effects on other non-volatile objects",
      Reason => "Non-overlap with Memory array is checked by "
      & "Config.Initial_Condition");
   --  Stores per-page typizations
   Typizations : Internal_Typization_Array
   with
     Import,
     Address => System'To_Address (Config.Typization_Base);
   pragma Warnings
     (GNATprove, On,
      "writing * is assumed to have no effects on other non-volatile objects");
   pragma Warnings
     (GNATprove, On,
      "indirect writes to * through a potential alias are ignored");

   --  Memory_Blocks comprises the blocks of memory which are usable for data
   --  allocation.  They are announced by the user at the start of the command
   --  stream.  For every interval of Memory_Blocks, the corresponding slice of
   --  the interval Typizations is backed by an mmap.
   Memory_Blocks : Page_Intervals.T := Page_Intervals.Empty_Set;

   --  Device_Blocks comprises memory associated to devices.  Every page that is
   --  an element of Device_Blocks has typization 'Device'.
   --
   --  FIXME/REVIEW: Presumably, Memory_Blocks and Device_Blocks should be
   --  disjoint.  Add invariant?  Or, if device memory may overlap with memory
   --  blocks, we should give precedence to Device_Blocks.
   Device_Blocks : Page_Intervals.T := Page_Intervals.Empty_Set;

   --  Content regions are memory regions which contain actual content, either
   --  created by file|fill-backed memory regions, or they contain data
   --  structures created by Tau0. This is a subset of Memory_Blocks.
   Content_Regions : Page_Intervals.T := Page_Intervals.Empty_Set;

   -----------------------------------------------------------------------------

   function To_Range (Address : Types.Tau0_Address_Type) return Typization_Range;

   function To_Byte_Range
     (Address : Types.Tau0_Address_Type)
      return Typization_Byte_Range;

   function To_Page_Range
     (Address : Types.Tau0_Address_Type)
      return Typization_Page_Range;

   procedure Map_Typization_Range
     (Address : Types.Tau0_Address_Type;
      Length  : Types.Tau0_Page_Count_Type)
   with
     Pre => Types.Aligned_Page (Address) and then
            Length > 0 and then
            Address + Types.From_Page_Count (Length) - 1
              <= Types.Tau0_Address_Type'Last;

   -----------------------------------------------------------------------------

   procedure Add_Device_Block
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
   is
      Last : constant Typization_Range
        := To_Range (Address + Types.From_Page_Count (Size) - 1);
   begin
      Page_Intervals.Add_Interval (Set  => Device_Blocks,
                                   From => To_Range (Address),
                                   To   => Last);
   end Add_Device_Block;

   -----------------------------------------------------------------------------

   procedure Add_Memory_Block
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
   is
      Last : constant Typization_Range
        := To_Range (Address + Types.From_Page_Count (Size) - 1);
   begin
      Map_Typization_Range (Address => Address, Length => Size);
      Page_Intervals.Add_Interval (Set  => Memory_Blocks,
                                   From => To_Range (Address),
                                   To   => Last);
   end Add_Memory_Block;

   -----------------------------------------------------------------------------

   function Content_Regions_Full return Boolean
   is (Page_Intervals.Full (Set => Content_Regions));

   -----------------------------------------------------------------------------

   function Device_Blocks_Full return Boolean
   is (Page_Intervals.Full (Set => Device_Blocks));

   -----------------------------------------------------------------------------

   procedure Declare_Content (Page : Types.Physical_Page_Count_Type)
   is
   begin
      Page_Intervals.Add_Interval (Set  => Content_Regions,
                                   From => Page,
                                   To   => Page);
   end Declare_Content;

   -----------------------------------------------------------------------------

   function Get (Address : Types.Tau0_Address_Type) return T.Typization_Type
   is (if Page_Intervals.Element (Elem => To_Range (Address),
                                  Set  => Memory_Blocks) or
          Address in Types.Tau0_Private_Address_Type
       then Typizations (To_Page_Range (Address)) (To_Byte_Range (Address))
       elsif Page_Intervals.Element (Elem => To_Range (Address),
                                     Set  => Device_Blocks)
       then T.Device_Page
       else T.Reserved);

   -----------------------------------------------------------------------------

   procedure Get_Lemma
   is
   begin
      pragma Assert
        (for all A in Types.Tau0_Address_Type =>
        (for all B in Types.Tau0_Address_Type =>
        (if Page.Within (Address => B, Page => A)
         then To_Byte_Range (A) = To_Byte_Range (B) and
              To_Page_Range (A) = To_Page_Range (B))));
   end Get_Lemma;

   -----------------------------------------------------------------------------

   function Get_Model return T.Typization_Model_Type
   with Refined_Post =>
     (for all A in Types.Tau0_Address_Type => Get_Model'Result (A) = Get (A))
   is
      Model : T.Typization_Model_Type := (others => T.Reserved);
   begin
      for Address in Types.Tau0_Address_Type loop
         pragma Loop_Invariant
           (for all A in Types.Tau0_Address_Type =>
              (if A < Address then Model (A) = Get (A)));

         Model (Address) := Get (Address);
      end loop;

      return Model;
   end Get_Model;

   -----------------------------------------------------------------------------

   function Has_Content
     (Page : Types.Physical_Page_Count_Type)
      return Boolean
   is
   begin
      return Page_Intervals.Contains
        (From => Page,
         To   => Page,
         Set  => Content_Regions);
   end Has_Content;

   -----------------------------------------------------------------------------

   --  Mmap memory region to hold typizations for Tau0 address interval
   --  Address .. Address + From_Page_Count (Length) - 1.
   --
   --  Problem: We store 2**14 page-typizations in one page mapped by mmap.
   --  However, memory blocks need not be aligned to 2**36.  Hence we must take
   --  care (i) that we mmap non-overlapping regions, and (ii) that we do not clobber
   --  typizations in existing mmap-mappings.
   procedure Map_Typization_Range
     (Address : Types.Tau0_Address_Type;
      Length  : Types.Tau0_Page_Count_Type)
   is
      use Types;

      --  How many typizations fit into one mmaped page
      Typizations_Per_Page : constant
        := Config.Page_Size ** 2 * Typization.Typizations_Per_Byte;

      function Mmap_Address (Address : Tau0_Address_Type) return Word64
      is (Config.Typization_Base
            + Word64 (Address / (Config.Page_Size * Typizations_Per_Byte)));

      Last : constant Types.Tau0_Address_Type
        := Address + From_Page_Count (Length) - 1;

      --  Address of the lowest page we touch
      Lowest_Page  : constant Tau0_Address_Type
        := (Address / Typizations_Per_Page) * Typizations_Per_Page;
      --  Address of the highest page we touch
      Highest_Page : constant Tau0_Address_Type
        := (Last / Typizations_Per_Page) * Typizations_Per_Page;

      --  Has the lowest or highest page already been mmapped in another call?
      Low_Mapped : constant Boolean := Page_Intervals.Contains_Some
        (Set  => Memory_Blocks,
         From => To_Range (Lowest_Page),
         To   => To_Range (Lowest_Page + Typizations_Per_Page - 1));
      High_Mapped : constant Boolean := Page_Intervals.Contains_Some
        (Set  => Memory_Blocks,
         From => To_Range (Highest_Page),
         To   => To_Range (Highest_Page + Typizations_Per_Page - 1));

      --  Range of addresses to mmap.
      Mmap_First : constant Word64
        := Mmap_Address (Lowest_Page) + (if Low_Mapped then Config.Page_Size else 0);
      Mmap_Last  : constant Word64
        := Mmap_Address (Highest_Page + Typizations_Per_Page - 1)
             - (if High_Mapped then Config.Page_Size else 0);

      pragma Assert (Mmap_First mod Config.Page_Size = 0);
      pragma Assert (Mmap_Last <= 2**62);

      Old_Model : constant T.Typization_Model_Type := Get_Model with Ghost;
      Page_Base : constant Types.Page_Count_Type   := Types.To_Page_Count (Address);
   begin
      if Mmap_Last >= Mmap_First then
         declare
            pragma Assert (Mmap_Last - Mmap_First < Word64 (Mmap.File_Size_Type'Last));

            Mmap_Length : constant Mmap.File_Size_Type
              := Mmap.File_Size_Type (Mmap_Last - Mmap_First + 1);
         begin
            Mmap.Mmap_Anon (Map_Address => Mmap_First, Size => Mmap_Length);
         end;
      end if;

      for Page in Page_Base .. Page_Base + Length - 1 loop
         declare
            Current_Address : constant Types.Tau0_Address_Type
              := Types.From_Page_Count (Page);
         begin
            --  FIXME: Redo proof
            pragma Loop_Invariant
              (for all A in Types.Tau0_Address_Type =>
              (if Address <= A and A < Current_Address then Get_Model (A) = T.Undefined
               elsif A < Address or A > Address + Types.From_Page_Count (Length) - 1
               then Get_Model (A) = Old_Model (A)));

            Set (Address => Current_Address, Typization => T.Undefined);

            pragma Annotate
              (GNATprove,
               Intentional,
               "precondition might fail, cannot prove T.Transition_Valid",
               "The range was shadowed before, its previous typization is irrelevant.");
         end;
      end loop;
   end Map_Typization_Range;

   -----------------------------------------------------------------------------

   function Memory_Blocks_Full return Boolean
   is (Page_Intervals.Full (Set => Memory_Blocks));

   -----------------------------------------------------------------------------

   procedure Set
     (Address    : Types.Tau0_Address_Type;
      Typization : T.Typization_Type)
   is
   begin
      Typizations (To_Page_Range (Address)) (To_Byte_Range (Address))
        := Typization;

      pragma Assert
        (for all A in Types.Tau0_Address_Type =>
        (if Page.Within (Address => A, Page => Address)
         then To_Byte_Range (A) = To_Byte_Range (Address) and
              To_Page_Range (A) = To_Page_Range (Address)
         else To_Byte_Range (A) /= To_Byte_Range (Address) or
              To_Page_Range (A) /= To_Page_Range (Address)));
   end Set;

   -----------------------------------------------------------------------------

   function To_Range (Address : Types.Tau0_Address_Type) return Typization_Range
   is (Typization_Range (Address / Config.Page_Size));

   function To_Byte_Range
     (Address : Types.Tau0_Address_Type)
      return Typization_Byte_Range
   is (Typization_Byte_Range
         ((Address / Config.Page_Size) mod Typizations_Per_Byte));

   function To_Page_Range
     (Address : Types.Tau0_Address_Type)
      return Typization_Page_Range
   is (Typization_Page_Range
         (Address / (Config.Page_Size * Typizations_Per_Byte)));

begin

   pragma Debug (Debug.Put_Line (Item  => "Mapping private typization backing...",
                                 Level => Debug.Notice));

   --  REVIEW/FIXME: trouble with new Map_Typization_Range?
   --  Establish backing for private memory typizations
   Map_Typization_Range
     (Address => Types.Tau0_Private_Address_Type'First,
      Length  => Config.Tau0_Private_Memory_Size / Config.Page_Size);

end Typization;
