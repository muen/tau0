--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Sax.Symbols;

with Debug;
with Tau0.Types.Parameters;

package body Tau0.Command_Stream.XML
with
  SPARK_Mode => Off
is

   type Symbol_Map_Type is array (Attribute_Type) of Sax.Symbols.Symbol;

   Symbol_Map : Symbol_Map_Type := (others => Sax.Symbols.No_Symbol);

   -----------------------------------------------------------------------------

   function Clear_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page : constant String := Get (Attrs => Attrs, Attr => page_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Clear_Page (Page => " & Page & ")"));

      return Types.Command.Command_Type'
        (ID                => Types.Command.Clear_Page,
         Sequence_Number   => 0,
         Clear_Page_Params => Types.Parameters.Clear_Page_Type'
           (Page => Types.Tau0_Address_Type'Value (Page)));
   end Clear_Page;

   -----------------------------------------------------------------------------

   function Clear_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      pragma Unreferenced (Attrs);

      use Types;

      Base   : constant Tau0_Address_Type := Tau0_Address_Type'Value (Str_Base);
      Offset : constant Tau0_Address_Type
        := From_Page_Count (Tau0_Page_Count_Type (Index));
   begin
      pragma Debug
        (Debug.Put_Line ("Clear_Pages" & ASCII.LF
         & "  (Base_Page => " & Str_Base  & "," & ASCII.LF
         & "   Index     =>"  & Index'Img & ")"));

      return Command.Command_Type'
        (ID                => Command.Clear_Page,
         Sequence_Number   => 0,
         Clear_Page_Params => Parameters.Clear_Page_Type'
           (Page => Base + Offset));
   end Clear_Pages;

   -----------------------------------------------------------------------------

   function Command_Base
     (Name  : String;
      Attrs : Sax.Readers.Sax_Attribute_List)
      return String
   is
   begin
      if Name = "clearPages" or
        Name = "appendVacuousPages" or
        Name = "mapDevicePagesSubject" or
        Name = "mapDevicePagesKernel" or
        Name = "appendPagesMRFile" or
        Name = "appendPagesMRFill"
      then
         return Get (Attrs => Attrs, Attr => basePage_attr);
      elsif Name = "activatePagesMR" or
        Name = "mapPagesSubject" or
        Name = "mapPagesDeviceDomain" or
        Name = "mapPagesKernel"
      then
         return Get (Attrs => Attrs, Attr => baseVirtualAddress_attr);
      else
         return "";
      end if;
   end Command_Base;

   -----------------------------------------------------------------------------

   function Command_Size
     (Name  : String;
      Attrs : Sax.Readers.Sax_Attribute_List)
      return Size_Type
   is
   begin
      if Name = "clearPages" or
        Name = "appendVacuousPages" or
        Name = "activatePagesMR" or
        Name = "mapPagesSubject" or
        Name = "mapPagesDeviceDomain" or
        Name = "mapPagesKernel" or
        Name = "mapDevicePagesSubject" or
        Name = "mapDevicePagesKernel" or
        Name = "appendPagesMRFile" or
        Name = "appendPagesMRFill"
      then
         return Size_Type'Value (Get (Attrs => Attrs, Attr => count_attr));
      else
         return 1;
      end if;
   end Command_Size;

   -----------------------------------------------------------------------------

   function Get
     (Attrs : Sax.Readers.Sax_Attribute_List;
      Attr  : Attribute_Type)
      return String
     with SPARK_Mode => Off
   is
      Index : constant Integer
        := Sax.Readers.Get_Index (List       => Attrs,
                                  URI        => Sax.Symbols.Empty_String,
                                  Local_Name => Symbol_Map (Attr));
   begin
      if Index >= 0 then
         return Sax.Symbols.Get
           (Sym => Sax.Readers.Get_Value (List  => Attrs,
                                          Index => Index)).all;
      end if;

      return "";
   end Get;

   -----------------------------------------------------------------------------

   function Parse_Caching (Caching : String) return Types.Caching_Type
   is
   begin
      if Caching = "UC" then
         return Types.UC;
      elsif Caching = "WC" then
         return Types.WC;
      elsif Caching = "WT" then
         return Types.WT;
      elsif Caching = "WB" then
         return Types.WB;
      elsif Caching = "WP" then
         return Types.WP;
      else
         raise Parse_Error with "Cannot parse caching type '" & Caching & "'";
      end if;
   end Parse_Caching;

   -----------------------------------------------------------------------------

   procedure Set (Symbols : Schema.Readers.Validating_Reader)
   is
      use type Sax.Symbols.Symbol;
   begin
      Symbol_Map :=
        (count_attr              => Symbols.Find_Symbol (Str => "count"),
         domain_attr             => Symbols.Find_Symbol (Str => "domain"),
         level_attr              => Symbols.Find_Symbol (Str => "level"),
         device_attr             => Symbols.Find_Symbol (Str => "device"),
         region_attr             => Symbols.Find_Symbol (Str => "region"),
         baseAddress_attr        => Symbols.Find_Symbol (Str => "baseAddress"),
         basePage_attr           => Symbols.Find_Symbol (Str => "basePage"),
         offset_attr             => Symbols.Find_Symbol (Str => "offset"),
         length_attr             => Symbols.Find_Symbol (Str => "length"),
         tableIndex_attr         => Symbols.Find_Symbol (Str => "tableIndex"),
         readable_attr           => Symbols.Find_Symbol (Str => "readable"),
         writable_attr           => Symbols.Find_Symbol (Str => "writable"),
         executable_attr         => Symbols.Find_Symbol (Str => "executable"),
         id_attr                 => Symbols.Find_Symbol (Str => "id"),
         page_attr               => Symbols.Find_Symbol (Str => "page"),
         baseOffset_attr         => Symbols.Find_Symbol (Str => "baseOffset"),
         from_attr               => Symbols.Find_Symbol (Str => "from"),
         to_attr                 => Symbols.Find_Symbol (Str => "to"),
         irq_attr                => Symbols.Find_Symbol (Str => "irq"),
         address_attr            => Symbols.Find_Symbol (Str => "address"),
         size_attr               => Symbols.Find_Symbol (Str => "size"),
         caching_attr            => Symbols.Find_Symbol (Str => "caching"),
         bus_attr                => Symbols.Find_Symbol (Str => "bus"),
         dev_attr                => Symbols.Find_Symbol (Str => "dev"),
         func_attr               => Symbols.Find_Symbol (Str => "func"),
         iommuGroup_attr         => Symbols.Find_Symbol (Str => "iommuGroup"),
         usesMSI_attr            => Symbols.Find_Symbol (Str => "usesMSI"),
         kernel_attr             => Symbols.Find_Symbol (Str => "kernel"),
         cpu_attr                => Symbols.Find_Symbol (Str => "cpu"),
         baseVirtualAddress_attr => Symbols.Find_Symbol (Str => "baseVirtualAddress"),
         filename_attr           => Symbols.Find_Symbol (Str => "filename"),
         fill_attr               => Symbols.Find_Symbol (Str => "fill"),
         hash_attr               => Symbols.Find_Symbol (Str => "hash"),
         sid_attr                => Symbols.Find_Symbol (Str => "sid"),
         apicId_attr             => Symbols.Find_Symbol (Str => "apicId"),
         subject_attr            => Symbols.Find_Symbol (Str => "subject"),
         virtualAddress_attr     => Symbols.Find_Symbol (Str => "virtualAddress"),
         ioBitmap_attr           => Symbols.Find_Symbol (Str => "ioBitmap"),
         msrBitmap_attr          => Symbols.Find_Symbol (Str => "msrBitmap"),
         mode_attr               => Symbols.Find_Symbol (Str => "mode"),
         paging_attr             => Symbols.Find_Symbol (Str => "paging"),
         entryPoint_attr         => Symbols.Find_Symbol (Str => "entryPoint"));

      for I in Symbol_Map'Range loop
         if Symbol_Map (I) = Sax.Symbols.No_Symbol then
            raise Parse_Error with "Error initialising symbol map entry"
              & I'Img;
         end if;
      end loop;
   end Set;

   -----------------------------------------------------------------------------

   function Write_Image_Commands
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Entry_Point : constant String
        := Get (Attrs => Attrs, Attr => entryPoint_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Write_Image_Commands (Entry_Point => " & Entry_Point & ")"));

      return Types.Command.Command_Type'
        (ID                         => Types.Command.Write_Image_Commands,
         Sequence_Number            => 0,
         Write_Image_Commands_Param =>
           (Entry_Point => Types.Tau0_Address_Type'Value (Entry_Point)));
   end Write_Image_Commands;

end Tau0.Command_Stream.XML;
