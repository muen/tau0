--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Command_Stream.XML.Subjects
with Abstract_State => null,
     SPARK_Mode     => Off
is

   function Create_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Lock_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Activate_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Attach_Memory_Region
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Create_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Activate_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Map_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Map_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type;

   function Map_Device_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Map_Device_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type;

   function Assign_Device_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Assign_IRQ_Subject
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Set_IO_Port_Range
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Set_MSR_Range
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

end Tau0.Command_Stream.XML.Subjects;
