--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Interfaces.C;
with System.Storage_Elements;

package body Tau0.Mmap
with SPARK_Mode => Off
is

   use type Interfaces.C.int;

   type off_t  is new Interfaces.Integer_64;
   type Bits   is new Interfaces.Unsigned_32;

   -----------------------------------------------------------------------------

   MAP_PRIVATE   : constant Bits := 2;
   MAP_FIXED     : constant Bits := 16;
   MAP_ANONYMOUS : constant Bits := 32;
   PROT_READ     : constant Bits := 1;
   PROT_WRITE    : constant Bits := 2;
   MAP_FAILED    : constant System.Address
     := System'To_Address (System.Storage_Elements.Integer_Address'Last);

   -----------------------------------------------------------------------------

   function mmap
     (addr   : System.Address;
      len    : Interfaces.C.size_t;
      prot   : Interfaces.C.int;
      flags  : Interfaces.C.int;
      fildes : Interfaces.C.int;
      off    : off_t)
      return System.Address;
   pragma Import (C, mmap, "mmap");

   function mremap
     (old_address : System.Address;
      old_size    : Interfaces.C.size_t;
      new_size    : Interfaces.C.size_t;
      flags       : Interfaces.C.int)
      return System.Address;
   pragma Import (C, mremap, "mremap");

   function munmap
     (addr : System.Address;
      len  : Interfaces.C.size_t)
      return Interfaces.C.int;
   pragma Import (C, munmap, "munmap");

   -----------------------------------------------------------------------------

   procedure Mmap_Anon
     (Map_Address : Types.Word64;
      Size        : File_Size_Type)
   is
      use type System.Address;

      Mapped_Address : System.Address;
   begin
      pragma Debug (Debug.Put ("Mmap_Anon (Map_Address => "));
      pragma Debug (Debug.Put (Map_Address, Justified => False));
      pragma Debug (Debug.Put (", Size => "));
      pragma Debug (Debug.Put (Types.Word64 (Size), Justified => False));
      pragma Debug (Debug.Put_Line (")"));

      Mapped_Address := mmap
        (addr   => System'To_Address (Map_Address),
         len    => Interfaces.C.size_t (Size),
         prot   => Interfaces.C.int (PROT_READ or PROT_WRITE),
         flags  => Interfaces.C.int (MAP_PRIVATE or MAP_ANONYMOUS or MAP_FIXED),
         fildes => -1,
         off    => 0);

      if Mapped_Address = MAP_FAILED then
         raise Mmap_Error with "Mmap_Anon: MAP_FAILED";
      elsif Mapped_Address /= System'To_Address (Map_Address) then
         raise Mmap_Error with "Mmap_Anon: mapped to unexpected address";
      end if;
   end Mmap_Anon;

   -----------------------------------------------------------------------------

   procedure Mremap_Fixed
     (Map_Address : Types.Word64;
      Old_Size    : File_Size_Type;
      New_Size    : File_Size_Type)
   is
      use type System.Address;

      Mapped_Address : System.Address;
   begin
      pragma Debug (Debug.Put ("Mmap_Fixed (Map_Address => "));
      pragma Debug (Debug.Put (Map_Address, Justified => False));
      pragma Debug (Debug.Put (", Old_Size => "));
      pragma Debug (Debug.Put (Types.Word64 (Old_Size), Justified => False));
      pragma Debug (Debug.Put (", New_Size => "));
      pragma Debug (Debug.Put (Types.Word64 (New_Size), Justified => False));
      pragma Debug (Debug.Put_Line (")"));

      Mapped_Address := mremap
        (old_address => System'To_Address (Map_Address),
         old_size    => Interfaces.C.size_t (Old_Size),
         new_size    => Interfaces.C.size_t (New_Size),
         flags       => Interfaces.C.int (0));

      if Mapped_Address = MAP_FAILED then
         raise Mmap_Error with "Mremap_Fixed: MAP_FAILED";
      elsif Mapped_Address /= System'To_Address (Map_Address) then
         raise Mmap_Error with "Mremap_Fixed: mapped to unexpected address";
      end if;
   end Mremap_Fixed;

   -----------------------------------------------------------------------------

   procedure Munmap
     (Map_Address : Types.Word64;
      Size        : File_Size_Type)
   is
      Return_Value : Interfaces.C.int;
   begin
      Return_Value := munmap
        (addr => System'To_Address (Map_Address),
         len  => Interfaces.C.size_t (Size));

      if Return_Value < 0 then
         raise Mmap_Error
           with "Munmap: Error in munmap (" & Return_Value'Img & ")";
      end if;
   end Munmap;

end Tau0.Mmap;
