--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

--  Command Stream Loader (CSL) types.
package Tau0.Types.CSL
is

   --  xxd -l 8 -p /dev/random
   Cmd_Stream_Magic : constant := 16#8adc_5fa2_448c_b65e#;

   type Image_Cmd_ID_Type is new Word16;

   Cmd_Write_Data      : constant Image_Cmd_ID_Type := 16#0000#;
   Cmd_Fill_Pattern    : constant Image_Cmd_ID_Type := 16#0001#;
   Cmd_Set_Entry_Point : constant Image_Cmd_ID_Type := 16#0002#;
   Cmd_Check_CPUID     : constant Image_Cmd_ID_Type := 16#0003#;

   --  Command data lengths in bytes.
   Cmd_Set_Entry_Point_Data_Len : constant := 8;
   Cmd_Write_Address_Len        : constant := 8;
   Cmd_Fill_Pattern_Data_Len    : constant := 24;
   Cmd_Check_CPUID_Data_Len     : constant := 88;

   Max_Check_String : constant := 64;

   type Check_Msg_Type is new String (1 .. Max_Check_String);

   function To_Msg (Str : String) return Check_Msg_Type
   with
      Pre  => Str'Length <= Check_Msg_Type'Last - 1
                 and Str'First + Str'Length < Positive'Last,
      Post => To_Msg'Result (To_Msg'Result'Last) = ASCII.NUL;

   type CPU_Register_Type is (Reg_EAX, Reg_EBX, Reg_ECX, Reg_EDX);

   for CPU_Register_Type use
     (Reg_EAX => 0,
      Reg_EBX => 1,
      Reg_ECX => 2,
      Reg_EDX => 3);

   type CPUID_Check_Type is record
      EAX, ECX   : Word32;
      Result_Reg : CPU_Register_Type;
      Mask       : Word32;
      Value      : Word32;
      Msg        : Check_Msg_Type;
   end record;

end Tau0.Types.CSL;
