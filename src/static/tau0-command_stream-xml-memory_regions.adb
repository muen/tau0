--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Characters.Handling;
with Debug;

with Mutools.Strings;
with Mutools.Utils;

with Tau0.Command_Line;
with Tau0.Files;
with Tau0.Types.Command;
with Tau0.Types.Parameters;
with Tau0.Types.Root;

package body Tau0.Command_Stream.XML.Memory_Regions
with SPARK_Mode => Off
is

   Input_Directories : constant Mutools.Strings.String_Array
     := Mutools.Strings.Tokenize (Str => Command_Line.Get_Input_Directory);

   -----------------------------------------------------------------------------

   function Activate_Memory_Region
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
   begin
      pragma Debug
        (Debug.Put_Line ("Activate_Memory_Region (Region => " & Region & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Activate_Memory_Region,
         Sequence_Number               => 0,
         Activate_Memory_Region_Params =>
           Types.Parameters.Activate_Memory_Region_Type'
             (Region => Types.Root_Range'Value (Region)));
   end Activate_Memory_Region;

   -----------------------------------------------------------------------------

   function Activate_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Region          : constant String := Get (Attrs => Attrs,
                                                Attr  => region_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Page" & ASCII.LF &
            "  (Region          => " & Region          & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & ")"));

      return Types.Command.Command_Type'
        (ID                      => Types.Command.Activate_Page_MR,
         Sequence_Number         => 0,
         Activate_Page_MR_Params => Types.Parameters.Activate_Page_MR_Type'
           (Region          => Types.Root_Range'Value (Region),
            Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address)));
   end Activate_Page;

   -----------------------------------------------------------------------------

   function Activate_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Base   : constant Virtual_Address_Type
        := Virtual_Address_Type'Value (Str_Base);
      Offset : constant Virtual_Address_Type
        := From_Page_Count (Page_Count_Type (Index));
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Pages" & ASCII.LF
              & "  (Base_Virtual_Address => " & Str_Base  & ", " & ASCII.LF
              & "   Index                =>"  & Index'Img & ")"));

      return Command.Command_Type'
        (ID                      => Command.Activate_Page_MR,
         Sequence_Number         => 0,
         Activate_Page_MR_Params => Parameters.Activate_Page_MR_Type'
           (Region          => Types.Root_Range'Value (Region),
            Virtual_Address => Base + Offset));
   end Activate_Pages;

   -----------------------------------------------------------------------------

   function Activate_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Region          : constant String := Get (Attrs => Attrs,
                                                Attr  => region_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
      Level           : constant String := Get (Attrs => Attrs,
                                                Attr  => level_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Activate_Page_Table" & ASCII.LF &
            "  (Region          => " & Region          & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & "," & ASCII.LF &
            "   Level           => " & Level           & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Activate_Page_Table_MR,
         Sequence_Number               => 0,
         Activate_Page_Table_MR_Params =>
           Types.Parameters.Activate_Page_Table_MR_Type'
             (Region          => Types.Root_Range'Value (Region),
              Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address),
              Level           => Types.Page_Level_Type'Value (Level)));
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   --  TODO: factor out common code with Append_Page_Fill
   function Append_Page_File
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      use Types;

      Str_Region : constant String := Get (Attrs => Attrs,
                                           Attr  => region_attr);
      Str_Page   : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Filename   : constant String := Get (Attrs => Attrs,
                                           Attr  => filename_attr);
      Str_Offset : constant String := Get (Attrs => Attrs, Attr => offset_attr);

      Region : constant Root_Range        := Root_Range'Value (Str_Region);
      Page   : constant Tau0_Address_Type := Tau0_Address_Type'Value (Str_Page);
      Offset : constant Word64            := Word64'Value (Str_Offset);

      File_Path : constant String := Mutools.Utils.Lookup_File
        (Filename    => Filename,
         Directories => Input_Directories);

      Contents : Types.Page_Array;
   begin
      pragma Debug
        (Debug.Put_Line
           ("Append_Page_File" & ASCII.LF   &
            "  (Region   => "  & Str_Region & "," & ASCII.LF &
            "   Page     => "  & Str_Page   & "," & ASCII.LF &
            "   Filename => "  & Filename   & "," & ASCII.LF &
            "   Offset   => "  & Str_Offset & ")"));

      Files.Read_Page
        (Filename => File_Path,
         Offset   => Offset,
         Contents => Contents);
      return Command.Command_Type'
        (ID                    => Command.Append_Page_MR,
         Sequence_Number       => 0,
         Append_Page_MR_Params => Parameters.Append_Page_MR_Type'
           (Region   => Region,
            Page     => Page,
            Contents => Contents));
   end Append_Page_File;

   -----------------------------------------------------------------------------

   function Append_Pages_File
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Str_Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Filename   : constant String := Get (Attrs => Attrs,
                                           Attr  => filename_attr);
      Str_Offset : constant String := Get (Attrs => Attrs,
                                           Attr  => baseOffset_attr);

      Region      : constant Root_Range        := Root_Range'Value (Str_Region);
      Base        : constant Tau0_Address_Type := Tau0_Address_Type'Value (Str_Base);
      File_Offset : constant Word64            := Types.Word64'Value (Str_Offset);
      Offset      : constant Tau0_Address_Type := From_Page_Count
        (Tau0_Page_Count_Type (Index));

      File_Path : constant String := Mutools.Utils.Lookup_File
        (Filename    => Filename,
         Directories => Input_Directories);

      Contents : Types.Page_Array;
   begin
      pragma Debug
        (Debug.Put_Line
           ("Append_Pages_File"  & ASCII.LF &
            "  (Region      => " & Str_Region & "," & ASCII.LF &
            "   Base_Page   => " & Str_Base   & "," & ASCII.LF &
            "   Filename    => " & Filename   & "," & ASCII.LF &
            "   Base_Offset => " & Str_Offset & "," & ASCII.LF &
            "   Index       =>"  & Index'Img & ")"));

      Files.Read_Page
        (Filename => File_Path,
         Offset   => File_Offset + Word64 (Offset),
         Contents => Contents);
      return Command.Command_Type'
        (ID                    => Command.Append_Page_MR,
         Sequence_Number       => 0,
         Append_Page_MR_Params => Parameters.Append_Page_MR_Type'
           (Region   => Region,
            Page     => Base + Offset,
            Contents => Contents));
   end Append_Pages_File;

   -----------------------------------------------------------------------------

   function Append_Page_Fill
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Str_Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Str_Page   : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Str_Fill   : constant String := Get (Attrs => Attrs, Attr => fill_attr);

      Region : constant Types.Root_Range
        := Types.Root_Range'Value (Str_Region);
      Page   : constant Types.Tau0_Address_Type
        := Types.Tau0_Address_Type'Value (Str_Page);
      Fill   : constant Types.Word64
        := Types.Word64'Value (Str_Fill);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Append_Page_Fill" & ASCII.LF &
            "  (Region => " & Str_Region & "," & ASCII.LF &
            "   Page   => " & Str_Page   & "," & ASCII.LF &
            "   Fill   => " & Str_Fill   & ")"));

      return Types.Command.Command_Type'
        (ID                    => Types.Command.Append_Page_MR,
         Sequence_Number       => 0,
         Append_Page_MR_Params => Types.Parameters.Append_Page_MR_Type'
           (Region   => Region,
            Page     => Page,
            Contents => (others => Fill)));
   end Append_Page_Fill;

   -----------------------------------------------------------------------------

   function Append_Pages_Fill
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Str_Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Str_Fill   : constant String := Get (Attrs => Attrs, Attr => fill_attr);

      Region : constant Root_Range        := Root_Range'Value (Str_Region);
      Base   : constant Tau0_Address_Type := Tau0_Address_Type'Value (Str_Base);
      Fill   : constant Word64            := Word64'Value (Str_Fill);
      Offset : constant Tau0_Address_Type
        := From_Page_Count (Tau0_Page_Count_Type (Index));
   begin
      pragma Debug
        (Debug.Put_Line
           ("Append_Pages_Fill" & ASCII.LF &
            "  (Region    => " & Str_Region & "," & ASCII.LF &
            "   Base_Page => " & Str_Base   & "," & ASCII.LF &
            "   Fill      => " & Str_Fill   & "," & ASCII.LF &
            "   Index     =>"  & Index'Img  & ")"));

      return Command.Command_Type'
        (ID                    => Command.Append_Page_MR,
         Sequence_Number       => 0,
         Append_Page_MR_Params => Parameters.Append_Page_MR_Type'
           (Region   => Region,
            Page     => Base + Offset,
            Contents => (others => Fill)));
   end Append_Pages_Fill;

   -----------------------------------------------------------------------------

   function Append_Vacuous_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Page   : constant String := Get (Attrs => Attrs, Attr => page_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Append_Vacuous_Page" & ASCII.LF &
            "  (Region => " & Region & "," & ASCII.LF &
            "   Page   => " & Page   & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Append_Vacuous_Page_MR,
         Sequence_Number               => 0,
         Append_Vacuous_Page_MR_Params =>
           Types.Parameters.Append_Vacuous_Page_MR_Type'
             (Region => Types.Root_Range'Value (Region),
              Page   => Types.Physical_Address_Type'Value (Page)));
   end Append_Vacuous_Page;

   -----------------------------------------------------------------------------

   function Append_Vacuous_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type
   is
      use Types;

      Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Base   : constant Tau0_Address_Type
        := Tau0_Address_Type'Value (Str_Base);
      Offset : constant Tau0_Address_Type
        := From_Page_Count (Tau0_Page_Count_Type (Index));
   begin
      pragma Debug
        (Debug.Put_Line
           ("Append_Vacuous_Pages" & ASCII.LF &
            "  (Region    => " & Region    & "," & ASCII.LF &
            "   Base_Page => " & Str_Base  & "," & ASCII.LF &
            "   Index     =>"  & Index'Img & ")"));

      return Types.Command.Command_Type'
        (ID                            => Types.Command.Append_Vacuous_Page_MR,
         Sequence_Number               => 0,
         Append_Vacuous_Page_MR_Params =>
           Types.Parameters.Append_Vacuous_Page_MR_Type'
             (Region => Types.Root_Range'Value (Region),
              Page   => Base + Offset));
   end Append_Vacuous_Pages;

   -----------------------------------------------------------------------------

   function Create_Memory_Region
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Region  : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Level   : constant String := Get (Attrs => Attrs, Attr => level_attr);
      Caching : constant String := Get (Attrs => Attrs, Attr => caching_attr);
      Hash    : constant String := Get (Attrs => Attrs, Attr => hash_attr);

      Skip_Hash : constant Boolean
        := Command_Line.Get_Skip_Hashing or Hash = "";
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_Memory_Region" & ASCII.LF &
            "  (Region  => " & Region  & "," & ASCII.LF &
            "   Level   => " & Level   & "," & ASCII.LF &
            "   Caching => " & Caching & "," & ASCII.LF &
            "   Hash    => " & Hash (Hash'First + 3 .. Hash'Last - 1) & ")"));

      return Types.Command.Command_Type'
        (ID                          => Types.Command.Create_Memory_Region,
         Sequence_Number             => 0,
         Create_Memory_Region_Params => Types.Parameters.Create_Memory_Region_Type'
           (Region  => Types.Root_Range'Value (Region),
            Level   => Types.Nonleaf_Level_Type'Value (Level),
            Caching => Parse_Caching (Caching),
            Hash    => (if Skip_Hash then Types.Root.Dont_Check
                        else Types.Root.Optional_Hash_Type'
                          (Check  => True,
                           Digest => Ada.Characters.Handling.To_Lower
                             (Hash (Hash'First + 3 .. Hash'Last - 1))))));
   end Create_Memory_Region;

   -----------------------------------------------------------------------------

   function Create_Page_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Page            : constant String := Get (Attrs => Attrs, Attr => page_attr);
      Region          : constant String := Get (Attrs => Attrs, Attr => region_attr);
      Level           : constant String := Get (Attrs => Attrs, Attr => level_attr);
      Virtual_Address : constant String := Get (Attrs => Attrs,
                                                Attr  => virtualAddress_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Create_Page_Table" & ASCII.LF &
            "  (Page            => " & Page            & "," & ASCII.LF &
            "   Region          => " & Region          & "," & ASCII.LF &
            "   Level           => " & Level           & "," & ASCII.LF &
            "   Virtual_Address => " & Virtual_Address & ")"));

      return Types.Command.Command_Type'
        (ID                          => Types.Command.Create_Page_Table_MR,
         Sequence_Number             => 0,
         Create_Page_Table_MR_Params => Types.Parameters.Create_Page_Table_MR_Type'
           (Page            => Types.Tau0_Private_Address_Type'Value (Page),
            Region          => Types.Root_Range'Value (Region),
            Level           => Types.Page_Level_Type'Value (Level),
            Virtual_Address => Types.Virtual_Address_Type'Value (Virtual_Address)));
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   function Lock_Memory_Region
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type
   is
      Region : constant String := Get (Attrs => Attrs, Attr => region_attr);
   begin
      pragma Debug
        (Debug.Put_Line
           ("Lock_Memory_Region (Region => " & Region & ")"));

      return Types.Command.Command_Type'
        (ID                        => Types.Command.Lock_Memory_Region,
         Sequence_Number           => 0,
         Lock_Memory_Region_Params => Types.Parameters.Lock_Memory_Region_Type'
           (Region => Types.Root_Range'Value (Region)));
   end Lock_Memory_Region;

end Tau0.Command_Stream.XML.Memory_Regions;
