--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Sax.Readers;
with Tau0.Types.Command;
with Schema.Readers;

private package Tau0.Command_Stream.XML
with Abstract_State => State,
     SPARK_Mode     => Off              --  FIXME: Use?
is

   Parse_Error : exception;

   type Index_Type is range 0 .. 2 ** 63 - 1;
   subtype Size_Type is Index_Type range Index_Type'First + 1 .. Index_Type'Last;

   function Command_Size
     (Name  : String;
      Attrs : Sax.Readers.Sax_Attribute_List)
      return Size_Type;

   function Command_Base
     (Name  : String;
      Attrs : Sax.Readers.Sax_Attribute_List)
      return String;

   procedure Set (Symbols : Schema.Readers.Validating_Reader);

private

   function Clear_Page
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Clear_Pages
     (Attrs    : Sax.Readers.Sax_Attribute_List;
      Index    : Index_Type;
      Str_Base : String)
      return Types.Command.Command_Type;

   type Attribute_Type is
     (address_attr,
      apicId_attr,
      baseAddress_attr,
      baseOffset_attr,
      basePage_attr,
      baseVirtualAddress_attr,
      bus_attr,
      caching_attr,
      count_attr,
      cpu_attr,
      dev_attr,
      device_attr,
      domain_attr,
      entryPoint_attr,
      executable_attr,
      filename_attr,
      fill_attr,
      from_attr,
      func_attr,
      hash_attr,
      id_attr,
      ioBitmap_attr,
      iommuGroup_attr,
      irq_attr,
      kernel_attr,
      length_attr,
      level_attr,
      mode_attr,
      msrBitmap_attr,
      offset_attr,
      page_attr,
      paging_attr,
      readable_attr,
      region_attr,
      sid_attr,
      size_attr,
      subject_attr,
      tableIndex_attr,
      to_attr,
      usesMSI_attr,
      virtualAddress_attr,
      writable_attr);

   --  Return attribute with given name from specified attribute list. If no
   --  such attribute is present, an empty string is returned.
   function Get
     (Attrs : Sax.Readers.Sax_Attribute_List;
      Attr  : Attribute_Type)
      return String;

   function Parse_Caching (Caching : String) return Types.Caching_Type;

   function Write_Image_Commands
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

end Tau0.Command_Stream.XML;
