--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Exceptions;
with Ada.Finalization;
with Ada.Strings.Unbounded;
with Ada.Text_IO;

with GNAT.Command_Line;
with GNAT.OS_Lib;
with GNAT.Strings;

package body Tau0.Command_Line
with SPARK_Mode => Off
is

   function S
     (Source : Ada.Strings.Unbounded.Unbounded_String)
      return String
      renames Ada.Strings.Unbounded.To_String;

   function U
     (Source : String)
      return Ada.Strings.Unbounded.Unbounded_String
      renames Ada.Strings.Unbounded.To_Unbounded_String;

   -----------------------------------------------------------------------------

   type Config_Type is new
     Ada.Finalization.Limited_Controlled with record
      Data : GNAT.Command_Line.Command_Line_Configuration;
   end record;

   overriding
   procedure Finalize (Config : in out Config_Type);

   -----------------------------------------------------------------------------

   Cmdline          : Config_Type;
   Command_File     : Ada.Strings.Unbounded.Unbounded_String;
   Image_File       : Ada.Strings.Unbounded.Unbounded_String := U ("muen.img");
   Output_Directory : Ada.Strings.Unbounded.Unbounded_String := U (".");
   Input_Directory  : Ada.Strings.Unbounded.Unbounded_String := U (".");
   Log_Level        : Debug.Level_Type := Debug.Notice;
   Skip_Hashing     : Boolean := False;

   Parser : constant GNAT.Command_Line.Opt_Parser
     := GNAT.Command_Line.Command_Line_Parser;

   -----------------------------------------------------------------------------

   overriding procedure Finalize (Config : in out Config_Type)
   is
   begin
      GNAT.Command_Line.Free (Config => Config.Data);
   end Finalize;

   -----------------------------------------------------------------------------

   function Get_Command_Filename return String
   is (S (Command_File));

   -----------------------------------------------------------------------------

   function Get_Image_Filename return String
   is (S (Image_File));

   -----------------------------------------------------------------------------

   function Get_Input_Directory return String
   is (S (Input_Directory));

   -----------------------------------------------------------------------------

   function Get_Log_Level return Debug.Level_Type
   is (Log_Level);

   -----------------------------------------------------------------------------

   function Get_Output_Directory return String
   is (S (Output_Directory));

   -----------------------------------------------------------------------------

   function Get_Skip_Hashing return Boolean
   is (Skip_Hashing);

   -----------------------------------------------------------------------------

   procedure Init (Description : String)
   is
      use Ada.Strings.Unbounded;

      Skip_Hash : aliased Boolean := False;
      Out_Dir, Out_Name, In_Dir, Lvl : aliased GNAT.Strings.String_Access;
   begin
      GNAT.Command_Line.Set_Usage
        (Config => Cmdline.Data,
         Usage  => "[options] <command_file>",
         Help   => Description);
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Switch      => "-h",
         Long_Switch => "--help",
         Help        => "Display usage and exit");
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Output      => Out_Dir'Access,
         Switch      => "-o:",
         Long_Switch => "--output-directory:",
         Help        => "Output directory");
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Output      => Out_Name'Access,
         Switch      => "-n:",
         Long_Switch => "--image-name:",
         Help        => "Name of generated image (default: muen.img)");
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Output      => In_Dir'Access,
         Switch      => "-i:",
         Long_Switch => "--input-directories:",
         Help        => "Colon-separated list of input directories");
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Output      => Lvl'Access,
         Switch      => "-l:",
         Long_Switch => "--log-level:",
         Help        => "Logging level (default: Notice)");
      GNAT.Command_Line.Define_Switch
        (Config      => Cmdline.Data,
         Output      => Skip_Hash'Access,
         Switch      => "-s",
         Long_Switch => "--skip-hashing",
         Help        => "Skip hash verification (default: False)");
      begin
         GNAT.Command_Line.Getopt
           (Config => Cmdline.Data,
            Parser => Parser);
         if Out_Dir'Length /= 0 then
            Output_Directory := To_Unbounded_String (Out_Dir.all);
         end if;
         if Out_Name'Length /= 0 then
            Image_File := To_Unbounded_String (Out_Name.all);
         end if;
         if In_Dir'Length /= 0 then
            Input_Directory := To_Unbounded_String (In_Dir.all);
         end if;
         if Lvl'Length /= 0 then
            begin
               Log_Level := Debug.Level_Type'Value (Lvl.all);

            exception
               when others => raise GNAT.Command_Line.Invalid_Parameter;
            end;
         end if;

         Skip_Hashing := Skip_Hash;

         GNAT.Strings.Free (X => Out_Dir);
         GNAT.Strings.Free (X => Out_Name);
         GNAT.Strings.Free (X => In_Dir);
         GNAT.Strings.Free (X => Lvl);

      exception
         when GNAT.Command_Line.Invalid_Switch |
              GNAT.Command_Line.Exit_From_Command_Line =>
            raise Invalid_Cmd_Line with "invalid switch";
         when GNAT.Command_Line.Invalid_Parameter =>
            raise Invalid_Cmd_Line with "invalid parameter";
      end;

      Command_File := U (GNAT.Command_Line.Get_Argument (Parser => Parser));

      if Command_File = Null_Unbounded_String then
         raise Invalid_Cmd_Line with "'command_file' is missing";
      end if;
   end Init;

   -----------------------------------------------------------------------------

begin

   Init (Description => "Tau0 system resource allocator");

exception
   when E : Tau0.Command_Line.Invalid_Cmd_Line =>
      Ada.Text_IO.Put_Line
        ("Invalid command line: "
           & Ada.Exceptions.Exception_Message (X => E));
      Ada.Text_IO.New_Line;

      GNAT.Command_Line.Display_Help (Config => Cmdline.Data);

      GNAT.OS_Lib.OS_Exit (1);

end Tau0.Command_Line;
