--
--  Copyright (C) 2023  secunet Security Networks AG
--  Copyright (C) 2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Command_Line;

with Debug.Command_ID;
with Debug.Command_Status;

with Tau0.Command_Line;
with Tau0.Command_Processor;
with Tau0.Command_Stream.XML.Device_Domains;
with Tau0.Command_Stream.XML.Devices;
with Tau0.Command_Stream.XML.Kernels;
with Tau0.Command_Stream.XML.Memory_Regions;
with Tau0.Command_Stream.XML.Setup;
with Tau0.Command_Stream.XML.Subjects;
with Tau0.Types.Command;

package body Tau0.Command_Stream.XML.Reader
with SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   function Get_Command
     (Name  : String;
      Attrs : Sax.Readers.Sax_Attribute_List;
      Index : XML.Index_Type;
      Base  : String)
      return Types.Command.Command_Type
   is
   begin
      if Name = "activateTau0" then
         return XML.Setup.Activate_Tau0 (Attrs => Attrs);
      elsif Name = "writeImageCmdStream" then
         return XML.Write_Image_Commands (Attrs => Attrs);
      elsif Name = "clearPage" then
         return XML.Clear_Page (Attrs => Attrs);
      elsif Name = "clearPages" then
         return XML.Clear_Pages (Attrs    => Attrs,
                                 Index    => Index,
                                 Str_Base => Base);
      elsif Name = "createVTdRootTable" then
         return XML.Setup.Create_VTd_Root_Table (Attrs => Attrs);
      elsif Name = "createVTdContextTable" then
         return XML.Setup.Create_VTd_Context_Table (Attrs => Attrs);
      elsif Name = "createVTdIRQRemapTable" then
         return XML.Setup.Create_VTd_IRQ_Remap_Table (Attrs => Attrs);
      elsif Name = "addMemoryBlock" then
         return XML.Setup.Add_Memory_Block (Attrs => Attrs);
      elsif Name = "addProcessor" then
         return XML.Setup.Add_Processor (Attrs => Attrs);
      elsif Name = "addIoapic" then
         return XML.Setup.Add_Ioapic (Attrs => Attrs);
      elsif Name = "createMemoryRegion" then
         return XML.Memory_Regions.Create_Memory_Region (Attrs => Attrs);
      elsif Name = "createPageTableMR" then
         return XML.Memory_Regions.Create_Page_Table (Attrs => Attrs);
      elsif Name = "lockMemoryRegion" then
         return XML.Memory_Regions.Lock_Memory_Region (Attrs => Attrs);
      elsif Name = "activateMemoryRegion" then
         return XML.Memory_Regions.Activate_Memory_Region (Attrs => Attrs);
      elsif Name = "activatePageMR" then
         return XML.Memory_Regions.Activate_Page (Attrs => Attrs);
      elsif Name = "activatePagesMR" then
         return XML.Memory_Regions.Activate_Pages (Attrs    => Attrs,
                                                   Index    => Index,
                                                   Str_Base => Base);
      elsif Name = "activatePageTableMR" then
         return XML.Memory_Regions.Activate_Page_Table (Attrs => Attrs);
      elsif Name = "appendPageMRFile" then
         return XML.Memory_Regions.Append_Page_File (Attrs => Attrs);
      elsif Name = "appendPagesMRFile" then
         return XML.Memory_Regions.Append_Pages_File (Attrs    => Attrs,
                                                      Index    => Index,
                                                      Str_Base => Base);
      elsif Name = "appendPageMRFill" then
         return XML.Memory_Regions.Append_Page_Fill (Attrs => Attrs);
      elsif Name = "appendPagesMRFill" then
         return XML.Memory_Regions.Append_Pages_Fill (Attrs    => Attrs,
                                                      Index    => Index,
                                                      Str_Base => Base);
      elsif Name = "appendVacuousPage" then
         return XML.Memory_Regions.Append_Vacuous_Page (Attrs => Attrs);
      elsif Name = "appendVacuousPages" then
         return XML.Memory_Regions.Append_Vacuous_Pages (Attrs    => Attrs,
                                                         Index    => Index,
                                                         Str_Base => Base);
      elsif Name = "createSubject" then
         return XML.Subjects.Create_Subject (Attrs => Attrs);
      elsif Name = "lockSubject" then
         return XML.Subjects.Lock_Subject (Attrs => Attrs);
      elsif Name = "activateSubject" then
         return XML.Subjects.Activate_Subject (Attrs => Attrs);
      elsif Name = "attachMemoryRegionSubject" then
         return XML.Subjects.Attach_Memory_Region (Attrs => Attrs);
      elsif Name = "createPageTableSubject" then
         return XML.Subjects.Create_Page_Table (Attrs => Attrs);
      elsif Name = "activatePageTableSubject" then
         return XML.Subjects.Activate_Page_Table (Attrs => Attrs);
      elsif Name = "mapDevicePageSubject" then
         return XML.Subjects.Map_Device_Page (Attrs => Attrs);
      elsif Name = "mapDevicePagesSubject" then
         return XML.Subjects.Map_Device_Pages (Attrs    => Attrs,
                                               Index    => Index,
                                               Str_Base => Base);
      elsif Name = "mapPageSubject" then
         return XML.Subjects.Map_Page (Attrs => Attrs);
      elsif Name = "mapPagesSubject" then
         return XML.Subjects.Map_Pages (Attrs    => Attrs,
                                        Index    => Index,
                                        Str_Base => Base);
      elsif Name = "assignDeviceSubject" then
         return XML.Subjects.Assign_Device_Subject (Attrs => Attrs);
      elsif Name = "assignIRQSubject" then
         return XML.Subjects.Assign_IRQ_Subject (Attrs => Attrs);
      elsif Name = "setIOPortRange" then
         return XML.Subjects.Set_IO_Port_Range (Attrs => Attrs);
      elsif Name = "setMSRRange" then
         return XML.Subjects.Set_MSR_Range (Attrs => Attrs);
      elsif Name = "createDeviceDomain" then
         return XML.Device_Domains.Create_Device_Domain (Attrs => Attrs);
      elsif Name = "lockDeviceDomain" then
         return XML.Device_Domains.Lock_Device_Domain (Attrs => Attrs);
      elsif Name = "activateDeviceDomain" then
         return XML.Device_Domains.Activate_Device_Domain (Attrs => Attrs);
      elsif Name = "attachMemoryRegionDeviceDomain" then
         return XML.Device_Domains.Attach_Memory_Region (Attrs => Attrs);
      elsif Name = "addDeviceToDeviceDomain" then
         return XML.Device_Domains.Add_Device (Attrs => Attrs);
      elsif Name = "createPageTableDeviceDomain" then
         return XML.Device_Domains.Create_Page_Table (Attrs => Attrs);
      elsif Name = "activatePageTableDeviceDomain" then
         return XML.Device_Domains.Activate_Page_Table (Attrs => Attrs);
      elsif Name = "mapPageDeviceDomain" then
         return XML.Device_Domains.Map_Page (Attrs => Attrs);
      elsif Name = "mapPagesDeviceDomain" then
         return XML.Device_Domains.Map_Pages (Attrs    => Attrs,
                                              Index    => Index,
                                              Str_Base => Base);
      elsif Name = "createKernel" then
         return XML.Kernels.Create_Kernel (Attrs => Attrs);
      elsif Name = "lockKernel" then
         return XML.Kernels.Lock_Kernel (Attrs => Attrs);
      elsif Name = "activateKernel" then
         return XML.Kernels.Activate_Kernel (Attrs => Attrs);
      elsif Name = "attachMemoryRegionKernel" then
         return XML.Kernels.Attach_Memory_Region (Attrs => Attrs);
      elsif Name = "createPageTableKernel" then
         return XML.Kernels.Create_Page_Table (Attrs => Attrs);
      elsif Name = "activatePageTableKernel" then
         return XML.Kernels.Activate_Page_Table (Attrs => Attrs);
      elsif Name = "mapPageKernel" then
         return XML.Kernels.Map_Page (Attrs => Attrs);
      elsif Name = "mapPagesKernel" then
         return XML.Kernels.Map_Pages (Attrs    => Attrs,
                                       Index    => Index,
                                       Str_Base => Base);
      elsif Name = "assignDeviceKernel" then
         return XML.Kernels.Assign_Device_Kernel (Attrs => Attrs);
      elsif Name = "mapDevicePageKernel" then
         return XML.Kernels.Map_Device_Page (Attrs => Attrs);
      elsif Name = "mapDevicePagesKernel" then
         return XML.Kernels.Map_Device_Pages (Attrs    => Attrs,
                                              Index    => Index,
                                              Str_Base => Base);
      elsif Name = "createLegacyDevice" then
         return XML.Devices.Create_Legacy_Device (Attrs => Attrs);
      elsif Name = "createPCIDevice" then
         return XML.Devices.Create_PCI_Device (Attrs => Attrs);
      elsif Name = "addIOPortRangeDevice" then
         return XML.Devices.Add_IO_Port_Range (Attrs => Attrs);
      elsif Name = "addIRQDevice" then
         return XML.Devices.Add_IRQ (Attrs => Attrs);
      elsif Name = "addMemoryDevice" then
         return XML.Devices.Add_Memory (Attrs => Attrs);
      elsif Name = "activateDevice" then
         return XML.Devices.Activate (Attrs => Attrs);
      else
         pragma Debug (Debug.Put_Line ("Ignoring command " & Name));
         return Types.Command.Command_Type'
           (ID              => Types.Command.Unused_Command_ID,
            Sequence_Number => 0);
      end if;
   end Get_Command;

   -------------------------------------------------------------------------

   overriding
   procedure Start_Element
     (Handler    : in out Reader_Type;
      NS         :        Sax.Utils.XML_NS;
      Local_Name :        Sax.Symbols.Symbol;
      Atts       :        Sax.Readers.Sax_Attribute_List)
   is
      use type Types.Word64;

      Tag_Name : constant String := Sax.Symbols.Get (Sym => Local_Name).all;
   begin
      if Handler.Do_Process_Commands then
         declare
            use type Types.Command.Status_Type;
            use type Debug.Level_Type;

            --  Size determines the number of sub-commands, i.e. repeated
            --  invocations for commands that can affect a consecutive region of
            --  memory (e.g. clearPages).
            Size   : constant XML.Size_Type := XML.Command_Size
              (Name  => Tag_Name,
               Attrs => Atts);
            Base   : constant String := XML.Command_Base
              (Name  => Tag_Name,
               Attrs => Atts);
            Status : Types.Command.Status_Type;
            Cmd    : Types.Command.Command_Type;
         begin
            for Idx in 0 .. Size - 1 loop
               Cmd := Get_Command (Name  => Tag_Name,
                                   Attrs => Atts,
                                   Index => Idx,
                                   Base  => Base);
               Cmd.Sequence_Number := Handler.Sequence_Number;
               Command_Processor.Process (Command => Cmd,
                                          Status  => Status);

               pragma Debug (Debug.Put ("Status: "));
               pragma Debug (Debug.Command_Status.Put (Status));
               pragma Debug (Debug.New_Line);

               if Status /= Types.Command.Success then
                  Debug.Put (Item  => "Error: Command """,
                             Level => Debug.Error);
                  Debug.Command_ID.Put (Command_ID => Cmd.ID,
                                        Level      => Debug.Error);
                  Debug.Put (Item  => """ with sequence number ",
                             Level => Debug.Error);
                  Debug.Put (Word      => Cmd.Sequence_Number,
                             Base      => 10,
                             Justified => False,
                             Level     => Debug.Error);
                  Debug.Put (Item  => " failed - ",
                             Level => Debug.Error);
                  Debug.Command_Status.Put (Status => Status,
                                            Level  => Debug.Error);
                  Debug.New_Line (Level => Debug.Error);

                  if Tau0.Command_Line.Get_Log_Level /= Debug.Level_Type'First then
                     Debug.Put_Line
                       (Item  => "Re-run command with " & Debug.Level_Type'First'Img
                        & " loglevel to see more context",
                        Level => Debug.Error);
                  end if;

                  Ada.Command_Line.Set_Exit_Status
                    (Code => Ada.Command_Line.Exit_Status (Status));
                  raise Processing_Error;
               end if;

               Handler.Sequence_Number := Handler.Sequence_Number + 1;
            end loop;
         end;
      else
         if Tag_Name = "commands" then
            Handler.Do_Process_Commands := True;
            Handler.Sequence_Number := 0;
            return;
         end if;
      end if;
   end Start_Element;

end Tau0.Command_Stream.XML.Reader;
