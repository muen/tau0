--
--  Copyright (C) 2019-2023  secunet Security Networks AG
--  Copyright (C) 2019-2023  codelabs GmbH
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Sax.Readers;

package Tau0.Command_Stream.XML.Setup
with Abstract_State => null,
     SPARK_Mode     => Off
is

   function Activate_Tau0
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Create_VTd_Context_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Create_VTd_IRQ_Remap_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Create_VTd_Root_Table
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Add_Memory_Block
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Add_Processor
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

   function Add_Ioapic
     (Attrs : Sax.Readers.Sax_Attribute_List)
      return Types.Command.Command_Type;

end Tau0.Command_Stream.XML.Setup;
