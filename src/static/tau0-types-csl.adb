--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Types.CSL
is

   -------------------------------------------------------------------------

   function To_Msg (Str : String) return Check_Msg_Type
   is
      Res : Check_Msg_Type := (others => ASCII.NUL);
   begin
      for I in 1 .. Str'Length loop
         Res (I) := Str (Str'First + I - 1);
      end loop;

      return Res;
   end To_Msg;

end Tau0.Types.CSL;
