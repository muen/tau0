--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.Paging;
with Tau0.Data.Memory_Region_Table;
with Tau0.Types.Root;
with Tau0.Types.Typization;

package body Tau0.Commands.Kernels.Paging
is

   package R renames Data.Roots;

   use type Types.Root.Kind_Type;
   use type Types.Root.State_Type;
   use type Types.Typization.Typization_Type;

   -----------------------------------------------------------------------------

   procedure Activate_Page_Table
     (Kernel          :     Types.Root_Range;
      Level           :     Types.Page_Level_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Status          : out Types.Command.Status_Type)
   is
      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Locked then
         Status := Types.Command.Kernel_Not_Locked;
      elsif Level >= R.Level (Root => Kernel) then
         Status := Types.Command.Level_Invalid;
      elsif not R.PTP_Present (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Not_Present;
      elsif R.PTP_Active (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Active;
      elsif not Types.Aligned_64 (Address => Virtual_Address) then
         Status := Types.Command.Address_Invalid;
      else
         pragma Assert (Data.Memory.Invariant);
         pragma Assert (Data.Roots.State (Kernel) = Types.Root.Locked);
         pragma Assert (not Data.Roots.PTP_Active (Kernel));
         pragma Assert (Level < Data.Roots.Level (Kernel));
         pragma Assert (Types.Aligned_64 (Virtual_Address));

         Data.Memory.Paging.Activate_Page
           (Root            => Kernel,
            Virtual_Address => Virtual_Address,
            Level           => Level,
            Success         => Success);

         if Success then
            Status := Types.Command.Success;
         else
            Status := Types.Command.Page_Translation_Error;
         end if;
      end if;
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   procedure Create_Page_Table
     (Page            :     Types.Physical_Address_Type;
      Kernel          :     Types.Root_Range;
      Level           :     Types.Page_Level_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Readable        :     Boolean;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Status          : out Types.Command.Status_Type)
   is
      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Setup then
         Status := Types.Command.Kernel_Not_Setup;
      elsif Level >= R.Level (Root => Kernel) then
         Status := Types.Command.Level_Invalid;
      elsif R.PTP_Active (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Active;
      elsif
        not Types.Aligned_64 (Address => Virtual_Address) or
        not Types.Aligned_Page (Address => Page)
      then
         Status := Types.Command.Address_Invalid;
      elsif
        Data.Memory.Get_Typization (Address => Page)
          /= Types.Typization.Zeroed
      then
         Status := Types.Command.Typization_Violation;
      elsif
        Level = R.Level (Root => Kernel) - 1 and
        R.PTP_Present (Root => Kernel)
      then
         Status := Types.Command.Kernel_PTP_Present;
      elsif
        Level /= R.Level (Root => Kernel) - 1 and
        not R.PTP_Present (Root => Kernel)
      then
         Status := Types.Command.Kernel_PTP_Not_Present;
      else
         Data.Memory.Paging.Create_Page
           (Root            => Kernel,
            Virtual_Address => Virtual_Address,
            Level           => Level,
            Page            => Page,
            Readable        => Readable,
            Writable        => Writable,
            Executable      => Executable,
            Success         => Success);

         if Success then
            Status := Types.Command.Success;
         else
            Status := Types.Command.Page_Translation_Error;
         end if;
      end if;
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   procedure Map_Device_Page
     (Kernel          :     Types.Root_Range;
      Device          :     Types.Device.Device_Range;
      Page            :     Types.Physical_Address_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Status          : out Types.Command.Status_Type)
   is
      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Setup then
         Status := Types.Command.Kernel_Not_Setup;
      elsif not R.PTP_Present (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Not_Present;
      elsif
        not Types.Aligned_Page (Address => Virtual_Address) or
        not Types.Aligned_Page (Address => Page)
      then
         Status := Types.Command.Address_Invalid;
      elsif not Data.Roots.Device_Assigned (Root => Kernel, Device => Device) then
         Status := Types.Command.Kernel_Device_Not_Assigned;
      elsif not Data.Devices.Owns_Page (Device => Device, Address => Page) then
         Status := Types.Command.Device_Memory_Invalid;
      else
         Data.Memory.Paging.Map_Device_Page
           (Root            => Kernel,
            Device          => Device,
            Page            => Page,
            Virtual_Address => Virtual_Address,
            Writable        => Writable,
            Executable      => Executable,
            Success         => Success);

         Status := (if Success then Types.Command.Success
                    else Types.Command.Page_Translation_Error);
      end if;
   end Map_Device_Page;

   -----------------------------------------------------------------------------

   procedure Map_Page
     (Kernel          :     Types.Root_Range;
      Virtual_Address :     Types.Virtual_Address_Type;
      Table_Index     :     Types.Memory_Region_Table_Range;
      Offset          :     Types.Page_Count_Type;
      Status          : out Types.Command.Status_Type)
   is
      package MRT renames Data.Memory_Region_Table;
      use type Types.Page_Count_Type;
      use type Types.Root_Range;

      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Setup then
         Status := Types.Command.Kernel_Not_Setup;
      elsif not R.PTP_Present (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Not_Present;
      elsif R.PTP_Active (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Active;
      elsif not Types.Aligned_Page (Address => Virtual_Address) then
         Status := Types.Command.Address_Invalid;
      elsif not Data.Memory_Region_Table.Used (Index => Table_Index) then
         Status := Types.Command.MR_Table_Index_Invalid;
      elsif MRT.Owner (Index => Table_Index) /= Kernel then
         Status := Types.Command.MR_Table_Index_Invalid;
      elsif
        Data.Memory_Region_Table.Mapped_Pages (Index => Table_Index)
          >= Data.Memory_Region_Table.Length (Index => Table_Index)
      then
         Status := Types.Command.Kernel_Mapped_Region_Full;
      elsif
        Offset < Data.Memory_Region_Table.Offset (Index => Table_Index) or else
        Offset - Data.Memory_Region_Table.Offset (Index => Table_Index)
          >= Data.Memory_Region_Table.Length (Index => Table_Index)
      then
         Status := Types.Command.Memory_Region_Out_Of_Bounds;
      else
         Data.Memory.Paging.Map_Page
           (Root            => Kernel,
            Index           => Table_Index,
            Offset          => Offset,
            Virtual_Address => Virtual_Address,
            Success         => Success);

         if Success then
            Status := Types.Command.Success;
         else
            Status := Types.Command.Page_Translation_Error;
         end if;
      end if;
   end Map_Page;

end Tau0.Commands.Kernels.Paging;
