--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Debug.Command_ID
is

   use Tau0.Types.Command;

   subtype Command_Name is String (1 .. 35);

   type Name_Table_Type is array (Command_ID_Type) of Command_Name;

   --  OPTIMIZE: size
   Names : constant Name_Table_Type
     := (Activate_Tau0                       => "Activate_Tau0                      ",
         Add_Memory_Block                    => "Add_Memory_Block                   ",
         Write_Image_Commands                => "Write_Image_Commands               ",
         Add_Processor                       => "Add_Processor                      ",
         Add_Ioapic                          => "Add_Ioapic                         ",
         Clear_Page                          => "Clear_Page                         ",
         Create_VTd_Root_Table               => "Create_VTd_Root_Table              ",
         Create_VTd_Context_Table            => "Create_VTd_Context_Table           ",
         Create_VTd_IRQ_Remap_Table          => "Create_VTd_IRQ_Remap_Table         ",
         Create_Memory_Region                => "Create_Memory_Region               ",
         Lock_Memory_Region                  => "Lock_Memory_Region                 ",
         Activate_Memory_Region              => "Activate_Memory_Region             ",
         Deactivate_Memory_Region            => "Deactivate_Memory_Region           ",
         Unlock_Memory_Region                => "Unlock_Memory_Region               ",
         Destroy_Memory_Region               => "Destroy_Memory_Region              ",
         Create_Page_Table_MR                => "Create_Page_Table_MR               ",
         Destroy_Page_Table_MR               => "Destroy_Page_Table_MR              ",
         Append_Page_MR                      => "Append_Page_MR                     ",
         Append_Vacuous_Page_MR              => "Append_Vacuous_Page_MR             ",
         Truncate_Page_MR                    => "Truncate_Page_MR                   ",
         Activate_Page_Table_MR              => "Activate_Page_Table_MR             ",
         Activate_Page_MR                    => "Activate_Page_MR                   ",
         Deactivate_Page_MR                  => "Deactivate_Page_MR                 ",
         Deactivate_Page_Table_MR            => "Deactivate_Page_Table_MR           ",
         Create_Subject                      => "Create_Subject                     ",
         Destroy_Subject                     => "Destroy_Subject                    ",
         Lock_Subject                        => "Lock_Subject                       ",
         Activate_Subject                    => "Activate_Subject                   ",
         Deactivate_Subject                  => "Deactivate_Subject                 ",
         Set_IO_Port_Range                   => "Set_IO_Port_Range                  ",
         Set_MSR_Range                       => "Set_MSR_Range                      ",
         Assign_Device_Subject               => "Assign_Device_Subject              ",
         Assign_IRQ_Subject                  => "Assign_IRQ_Subject                 ",
         Attach_Memory_Region_Subject        => "Attach_Memory_Region_Subject       ",
         Detach_Memory_Region_Subject        => "Detach_Memory_Region_Subject       ",
         Create_Page_Table_Subject           => "Create_Page_Table_Subject          ",
         Destroy_Page_Table_Subject          => "Destroy_Page_Table_Subject         ",
         Activate_Page_Table_Subject         => "Activate_Page_Table_Subject        ",
         Deactivate_Page_Table_Subject       => "Deactivate_Page_Table_Subjec       ",
         Map_Page_Subject                    => "Map_Page_Subject                   ",
         Map_Device_Page_Subject             => "Map_Device_Page_Subject            ",
         Unmap_Page_Subject                  => "Unmap_Page_Subject                 ",
         Create_Device_Domain                => "Create_Device_Domain               ",
         Destroy_Device_Domain               => "Destroy_Device_Domain              ",
         Add_Device_To_Device_Domain         => "Add_Device_To_Device_Domain        ",
         Remove_Device_From_Device_Domain    => "Remove_Device_From_Device_Domain   ",
         Attach_Memory_Region_Domain         => "Attach_Memory_Region_Domain        ",
         Detach_Memory_Region_Domain         => "Detach_Memory_Region_Domain        ",
         Lock_Device_Domain                  => "Lock_Device_Domain                 ",
         Activate_Device_Domain              => "Activate_Device_Domain             ",
         Deactivate_Device_Domain            => "Deactivate_Device_Domain           ",
         Create_Page_Table_Device_Domain     => "Create_Page_Table_Device_Domain    ",
         Destroy_Page_Table_Device_Domain    => "Destroy_Page_Table_Device_Domain   ",
         Activate_Page_Table_Device_Domain   => "Activate_Page_Table_Device_Domain  ",
         Deactivate_Page_Table_Device_Domain => "Deactivate_Page_Table_Device_Domain",
         Map_Page_Device_Domain              => "Map_Page_Device_Domain             ",
         Unmap_Page_Device_Domain            => "Unmap_Page_Device_Domain           ",
         Create_Kernel                       => "Create_Kernel                      ",
         Attach_Memory_Region_Kernel         => "Attach_Memory_Region_Kernel        ",
         Lock_Kernel                         => "Lock_Kernel                        ",
         Activate_Kernel                     => "Activate_Kernel                    ",
         Create_Page_Table_Kernel            => "Create_Page_Table_Kernel           ",
         Activate_Page_Table_Kernel          => "Activate_Page_Table_Kernel         ",
         Map_Page_Kernel                     => "Map_Page_Kernel                    ",
         Map_Device_Page_Kernel              => "Map_Device_Page_Kernel             ",
         Assign_Device_Kernel                => "Assign_Device_Kernel               ",
         Create_Legacy_Device                => "Create_Legacy_Device               ",
         Create_PCI_Device                   => "Create_PCI_Device                  ",
         Add_IO_Port_Range_Device            => "Add_IO_Port_Range_Device           ",
         Add_IRQ_Device                      => "Add_IRQ_Device                     ",
         Add_Memory_Device                   => "Add_Memory_Device                  ",
         Activate_Device                     => "Activate_Device                    ",

         others                              => "UNKNOWN ID                         ");

   -----------------------------------------------------------------------------

   procedure Put
     (Command_ID : Command_ID_Type;
      Level      : Level_Type := Info)
   is
   begin
      if Level >= Get_Current_Level then
         declare
            Name : constant String := Trim (Source => Names (Command_ID),
                                            Side   => Right);
         begin
            Put (Item => Name, Level => Level);
         end;
      end if;
   end Put;

end Debug.Command_ID;
