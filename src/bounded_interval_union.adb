--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Bounded_Interval_Union.Lemmas;
with Debug;

package body Bounded_Interval_Union
is

   package Lems is new Bounded_Interval_Union.Lemmas;

   -----------------------------------------------------------------------------

   procedure Add_Interval
     (Set  : in out T;
      From :        Elem_Type;
      To   :        Elem_Type)
   is
      Old_Set : constant T := Set with Ghost;
      Left    : Integer    := Cell_Range'First;
      Right   : Integer    := Set.Last;
   begin
      while  --  Find the leftmost cell that must be modified
        Left <= Set.Last and then
        Clamped_Succ (Set.Elems (Left).To) < From
      loop
         pragma Loop_Invariant (Left >= Cell_Range'First);
         pragma Loop_Invariant (for all I in Cell_Range'First .. Left - 1 =>
                                Elem_Type'Succ (Set.Elems (I).To) < From);
         pragma Loop_Variant (Increases => Left);

         Left := Left + 1;
      end loop;

      while  --  Find the rightmost cell that must be modified
        Right >= Cell_Range'First and then
        To < Clamped_Pred (Set.Elems (Right).From)
      loop
         pragma Loop_Invariant (Right <= Set.Last);
         pragma Loop_Invariant (for all I in Right + 1 .. Set.Last =>
                                To < Elem_Type'Pred (Set.Elems (I).From));
         pragma Loop_Variant (Decreases => Right);

         Right := Right - 1;
      end loop;

      if Left > Right then  --  we must insert a new element
         Set.Last                         := Set.Last + 1;
         Set.Elems (Left + 1 .. Set.Last) := Set.Elems (Left .. Set.Last - 1);
         Set.Elems (Left)                 := Cell_Type'(From => From, To => To);

         if Left < Set.Last then
            Lems.Insert_Lemma
              (Old_Set  => Old_Set,
               New_Set  => Set,
               Old_Cell => Left,
               New_Cell => Left + 1,
               To       => To);
         end if;
      else  --  we coalesce with existing elements
         declare
            New_From : constant Elem_Type
              := Elem_Type'Min (From, Set.Elems (Left).From);
            New_To   : constant Elem_Type
              := Elem_Type'Max (To, Set.Elems (Right).To);
         begin
            Set.Elems (Left)
              := Cell_Type'(From => New_From, To => New_To);
            Set.Elems (Left + 1 .. Set.Last + Left - Right)
              := Set.Elems (Right + 1 .. Set.Last);
            Set.Elems (Set.Last + Left - Right + 1 .. Set.Last)
              := (others => Null_Cell);
            Set.Last := Set.Last + (Left - Right);

            if Right < Set.Last + Right - Left then
               Lems.Coalesce_Lemma
                 (Old_Set   => Old_Set,
                  New_Set   => Set,
                  Old_Right => Right,
                  New_Right => Left,
                  To        => To);
            end if;
         end;
      end if;

      pragma Assert
        (Model (Set) = (Model (Old_Set) with delta From .. To => True));
   end Add_Interval;

   -----------------------------------------------------------------------------

   function Contains (From, To : Elem_Type; Set : T) return Boolean
   is
      Left  : Integer := Cell_Range'First;
      Right : Integer := Set.Last;

      function Middle return Integer
      is (Left + (Right - Left) / 2)
      with
        Pre  => Cell_Range'First <= Left and Left <= Right and Right <= Set.Last,
        Post => Middle'Result in Left .. Right;

      pragma Inline_Always (Middle);

   begin
      --  Empty set always contained
      if To < From then
         pragma Assert (for all Elem in From .. To => Model (Set) (Elem));
         return True;
      end if;

      --  Perform bisection search
      while Left <= Right loop
         pragma Loop_Invariant (Cell_Range'First <= Left and Right <= Set.Last);
         pragma Loop_Invariant
           (for all C in Cell_Range'First .. Set.Last =>
              (if Set.Elems (C).From <= From and To <= Set.Elems (C).To
               then C in Left .. Right));
         pragma Loop_Invariant
           (for all C in Cell_Range'First .. Set.Last =>
              (if C > Right then To < Set.Elems (C).From) and
              (if C < Left then From > Set.Elems (C).To));
         pragma Loop_Variant (Decreases => Right - Left);

         --  Current cell contains [From, To]
         if Set.Elems (Middle).From <= From and To <= Set.Elems (Middle).To then
            pragma Assert (for all Elem in From .. To => Model (Set) (Elem));
            return True;
         end if;

         --  [From, To] might be left of current cell
         if To < Set.Elems (Middle).From then
            --  ... can it be?
            if
              Middle = Cell_Range'First or else
              Set.Elems (Middle - 1).To < To
            then
               pragma Assert (for some Elem in From .. To =>
                              not Model (Set) (Elem));
               return False;
            else
               Right := Middle - 1;
            end if;
         --  or [From, To] might be right of current cell
         elsif From > Set.Elems (Middle).To then
            --  ... can it be?
            if
              Middle = Set.Last or else
              Set.Elems (Middle + 1).From > From
            then
               pragma Assert (for some Elem in From .. To =>
                              not Model (Set) (Elem));
               return False;
            else
               --  Search right half
               Left := Middle + 1;
            end if;
         else
            pragma Assert
              ((Set.Elems (Middle).From > Elem_Type'First and then
                not Model (Set) (Elem_Type'Pred (Set.Elems (Middle).From)))
               or else
               (Set.Elems (Middle).To < Elem_Type'Last and then
                not Model (Set) (Elem_Type'Succ (Set.Elems (Middle).To))));

            pragma Assert (for some Elem in From .. To =>
                           not Model (Set) (Elem));
            return False;
         end if;
      end loop;

      pragma Assert (for some E in From .. To => not Model (Set) (E));
      return False;
   end Contains;

   -----------------------------------------------------------------------------

   --  OPTIMIZE: Use bisection search if necessary
   function Contains_Some (Set : T; From, To : Elem_Type) return Boolean
   is
   begin
      if From > To then
         return False;
      end if;

      for I in Set.Elems'First .. Set.Last loop
         pragma Loop_Invariant
           (if I > Set.Elems'First
            then (for all E in Elem_Type'First .. Set.Elems (I - 1).To =>
                  not (E in From .. To and Model (Set) (E))));

         if (Set.Elems (I).From <= From and From <= Set.Elems (I).To) or
            (Set.Elems (I).From <= To and To <= Set.Elems (I).To) or
            (From <= Set.Elems (I).From and Set.Elems (I).To <= To)
         then
            pragma Assert (Model (Set) (From) or
                           Model (Set) (To) or
                           Model (Set) (Set.Elems (I).From));
            pragma Assert (for some E in From .. To => Model (Set) (E));

            return True;
         end if;
      end loop;

      return False;
   end Contains_Some;

   -----------------------------------------------------------------------------

   function Disjoint (L, R : T) return Boolean
   is
   begin
      for I in L.Elems'First .. L.Last loop
         pragma Loop_Invariant (for all J in L.Elems'First .. I - 1 =>
                                not Model (R) (L.Elems (J).From) and
                                not Model (R) (L.Elems (J).To));

         if
           Element (Set => R, Elem => L.Elems (I).From) or
           Element (Set => R, Elem => L.Elems (I).To)
         then
            return False;
         end if;
      end loop;

      for I in R.Elems'First .. R.Last loop
         pragma Loop_Invariant (for all J in R.Elems'First .. I - 1 =>
                                not Model (L) (R.Elems (J).From) and
                                not Model (L) (R.Elems (J).To));

         if
           Element (Set => L, Elem => R.Elems (I).From) or
           Element (Set => L, Elem => R.Elems (I).To)
         then
            return False;
         end if;
      end loop;

      return True;
   end Disjoint;

   -----------------------------------------------------------------------------

   function Element (Elem : Elem_Type; Set : T) return Boolean
   is (Contains (From => Elem, To => Elem, Set => Set));

   -----------------------------------------------------------------------------

   function Empty_Set return T
   is (T'(Last  => 0,
          Elems => (others => Null_Cell)));

   -----------------------------------------------------------------------------

   --  Stricter than necessary, we might coalesce elements
   function Full (Set : T) return Boolean
   is (Set.Last = Bound);

   -----------------------------------------------------------------------------

   function Length (Set : T) return Natural
   is (Set.Last);

   -----------------------------------------------------------------------------

   function Model (Set : T) return Model_Type
   with Refined_Post =>
     (for all Elem in Elem_Type =>
        Model'Result (Elem) =
        (for some Cell of Set.Elems (Set.Elems'First .. Set.Last) =>
           Elem in Cell.From .. Cell.To))
   is
      M : Model_Type := (others => False);
   begin
      for Elem in Elem_Type loop
         pragma Loop_Invariant
           (for all E in Elem_Type =>
              (if E < Elem
               then M (E) = (for some Cell of Set.Elems (Set.Elems'First .. Set.Last) =>
                             E in Cell.From .. Cell.To)));

         M (Elem) := (for some Cell of Set.Elems (Set.Elems'First .. Set.Last) =>
                      Elem in Cell.From .. Cell.To);
      end loop;

      return M;
   end Model;

   -----------------------------------------------------------------------------

   procedure Print (Set : T)
   is
      use Debug;
   begin
      Put_Line ("Last:" & Set.Last'Img);
      for I in Set.Elems'First .. Set.Last loop
         Put
           ("[" & Trim (Source => Set.Elems (I).From'Img, Side => Left) & ", "
                & Trim (Source => Set.Elems (I).To'Img, Side => Left) & "]");
         if I < Set.Last then
            Put (", ");
         end if;
      end loop;

      New_Line;
   end Print;

   -----------------------------------------------------------------------------

   procedure Subtract_Interval
     (Set  : in out T;
      From :        Elem_Type;
      To   :        Elem_Type)
   is
      Old_Set   : constant T          := Set         with Ghost;
      Old_Model : constant Model_Type := Model (Set) with Ghost;
      Left      : Integer             := Cell_Range'First;
      Right     : Integer             := Set.Last;
   begin
      if Set.Last < Cell_Range'First then  --  Set is empty.
         pragma Assert (for all Elem in Elem_Type =>
                        not Old_Model (Elem));
         pragma Assert (Model (Set) =
                        (Old_Model with delta From .. To => False));

         return;
      end if;

      --  Determine leftmost cell to be modified
      while Left <= Set.Last loop
         pragma Loop_Invariant (Left >= Cell_Range'First);
         pragma Loop_Invariant (for all C in Cell_Range'First .. Left - 1 =>
                                Set.Elems (C).To < From);
         pragma Loop_Variant (Increases => Left);

         exit when From <= Set.Elems (Left).To;

         if Left = Set.Last then
            pragma Assert
              (for all Elem in Elem_Type =>
              (if Elem > Set.Elems (Set.Last).To then not Old_Model (Elem)));
            pragma Assert (Model (Set) =
                           (Old_Model with delta From .. To => False));

            return;
         end if;

         Left := Left + 1;
      end loop;

      pragma Assert (Left in Cell_Range'First .. Set.Last and
                     From <= Set.Elems (Left).To and
                     (for all C in Cell_Range'First .. Left - 1 =>
                      Set.Elems (C).To < From));

      --  Determine rightmost cell to be modified
      while Right >= Cell_Range'First loop
         pragma Loop_Invariant (Right <= Set.Last);
         pragma Loop_Invariant (for all C in Right + 1 .. Set.Last =>
                                To < Set.Elems (C).From);
         pragma Loop_Variant (Decreases => Right);

         exit when Set.Elems (Right).From <= To;

         if Right = Cell_Range'First then
            pragma Assert
              (for all Elem in Elem_Type =>
              (if Elem < Set.Elems (Cell_Range'First).From then not Old_Model (Elem)));
            pragma Assert (Model (Set) =
                           (Old_Model with delta From .. To => False));

            return;
         end if;

         Right := Right - 1;
      end loop;

      pragma Assert (Right in Cell_Range'First .. Set.Last and
                     Set.Elems (Right).From <= To and
                     (for all C in Right + 1 .. Set.Last =>
                      To < Set.Elems (C).From));

      declare
         Change_Right_From : constant Boolean := To < Set.Elems (Right).To;
         Change_Left_To    : constant Boolean := Set.Elems (Left).From < From;
      begin
         --  REVIEW: Maybe one can merge some code paths... Take Max (0, Right - Left)?
         if Left = Right then
            if Change_Left_To and Change_Right_From then --  split cell
               Set.Elems (Left + 1 .. Set.Last + 1) := Set.Elems (Left .. Set.Last);
               Set.Last                             := Set.Last + 1;
               Set.Elems (Left).To                  := Elem_Type'Pred (From);
               Set.Elems (Left + 1).From            := Elem_Type'Succ (To);

               Lems.Split_Lemma
                 (Old_Set  => Old_Set,
                  New_Set  => Set,
                  Old_Cell => Left,
                  New_Cell => Left + 1);

               pragma Assert (Model (Set) =
                              (Old_Model with delta From .. To => False));
            elsif not Change_Left_To and not Change_Right_From then
               --  delete cell
               Set.Elems (Left .. Set.Last - 1) := Set.Elems (Left + 1 .. Set.Last);
               Set.Last                         := Set.Last - 1;

               Lems.Delete_Lemma
                 (Old_Set     => Old_Set,
                  New_Set     => Set,
                  Delete_From => Left,
                  Delete_To   => Right);  --  resp., Left

               pragma Assert (Model (Set) =
                              (Old_Model with delta From .. To => False));
            elsif Change_Right_From then    --  truncate cell
               Set.Elems (Right).From := Elem_Type'Max (Elem_Type'Succ (To),
                                                        Set.Elems (Right).From);

               pragma Assert (Model (Set) =
                              (Old_Model with delta From .. To => False));
            elsif Change_Left_To then
               Set.Elems (Left).To := Elem_Type'Min (Elem_Type'Pred (From),
                                                     Set.Elems (Left).To);

               pragma Assert (Model (Set) =
                              (Old_Model with delta From .. To => False));
            else
               pragma Assert (False);
            end if;
         elsif Left < Right then
            if Change_Left_To then
               --  truncate current
               Set.Elems (Left).To := Elem_Type'Min (Elem_Type'Pred (From),
                                                     Set.Elems (Left).To);
               Left                := Left + 1;  --  and delete from next cell
            end if;

            if Change_Right_From then
               --  truncate current
               Set.Elems (Right).From := Elem_Type'Max (Elem_Type'Succ (To),
                                                        Set.Elems (Right).From);
               Right                  := Right - 1;  --  and delete previous cell
            end if;

            --  Left and Right might have been next to each other, then we need
            --  not delete anything inbetween.
            if Left > Right then
               --  pragma Assert (for all I in Elem_Type =>
               --     (if I > To then Model (Set) (I) = Model (Old_Set) (I)));
               pragma Assert (Model (Set) =
                              (Old_Model with delta From .. To => False));
            else  --  Delete [Left .. Right]
               declare
                  Intermediate_Set  : constant T := Set with Ghost;
               begin
                  Set.Elems (Left .. Set.Last - (Right - Left + 1))
                    := Set.Elems (Right + 1 .. Set.Last);
                  Set.Last := Set.Last - (Right - Left + 1);

                  Lems.Delete_Lemma
                    (Old_Set     => Intermediate_Set,
                     New_Set     => Set,
                     Delete_From => Left,
                     Delete_To   => Right);

                  pragma Assert
                    (for all I in Elem_Type =>
                    (if I > To then Model (Set) (I) = Model (Old_Set) (I)));
                  pragma Assert (Model (Set) =
                                 (Old_Model with delta From .. To => False));
               end;
            end if;
         else  --  Left > Right
            pragma Assert
              (for all Elem in Elem_Type =>
               not (Set.Elems (Left).From <= Elem and Elem <= Set.Elems (Right).To));
            pragma Assert (for all Elem in From .. To => not Old_Model (Elem));
            pragma Assert (Model (Set) =
                           (Old_Model with delta From .. To => False));

            return;
         end if;
      end;
   end Subtract_Interval;

end Bounded_Interval_Union;
