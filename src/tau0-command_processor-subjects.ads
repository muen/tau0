--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Command;

package Tau0.Command_Processor.Subjects
with Abstract_State => null
is

   use type Types.Command.Command_ID_Type;

   -----------------------------------------------------------------------------

   procedure Create_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Create_Subject,
     Post => Invariants;

   procedure Lock_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Lock_Subject,
     Post => Invariants;

   procedure Activate_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Activate_Subject,
     Post => Invariants;

   procedure Attach_Memory_Region_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Attach_Memory_Region_Subject,
     Post => Invariants;

   procedure Create_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Create_Page_Table_Subject,
     Post => Invariants;

   procedure Activate_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Activate_Page_Table_Subject,
     Post => Invariants;

   procedure Map_Device_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Map_Device_Page_Subject,
     Post => Invariants;

   procedure Map_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Map_Page_Subject,
     Post => Invariants;

   procedure Assign_IRQ_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Assign_IRQ_Subject,
     Post => Invariants;

   procedure Assign_Device_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Assign_Device_Subject,
     Post => Invariants;

   procedure Set_IO_Port_Range
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Set_IO_Port_Range,
     Post => Invariants;

   procedure Set_MSR_Range
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Set_MSR_Range,
     Post => Invariants;

end Tau0.Command_Processor.Subjects;
