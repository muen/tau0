--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with GNAT.Source_Info;
with Tau0.Command_Processor;
with Tau0.Initialization;

procedure Tau0_Main
with SPARK_Mode => Off
is
   Date    : constant String := GNAT.Source_Info.Compilation_ISO_Date;
   Time    : constant String := GNAT.Source_Info.Compilation_Time;
   Version : constant String := Standard'Compiler_Version;
   Success : Boolean;
begin
   pragma Debug (Debug.Put_Line
                 (Item  => "Starting Tau0...",
                  Level => Debug.Notice));
   pragma Debug (Debug.Put_Line
                 (Item => "Compiled on " & Date & ", " & Time
                  & ", using " & Version,
                  Level => Debug.Notice));

   Tau0.Initialization.Initialize (Success => Success);

   if not Success then
      pragma Debug (Debug.Put_Line ("Could not initialize Tau0, aborting...",
                                    Level => Debug.Error));
   else
      pragma Debug (Debug.Put_Line
                    (Item  => "Processing commands...please wait",
                     Level => Debug.Notice));
      Tau0.Command_Processor.Process_Command_Stream;
   end if;

end Tau0_Main;
