--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Data.Roots.Writers.Kernels
is

   procedure Activate (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Locked and then
               Kind (Root) = Types.Root.Kernel and then
               not Active (Root) and then
               (if Paging (Root) in Types.Root.Paging_Type
                then PTP_Active (Root)),
     Post   => Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Update may not modify discriminants
               State (Root) = Types.Root.Active and
               Kind (Root) = Kind (Root)'Old and
               PTP_Present (Root) = PTP_Present (Root)'Old and
               PTP_Active (Root) = PTP_Active (Root)'Old and
               PTP (Root) = PTP (Root)'Old;

   procedure Create_Kernel (Root : Types.Root_Range; CPU : Types.CPU_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               not Used (Root) and then
               Data.Processors.Present (CPU) and then
               not Kernel_Present (CPU),
     Post   => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and then
               Used (Root) and then
               not PTP_Present (Root) and then
               not Active (Root) and then
               Kernel_Present (CPU);

end Tau0.Data.Roots.Writers.Kernels;
