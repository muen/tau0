--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Bounded_Table;
with Tau0.Types.Device;
with Tau0.Types.Hash;
with Tau0.Types.VTd_Tables;

package Tau0.Types.Root
with Abstract_State => null
is

   package Subject_Device_Table is new Bounded_Table
     (Max       => Config.Max_Devices_Per_Subject,
      Elem_Type => Types.Device.Device_Range,
      Null_Elem => 0);

   package Kernel_Device_Table is new Bounded_Table
     (Max       => Config.Max_Devices_Per_Kernel,
      Elem_Type => Types.Device.Device_Range,
      Null_Elem => 0);

   package Domain_Device_Table is new Bounded_Table
     (Max       => Config.Max_Devices_Per_Domain,
      Elem_Type => Types.Device.Device_Range,
      Null_Elem => 0);

   type IRQ_Cell is record
      Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type;
   end record;

   package IRQ_Table is new Bounded_Table
     (Max       => Config.Max_IRQs_Per_Subject,
      Elem_Type => IRQ_Cell,
      Null_Elem => IRQ_Cell'(Device => 0, IRQ => 0));

   type Paging_Type is (IA32e, EPT, MPT, VTd);

   --  Subjects
   subtype Subject_Paging_Type is Paging_Type range IA32e .. EPT;

   --  Memory regions
   type Counter_Type is new Natural;

   type Optional_Hash_Type (Check : Boolean := False) is record
      case Check is
         when True  => Digest : Hash.Digest_Type;
         when False => null;
      end case;
   end record;

   Dont_Check : constant Optional_Hash_Type
     := Optional_Hash_Type'(Check => False);

   -----------------------------------------------------------------------------

   type State_Type is (Unused, Setup, Teardown, Locked, Active);
   subtype Unlocked_State_Type is State_Type range Setup .. Teardown;

   type Kind_Type is (Memory_Region, Subject, Device_Domain, Kernel);

   type Root_Type
     (State            : State_Type := Unused;
      Is_Memory_Region : Boolean    := False;
      Kind             : Kind_Type  := Subject)
   is record
      case State is
         when Unused => null;
         when others =>
            PTP_Present : Boolean;
            PTP_Active  : Boolean;
            PTP         : Tau0_Address_Type;

            case Is_Memory_Region is
               when True =>
                  Length       : Page_Count_Type;
                  Region_Level : Nonleaf_Level_Type;
                  Caching      : Caching_Type;
                  Hash         : Optional_Hash_Type;
                  Ref_Counter  : Counter_Type;
               when False =>
                  Attached_Tables : Natural;

                  case Kind is
                     when Subject =>
                        Paging                   : Subject_Paging_Type;
                        --  We save the address of the IOBM A, while IOBM B is
                        --  stored in the successor page.  While not strictly
                        --  required, this corresponds to the assumptions in
                        --  sk-vmx.adb.
                        IO_Bitmap                : Physical_Address_Type;
                        MSR_Bitmap               : Physical_Address_Type;
                        Subject_Assigned_Devices : Subject_Device_Table.T;
                        Assigned_IRQs            : IRQ_Table.T;
                        Subject_CPU              : CPU_Range;

                     when Device_Domain =>
                        Domain_Devices : Domain_Device_Table.T;
                        Domain_Level   : Nonleaf_Level_Type;
                        Domain_ID      : Types.VTd_Tables.Domain_Identifier_Type;
                     when Kernel =>
                        Kernel_CPU              : CPU_Range;
                        Kernel_Assigned_Devices : Kernel_Device_Table.T;
                     when others => null;
                  end case;
            end case;
      end case;
   end record
   with Dynamic_Predicate =>
     --  HACK: circumvent prohibition of nested matches.
     Is_Memory_Region = (Kind = Memory_Region) and then
     (if State /= Unused then
       Aligned_Page (PTP) and
       (if PTP_Active then PTP_Present) and
       (if State = Active then PTP_Active) and
       (if State in Locked | Active then PTP_Present) and
       (if PTP_Present and then
           Kind = Memory_Region and then
           Region_Level > 1
        then PTP in Tau0_Private_Address_Type
        elsif PTP_Present
        then PTP in Physical_Address_Type) and
       (if Kind = Subject
        then Aligned_Page (IO_Bitmap) and then
             Aligned_Page (MSR_Bitmap) and then
             Successor_Page (IO_Bitmap) in Physical_Address_Type) and
       (if Kind = Device_Domain
        then Domain_Level in 4 | 5));

   Null_Root : constant Root_Type := Root_Type'
     (State => Unused, Is_Memory_Region => False, Kind => Subject);

   type Root_Array is array (Root_Range) of Root_Type;

end Tau0.Types.Root;
