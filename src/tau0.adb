--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Types.Device;

package body Tau0
with Refined_State => (State => Current_State)
is

   Current_State : Tau0_State_Type := Setup;

   -----------------------------------------------------------------------------

   procedure Set_Tau0_State (State : Tau0_State_Type)
   is
   begin
      Current_State := State;
   end Set_Tau0_State;

   -----------------------------------------------------------------------------

   function Setup_Phase_Complete return Boolean
   is (for all Device in Types.Device.Device_Range =>
       Data.Devices.State (Device) in Types.Device.Unused | Types.Device.Active);

   -----------------------------------------------------------------------------

   function Tau0_State return Tau0_State_Type
   is (Current_State);

end Tau0;
