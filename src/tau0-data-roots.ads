--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Processors;
with Tau0.Page;
with Tau0.Types.Device;
with Tau0.Types.Root;
with Tau0.Types.VTd_Tables;

--  This package manages Tau0's root objects -- subjects, memory regions, device
--  domains, and kernels.  Each root object has its own state and page table
--  pointer.
package Tau0.Data.Roots
with Abstract_State => State
is

   use type Types.CPU_Range;
   use type Types.IO_Port_Type;
   use type Types.Device.State_Type;
   use type Types.Page_Count_Type;
   use type Types.Root.Counter_Type;
   use type Types.Root.Kind_Type;
   use type Types.Root.Paging_Type;
   use type Types.Root.Root_Type;
   use type Types.Root.State_Type;
   use type Types.Root_Range;
   use type Types.Tau0_Address_Type;
   use type Types.VTd_Tables.Domain_Identifier_Type;
   use type Types.Word64;

   --  Ghost model
   function Get_Roots return Types.Root.Root_Array
   with
     Ghost,
     Global => State;

   function Invariant (Memory_Model : Memory.Memory_Model_Type) return Boolean
   with
     Ghost,
     Global => (Input => (State, Tau0.State, Data.Processors.State,
                          Devices.State, Memory.VTd_IOMMU.State));

   function Invariant return Boolean
   is (Invariant (Memory_Model => Memory.Get_Memory_Model))
   with
     Ghost,
     Global => (Input => (State,
                          Tau0.State,
                          Data.Processors.State,
                          Devices.State,
                          Memory.Memory_State,
                          Memory.VTd_IOMMU.State));

   function Roots_Unchanged
     (New_Roots : Types.Root.Root_Array;
      Old_Roots : Types.Root.Root_Array;
      Except    : Types.Root_Range)
      return Boolean
   is (for all Root in Types.Root_Range =>
         (if Root /= Except then New_Roots (Root) = Old_Roots (Root)))
   with
     Ghost,
     Global => null;

   -----------------------------------------------------------------------------

   --  Get current state of Root
   function State (Root : Types.Root_Range) return Types.Root.State_Type
   with
     Global => Roots.State,
     Post   => State'Result = Get_Roots (Root).State;

   function Used (Root : Types.Root_Range) return Boolean
   is (State (Root) /= Types.Root.Unused)
   with
     Global => Roots.State;

   function Active (Root : Types.Root_Range) return Boolean
   is (State (Root) = Types.Root.Active)
   with
     Global => Roots.State;

   function Kind (Root : Types.Root_Range) return Types.Root.Kind_Type
   with
     Global => Roots.State,
     Post   => Kind'Result = Get_Roots (Root).Kind;

   --  True iff the top-level page table of Root is present
   function PTP_Present (Root : Types.Root_Range) return Boolean
   with
     Global => Roots.State,
     Pre    => Used (Root),
     Post   => PTP_Present'Result = Get_Roots (Root).PTP_Present;

   --  True iff the top-level page table of Root is active
   function PTP_Active (Root : Types.Root_Range) return Boolean
   with
     Global => Roots.State,
     Pre    => Used (Root),
     Post   => PTP_Active'Result = Get_Roots (Root).PTP_Active;

   --  Return Root's page table pointer
   function PTP (Root : Types.Root_Range) return Types.Tau0_Address_Type
   with
     Global => Roots.State,
     Pre    => Used (Root),
     Post   => PTP'Result = Get_Roots (Root).PTP;

   --  The paging level of Root.  The top-level page table of Root is at
   --  level (Level (Root) - 1).
   function Level (Root : Types.Root_Range) return Types.Nonleaf_Level_Type
   with
     Global => Roots.State,
     Pre    => Used (Root),
     Post   => (if Kind (Root) /= Types.Root.Memory_Region then Level'Result > 1) and
               (if Kind (Root) = Types.Root.Memory_Region
                then Level'Result = Get_Roots (Root).Region_Level
                elsif Kind (Root) = Types.Root.Device_Domain
                then Level'Result = Get_Roots (Root).Domain_Level);

   --  Paging mode of Root
   function Paging (Root : Types.Root_Range) return Types.Root.Paging_Type
   with
     Global => Roots.State,
     Pre    => Used (Root),
     Post   => (if Kind (Root) = Types.Root.Subject
                then Paging'Result = Get_Roots (Root).Paging);

   --  Number of memory regions attached to root
   function Attached_Memory_Regions (Root : Types.Root_Range) return Natural
   with
     Global => Roots.State,
     Pre    => Used (Root) and Kind (Root) /= Types.Root.Memory_Region,
     Post   => Attached_Memory_Regions'Result = Get_Roots (Root).Attached_Tables;

   --  Return true if no more devices can be added to Root.  Applies to
   --  subjects, kernels, and device domains.
   function Device_Table_Full (Root : Types.Root_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Root) and then
               Kind (Root) in Types.Root.Subject
                            | Types.Root.Device_Domain
                            | Types.Root.Kernel;

   --  May we assign Device to Root?
   function Device_Assignable
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
      return Boolean
   with
     Global => (Proof_In => Roots.State),
     Pre    => Used (Root) and then
               Kind (Root) in Types.Root.Subject | Types.Root.Kernel;

   --  Is Device assigned to Root?
   function Device_Assigned
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
      return Boolean
   with
     Global => Roots.State,
     Pre    => Used (Root) and then
               Kind (Root) in Types.Root.Subject | Types.Root.Kernel;

   -----------------------------------------------------------------------------
   --  Subjects
   -----------------------------------------------------------------------------

   --  Get first page of IO Bitmap of subject Root
   function IO_Bitmap_Low (Root : Types.Root_Range) return Types.Physical_Address_Type
   with
     Global => Roots.State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Subject,
     Post   => IO_Bitmap_Low'Result = Get_Roots (Root).IO_Bitmap;

   --  Get second page of IO Bitmap of subject Root
   function IO_Bitmap_High (Root : Types.Root_Range) return Types.Physical_Address_Type
   with
     Global => Roots.State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Subject,
     Post   => IO_Bitmap_High'Result
                 = Types.Successor_Page (Get_Roots (Root).IO_Bitmap);

   --  Get MSR Bitmap of subject Root
   function MSR_Bitmap (Root : Types.Root_Range) return Types.Physical_Address_Type
   with
     Global => Roots.State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Subject,
     Post   => MSR_Bitmap'Result = Get_Roots (Root).MSR_Bitmap;

   function Assigned_IRQs (Root : Types.Root_Range) return Natural
   with
     Global => State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Subject;

   function IRQ_Table_Full (Root : Types.Root_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Subject;

   --  Check whether the IO port range From .. To is owned by Device, and
   --  whether Device is assigned to subject Root
   function IO_Port_Range_Assignable
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type)
      return Boolean
   with
     Global => (Devices.State, Roots.State),
     Pre    => From <= To and then
               Used (Root) and then
               Kind (Root) = Types.Root.Subject and then
               Data.Devices.State (Device) = Types.Device.Active;

   function IRQ_Assigned
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type)
      return Boolean
   with
     Global => Roots.State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Subject;

   --  Check whether IRQ is owned by Device, and whether Device is assigned to
   --  subject Root
   function IRQ_Assignable
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type)
      return Boolean
   with
     Global => (Devices.State, Roots.State),
     Pre    => Used (Root) and then
               Kind (Root) = Types.Root.Subject and then
               Data.Devices.State (Device) = Types.Device.Active;

   -----------------------------------------------------------------------------
   --  Memory regions
   -----------------------------------------------------------------------------

   --  Get number of references to memory region Root
   function Reference_Count
     (Root : Types.Root_Range)
      return Types.Root.Counter_Type
   with
     Global => Roots.State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Memory_Region,
     Post   => Reference_Count'Result = Get_Roots (Root).Ref_Counter;

   --  Check if the hash of memory region Root is as specified on Root's
   --  creation
   function Hash_Valid (Root : Types.Root_Range) return Boolean
   with
     Global => (Proof_In => (Tau0.State,
                             Memory.Typization_State,
                             Memory.VTd_IOMMU.State,
                             Devices.State,
                             Data.Processors.State),
                Input    => (Roots.State,
                             Memory.Memory_State)),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Memory_Region and then
               PTP_Present (Root) and then
               Must_Check_Hash (Root);

   --  Get the number of pages the memory region Root consists of
   function Length
     (Root : Types.Root_Range)
      return Types.Page_Count_Type
   with
     Global => Roots.State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Memory_Region,
     Post   => Length'Result = Get_Roots (Root).Length;

   --  Should the memory region's hash be checked before locking?
   function Must_Check_Hash (Root : Types.Root_Range) return Boolean
   with
     Global => Roots.State,
     Pre    => Used (Root) and then
               Kind (Root) = Types.Root.Memory_Region;

   function Caching (Root : Types.Root_Range) return Types.Caching_Type
   with
     Global => Roots.State,
     Pre    => Used (Root) and then
               Kind (Root) = Types.Root.Memory_Region;

   -----------------------------------------------------------------------------
   --  Device domains
   -----------------------------------------------------------------------------

   --  Return true iff Device has been added to some device domain
   function Device_Included (Device : Types.Device.Device_Range) return Boolean
   with Global => State;

   --  Return true iff Device has been added to device domain Root
   function Includes_Device
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
      return Boolean
   with
     Global => State,
     Pre    => Used (Root) and then Kind (Root) = Types.Root.Device_Domain;

   --  Return true if all VT-d tables necessary for the devices attached to Root
   --  have been allocated
   function VTd_Tables_Valid (Root : Types.Root_Range) return Boolean
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Typization_State,
                             Data.Processors.State),
                Input    => (Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Devices.State,
                             Roots.State)),
     Pre    => Used (Root) and then
               Kind (Root) = Types.Root.Device_Domain and then
               Invariant;

   function Domain_ID
     (Root : Types.Root_Range)
      return Types.VTd_Tables.Domain_Identifier_Type
   with
     Global => State,
     Pre    => Used (Root) and then
               Kind (Root) = Types.Root.Device_Domain;

   -----------------------------------------------------------------------------
   --  Kernels
   -----------------------------------------------------------------------------

   --  Return true iff some kernel has been assigned to CPU
   function Kernel_Present (CPU : Types.CPU_Range) return Boolean
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Processors.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State),
                Input    => State),
     Pre    => Invariant and Data.Processors.Present (CPU),
     Post   => Kernel_Present'Result = (for some Root in Types.Root_Range =>
                                        Used (Root) and then
                                        Kind (Root) = Types.Root.Kernel and then
                                        Get_Roots (Root).Kernel_CPU = CPU);

   -----------------------------------------------------------------------------

   procedure Lemma_Invariant (Old_Memory, New_Memory : Data.Memory.Memory_Model_Type)
   with
     Ghost,
     Pre  => Invariant (Old_Memory) and then
             Memory.VTd_IOMMU.Root_Table_Present and then
             (for all A in Types.Tau0_Address_Type =>
             (if Page.Within (Address => A, Page => Memory.VTd_IOMMU.Root_Table_Address)
              then Old_Memory (Memory.To_Range (A)) = New_Memory (Memory.To_Range (A)))),
     Post => Invariant (New_Memory);

private

   type CPU_Array is array (Types.CPU_Range) of Boolean;

   Roots : Types.Root.Root_Array := (others => Types.Root.Null_Root)
   with Part_Of => State;

   CPUs  : CPU_Array := (others => False)
   with Part_Of => State;

   -----------------------------------------------------------------------------
   --  Invariants
   -----------------------------------------------------------------------------

   --  For each CPU, CPUs (CPU) holds iff some kernel is assigned to CPU.
   function Invariant_CPUs_Correct
     (Roots : Types.Root.Root_Array;
      CPUs  : CPU_Array)
      return Boolean
   is (for all CPU in CPU_Array'Range =>
       CPUs (CPU) = (for some Root in Roots'Range =>
                     Roots (Root).State /= Types.Root.Unused and then
                     Roots (Root).Kind = Types.Root.Kernel and then
                     Roots (Root).Kernel_CPU = CPU))
   with
     Ghost,
     Global => null;

   --  For all subjects and kernels, their assigned CPUs are present.
   function Invariant_CPUs_Present (Roots : Types.Root.Root_Array) return Boolean
   is (for all U in Roots'Range =>
       (if Roots (U).State /= Types.Root.Unused then
          (if Roots (U).Kind = Types.Root.Subject
           then Data.Processors.Present (Roots (U).Subject_CPU)) and
          (if Roots (U).Kind = Types.Root.Kernel
           then Data.Processors.Present (Roots (U).Kernel_CPU))))
   with
     Ghost,
     Global => (Input => Data.Processors.State);

   --  At most one kernel is assigned to each CPU.
   function Invariant_No_Shared_CPUs
     (Roots : Types.Root.Root_Array)
      return Boolean
   is (for all U in Roots'Range =>
      (for all V in Roots'Range =>
         (if Roots (U).State /= Types.Root.Unused and
             Roots (V).State /= Types.Root.Unused and
             Roots (U).Kind = Types.Root.Kernel and
             Roots (V).Kind = Types.Root.Kernel
          then (if Roots (U).Kernel_CPU = Roots (V).Kernel_CPU
                then U = V))))
   with
     Ghost,
     Global => null;

   --  Each device assigned to a subject or root is active
   function Invariant_Assigned_Devices
     (Roots : Types.Root.Root_Array)
      return Boolean
   is (for all U in Roots'Range =>
      (if Roots (U).State /= Types.Root.Unused
       then (if Roots (U).Kind = Types.Root.Subject
             then (for all Device of Roots (U).Subject_Assigned_Devices =>
                   Data.Devices.State (Device) = Types.Device.Active)) and
            (if Roots (U).Kind = Types.Root.Kernel
             then (for all Device of Roots (U).Kernel_Assigned_Devices =>
                   Data.Devices.State (Device) = Types.Device.Active))))
   with
     Ghost,
     Global => Devices.State;

   --  Each assigned IRQ is owned by the specified device
   function Invariant_Assigned_IRQs
     (Roots : Types.Root.Root_Array)
      return Boolean
   is (for all U in Roots'Range =>
      (if Roots (U).State /= Types.Root.Unused and
          Roots (U).Kind = Types.Root.Subject
       then (for all I in Roots (U).Assigned_IRQs =>
               Types.Root.Subject_Device_Table.Member
                 (Table => Roots (U).Subject_Assigned_Devices,
                  Elem  => Types.Root.IRQ_Table.Element
                    (Table  => Roots (U).Assigned_IRQs,
                     Cursor => I).Device) and then
               Data.Devices.Owns_IRQ
                 (Device => Types.Root.IRQ_Table.Element
                    (Table  => Roots (U).Assigned_IRQs,
                     Cursor => I).Device,
                  IRQ => Types.Root.IRQ_Table.Element
                    (Table  => Roots (U).Assigned_IRQs,
                     Cursor => I).IRQ))))
   with
     Ghost,
     Global => Devices.State,
     Pre    => Invariant_Assigned_Devices (Roots);

   --  Each device added to a domain is active, a PCI device, and its VT-d
   --  context table has been created.
   function Invariant_Devices_Device_Domain
     (Roots        : Types.Root.Root_Array;
      Memory_Model : Memory.Memory_Model_Type)
      return Boolean
   is (for all U in Roots'Range =>
      (if Roots (U).State /= Types.Root.Unused and
          Roots (U).Kind = Types.Root.Device_Domain
       then (for all Device of Roots (U).Domain_Devices =>
             Devices.State (Device) = Types.Device.Active and then
             Devices.Is_PCI (Device) and then
             Memory.VTd_IOMMU.Root_Table_Present and then
             Memory.VTd_IOMMU.Root_Table_Model_Using (Model => Memory_Model)
               (Devices.PCI_Bus (Device)).Present)))
   with
     Ghost,
     Global => (Input => (Devices.State, Memory.VTd_IOMMU.State));

   function Invariant_IDs_Device_Domain
     (Roots : Types.Root.Root_Array)
      return Boolean
   is (for all U in Roots'Range =>
      (for all V in Roots'Range =>
      (if Roots (U).State /= Types.Root.Unused and then
          Roots (U).Kind = Types.Root.Device_Domain and then
          Roots (V).State /= Types.Root.Unused and then
          Roots (V).Kind = Types.Root.Device_Domain and then
          Roots (U).Domain_ID = Roots (V).Domain_ID
       then U = V)))
   with Ghost;

   --  If Tau0 is still in setup phase, then there are no root objects
   function Invariant_Tau0_State
     (Roots : Types.Root.Root_Array)
      return Boolean
   is (if Tau0_State = Setup
       then (for all U in Roots'Range => Roots (U).State = Types.Root.Unused))
   with
     Ghost,
     Global => (Input => Tau0.State);

   -----------------------------------------------------------------------------

   function Invariant (Memory_Model : Memory.Memory_Model_Type) return Boolean
   is (Invariant_CPUs_Correct (Roots, CPUs) and then
       Invariant_CPUs_Present (Roots) and then
       Invariant_No_Shared_CPUs (Roots) and then
       Invariant_Assigned_Devices (Roots) and then
       Invariant_Assigned_IRQs (Roots) and then
       Invariant_Devices_Device_Domain (Roots, Memory_Model) and then
       Invariant_IDs_Device_Domain (Roots) and then
       Invariant_Tau0_State (Roots));

   -----------------------------------------------------------------------------
   --  Function bodies
   -----------------------------------------------------------------------------

   function Get_Roots return Types.Root.Root_Array
   is (Roots);

   -----------------------------------------------------------------------------

   function Assigned_IRQs (Root : Types.Root_Range) return Natural
   is (Types.Root.IRQ_Table.Length (Roots (Root).Assigned_IRQs));

   -----------------------------------------------------------------------------

   function Attached_Memory_Regions (Root : Types.Root_Range) return Natural
   is (Roots (Root).Attached_Tables);

   -----------------------------------------------------------------------------

   function Caching (Root : Types.Root_Range) return Types.Caching_Type
   is (Roots (Root).Caching);

   -----------------------------------------------------------------------------

   function Device_Assigned
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
      return Boolean
   is (if Kind (Root) = Types.Root.Subject
       then Types.Root.Subject_Device_Table.Member
              (Table => Roots (Root).Subject_Assigned_Devices,
               Elem  => Device)
       elsif Kind (Root) = Types.Root.Kernel
       then Types.Root.Kernel_Device_Table.Member
              (Table => Roots (Root).Kernel_Assigned_Devices,
               Elem  => Device)
       else False);  -- Precluded by precondition

   -----------------------------------------------------------------------------

   --  OPTIMIZE?
   function Device_Included (Device : Types.Device.Device_Range) return Boolean
   is (for some Root in Types.Root_Range =>
       Used (Root) and then
       Kind (Root) = Types.Root.Device_Domain and then
       Includes_Device (Root => Root, Device => Device));

   -----------------------------------------------------------------------------

   function Device_Table_Full (Root : Types.Root_Range) return Boolean
   is (if Kind (Root) = Types.Root.Subject
       then Types.Root.Subject_Device_Table.Full
              (Roots (Root).Subject_Assigned_Devices)
       elsif Kind (Root) = Types.Root.Kernel
       then Types.Root.Kernel_Device_Table.Full
              (Roots (Root).Kernel_Assigned_Devices)
       elsif Kind (Root) = Types.Root.Device_Domain
       then Types.Root.Domain_Device_Table.Full
              (Roots (Root).Domain_Devices)
       else False);  -- Precluded by precondition

   -----------------------------------------------------------------------------

   function Domain_ID
     (Root : Types.Root_Range)
      return Types.VTd_Tables.Domain_Identifier_Type
   is (Roots (Root).Domain_ID);

   -----------------------------------------------------------------------------

   function Must_Check_Hash (Root : Types.Root_Range) return Boolean
   is (Roots (Root).Hash.Check);

   -----------------------------------------------------------------------------

   function Includes_Device
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
      return Boolean
   is (Types.Root.Domain_Device_Table.Member
         (Elem  => Device,
          Table => Roots (Root).Domain_Devices));

   -----------------------------------------------------------------------------

   function IO_Bitmap_Low (Root : Types.Root_Range) return Types.Physical_Address_Type
   is (Roots (Root).IO_Bitmap);

   -----------------------------------------------------------------------------

   function IO_Bitmap_High (Root : Types.Root_Range) return Types.Physical_Address_Type
   is (Types.Successor_Page (Roots (Root).IO_Bitmap));

   -----------------------------------------------------------------------------

   function IO_Port_Range_Assignable
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type)
      return Boolean
   is (Device_Assigned (Root => Root, Device => Device) and then
       Data.Devices.Owns_IO_Port_Range (Device => Device,
                                        From   => From,
                                        To     => To));

   -----------------------------------------------------------------------------

   function IRQ_Assignable
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type)
      return Boolean
   is (Device_Assigned (Root => Root, Device => Device) and then
       Data.Devices.Owns_IRQ (Device => Device, IRQ => IRQ));

   -----------------------------------------------------------------------------

   function IRQ_Assigned
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type)
      return Boolean
   is (Types.Root.IRQ_Table.Member (Table => Roots (Root).Assigned_IRQs,
                                    Elem  => (Device => Device,
                                              IRQ    => IRQ)));

   -----------------------------------------------------------------------------

   function IRQ_Table_Full (Root : Types.Root_Range) return Boolean
   is (Types.Root.IRQ_Table.Full (Roots (Root).Assigned_IRQs));

   -----------------------------------------------------------------------------

   function Kernel_Present (CPU : Types.CPU_Range) return Boolean
   is (CPUs (CPU));

   -----------------------------------------------------------------------------

   function Kind (Root : Types.Root_Range) return Types.Root.Kind_Type
   is (Roots (Root).Kind);

   -----------------------------------------------------------------------------

   function Length
     (Root : Types.Root_Range)
      return Types.Page_Count_Type
   is (Roots (Root).Length);

   -----------------------------------------------------------------------------

   function Level (Root : Types.Root_Range) return Types.Nonleaf_Level_Type
   is (case Kind (Root) is
         when Types.Root.Subject       => 5,
         when Types.Root.Memory_Region => Roots (Root).Region_Level,
         when Types.Root.Device_Domain => Roots (Root).Domain_Level,
         when Types.Root.Kernel        => 5);

   -----------------------------------------------------------------------------

   function MSR_Bitmap (Root : Types.Root_Range) return Types.Physical_Address_Type
   is (Roots (Root).MSR_Bitmap);

   -----------------------------------------------------------------------------

   function State (Root : Types.Root_Range) return Types.Root.State_Type
   is (Roots (Root).State);

   -----------------------------------------------------------------------------

   function Paging
     (Root : Types.Root_Range)
      return Types.Root.Paging_Type
   is (case Kind (Root) is
         when Types.Root.Subject       => Roots (Root).Paging,
         when Types.Root.Memory_Region => Types.Root.MPT,
         when Types.Root.Device_Domain => Types.Root.EPT,     --  REVIEW
         when Types.Root.Kernel        => Types.Root.IA32e);

   -----------------------------------------------------------------------------

   function PTP
     (Root : Types.Root_Range)
      return Types.Tau0_Address_Type
   is (Roots (Root).PTP);

   -----------------------------------------------------------------------------

   function PTP_Active (Root : Types.Root_Range) return Boolean
   is (Roots (Root).PTP_Active);

   -----------------------------------------------------------------------------

   function PTP_Present (Root : Types.Root_Range) return Boolean
   is (Roots (Root).PTP_Present);

   -----------------------------------------------------------------------------

   function Reference_Count
     (Root : Types.Root_Range)
      return Types.Root.Counter_Type
   is (Roots (Root).Ref_Counter);

   -----------------------------------------------------------------------------

   function VTd_Tables_Valid (Root : Types.Root_Range) return Boolean
   is (Data.Memory.VTd_IOMMU.Root_Table_Present and then
      (for all D of Roots (Root).Domain_Devices =>
       Data.Memory.VTd_IOMMU.Context_Table_Present
         (Bus => Data.Devices.PCI_Bus (D))));

end Tau0.Data.Roots;
