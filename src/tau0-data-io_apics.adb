--
--  Copyright (C) 2020  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.IO_APICs
with Refined_State => (State => IO_APIC)
is

   type IOAPIC_Type (Present : Boolean := False) is record
      case Present is
         when True  => Source_ID : Types.Source_ID_Type;
         when False => null;
      end case;
   end record;

   IO_APIC : IOAPIC_Type := IOAPIC_Type'(Present => False);

   -----------------------------------------------------------------------------

   function Is_Set return Boolean
   is (IO_APIC.Present);

   -----------------------------------------------------------------------------

   procedure Set_Source_ID (ID : Types.Source_ID_Type)
   is
   begin
      IO_APIC := IOAPIC_Type'(Present => True,
                              Source_ID => ID);
   end Set_Source_ID;

   -----------------------------------------------------------------------------

   function Source_ID return Types.Source_ID_Type
   is (IO_APIC.Source_ID);

end Tau0.Data.IO_APICs;
