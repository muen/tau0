--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

--  Store a set which is the union of a bounded number of intervals.
--  O (log n) lookup (with n = number of intervals),
--  O (n)     insertion (sic!)
--            REVIEW/OPTIMIZE: improve insertion time?  AVL search trees?
generic
   Bound : Positive;
   type Elem_Type is (<>);
package Bounded_Interval_Union
with Abstract_State => null
is

   pragma Annotate (GNATprove, Terminating, Bounded_Interval_Union);

   type T is private;

   type Model_Type is array (Elem_Type) of Boolean;

   function Model (Set : T) return Model_Type
   with Ghost;

   function Empty_Set return T
   with Post => (for all E in Elem_Type =>
                 not Model (Empty_Set'Result) (E));

   --  Return True if Elem is an element of Set
   function Element (Elem : Elem_Type; Set : T) return Boolean
   with Post => Element'Result = Model (Set) (Elem);

   --  Return True if [From, To] ⊆ Set
   function Contains (From, To : Elem_Type; Set : T) return Boolean
   with
     Global => null,
     Post   => Contains'Result
                 = (for all Elem in From .. To => Model (Set) (Elem));

   --  The length of the internal array representation of Set
   function Length (Set : T) return Natural
   with
     Global => null,
     Post   => Length'Result <= Bound;

   --  May we add another interval?
   function Full (Set : T) return Boolean
   with
     Global => null,
     Post   => Full'Result = (Length (Set) = Bound);

   --  Add interval [From, To] to Set
   procedure Add_Interval
     (Set  : in out T;
      From :        Elem_Type;
      To   :        Elem_Type)
   with
     Global => null,
     Pre    => From <= To and not Full (Set),
     Post   => Length (Set) <= Length (Set)'Old + 1 and
               Model (Set) = (Model (Set)'Old with delta From .. To => True);

   --  Remove interval [From, To] from Set
   procedure Subtract_Interval
     (Set  : in out T;
      From :        Elem_Type;
      To   :        Elem_Type)
   with
     Global => null,
     Pre    => From <= To and not Full (Set),
     Post   => Length (Set) <= Length (Set)'Old + 1 and
               Model (Set) = (Model (Set)'Old with delta From .. To => False);

   --  True iff Set contains some element of [From, To].  O (n).
   function Contains_Some (Set : T; From, To : Elem_Type) return Boolean
   with
     Global => null,
     Post   => Contains_Some'Result
                 = (for some Elem in From .. To => Model (Set) (Elem));

   --  True iff the sets L and R are disjoint.  O (n log n).
   function Disjoint (L, R : T) return Boolean
   with
     Global => null,
     Post   => Disjoint'Result
                 = (for all Elem in Elem_Type =>
                    not (Model (L) (Elem) and Model (R) (Elem)));

   --  Print the intervals of Set to debug output
   procedure Print (Set : T)
   with Global => null;

private

   function Clamped_Pred (E : Elem_Type) return Elem_Type'Base;
   function Clamped_Succ (E : Elem_Type) return Elem_Type'Base;

   -----------------------------------------------------------------------------

   function Clamped_Pred (E : Elem_Type) return Elem_Type'Base
   is (if E = Elem_Type'Base'First then E else Elem_Type'Pred (E));

   -----------------------------------------------------------------------------

   function Clamped_Succ (E : Elem_Type) return Elem_Type'Base
   is (if E = Elem_Type'Base'Last then E else Elem_Type'Succ (E));

   -----------------------------------------------------------------------------

   type Cell_Type is record
      From : Elem_Type;
      To   : Elem_Type;
   end record
   with Dynamic_Predicate => (Cell_Type.From <= Cell_Type.To);

   Null_Cell : constant Cell_Type :=
     Cell_Type'(From => Elem_Type'First,
                To   => Elem_Type'First);

   function Less (L, R : Cell_Type) return Boolean
   is (Elem_Type'Succ (L.To) < R.From)
   with
     Ghost,
     Global => null,
     Pre => L.To < Elem_Type'Base'Last;
   pragma Annotate (GNATprove, Inline_For_Proof, Less);

   -----------------------------------------------------------------------------

   subtype Cell_Range is Positive range 1 .. Bound;

   type T_Array is array (Cell_Range) of Cell_Type;

   --  Data structure invariant.  Ensures that
   --  - cells are stored in increasing order,
   --  - between two adjacent cells, there is a gap of at least one element,
   --  - overflow of Elem_Type is prevented.
   function Invariant (Last : Natural; Elems : T_Array) return Boolean
   is (Last <= Bound and then
       (for all Idx in Cell_Range'First .. Last =>
        Elems (Idx).From <= Elems (Idx).To) and then
       (for all Idx in Cell_Range'First + 1 .. Last =>
        Elems (Idx).From > Elem_Type'Base'First) and then
       (for all Idx in Cell_Range'First .. Last - 1 =>
        Elems (Idx).To < Elem_Type'Base'Last) and then
       (for all I in Cell_Range'First .. Last =>
       (for all J in Cell_Range'First .. Last =>
          (if I < J then Less (Elems (I), Elems (J))))))
   with Ghost;
   pragma Annotate (GNATprove, Inline_For_Proof, Invariant);

   type T is record
      Last  : Natural := 0;
      Elems : T_Array;
   end record
   with Type_Invariant => Invariant (Last, Elems);

end Bounded_Interval_Union;
