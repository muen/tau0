--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Processors;
with Tau0.Data.Roots;
with Tau0.Page;
with Tau0.Types.Typization;

package Tau0.Data.Devices.Writers
with Abstract_State => null
is

   use type Types.Page_Count_Type;
   use type Types.Physical_Address_Type;
   use type Types.Typization.Typization_Type;

   -----------------------------------------------------------------------------

   function PCI_BDF_Used
     (PCI_Bus  : Types.Device.PCI_Bus_Range;
      PCI_Dev  : Types.Device.PCI_Device_Range;
      PCI_Func : Types.Device.PCI_Function_Range)
      return Boolean
   with Global => State;

   procedure Create_Legacy_Device (Device : Types.Device.Device_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                In_Out   => Data.Devices.State),
     Pre    => Data.Roots.Invariant and
               Tau0_State = Setup and
               State (Device) = Types.Device.Unused,
     Post   => Data.Roots.Invariant and
               State (Device) = Types.Device.Setup;

   procedure Create_PCI_Device
     (Device      : Types.Device.Device_Range;
      PCI_Bus     : Types.Device.PCI_Bus_Range;
      PCI_Dev     : Types.Device.PCI_Device_Range;
      PCI_Func    : Types.Device.PCI_Function_Range;
      IOMMU_Group : Types.Device.IOMMU_Group_Type;
      Uses_MSI    : Boolean;
      Classcode   : Types.Device.Classcode_Type;
      Vendor_ID   : Types.Device.Vendor_ID_Type;
      Device_ID   : Types.Device.Device_ID_Type;
      Revision_ID : Types.Device.Revision_ID_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                In_Out   => Data.Devices.State),
     Pre    => Data.Roots.Invariant and
               Tau0_State = Setup and
               State (Device) = Types.Device.Unused and
               not PCI_BDF_Used (PCI_Bus, PCI_Dev, PCI_Func),
     Post   => Data.Roots.Invariant and
               State (Device) = Types.Device.Setup;

   procedure Add_IO_Port_Range
     (Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                In_Out   => Data.Devices.State),
     Pre    => Data.Roots.Invariant and then
               Tau0_State = Setup and then
               State (Device) = Types.Device.Setup and then
               not IO_Port_Set_Full (Device) and then
               From <= To and then
               not Some_IO_Port_Used (From, To),
     Post   => Data.Roots.Invariant and
               Owns_IO_Port_Range (Device, From, To);

   procedure Add_IRQ
     (Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                In_Out   => Data.Devices.State),
     Pre    => Data.Roots.Invariant and then
               Tau0_State = Setup and then
               State (Device) = Types.Device.Setup and then
               not IRQ_Table_Full (Device) and then
               not IRQ_Used (IRQ),
     Post   => Data.Roots.Invariant and
               Owns_IRQ (Device, IRQ);

   procedure Add_Memory
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type;
      Caching : Types.Caching_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                In_Out   => (Data.Devices.State,
                             Data.Memory.Typization_State)),
     Pre    => Data.Memory.Invariant and then
               Data.Roots.Invariant and then
               Tau0_State = Setup and then
               State (Device) = Types.Device.Setup and then
               not Memory_Table_Full (Device) and then
               Types.Aligned_Page (Address) and then
               Size > 0 and then
               Address + Types.From_Page_Count (Size) - 1
                 in Types.Physical_Address_Type and then
               not Data.Memory.Device_Blocks_Full and then
               not Memory_Used (Address, Size),
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant and
               (for all A in Types.Tau0_Address_Type =>
               (if Page.Within (Address => A, Base => Address, Length => Size)
                then (if Types.Aligned_Page (A) then Owns_Page (Device, A)) and
                      Data.Memory.Get_Typization (A) = Types.Typization.Device_Page));

   procedure Activate (Device : Types.Device.Device_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                In_Out   => Data.Devices.State),
     Pre    => Data.Roots.Invariant and
               Tau0_State = Setup and
               State (Device) = Types.Device.Setup,
     Post   => Data.Roots.Invariant and
               State (Device) = Types.Device.Active;

private

   use type Types.Device.PCI_Bus_Range;
   use type Types.Device.PCI_Device_Range;
   use type Types.Device.PCI_Function_Range;

   function PCI_BDF_Used
     (PCI_Bus  : Types.Device.PCI_Bus_Range;
      PCI_Dev  : Types.Device.PCI_Device_Range;
      PCI_Func : Types.Device.PCI_Function_Range)
      return Boolean
   is (for some D in Types.Device.Device_Range =>
         Used (D) and then
         Is_PCI (D) and then
         Data.Devices.PCI_Bus (D)  = PCI_Bus and then
         Data.Devices.PCI_Dev (D)  = PCI_Dev and then
         Data.Devices.PCI_Func (D) = PCI_Func);

end Tau0.Data.Devices.Writers;
