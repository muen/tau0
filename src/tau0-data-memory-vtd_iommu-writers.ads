--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Roots;
with Tau0.Types.Root;

package Tau0.Data.Memory.VTd_IOMMU.Writers
is

   use type Types.Device.State_Type;
   use type Types.Root.Kind_Type;

   -----------------------------------------------------------------------------

   procedure Create_Root_Table (Address : Types.Physical_Address_Type)
   with
     Global => (Proof_In => (Tau0.State, Data.Memory.Memory_State),
                In_Out   => (State, Data.Memory.Typization_State)),
     Pre  => Data.Memory.Invariant and
             Tau0_State = Setup and
             Types.Aligned_Page (Address) and
             Get_Typization (Address) = Types.Typization.Zeroed and
             not Root_Table_Present,
     Post => Data.Memory.Invariant and
             Get_Typization (Address) = Types.Typization.Used and
             Root_Table_Present;

   procedure Create_Context_Table
     (Bus     : Types.Device.PCI_Bus_Range;
      Address : Types.Physical_Address_Type)
   with
     Global => (Proof_In => Tau0.State,
                Input    => State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Memory.Typization_State)),
     Pre  => Data.Memory.Invariant and then
             Tau0_State = Setup and then
             Types.Aligned_Page (Address) and then
             Get_Typization (Address) = Types.Typization.Zeroed and then
             Root_Table_Present and then
             not Context_Table_Present (Bus),
     Post => Data.Memory.Invariant and
             Get_Typization (Address) = Types.Typization.Used and
             Context_Table_Present (Bus);

   procedure Add_Device
     (Device : Types.Device.Device_Range;
      Domain : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State, Data.Memory.Typization_State),
                Input    => (State, Data.Roots.State, Devices.State),
                In_Out   => Data.Memory.Memory_State),
     Pre  => Data.Memory.Invariant and then
             Tau0_State = Running and then
             Data.Roots.Used (Domain) and then
             Data.Roots.PTP_Present (Domain) and then
             Data.Roots.Kind (Domain) = Types.Root.Device_Domain and then
             not Data.Roots.Active (Domain) and then
             Data.Devices.State (Device) = Types.Device.Active and then
             Data.Devices.Is_PCI (Device) and then
             Root_Table_Present and then
             Context_Table_Present (Data.Devices.PCI_Bus (Device)) and then
             not Context_Table_Entry_Used
               (Data.Devices.PCI_Bus (Device),
                Data.Devices.PCI_Dev (Device),
                Data.Devices.PCI_Func (Device)),
     Post => Data.Memory.Invariant and then  --  TODO prove
             Context_Table_Present (Data.Devices.PCI_Bus (Device)) and then
             Context_Table_Entry_Used
               (Data.Devices.PCI_Bus (Device),
                Data.Devices.PCI_Dev (Device),
                Data.Devices.PCI_Func (Device));

private

   procedure Set_Root_Table_Entry
     (Bus : Types.Device.PCI_Bus_Range;
      E   : Types.VTd_Tables.Root_Entry_Type)
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => State,
                In_Out   => Data.Memory.Memory_State),
     Pre  => Data.Memory.Invariant and
             Root_Table_Present and
             (if E.Present
              then Get_Typization (E.Context_Table_Address)
                     = Types.Typization.Used),
     Post => Data.Memory.Invariant and then
             Data.Memory.Get_Typization_Model
               = Data.Memory.Get_Typization_Model'Old and then
             Get_Root_Table_Entry (Bus) = E;

   procedure Set_Context_Table_Entry
     (Bus  : Types.Device.PCI_Bus_Range;
      Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range;
      E    : Types.VTd_Tables.Context_Entry_Type)
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => State,
                In_Out   => Data.Memory.Memory_State),
     Pre  => Data.Memory.Invariant and then
             Root_Table_Present and then
             Context_Table_Present (Bus),
     Post => Data.Memory.Invariant and then
             Context_Table_Present (Bus) and then
             Get_Context_Table_Entry (Bus, Dev, Func) = E;

end Tau0.Data.Memory.VTd_IOMMU.Writers;
