--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Roots;
with Tau0.Data.Memory.Interrupt_Remapping;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Types.Device;
with Tau0.Types.Page_Table;
with Tau0.Types.Root;

--  Model reachability via page translation
package Tau0.Data.Memory.Full_Typization
is

   use type Types.Root.Kind_Type;

   --  A path is a list of tuples (page address, PTE offset).  It witnesses that
   --  a physical page can be reached from a root object during page
   --  translation.

   subtype Offset_Type is Types.Tau0_Address_Type
     range 0 .. Config.Page_Size - 1
   with Dynamic_Predicate => Types.Aligned_64 (Offset_Type);

   type Cell_Type is record
      Page   : Types.Tau0_Address_Type;
      Offset : Offset_Type;
   end record
   with Dynamic_Predicate => Types.Aligned_Page (Page);

   subtype Path_Index is Natural range 1 .. 5;

   type Path_Type is array (Path_Index) of Cell_Type;

   Null_Path : constant Path_Type := Path_Type'(others => Cell_Type'(0, 0));

   --  Model one step of page translation
   function Page_Table_Maps
     (Paging : Types.Root.Paging_Type;
      Level  : Types.Page_Level_Type;
      Parent : Types.Tau0_Address_Type;
      Offset : Offset_Type;
      Child  : Types.Tau0_Address_Type)
      return Boolean
   with
     Ghost,
     Pre => Types.Aligned_Page (Parent) and
            Types.Aligned_Page (Child);

   function Page_Table_Maps
     (Paging : Types.Root.Paging_Type;
      Level  : Types.Page_Level_Type;
      Parent : Types.Tau0_Address_Type;
      Offset : Offset_Type;
      Child  : Types.Tau0_Address_Type)
      return Boolean
   is (Types.Page_Table.To_Entry
         (Paging => Paging,
          Level  => Level,
          Word   => Get_Memory_Model
            (To_Range (Parent + Offset))).Present and
       Types.Page_Table.To_Entry
         (Paging => Paging,
          Level  => Level,
          Word   => Get_Memory_Model
            (To_Range (Parent + Offset))).Address = Child);

   --  Return true if given Path witnesses that Destination can be reached from
   --  Root.
   function Reachable
     (Root        : Types.Root_Range;
      Destination : Types.Tau0_Address_Type;
      Path        : Path_Type;
      Path_Last   : Path_Index)
      return Boolean
   is (Path_Last <= Data.Roots.Level (Root) and then
       Path (Path'First).Page = Data.Roots.PTP (Root) and then
       (for all I in Path'First .. Path_Last - 1 =>
        Page_Table_Maps
          (Paging => Data.Roots.Paging (Root),
           Level  => Data.Roots.Level (Root) - I,
           Parent => Path (I).Page,
           Offset => Path (I).Offset,
           Child  => Path (I + 1).Page)) and then
       Destination = Path (Path_Last).Page + Path (Path_Last).Offset)
   with
     Ghost,
     Pre => Data.Roots.Used (Root);

   --  As above, but omit destination (given by endpoint)
   function Reachable
     (Root        : Types.Root_Range;
      Path        : Path_Type;
      Path_Last   : Path_Index)
      return Boolean
   is (Reachable (Root        => Root,
                  Destination => Path (Path_Last).Page + Path (Path_Last).Offset,
                  Path        => Path,
                  Path_Last   => Path_Last))
   with
     Ghost,
     Pre => Data.Roots.Used (Root);

   --  Return true if Active flag is set somewhere along Path
   function Active
     (Root        : Types.Root_Range;
      Destination : Types.Tau0_Address_Type;
      Path        : Path_Type;
      Path_Last   : Path_Index)
      return Boolean
   is (Data.Roots.PTP_Active (Root) or else
       (for some I in Path'First .. Path_Last - 1 =>
        Types.Page_Table.To_Entry
         (Paging => Data.Roots.Paging (Root),
          Level  => Data.Roots.Level (Root) - I,
          Word   => Get_Memory_Model
            (To_Range (Path (I).Page + Path (I).Offset))).Active))
   with
     Ghost,
     Pre => Data.Roots.Used (Root) and then
            Reachable (Root, Destination, Path, Path_Last);

   --  Same as above, but with implicit destination
   function Active
     (Root        : Types.Root_Range;
      Path        : Path_Type;
      Path_Last   : Path_Index)
      return Boolean
   is (Active (Root        => Root,
               Destination => Path (Path_Last).Page + Path (Path_Last).Offset,
               Path        => Path,
               Path_Last   => Path_Last))
   with
     Ghost,
     Pre => Data.Roots.Used (Root) and then
            Reachable (Root, Path, Path_Last);

   --  Auxiliary lemma: 'Active' is defined up to pointwise equality of paths
   procedure Active_Cong
     (Root      : Types.Root_Range;
      Old_Path  : Path_Type;
      New_Path  : Path_Type;
      Path_Last : Path_Index)
   with
     Ghost,
     Pre  => Data.Roots.Used (Root) and then
             Path_Last <= Data.Roots.Level (Root) and then
             Reachable (Root, Old_Path, Path_Last) and then
             Reachable (Root, New_Path, Path_Last) and then
             (for all I in Path_Index'First .. Path_Last =>
              Old_Path (I) = New_Path (I)),
     Post => Active (Root, New_Path, Path_Last) = Active (Root, Old_Path, Path_Last);

   --  Auxiliary lemma: extend Path with new end node
   procedure Reachable_Step
     (Root        : Types.Root_Range;
      Path        : Path_Type;
      Destination : Types.Tau0_Address_Type;
      Path_Last   : Path_Index)
   with
     Ghost,
     Pre => Data.Roots.Used (Root) and then
            Path_Last in 2 .. Data.Roots.Level (Root) and then
            Reachable (Root,
                       Path,
                       Path_Last - 1) and then
            Page_Table_Maps
              (Paging => Data.Roots.Paging (Root),
               Level  => Data.Roots.Level (Root) - Path_Last + 1,
               Parent => Path (Path_Last - 1).Page,
               Offset => Path (Path_Last - 1).Offset,
               Child  => Path (Path_Last).Page) and then
            Destination = Path (Path_Last).Page + Path (Path_Last).Offset,
     Post => Reachable (Root, Destination, Path, Path_Last) and then
             Active (Root, Destination, Path, Path_Last) =
               (Active (Root, Path, Path_Last - 1) or
                Types.Page_Table.To_Entry
                  (Paging => Data.Roots.Paging (Root),
                   Level  => Data.Roots.Level (Root) - (Path_Last - 1),
                   Word   => Get_Memory_Model
                     (To_Range (Path (Path_Last - 1).Page
                                  + Path (Path_Last - 1).Offset))).Active);

   function Correct_Page_Table_Kind
     (Full   : Types.Typization.Full_Typization_Type;
      Paging : Types.Root.Paging_Type)
      return Boolean
   is (case Paging is
       when Types.Root.IA32e => T.Is_IA32e_PT (Full),
       when Types.Root.EPT   => T.Is_EPT (Full),
       when Types.Root.MPT   => T.Is_MPT (Full),
       when Types.Root.VTd   => T.Is_VTd_PT (Full))
   with Ghost;

   --  Specify full typization of page tables/frames
   procedure Paging_Typization_Lemma
     (Root      : Types.Root_Range;
      Address   : Types.Tau0_Address_Type;
      Path      : Path_Type;
      Path_Last : Path_Index)
   with
     Ghost,
     Import,
     Pre  => Data.Roots.Used (Root) and then
             Reachable (Root, Address, Path, Path_Last),
     Post => (if Path_Last < Data.Roots.Level (Root)
              then T.Is_Page_Table (Get_Full_Typization (Address)) and then
                   T.Level (Get_Full_Typization (Address))
                     = Data.Roots.Level (Root) - Path_Last and then
                   Correct_Page_Table_Kind
                     (Full   => Get_Full_Typization (Address),
                      Paging => Data.Roots.Paging (Root))
              else Get_Full_Typization (Address) in T.MR_Page | T.Device_Page);

   --  Specify full typization of devices
   procedure Device_Page_Lemma
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type)
   with
     Ghost,
     Import,
     Pre  => Data.Devices.Used (Device) and then
             Data.Devices.Owns_Page (Device, Address),
     Post => Get_Full_Typization (Address) = T.Device_Page;

   --  TODO: prove lemmas using Isabelle
   --  REVIEW: Should we state them as postconditions in Data.Roots instead?
   procedure IO_Bitmap_Low_Lemma (Address : Types.Physical_Address_Type)
   with
     Ghost,
     Import,
     Post => (if (for some Root in Types.Root_Range =>
                     Roots.Used (Root) and then
                     Roots.Kind (Root) = Types.Root.Subject and then
                     Address = Roots.IO_Bitmap_Low (Root))
              then Get_Full_Typization (Address) = Types.Typization.IO_Bitmap_Low);

   procedure IO_Bitmap_High_Lemma (Address : Types.Physical_Address_Type)
   with
     Ghost,
     Import,
     Post => (if (for some Root in Types.Root_Range =>
                     Roots.Used (Root) and then
                     Roots.Kind (Root) = Types.Root.Subject and then
                     Address = Roots.IO_Bitmap_High (Root))
              then Get_Full_Typization (Address) = Types.Typization.IO_Bitmap_High);

   procedure MSR_Bitmap_Lemma (Address : Types.Physical_Address_Type)
   with
     Ghost,
     Import,
     Post => (if (for some Root in Types.Root_Range =>
                     Roots.Used (Root) and then
                     Roots.Kind (Root) = Types.Root.Subject and then
                     Address = Roots.MSR_Bitmap (Root))
              then Get_Full_Typization (Address) = Types.Typization.MSR_Bitmap);

   procedure VTd_IR_Table_Lemma (Address : Types.Physical_Address_Type)
   with
     Ghost,
     Import,
     Pre  => Interrupt_Remapping.Table_Present and then
             Page.Within (Address, Interrupt_Remapping.Table_Address),
     Post => Get_Full_Typization (Address) = Types.Typization.VTd_IR_Table;

   procedure VTd_Root_Table_Lemma (Address : Types.Physical_Address_Type)
   with
     Ghost,
     Import,
     Pre  => VTd_IOMMU.Root_Table_Present and then
             Page.Within (Address, VTd_IOMMU.Root_Table_Address),
     Post => Get_Full_Typization (Address) = Types.Typization.VTd_Root_Table;

   procedure VTd_Context_Table_Lemma
     (Address : Types.Physical_Address_Type;
      Bus     : Types.Device.PCI_Bus_Range)
   with
     Ghost,
     Import,
     Pre  => VTd_IOMMU.Root_Table_Present and then
             VTd_IOMMU.Model_Root_Table_Entry (Bus).Present and then
             Page.Within (Address,
                          VTd_IOMMU.Model_Root_Table_Entry (Bus).Context_Table_Address),
     Post => Get_Full_Typization (Address) = Types.Typization.VTd_Context_Table;

   procedure Full_Typization_Page
   with
     Ghost,
     Import,
     Post => (for all A in Types.Tau0_Address_Type =>
             (for all B in Page.First (A) .. Page.Last (A) =>
              Get_Full_Typization (A) = Get_Full_Typization (B)));

end Tau0.Data.Memory.Full_Typization;
