--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Root;

package Tau0.Types.Page_Table
is

   --  Abstract PTE type
   type Entry_Type (Maps_Page : Boolean := False) is record
      Level      : Page_Level_Type;
      Present    : Boolean;
      Readable   : Boolean;
      Writable   : Boolean;
      Executable : Boolean;
      Active     : Boolean;
      Address    : Tau0_Address_Type;

      case Maps_Page is
         when True  => Caching : Caching_Type;
         when False => null;
      end case;
   end record
   with Dynamic_Predicate => Aligned_Page (Entry_Type.Address) and
                             (if Entry_Type.Level = 1 then Entry_Type.Maps_Page);

   Null_Entry : constant Entry_Type;

   Bytes_PTE        : constant := 8;
   Entries_Per_Page : constant := Config.Page_Size / Bytes_PTE;

   function From_Entry
     (Paging : Root.Paging_Type;
      Entr   : Entry_Type)
      return Types.Word64;

   function To_Entry
     (Paging : Root.Paging_Type;
      Word   : Word64;
      Level  : Page_Level_Type)
      return Entry_Type
   with Post => To_Entry'Result.Level = Level;

private

   Null_Entry : constant Entry_Type := Entry_Type'
     (Maps_Page  => False,
      Level      => 4,
      Present    => False,
      Readable   => False,
      Writable   => False,
      Executable => False,
      Active     => False,
      Address    => 0);

end Tau0.Types.Page_Table;
