--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;
with Tau0.Types;

package Tau0.Files
is

   use type Types.Word64;

   --  Read 4KB from file Filename at Offset into the array Contents of Word64s.
   procedure Read_Page
     (Filename :     String;
      Offset   :     Types.Word64;
      Contents : out Types.Page_Array)
   with Pre => Offset mod Config.Page_Size = 0;

end Tau0.Files;
