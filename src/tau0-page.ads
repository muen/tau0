--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;
with Tau0.Types;

package Tau0.Page
with Ghost
is

   use Types;

   -----------------------------------------------------------------------------

   function First
     (Address : Tau0_Address_Type)
      return Tau0_Address_Type
   is ((Address / Config.Page_Size) * Config.Page_Size);
   pragma Annotate (GNATprove, Inline_For_Proof, First);

   -----------------------------------------------------------------------------

   function Last
     (Address : Tau0_Address_Type)
      return Tau0_Address_Type
   is (First (Address) + Config.Page_Size - 1);
   pragma Annotate (GNATprove, Inline_For_Proof, Last);

   -----------------------------------------------------------------------------

   function Within
     (Address : Tau0_Address_Type; Page : Tau0_Address_Type)
      return Boolean
   is (Address in First (Page) .. Last (Page));
   pragma Annotate (GNATprove, Inline_For_Proof, Within);

   -----------------------------------------------------------------------------

   function Within
     (Address : Tau0_Address_Type;
      Base    : Tau0_Address_Type;
      Length  : Tau0_Page_Count_Type)
      return Boolean
   is (Address in Base .. Base + From_Page_Count (Length) - 1);
   pragma Annotate (GNATprove, Inline_For_Proof, Within);

end Tau0.Page;
