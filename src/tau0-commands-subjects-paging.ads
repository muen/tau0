--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory;
with Tau0.Data.Roots;

package Tau0.Commands.Subjects.Paging
is

   procedure Create_Page_Table
     (Page            :     Types.Physical_Address_Type;
      Subject         :     Types.Root_Range;
      Level           :     Types.Page_Level_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Readable        :     Boolean;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Roots.State)),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Activate_Page_Table
     (Subject         :     Types.Root_Range;
      Level           :     Types.Page_Level_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Map_Device_Page
     (Subject         :     Types.Root_Range;
      Device          :     Types.Device.Device_Range;
      Page            :     Types.Physical_Address_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Tau0.State,
                             Data.Devices.State),
                In_Out   => (Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Map_Page
     (Subject         :     Types.Root_Range;
      Virtual_Address :     Types.Virtual_Address_Type;
      Table_Index     :     Types.Memory_Region_Table_Range;
      Offset          :     Types.Page_Count_Type;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Roots.State,
                             Data.Memory_Region_Table.State)),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant and
               Data.Memory_Region_Table.Invariant;

end Tau0.Commands.Subjects.Paging;
