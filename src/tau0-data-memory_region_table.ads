--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Processors;
with Tau0.Data.Roots;
with Tau0.Types.Memory_Region_Table;
with Tau0.Types.Root;

limited with Tau0.Data.Memory.VTd_IOMMU;

package Tau0.Data.Memory_Region_Table
with Abstract_State => State
is

   use type Types.Page_Count_Type;
   use type Types.Root.Counter_Type;
   use type Types.Root.Kind_Type;
   use type Types.Root.Root_Array;
   use type Types.Root.Root_Type;
   use type Types.Root.State_Type;

   --  Ensures that for every entry of the table, the Region field indeed points
   --  to a memory region, which must moreover be active.  The invariant is
   --  abstracted over the state of Data.Roots for proof purposes
   function Invariant (Roots : Types.Root.Root_Array) return Boolean
   with
     Ghost,
     Global => (Input => State);

   function Invariant return Boolean
   is (Invariant (Data.Roots.Get_Roots))
   with
     Ghost,
     Global => (Input => (State, Roots.State));

   function Used (Index : Types.Memory_Region_Table_Range) return Boolean
   with Global => State;

   function Length
     (Index : Types.Memory_Region_Table_Range)
      return Types.Page_Count_Type
   with
     Global => State,
     Pre    => Used (Index);

   function Region (Index : Types.Memory_Region_Table_Range) return Types.Root_Range
   with
     Global => (Proof_In => Roots.State,
                Input    => State),
     Pre    => Invariant and Used (Index),
     Post   => Data.Roots.State (Region'Result) = Types.Root.Active and then
               Data.Roots.Kind (Region'Result) = Types.Root.Memory_Region;

   function Owner (Index : Types.Memory_Region_Table_Range) return Types.Root_Range
   with
     Global => State,
     Pre    => Used (Index);

   function Mapped_Pages
     (Index : Types.Memory_Region_Table_Range)
      return Types.Page_Count_Type
   with
     Global => State,
     Pre    => Used (Index);

   function Offset
     (Index : Types.Memory_Region_Table_Range)
      return Types.Page_Count_Type
   with
     Global => State,
     Pre    => Used (Index);

   function Has_Base_Address
     (Index : Types.Memory_Region_Table_Range)
      return Boolean
   with
     Global => State,
     Pre    => Used (Index);

   function Shifted_Pages
     (Index : Types.Memory_Region_Table_Range)
      return Types.Page_Count_Type
   with
     Global => State,
     Pre    => Used (Index) and then Has_Base_Address (Index);

   function Writable
     (Index : Types.Memory_Region_Table_Range)
      return Boolean
   with
     Global => State,
     Pre    => Used (Index);

   function Executable
     (Index : Types.Memory_Region_Table_Range)
      return Boolean
   with
     Global => State,
     Pre    => Used (Index);

   procedure Add_Region
     (Index            : Types.Memory_Region_Table_Range;
      Owner            : Types.Root_Range;
      Region           : Types.Root_Range;
      Use_Base_Address : Boolean;
      Base_Address     : Types.Virtual_Address_Type := 0;
      Offset           : Types.Page_Count_Type;
      Length           : Types.Page_Count_Type;
      Writable         : Boolean;
      Executable       : Boolean)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                In_Out   => (State, Data.Roots.State)),
     Pre    => Invariant and then
               Data.Roots.Invariant and then
               Tau0_State = Running and then
               Data.Roots.State (Owner) = Types.Root.Setup and then
               Data.Roots.State (Region) = Types.Root.Active and then
               Data.Roots.Kind (Owner) /= Types.Root.Memory_Region and then
               Data.Roots.Kind (Region) = Types.Root.Memory_Region and then
               not Used (Index) and then
               Types.Aligned_Page (Base_Address) and then
               Data.Roots.Attached_Memory_Regions (Owner) < Natural'Last and then
               Data.Roots.Reference_Count (Region)
                 < Types.Root.Counter_Type'Last and then
               Offset + Length <= Roots.Length (Region),
     Post   => Invariant and then
               Data.Roots.Invariant and then
               Data.Roots.Get_Roots = (Data.Roots.Get_Roots'Old with delta
                 Owner  => (Data.Roots.Get_Roots'Old (Owner) with delta
                   Attached_Tables =>
                       Data.Roots.Get_Roots'Old (Owner).Attached_Tables + 1),
                  Region => (Data.Roots.Get_Roots'Old (Region) with delta
                    Ref_Counter =>
                       Data.Roots.Get_Roots'Old (Region).Ref_Counter + 1)) and then
               Used (Index);

   procedure Add_Page
     (Index   : Types.Memory_Region_Table_Range;
      Offset  : Types.Page_Count_Type;
      Address : Types.Virtual_Address_Type) --  Address MR_Page is mapped to
   with
     Global => (Proof_In => (Tau0.State, Roots.State),
                In_Out   => State),
     Pre    => Invariant and then
               Types.Aligned_Page (Address) and then
               Tau0_State = Running and then
               Used (Index) and then
               Mapped_Pages (Index) < Length (Index) and then
               Offset in Memory_Region_Table.Offset (Index)
                      .. Memory_Region_Table.Offset (Index) + Length (Index) - 1,
     Post   => Invariant and
               Used (Index) and
               Mapped_Pages (Index) = Mapped_Pages (Index)'Old + 1;

   --  If active memory regions are not changed, the MRT invariant is preserved.
   procedure Lemma_Invariant (Old_Roots, New_Roots : Types.Root.Root_Array)
   with
     Ghost,
     Global => (Proof_In => State),
     Pre    => Invariant (Old_Roots) and
               (for all R in Old_Roots'Range =>
               (if Old_Roots (R).State = Types.Root.Active and then
                   Old_Roots (R).Kind  = Types.Root.Memory_Region
                then Old_Roots (R) = New_Roots (R))),
     Post   => Invariant (New_Roots);

private

   Table : Types.Memory_Region_Table.Memory_Region_Table_Type
     := (others => Types.Memory_Region_Table.Null_Cell)
   with Part_Of => State;

   -----------------------------------------------------------------------------

   function Invariant (Roots : Types.Root.Root_Array) return Boolean
   is (for all Index in Types.Memory_Region_Table_Range =>
      (if Used (Index)
       then Roots (Table (Index).Region).State = Types.Root.Active and then
            Roots (Table (Index).Region).Kind = Types.Root.Memory_Region));

end Tau0.Data.Memory_Region_Table;
