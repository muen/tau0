--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands.Setup;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Setup
is

   -----------------------------------------------------------------------------

   procedure Activate_Tau0
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
      pragma Unreferenced (Command);
   begin
      Commands.Setup.Activate_Tau0 (Status => Status);
   end Activate_Tau0;

   -----------------------------------------------------------------------------

   procedure Add_Memory_Block
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Add_Memory_Block_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Setup.Add_Memory_Block
           (Address => Command.Add_Memory_Block_Params.Address,
            Size    => Command.Add_Memory_Block_Params.Size,
            Status  => Status);
      end if;
   end Add_Memory_Block;

   -----------------------------------------------------------------------------

   procedure Add_Ioapic
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Add_Ioapic_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Setup.Add_Ioapic
           (Source_ID => Command.Add_Ioapic_Params.Source_ID,
            Status    => Status);
      end if;
   end Add_Ioapic;

   -----------------------------------------------------------------------------

   procedure Add_Processor
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Add_Processor_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Setup.Add_Processor
           (ID      => Command.Add_Processor_Params.ID,
            APIC_ID => Command.Add_Processor_Params.APIC_ID,
            Status  => Status);
      end if;
   end Add_Processor;

   -----------------------------------------------------------------------------

   procedure Create_VTd_Context_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_VTd_Context_Table_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Setup.Create_VTd_Context_Table
           (Page   => Command.Create_VTd_Context_Table_Params.Page,
            Bus    => Command.Create_VTd_Context_Table_Params.Bus,
            Status => Status);
      end if;
   end Create_VTd_Context_Table;

   -----------------------------------------------------------------------------

   procedure Create_VTd_IRQ_Remap_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_VTd_IRQ_Remap_Table_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Setup.Create_VTd_IRQ_Remap_Table
           (Page   => Command.Create_VTd_IRQ_Remap_Table_Params.Page,
            Status => Status);
      end if;
   end Create_VTd_IRQ_Remap_Table;

   -----------------------------------------------------------------------------

   procedure Create_VTd_Root_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_VTd_Root_Table_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Setup.Create_VTd_Root_Table
           (Page   => Command.Create_VTd_Root_Table_Params.Page,
            Status => Status);

         --  FIXME: prove
         pragma Assert (Data.Roots.Invariant);
      end if;
   end Create_VTd_Root_Table;

end Tau0.Command_Processor.Setup;
