--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Roots.Writers.Kernels
is

   -----------------------------------------------------------------------------

   procedure Lemma_CPUs
     (Old_Roots : Types.Root.Root_Array;
      New_Roots : Types.Root.Root_Array;
      Old_CPUs  : CPU_Array;
      New_CPUs  : CPU_Array;
      Root      : Types.Root_Range;
      CPU       : Types.CPU_Range)
   with
     Ghost,
     Global => null,
     Pre    => (for all R in Types.Root_Range =>
                (if R /= Root
                 then Old_Roots (R) = New_Roots (R)
                 else Old_Roots (R).State = Types.Root.Unused and then
                      New_Roots (R).State = Types.Root.Setup and then
                      New_Roots (R).Kind = Types.Root.Kernel and then
                      New_Roots (R).Kernel_CPU = CPU) and
                (if Old_Roots (R).State /= Types.Root.Unused and
                    Old_Roots (R).Kind = Types.Root.Kernel
                 then Old_Roots (R).Kernel_CPU /= CPU)) and
               not Old_CPUs (CPU) and
               New_CPUs = (Old_CPUs with delta CPU => True) and
               Invariant_CPUs_Correct (Old_Roots, Old_CPUs),
     Post   => Invariant_CPUs_Correct (New_Roots, New_CPUs);

   -----------------------------------------------------------------------------

   procedure Activate (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State                   => Types.Root.Active,
         Is_Memory_Region        => False,
         Kind                    => Types.Root.Kernel,
         PTP                     => Roots (Root).PTP,
         PTP_Present             => Roots (Root).PTP_Present,
         PTP_Active              => Roots (Root).PTP_Active,
         Attached_Tables         => Roots (Root).Attached_Tables,
         Kernel_CPU              => Roots (Root).Kernel_CPU,
         Kernel_Assigned_Devices => Roots (Root).Kernel_Assigned_Devices);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Activate;

   -----------------------------------------------------------------------------

   procedure Create_Kernel (Root : Types.Root_Range; CPU : Types.CPU_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
      Old_CPUs  : constant CPU_Array             := CPUs  with Ghost;
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State                   => Types.Root.Setup,
         Is_Memory_Region        => False,
         Kind                    => Types.Root.Kernel,
         PTP_Present             => False,
         PTP_Active              => False,
         PTP                     => 0,
         Attached_Tables         => 0,
         Kernel_CPU              => CPU,
         Kernel_Assigned_Devices => Types.Root.Kernel_Device_Table.Empty_Table);

      CPUs (CPU) := True;

      Lemma_CPUs (Old_Roots => Old_Roots,
                  New_Roots => Roots,
                  Old_CPUs  => Old_CPUs,
                  New_CPUs  => CPUs,
                  Root      => Root,
                  CPU       => CPU);

      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Create_Kernel;

   -----------------------------------------------------------------------------

   procedure Lemma_CPUs
     (Old_Roots : Types.Root.Root_Array;
      New_Roots : Types.Root.Root_Array;
      Old_CPUs  : CPU_Array;
      New_CPUs  : CPU_Array;
      Root      : Types.Root_Range;
      CPU       : Types.CPU_Range)
   is null;

end Tau0.Data.Roots.Writers.Kernels;
