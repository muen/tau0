--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Types.Page_Table.EPT
is

   Readable_Mask   : constant Word64 := 2 ** 0;
   Writable_Mask   : constant Word64 := 2 ** 1;
   Executable_Mask : constant Word64 := 2 ** 2;
   Ignore_PAT_Mask : constant Word64 := 2 ** 6;
   Active_Mask     : constant Word64 := 2 ** 52;
   Caching_Mask    : constant Word64 := 2 ** 6 - 2 ** 3;
   Page_Mask       : constant Word64 := Config.Page_Size - 1;
   Address_Mask    : constant Word64
     := (2 ** Config.Max_Phys_Address - 1) and not Page_Mask;

   function From_Caching (Caching : Caching_Type) return Word64;
   function To_Caching (W : Word64) return Caching_Type;

   -----------------------------------------------------------------------------

   function From_Caching (Caching : Caching_Type) return Word64
   is (2 ** 3 * (case Caching is
                   when UC => 0,
                   when WC => 1,
                   when WT => 4,
                   when WP => 5,
                   when WB => 6));

   -----------------------------------------------------------------------------

   function From_Entry (Entr : Entry_Type) return Word64
   is (Inject (Entr.Readable) * Readable_Mask or
       Inject (Entr.Writable) * Writable_Mask or
       Inject (Entr.Executable) * Executable_Mask or
       Inject (Entr.Active) * Active_Mask or
       (if Entr.Maps_Page then Ignore_PAT_Mask or From_Caching (Entr.Caching)
                          else 0) or
       Word64 (Entr.Address));

   -----------------------------------------------------------------------------

   function To_Caching (W : Word64) return Caching_Type
   is (case (W and Caching_Mask) / 2 ** 3 is
         when 0      => UC,
         when 1      => WC,
         when 4      => WT,
         when 5      => WP,
         when 6      => WB,
         when others => WB);  --  REVIEW: Should we catch this condition?

   -----------------------------------------------------------------------------

   pragma Warnings (Off, "formal parameter ""Level"" is not referenced",
                    Reason => "Function is parameter to generic package");

   function To_Entry (Word : Word64; Level : Page_Level_Type) return Entry_Type
   is
      PTE_Address : constant Physical_Address_Type
        := Physical_Address_Type (Word and Address_Mask);

      Readable   : constant Boolean      := (Word and Readable_Mask) /= 0;
      Writable   : constant Boolean      := (Word and Writable_Mask) /= 0;
      Executable : constant Boolean      := (Word and Executable_Mask) /= 0;
      Active     : constant Boolean      := (Word and Active_Mask) /= 0;
      Caching    : constant Caching_Type := To_Caching (Word);
   begin
      pragma Assert ((Word and Address_Mask) mod 4096 = 0);
      pragma Assert (Aligned_Page (PTE_Address));

      if Level = 1 then
         return Entry_Type'
           (Maps_Page  => True,
            Level      => Level,
            Present    => Readable or Writable or Executable,
            Readable   => Readable,
            Writable   => Writable,
            Executable => Executable,
            Active     => Active,
            Caching    => Caching,
            Address    => PTE_Address);
      else
         return Entry_Type'
           (Maps_Page  => False,
            Level      => Level,
            Present    => Readable or Writable or Executable,
            Readable   => Readable,
            Writable   => Writable,
            Executable => Executable,
            Active     => Active,
            Address    => PTE_Address);
      end if;
   end To_Entry;

   pragma Warnings (On, "formal parameter ""Level"" is not referenced");

end Tau0.Types.Page_Table.EPT;
