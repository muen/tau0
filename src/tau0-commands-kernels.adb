--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Roots.Writers.Kernels;
with Tau0.Types.Root;

package body Tau0.Commands.Kernels
is

   package R renames Data.Roots;

   use type Types.Root.Kind_Type;
   use type Types.Root.State_Type;

   -----------------------------------------------------------------------------

   procedure Activate_Kernel
     (Kernel :     Types.Root_Range;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Locked then
         Status := Types.Command.Kernel_Not_Locked;
      --  FIXME: check if we can prove this holds
      elsif not R.PTP_Present (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Not_Present;
      elsif not R.PTP_Active (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Inactive;
      else
         R.Writers.Activate (Root => Kernel);
         Status := Types.Command.Success;
      end if;
   end Activate_Kernel;

   -----------------------------------------------------------------------------

   procedure Assign_Device
     (Kernel :     Types.Root_Range;
      Device :     Types.Device.Device_Range;
      Status : out Types.Command.Status_Type)
   is
      use type Types.Device.State_Type;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Setup then
         Status := Types.Command.Kernel_Not_Setup;
      elsif R.Device_Table_Full (Root => Kernel) then
         Status := Types.Command.Kernel_Device_Table_Full;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Active then
         Status := Types.Command.Device_Inactive;
      elsif not R.Device_Assignable (Root => Kernel, Device => Device) then
         Status := Types.Command.Kernel_Device_Unassignable;
      else
         Data.Roots.Writers.Assign_Device (Root => Kernel, Device => Device);
         Status := Types.Command.Success;
      end if;
   end Assign_Device;

   -----------------------------------------------------------------------------

   procedure Attach_Memory_Region
     (Kernel           :     Types.Root_Range;
      Region           :     Types.Root_Range;
      Use_Base_Address :     Boolean;
      Base_Address     :     Types.Virtual_Address_Type := 0;
      Offset           :     Types.Page_Count_Type;
      Length           :     Types.Page_Count_Type;
      Index            :     Types.Memory_Region_Table_Range;
      Writable         :     Boolean;
      Executable       :     Boolean;
      Status           : out Types.Command.Status_Type)
   is
      package MRT renames Data.Memory_Region_Table;

      use type Types.Root.Counter_Type;
      use type Types.Page_Count_Type;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif
        R.Kind (Root => Kernel) /= Types.Root.Kernel or
        R.Kind (Root => Region) /= Types.Root.Memory_Region
      then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Setup then
         Status := Types.Command.Kernel_Not_Setup;
      elsif R.State (Region) /= Types.Root.Active then
         Status := Types.Command.Memory_Region_Inactive;
      elsif MRT.Used (Index => Index) then
         Status := Types.Command.MR_Table_Index_Invalid;
      elsif not Types.Aligned_Page (Base_Address) then
         Status := Types.Command.Address_Invalid;
      elsif R.Reference_Count (Region) = Types.Root.Counter_Type'Last then
         Status := Types.Command.Memory_Region_Counter_Full;
      elsif R.Attached_Memory_Regions (Kernel) = Natural'Last then
         Status := Types.Command.Attached_Region_Counter_Full;
      elsif Offset + Length > R.Length (Region) then
         Status := Types.Command.Memory_Region_Out_Of_Bounds;
      else
         MRT.Add_Region
           (Owner            => Kernel,
            Region           => Region,
            Use_Base_Address => Use_Base_Address,
            Base_Address     => Base_Address,
            Offset           => Offset,
            Length           => Length,
            Index            => Index,
            Writable         => Writable,
            Executable       => Executable);

         Status := Types.Command.Success;
      end if;
   end Attach_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Create_Kernel
     (Kernel :     Types.Root_Range;
      CPU    :     Types.CPU_Range;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif not Data.Processors.Present (ID => CPU) then
         Status := Types.Command.CPU_Invalid;
      elsif R.Used (Root => Kernel) then
         Status := Types.Command.Kernel_Invalid;
      elsif R.Kernel_Present (CPU => CPU) then
         Status := Types.Command.Kernel_CPU_Used;
      else
         R.Writers.Kernels.Create_Kernel (Root => Kernel, CPU => CPU);

         Status := Types.Command.Success;
      end if;
   end Create_Kernel;

   -----------------------------------------------------------------------------

   procedure Lock_Kernel
     (Kernel :     Types.Root_Range;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Kernel) /= Types.Root.Kernel then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Kernel) /= Types.Root.Setup then
         Status := Types.Command.Kernel_Not_Setup;
      elsif not R.PTP_Present (Root => Kernel) then
         Status := Types.Command.Kernel_PTP_Not_Present;
      else
         R.Writers.Lock (Root => Kernel);
         Status := Types.Command.Success;
      end if;
   end Lock_Kernel;

end Tau0.Commands.Kernels;
