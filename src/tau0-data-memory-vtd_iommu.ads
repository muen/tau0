--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Device;
with Tau0.Types.VTd_Tables;

package Tau0.Data.Memory.VTd_IOMMU
with
  Abstract_State => State,
  Initializes    => State
is

   use type Types.VTd_Tables.Context_Entry_Type;
   use type Types.VTd_Tables.Root_Entry_Type;
   use type Types.VTd_Tables.Root_Table_Model_Type;

   -----------------------------------------------------------------------------

   function Root_Table_Model return Types.VTd_Tables.Root_Table_Model_Type
   with
     Ghost,
     Post => (for all Bus in Types.Device.PCI_Bus_Range =>
              Root_Table_Model'Result (Bus) = Model_Root_Table_Entry (Bus));

   function Root_Table_Model_Using
     (Model : Memory_Model_Type)
      return Types.VTd_Tables.Root_Table_Model_Type
   with
     Ghost,
     Post => (for all Bus in Types.Device.PCI_Bus_Range =>
              Root_Table_Model_Using'Result (Bus)
                = Model_Root_Table_Entry_Using (Model => Model, Bus => Bus));

   function Model_Root_Table_Entry
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.VTd_Tables.Root_Entry_Type
   with
     Ghost,
     Post => Model_Root_Table_Entry'Result = Model_Root_Table_Entry_Using
               (Bus   => Bus,
                Model => Get_Memory_Model);
   pragma Annotate (GNATprove, Inline_For_Proof, Model_Root_Table_Entry);

   function Model_Root_Table_Entry_Using
     (Bus   : Types.Device.PCI_Bus_Range;
      Model : Memory_Model_Type)
     return Types.VTd_Tables.Root_Entry_Type
   with Ghost;
   pragma Annotate (GNATprove, Inline_For_Proof, Model_Root_Table_Entry_Using);

   procedure Root_Table_Entry_Lemma
     (Old_Model : Memory_Model_Type;
      New_Model : Memory_Model_Type)
   with
     Ghost,
     Pre  => Root_Table_Present and then
             (for all Address in Types.Tau0_Address_Type =>
             (if Page.Within (Address, Root_Table_Address)
              then New_Model (To_Range (Address)) = Old_Model (To_Range (Address)))),
     Post => Root_Table_Model_Using (Old_Model) = Root_Table_Model_Using (New_Model);

   -----------------------------------------------------------------------------

   function Root_Table_Present return Boolean
   with Global => (Input => State);

   function Root_Table_Address return Types.Physical_Address_Type
   with
     Global => (Input => State),
     Pre    => Root_Table_Present,
     Post   => Types.Aligned_Page (Root_Table_Address'Result);

   function Context_Table_Present
     (Bus : Types.Device.PCI_Bus_Range)
      return Boolean
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => (State, Data.Memory.Memory_State)),
     Pre    => Root_Table_Present,
     Post   => Context_Table_Present'Result = Model_Root_Table_Entry (Bus).Present;

   function Context_Table_Address
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.Physical_Address_Type
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => (State, Data.Memory.Memory_State)),
     Pre    => Root_Table_Present and then
               Context_Table_Present (Bus),
     Post   => Types.Aligned_Page (Context_Table_Address'Result);

   function Context_Table_Entry_Used
     (Bus  : Types.Device.PCI_Bus_Range;
      Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range)
      return Boolean
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => (State, Data.Memory.Memory_State)),
     Pre    => Root_Table_Present and then
               Context_Table_Present (Bus);

   -----------------------------------------------------------------------------

private

   use type Types.Device.PCI_Bus_Range;
   use Types.VTd_Tables;

   -----------------------------------------------------------------------------

   subtype Table_Address_Type is Types.Physical_Address_Type
   with Dynamic_Predicate => Types.Aligned_Page (Table_Address_Type);

   Root_Entry_Size    : constant := 16;  --  size of entry of root table (bytes)
   Context_Entry_Size : constant := 16;

   Root_Table_Present_Flag : Boolean            := False with Part_Of => State;
   Root_Table_Pointer      : Table_Address_Type := 0     with Part_Of => State;

   -----------------------------------------------------------------------------

   function Root_Table_Offset
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.Physical_Address_Type
   with
     Post => Root_Table_Offset'Result in 0 .. 4095 and
             (Root_Table_Offset'Result mod 16 = 0);

   function Context_Table_Offset
     (Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range)
      return Types.Physical_Address_Type;

   function Get_Root_Table_Entry
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.VTd_Tables.Root_Entry_Type
   with
     --  FIXME: GNATprove wants us to mention Root_Table_Present_Flag.  If we do
     --  so, we must omit State, and then GNATprove complains that we should
     --  mention State.
     Global => (Proof_In => (Data.Memory.Typization_State),
                Input    => (State,
                             Data.Memory.Memory_State)),
     Pre  => Root_Table_Present,
     Post => Get_Root_Table_Entry'Result = Types.VTd_Tables.From_Word128
       (Types.Word128'(0 => Get_Memory_Model
                   (To_Range (Root_Table_Pointer + Root_Table_Offset (Bus))),
                 1 => Get_Memory_Model
                   (To_Range (Root_Table_Pointer + Root_Table_Offset (Bus) + 8))));

   function Get_Context_Table_Entry
     (Bus  : Types.Device.PCI_Bus_Range;
      Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range)
      return Types.VTd_Tables.Context_Entry_Type
   with
     --  FIXME: GNATprove wants us to mention Root_Table_Present_Flag.  If we do
     --  so, we must omit State, and then GNATprove complains that we should
     --  mention State.
     Global => (Proof_In => (Data.Memory.Typization_State),
                Input    => (State,
                             Data.Memory.Memory_State)),
     Pre  => Root_Table_Present and then Context_Table_Present (Bus),
     Post => Get_Context_Table_Entry'Result = Types.VTd_Tables.From_Word128
       (Types.Word128'(0 => Get_Memory_Model
                   (To_Range (Context_Table_Address (Bus)
                                + Context_Table_Offset (Dev, Func))),
                 1 => Get_Memory_Model
                   (To_Range (Context_Table_Address (Bus)
                                + Context_Table_Offset (Dev, Func) + 8))));

   -----------------------------------------------------------------------------
   --  Implementations
   -----------------------------------------------------------------------------

   function Context_Table_Address
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.Physical_Address_Type
   is (Get_Root_Table_Entry (Bus => Bus).Context_Table_Address);

   -----------------------------------------------------------------------------

   function Context_Table_Entry_Used
     (Bus  : Types.Device.PCI_Bus_Range;
      Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range)
      return Boolean
   is (Get_Context_Table_Entry (Bus => Bus, Dev => Dev, Func => Func).Present);

   -----------------------------------------------------------------------------

   function Context_Table_Offset
     (Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range)
      return Types.Physical_Address_Type
   is ((Types.Physical_Address_Type (Dev) * 8 + Types.Physical_Address_Type (Func))
       * Context_Entry_Size);

   -----------------------------------------------------------------------------

   function Context_Table_Present
     (Bus : Types.Device.PCI_Bus_Range)
      return Boolean
   is (Get_Root_Table_Entry (Bus => Bus).Present);

   -----------------------------------------------------------------------------

   function Model_Root_Table_Entry
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.VTd_Tables.Root_Entry_Type
   is (Model_Root_Table_Entry_Using (Bus => Bus, Model => Get_Memory_Model));

   -----------------------------------------------------------------------------

   function Model_Root_Table_Entry_Using
     (Bus   : Types.Device.PCI_Bus_Range;
      Model : Memory_Model_Type)
     return Types.VTd_Tables.Root_Entry_Type
   is (Types.VTd_Tables.Root_Entry_Type'
         (Types.VTd_Tables.From_Word128
            (Types.Word128'
               (0 => Model (To_Range (Root_Table_Pointer
                                        + Root_Table_Offset (Bus))),
                1 => Model (To_Range (Root_Table_Pointer
                                        + Root_Table_Offset (Bus) + 8))))));

   -----------------------------------------------------------------------------

   function Root_Table_Offset
     (Bus : Types.Device.PCI_Bus_Range)
      return Types.Physical_Address_Type
   is (Types.Physical_Address_Type (Bus) * Root_Entry_Size);

   -----------------------------------------------------------------------------

   function Root_Table_Address return Types.Physical_Address_Type
   is (Root_Table_Pointer);

   -----------------------------------------------------------------------------

   function Root_Table_Present return Boolean
   is (Root_Table_Present_Flag);

end Tau0.Data.Memory.VTd_IOMMU;
