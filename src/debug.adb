--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;                       --  FIXME: dynamic case

with Tau0.Command_Line;
pragma Elaborate (Tau0.Command_Line);

package body Debug
is

   Current_Level : constant Level_Type := Tau0.Command_Line.Get_Log_Level;

   -----------------------------------------------------------------------------

   function Get_Current_Level return Level_Type
   is (Current_Level);

   -----------------------------------------------------------------------------

   pragma Warnings (Off, "condition is always *",
                    Reason => "Compile-time parameter");
   pragma Warnings (Off, "condition can only be * if invalid values present",
                    Reason => "Compile-time parameter");

   procedure New_Line (Level : Level_Type := Info)
   is
   begin
      Put (ASCII.LF & "", Level => Level);
   end New_Line;

   -----------------------------------------------------------------------------

   function Is_Whitespace (C : Character) return Boolean
   is (C in ' ' | ASCII.HT);

   -----------------------------------------------------------------------------

   procedure Put (Item : String; Level : Level_Type := Info)
   with SPARK_Mode => Off
   is
   begin
      if Level >= Current_Level then
         Ada.Text_IO.Put (Item);
      end if;
   end Put;

   -----------------------------------------------------------------------------

   procedure Put
     (Word      : Tau0.Types.Word64;
      Base      : Positive   := 16;
      Level     : Level_Type := Info;
      Justified : Boolean    := True)
   with SPARK_Mode => Off
   is
      package Word_IO is new Ada.Text_IO.Modular_IO
        (Num => Tau0.Types.Word64);
   begin
      if Level >= Current_Level then
         Word_IO.Put (Item  => Word,
                      Base  => Base,
                      Width => (if Justified then Tau0.Types.Word64'Width else 1));
      end if;
   end Put;

   -----------------------------------------------------------------------------

   procedure Put
     (Address : Tau0.Types.Tau0_Address_Type;
      Level   : Level_Type := Info)
   with SPARK_Mode => Off
   is
      package Address_IO is new Ada.Text_IO.Integer_IO
        (Num => Tau0.Types.Tau0_Address_Type);
   begin
      if Level >= Current_Level then
         Address_IO.Put (Item => Address, Base => 16);
      end if;
   end Put;

   -----------------------------------------------------------------------------

   procedure Put
     (Address : Tau0.Types.Virtual_Address_Type;
      Level   : Level_Type := Info)
   with SPARK_Mode => Off
   is
      package Address_IO is new Ada.Text_IO.Integer_IO
        (Num => Tau0.Types.Virtual_Address_Type);
   begin
      if Level >= Current_Level then
         Address_IO.Put (Item => Address, Base => 16);
      end if;
   end Put;

   -----------------------------------------------------------------------------

   procedure Put_Line (Item : String; Level : Level_Type := Info)
   is
   begin
      Put (Item => Item, Level => Level);
      New_Line (Level => Level);
   end Put_Line;

   ------------------------------------------------------------------------------

   procedure Put
     (Page_Level : Tau0.Types.Level_Type;
      Level      : Level_Type := Info)
   with SPARK_Mode => Off
   is
      package Page_IO is new Ada.Text_IO.Integer_IO
        (Num => Tau0.Types.Level_Type);
   begin
      if Level >= Current_Level then
         Page_IO.Put (Page_Level);
      end if;
   end Put;

   -----------------------------------------------------------------------------

   procedure Put
     (Root  : Tau0.Types.Root_Range;
      Level : Level_Type := Info)
   with SPARK_Mode => Off
   is
      package Root_IO is new Ada.Text_IO.Integer_IO
        (Num => Tau0.Types.Root_Range);
   begin
      if Level >= Current_Level then
         Root_IO.Put (Item => Root, Width => 1);
      end if;
   end Put;

   -----------------------------------------------------------------------------

   function Trim (Source : String; Side : Side_Type) return String
   is
      I : Integer := Source'First;
      J : Integer := Source'Last;
   begin
      if Side in Left | Both then
         while I in Source'Range and then Is_Whitespace (Source (I)) loop
            pragma Loop_Invariant (for all K in Source'First .. I - 1 =>
                                   Is_Whitespace (Source (K)));
            pragma Loop_Variant (Increases => I);
            I := I + 1;
         end loop;
      end if;

      if Side in Right | Both then
         while J in Source'Range and then Is_Whitespace (Source (J)) loop
            pragma Loop_Invariant (for all K in J + 1 .. Source'Last =>
                                   Is_Whitespace (Source (K)));
            pragma Loop_Variant (Decreases => J);
            J := J - 1;
         end loop;
      end if;

      return Source (I .. J);
   end Trim;

   -----------------------------------------------------------------------------

   pragma Warnings (On, "condition is always *");
   pragma Warnings (On, "condition can only be * if invalid values present");

end Debug;
