--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Parameters;

package Tau0.Types.Command
with Abstract_State => null
is

   package P renames Types.Parameters;

   type Command_ID_Type is new Word16;

   Activate_Tau0                       : constant Command_ID_Type := 16#0000#;
   Clear_Page                          : constant Command_ID_Type := 16#0001#;
   Write_Immediate                     : constant Command_ID_Type := 16#0003#;
   Add_Memory_Block                    : constant Command_ID_Type := 16#0004#;
   Add_Processor                       : constant Command_ID_Type := 16#0006#;
   Write_Image_Commands                : constant Command_ID_Type := 16#0007#;
   Add_Ioapic                          : constant Command_ID_Type := 16#0008#;

   --  Setup global data structures
   Create_VTd_Root_Table               : constant Command_ID_Type := 16#1000#;
   Create_VTd_Context_Table            : constant Command_ID_Type := 16#1001#;
   Create_VTd_IRQ_Remap_Table          : constant Command_ID_Type := 16#1002#;

   --  Memory regions
   Create_Memory_Region                : constant Command_ID_Type := 16#2000#;
   Lock_Memory_Region                  : constant Command_ID_Type := 16#2001#;
   Activate_Memory_Region              : constant Command_ID_Type := 16#2002#;
   Deactivate_Memory_Region            : constant Command_ID_Type := 16#2003#;
   Unlock_Memory_Region                : constant Command_ID_Type := 16#2004#;
   Destroy_Memory_Region               : constant Command_ID_Type := 16#2005#;
   Create_Page_Table_MR                : constant Command_ID_Type := 16#2006#;
   Destroy_Page_Table_MR               : constant Command_ID_Type := 16#2007#;
   Append_Page_MR                      : constant Command_ID_Type := 16#2008#;
   Append_Vacuous_Page_MR              : constant Command_ID_Type := 16#2009#;
   Truncate_Page_MR                    : constant Command_ID_Type := 16#200A#;
   Activate_Page_Table_MR              : constant Command_ID_Type := 16#200B#;
   Activate_Page_MR                    : constant Command_ID_Type := 16#200C#;
   Deactivate_Page_MR                  : constant Command_ID_Type := 16#200D#;
   Deactivate_Page_Table_MR            : constant Command_ID_Type := 16#200E#;

   --  Subjects
   Create_Subject                      : constant Command_ID_Type := 16#4000#;
   Destroy_Subject                     : constant Command_ID_Type := 16#4001#;
   Lock_Subject                        : constant Command_ID_Type := 16#4002#;
   Unlock_Subject                      : constant Command_ID_Type := 16#4003#;
   Activate_Subject                    : constant Command_ID_Type := 16#4004#;
   Deactivate_Subject                  : constant Command_ID_Type := 16#4005#;
   Attach_Memory_Region_Subject        : constant Command_ID_Type := 16#4006#;
   Detach_Memory_Region_Subject        : constant Command_ID_Type := 16#4007#;
   Assign_Device_Subject               : constant Command_ID_Type := 16#4008#;
   Assign_IRQ_Subject                  : constant Command_ID_Type := 16#4009#;
   Set_IO_Port_Range                   : constant Command_ID_Type := 16#400A#;
   Set_MSR_Range                       : constant Command_ID_Type := 16#400B#;
   Create_Page_Table_Subject           : constant Command_ID_Type := 16#400C#;
   Destroy_Page_Table_Subject          : constant Command_ID_Type := 16#400D#;
   Activate_Page_Table_Subject         : constant Command_ID_Type := 16#400E#;
   Deactivate_Page_Table_Subject       : constant Command_ID_Type := 16#400F#;
   Map_Page_Subject                    : constant Command_ID_Type := 16#4010#;
   Unmap_Page_Subject                  : constant Command_ID_Type := 16#4011#;
   Map_Device_Page_Subject             : constant Command_ID_Type := 16#4012#;

   --  Device domains
   Create_Device_Domain                : constant Command_ID_Type := 16#5000#;
   Destroy_Device_Domain               : constant Command_ID_Type := 16#5001#;
   Add_Device_To_Device_Domain         : constant Command_ID_Type := 16#5002#;
   Remove_Device_From_Device_Domain    : constant Command_ID_Type := 16#5003#;
   Lock_Device_Domain                  : constant Command_ID_Type := 16#5004#;
   Activate_Device_Domain              : constant Command_ID_Type := 16#5005#;
   Deactivate_Device_Domain            : constant Command_ID_Type := 16#5006#;
   Attach_Memory_Region_Domain         : constant Command_ID_Type := 16#5007#;
   Detach_Memory_Region_Domain         : constant Command_ID_Type := 16#5008#;
   Create_Page_Table_Device_Domain     : constant Command_ID_Type := 16#5009#;
   Destroy_Page_Table_Device_Domain    : constant Command_ID_Type := 16#500A#;
   Activate_Page_Table_Device_Domain   : constant Command_ID_Type := 16#500B#;
   Deactivate_Page_Table_Device_Domain : constant Command_ID_Type := 16#500C#;
   Map_Page_Device_Domain              : constant Command_ID_Type := 16#500D#;
   Unmap_Page_Device_Domain            : constant Command_ID_Type := 16#500E#;

   --  Kernel
   Create_Kernel                       : constant Command_ID_Type := 16#6000#;
   Lock_Kernel                         : constant Command_ID_Type := 16#6001#;
   Activate_Kernel                     : constant Command_ID_Type := 16#6002#;
   Attach_Memory_Region_Kernel         : constant Command_ID_Type := 16#6003#;
   Detach_Memory_Region_Kernel         : constant Command_ID_Type := 16#6004#;
   Create_Page_Table_Kernel            : constant Command_ID_Type := 16#6005#;
   Activate_Page_Table_Kernel          : constant Command_ID_Type := 16#6006#;
   Map_Page_Kernel                     : constant Command_ID_Type := 16#6007#;
   Map_Device_Page_Kernel              : constant Command_ID_Type := 16#6008#;
   Assign_Device_Kernel                : constant Command_ID_Type := 16#6009#;

   --  Devices (setup phase)
   Create_Legacy_Device                : constant Command_ID_Type := 16#7000#;
   Create_PCI_Device                   : constant Command_ID_Type := 16#7001#;
   Add_IO_Port_Range_Device            : constant Command_ID_Type := 16#7002#;
   Add_IRQ_Device                      : constant Command_ID_Type := 16#7003#;
   Add_Memory_Device                   : constant Command_ID_Type := 16#7004#;
   Activate_Device                     : constant Command_ID_Type := 16#7005#;

   Unused_Command_ID                   : constant Command_ID_Type := 16#FFFF#;

   -----------------------------------------------------------------------------

   type Status_Type is new Word16;

   Success                           : constant Status_Type := 16#0000#;
   Address_Invalid                   : constant Status_Type := 16#0001#;
   Level_Invalid                     : constant Status_Type := 16#0002#;
   VTd_Tables_Invalid                : constant Status_Type := 16#0003#;
   Page_Translation_Error            : constant Status_Type := 16#0004#;
   Typization_Violation              : constant Status_Type := 16#0005#;
   Root_Object_Kind_Invalid          : constant Status_Type := 16#0006#;
   Invalid_Parameters                : constant Status_Type := 16#0007#;
   Immediate_Command_Expected        : constant Status_Type := 16#0008#;
   Immediate_Mode_Violation          : constant Status_Type := 16#0009#;
   Attached_Region_Counter_Full      : constant Status_Type := 16#000A#;
   VTd_Root_Table_Present            : constant Status_Type := 16#000B#;
   VTd_Root_Table_Not_Present        : constant Status_Type := 16#000C#;
   VTd_Context_Table_Present         : constant Status_Type := 16#000D#;
   VTd_Context_Table_Not_Present     : constant Status_Type := 16#000E#;
   VTd_IRQ_Remap_Table_Present       : constant Status_Type := 16#000F#;
   VTd_IRQ_Remap_Table_Not_Present   : constant Status_Type := 16#0010#;
   Device_Memory_Invalid             : constant Status_Type := 16#0011#;
   Tau0_Mode_Invalid                 : constant Status_Type := 16#0012#;
   Reserved_Memory_Set_Full          : constant Status_Type := 16#0013#;
   Setup_Phase_Incomplete            : constant Status_Type := 16#0014#;
   CPU_Invalid                       : constant Status_Type := 16#0015#;
   Memory_Blocks_Full                : constant Status_Type := 16#0016#;
   Some_Root_Inactive                : constant Status_Type := 16#0017#;
   Processor_Present                 : constant Status_Type := 16#0018#;
   APIC_ID_Used                      : constant Status_Type := 16#0019#;
   IOAPIC_ID_Not_Set                 : constant Status_Type := 16#001A#;
   Content_Regions_Full              : constant Status_Type := 16#001B#;

   MR_Table_Index_Invalid            : constant Status_Type := 16#1000#;
   MR_Table_Mapped_Pages_Error       : constant Status_Type := 16#1001#;

   Subject_Not_Locked                : constant Status_Type := 16#2000#;
   Subject_Not_Unlocked              : constant Status_Type := 16#2001#;
   Subject_Not_Setup                 : constant Status_Type := 16#2002#;
   Subject_Used                      : constant Status_Type := 16#2003#;
   Subject_Invalid                   : constant Status_Type := 16#2004#;
   Subject_Paging_Error              : constant Status_Type := 16#2005#;
   Subject_Active                    : constant Status_Type := 16#2006#;
   Subject_Inactive                  : constant Status_Type := 16#2007#;
   Subject_PTP_Active                : constant Status_Type := 16#2008#;
   Subject_PTP_Inactive              : constant Status_Type := 16#2009#;
   Subject_PTP_Present               : constant Status_Type := 16#200A#;
   Subject_PTP_Not_Present           : constant Status_Type := 16#200B#;
   Subject_Mapped_Region_Full        : constant Status_Type := 16#200C#;
   Subject_Mapped_Region_Empty       : constant Status_Type := 16#200D#;
   Subject_MSR_Range_Invalid         : constant Status_Type := 16#200E#;
   Subject_IO_Range_Invalid          : constant Status_Type := 16#200F#;
   Subject_Device_Table_Full         : constant Status_Type := 16#2010#;
   Subject_Device_Unassignable       : constant Status_Type := 16#2011#;
   Subject_IRQ_Table_Full            : constant Status_Type := 16#2012#;
   Subject_IRQ_Unassignable          : constant Status_Type := 16#2013#;
   Subject_Device_Not_Assigned       : constant Status_Type := 16#2014#;

   Device_Domain_Not_Locked          : constant Status_Type := 16#3000#;
   Device_Domain_Not_Unlocked        : constant Status_Type := 16#3001#;
   Device_Domain_Not_Setup           : constant Status_Type := 16#3002#;
   Device_Domain_Used                : constant Status_Type := 16#3003#;
   Device_Domain_Invalid             : constant Status_Type := 16#3004#;
   Device_Domain_Paging_Error        : constant Status_Type := 16#3005#;
   Device_Domain_Active              : constant Status_Type := 16#3006#;
   Device_Domain_Inactive            : constant Status_Type := 16#3007#;
   Device_Domain_PTP_Active          : constant Status_Type := 16#3008#;
   Device_Domain_PTP_Inactive        : constant Status_Type := 16#3009#;
   Device_Domain_PTP_Present         : constant Status_Type := 16#300A#;
   Device_Domain_PTP_Not_Present     : constant Status_Type := 16#300B#;
   Device_Domain_Mapped_Region_Full  : constant Status_Type := 16#300C#;
   Device_Domain_Mapped_Region_Empty : constant Status_Type := 16#300D#;
   Device_Included                   : constant Status_Type := 16#300E#;
   Device_Domain_Table_Full          : constant Status_Type := 16#300F#;
   Device_Domain_ID_Used             : constant Status_Type := 16#3010#;

   Memory_Region_Inactive            : constant Status_Type := 16#4000#;
   Memory_Region_Not_Locked          : constant Status_Type := 16#4001#;
   Memory_Region_Not_Unlocked        : constant Status_Type := 16#4002#;
   Memory_Region_Not_Setup           : constant Status_Type := 16#4003#;
   Memory_Region_Used                : constant Status_Type := 16#4004#;
   Memory_Region_Hash_Invalid        : constant Status_Type := 16#4005#;
   Memory_Region_MPTP_Present        : constant Status_Type := 16#4006#;
   Memory_Region_MPTP_Not_Present    : constant Status_Type := 16#4007#;
   Memory_Region_MPTP_Active         : constant Status_Type := 16#4008#;
   Memory_Region_MPTP_Inactive       : constant Status_Type := 16#4009#;
   Memory_Region_Counter_Empty       : constant Status_Type := 16#400A#;
   Memory_Region_Counter_Full        : constant Status_Type := 16#400B#;
   Memory_Region_Out_Of_Bounds       : constant Status_Type := 16#400C#;
   Memory_Region_Referenced          : constant Status_Type := 16#400D#;
   Memory_Region_Length_Too_Large    : constant Status_Type := 16#400E#;
   Memory_Region_Empty               : constant Status_Type := 16#400F#;

   Kernel_Not_Locked                 : constant Status_Type := 16#5000#;
   Kernel_Not_Unlocked               : constant Status_Type := 16#5001#;
   Kernel_Not_Setup                  : constant Status_Type := 16#5002#;
   Kernel_Used                       : constant Status_Type := 16#5003#;
   Kernel_Invalid                    : constant Status_Type := 16#5004#;
   Kernel_Paging_Error               : constant Status_Type := 16#5005#;
   Kernel_Active                     : constant Status_Type := 16#5006#;
   Kernel_Inactive                   : constant Status_Type := 16#5007#;
   Kernel_PTP_Active                 : constant Status_Type := 16#5008#;
   Kernel_PTP_Inactive               : constant Status_Type := 16#5009#;
   Kernel_PTP_Present                : constant Status_Type := 16#500A#;
   Kernel_PTP_Not_Present            : constant Status_Type := 16#500B#;
   Kernel_Mapped_Region_Full         : constant Status_Type := 16#500C#;
   Kernel_Mapped_Region_Empty        : constant Status_Type := 16#500D#;
   Kernel_CPU_Used                   : constant Status_Type := 16#500E#;
   Kernel_Device_Table_Full          : constant Status_Type := 16#500F#;
   Kernel_Device_Unassignable        : constant Status_Type := 16#5010#;
   Kernel_Device_Not_Assigned        : constant Status_Type := 16#5011#;

   Device_Used                       : constant Status_Type := 16#6000#;
   Device_Not_Setup                  : constant Status_Type := 16#6001#;
   Device_BDF_Used                   : constant Status_Type := 16#6002#;
   Device_IO_Port_Set_Full           : constant Status_Type := 16#6003#;
   Device_IRQ_Table_Full             : constant Status_Type := 16#6004#;
   Device_Memory_Table_Full          : constant Status_Type := 16#6005#;
   Device_Inactive                   : constant Status_Type := 16#6006#;
   Device_Not_PCI                    : constant Status_Type := 16#6007#;
   Device_IO_Port_Used               : constant Status_Type := 16#6008#;
   Device_IRQ_Used                   : constant Status_Type := 16#6009#;
   Device_Memory_Used                : constant Status_Type := 16#600A#;

   Unknown_Command                   : constant Status_Type := 16#FFFF#;

   ---------------------------------------------------------------------------

   type Command_Type (ID : Command_ID_Type := Unused_Command_ID) is record
      Sequence_Number    : Word64;

      case ID is
         when Create_VTd_Root_Table =>
            Create_VTd_Root_Table_Params         : P.Create_VTd_Root_Table_Type;
         when Create_VTd_Context_Table =>
            Create_VTd_Context_Table_Params      : P.Create_VTd_Context_Table_Type;
         when Create_VTd_IRQ_Remap_Table =>
            Create_VTd_IRQ_Remap_Table_Params    : P.Create_VTd_IRQ_Remap_Table_Type;
         when Activate_Tau0 =>
            null;
         when Write_Image_Commands =>
            Write_Image_Commands_Param           : P.Write_Image_Commands_Type;
         when Add_Processor =>
            Add_Processor_Params                 : P.Add_Processor_Type;
         when Add_Ioapic =>
            Add_Ioapic_Params                    : P.Add_Ioapic_Type;
         when Clear_Page =>
            Clear_Page_Params                    : P.Clear_Page_Type;
         when Add_Memory_Block =>
            Add_Memory_Block_Params              : P.Add_Memory_Block_Type;
         when Create_Memory_Region =>
            Create_Memory_Region_Params          : P.Create_Memory_Region_Type;
         when Lock_Memory_Region =>
            Lock_Memory_Region_Params            : P.Lock_Memory_Region_Type;
         when Activate_Memory_Region =>
            Activate_Memory_Region_Params        : P.Activate_Memory_Region_Type;
         when Deactivate_Memory_Region =>
            Deactivate_Memory_Region_Params      : P.Deactivate_Memory_Region_Type;
         when Unlock_Memory_Region =>
            Unlock_Memory_Region_Params          : P.Unlock_Memory_Region_Type;
         when Destroy_Memory_Region =>
            Destroy_Memory_Region_Params         : P.Destroy_Memory_Region_Type;
         when Create_Page_Table_MR =>
            Create_Page_Table_MR_Params          : P.Create_Page_Table_MR_Type;
         when Destroy_Page_Table_MR =>
            Destroy_Page_Table_MR_Params         : P.Destroy_Page_Table_MR_Type;
         when Append_Page_MR =>
            Append_Page_MR_Params                : P.Append_Page_MR_Type;
         when Append_Vacuous_Page_MR =>
            Append_Vacuous_Page_MR_Params        : P.Append_Vacuous_Page_MR_Type;
         when Truncate_Page_MR =>
            Truncate_Page_MR_Params              : P.Truncate_Page_MR_Type;
         when Activate_Page_MR =>
            Activate_Page_MR_Params              : P.Activate_Page_MR_Type;
         when Activate_Page_Table_MR =>
            Activate_Page_Table_MR_Params        : P.Activate_Page_Table_MR_Type;
         when Deactivate_Page_MR =>
            Deactivate_Page_MR_Params            : P.Deactivate_Page_MR_Type;
         when Deactivate_Page_Table_MR =>
            Deactivate_Page_Table_MR_Params      : P.Deactivate_Page_Table_MR_Type;
         when Create_Subject =>
            Create_Subject_Params                : P.Create_Subject_Type;
         when Destroy_Subject =>
            Destroy_Subject_Params               : P.Destroy_Subject_Type;
         when Lock_Subject =>
            Lock_Subject_Params                  : P.Lock_Subject_Type;
         when Activate_Subject =>
            Activate_Subject_Params              : P.Activate_Subject_Type;
         when Deactivate_Subject =>
            Deactivate_Subject_Params            : P.Deactivate_Subject_Type;
         when Attach_Memory_Region_Subject =>
            Attach_Memory_Region_Subject_Params  : P.Attach_Memory_Region_Subject_Type;
         when Detach_Memory_Region_Subject =>
            Detach_Memory_Region_Subject_Params  : P.Detach_Memory_Region_Subject_Type;
         when Assign_Device_Subject =>
            Assign_Device_Subject_Params         : P.Assign_Device_Subject_Type;
         when Assign_IRQ_Subject =>
            Assign_IRQ_Subject_Params            : P.Assign_IRQ_Subject_Type;
         when Set_IO_Port_Range =>
            Set_IO_Port_Range_Params             : P.Set_IO_Port_Range_Type;
         when Set_MSR_Range =>
            Set_MSR_Range_Params                 : P.Set_MSR_Range_Type;
         when Create_Page_Table_Subject =>
            Create_Page_Table_Subject_Params     : P.Create_Page_Table_Subject_Type;
         when Destroy_Page_Table_Subject =>
            Destroy_Page_Table_Subject_Params    : P.Destroy_Page_Table_Subject_Type;
         when Activate_Page_Table_Subject =>
            Activate_Page_Table_Subject_Params   : P.Activate_Page_Table_Subject_Type;
         when Deactivate_Page_Table_Subject =>
            Deactivate_Page_Table_Subject_Params : P.Deactivate_Page_Table_Subject_Type;
         when Map_Page_Subject =>
            Map_Page_Subject_Params              : P.Map_Page_Subject_Type;
         when Unmap_Page_Subject =>
            Unmap_Page_Subject_Params            : P.Unmap_Page_Subject_Type;
         when Map_Device_Page_Subject =>
            Map_Device_Page_Subject_Params       : P.Map_Device_Page_Subject_Type;
         when Create_Device_Domain =>
            Create_Device_Domain_Params          : P.Create_Device_Domain_Type;
         when Attach_Memory_Region_Domain =>
            Attach_Memory_Region_Domain_Params   : P.Attach_Memory_Region_Domain_Type;
         when Add_Device_To_Device_Domain =>
            Add_Device_To_Device_Domain_Params   : P.Add_Device_To_Device_Domain_Type;
         when Lock_Device_Domain =>
            Lock_Device_Domain_Params            : P.Lock_Device_Domain_Type;
         when Activate_Device_Domain =>
            Activate_Device_Domain_Params        : P.Activate_Device_Domain_Type;
         when Create_Page_Table_Device_Domain =>
            Create_Page_Table_Device_Domain_Params
              : P.Create_Page_Table_Device_Domain_Type;
         when Activate_Page_Table_Device_Domain =>
            Activate_Page_Table_Device_Domain_Params
              : P.Activate_Page_Table_Device_Domain_Type;
         when Map_Page_Device_Domain =>
            Map_Page_Device_Domain_Params        : P.Map_Page_Device_Domain_Type;
         when Create_Kernel =>
            Create_Kernel_Params                 : P.Create_Kernel_Type;
         when Lock_Kernel =>
            Lock_Kernel_Params                   : P.Lock_Kernel_Type;
         when Activate_Kernel =>
            Activate_Kernel_Params               : P.Activate_Kernel_Type;
         when Attach_Memory_Region_Kernel =>
            Attach_Memory_Region_Kernel_Params   : P.Attach_Memory_Region_Kernel_Type;
         when Create_Page_Table_Kernel =>
            Create_Page_Table_Kernel_Params      : P.Create_Page_Table_Kernel_Type;
         when Activate_Page_Table_Kernel =>
            Activate_Page_Table_Kernel_Params    : P.Activate_Page_Table_Kernel_Type;
         when Map_Page_Kernel =>
            Map_Page_Kernel_Params               : P.Map_Page_Kernel_Type;
         when Map_Device_Page_Kernel =>
            Map_Device_Page_Kernel_Params        : P.Map_Device_Page_Kernel_Type;
         when Assign_Device_Kernel =>
            Assign_Device_Kernel_Params          : P.Assign_Device_Kernel_Type;
         when Create_Legacy_Device =>
            Create_Legacy_Device_Params          : P.Create_Legacy_Device_Type;
         when Create_PCI_Device =>
            Create_PCI_Device_Params             : P.Create_PCI_Device_Type;
         when Add_IO_Port_Range_Device =>
            Add_IO_Port_Range_Device_Params      : P.Add_IO_Port_Range_Device_Type;
         when Add_IRQ_Device =>
            Add_IRQ_Device_Params                : P.Add_IRQ_Device_Type;
         when Add_Memory_Device =>
            Add_Memory_Device_Params             : P.Add_Memory_Device_Type;
         when Activate_Device =>
            Activate_Device_Params               : P.Activate_Device_Type;
         when others =>
            null;
      end case;
   end record;

   Null_Command : constant Command_Type := Command_Type'
     (ID              => Unused_Command_ID,
      Sequence_Number => 0);

   -----------------------------------------------------------------------------

   type Response_Type is record
      Sequence_Number : Word64;              --  SN of command we respond to
      Command_ID      : Command_ID_Type;     --  ID of command we respond to
      Status          : Status_Type;
   end record;

end Tau0.Types.Command;
