--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory;
with Tau0.Data.Memory_Region_Table;
with Tau0.Data.Roots;
with Tau0.Types.Command;

--  Process Tau0 command stream.
package Tau0.Command_Processor
with Abstract_State => null
is

   function Invariants return Boolean
   is (Data.Roots.Invariant and
       Data.Memory.Invariant and
       Data.Memory_Region_Table.Invariant)
   with Ghost;

   --  Process the commands provided by Tau0.Command_Stream.  Dispatch commands
   --  to the procedures in Tau0.Commands.
   procedure Process_Command_Stream
   with
     Pre  => Invariants,
     Post => Invariants;

   --  Process the given command by dispatching it to the procedures in
   --  Tau0.Commands. Status is set according to the result of the command
   --  handling.
   procedure Process
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants,
     Post => Invariants;

end Tau0.Command_Processor;
