--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;
with Tau0.Data.Devices;
with Tau0.Data.Memory_Region_Table;
with Tau0.Data.Memory.Full_Typization;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Processors;
with Tau0.Data.Roots;
with Tau0.Types.Device;
with Tau0.Types.Hash;
with Tau0.Types.Page_Table;
with Tau0.Types.Root;

package Tau0.Data.Memory.Paging
is

   use Types;

   use type Types.Device.State_Type;
   use type Types.Root.Kind_Type;
   use type Types.Root.Paging_Type;
   use type Types.Root.Root_Array;
   use type Types.Root.Root_Type;
   use type Types.Root.State_Type;

   -----------------------------------------------------------------------------

   type Result_Type (Success : Boolean := False) is record
      case Success is
         when True =>
            Active    : Boolean;
            Readable  : Boolean;
            Writable  : Boolean;
            Address   : Tau0_Address_Type;
            --  Path is a witness for reachability.  It is only used in proofs,
            --  but unfortunately there are no Ghost record components.
            Path      : Full_Typization.Path_Type;
            Path_Last : Full_Typization.Path_Index;
         when False =>
            null;
      end case;
   end record;

   ---------------------------------------------------------------------------

   --  Page tables of memory regions must be allocated in Tau0 private memory,
   --  while all other objects must be allocated in physical memory.
   function Address_Predicate
     (Root    : Types.Root_Range;
      Address : Tau0_Address_Type;
      Level   : Level_Type)
      return Boolean
   is (case Data.Roots.Kind (Root) is
         when Types.Root.Memory_Region =>
           (if Level = 0 then Address in Types.Physical_Address_Type
                         else Address in Types.Tau0_Private_Address_Type),
         when others =>
           Address in Types.Physical_Address_Type)
   with Pre => Data.Roots.Used (Root);

   --  Used to claim full typization of translated address in postcondition of
   --  Translate_Address
   function Correct_PT_Typization
     (Root : Types.Root_Range;
      T    : Types.Typization.Full_Typization_Type)
      return Boolean
   with
     Ghost,
     Pre => Data.Roots.Used (Root);

   -----------------------------------------------------------------------------

   function Translatable
     (Root            : Types.Root_Range;
      Virtual_Address : Virtual_Address_Type;
      Level           : Level_Type)
      return Boolean
   with
     Pre  => Invariant and then
             Data.Roots.Invariant and then
             Data.Roots.Used (Root) and then
             Data.Roots.PTP_Present (Root) and then
             Level < Data.Roots.Level (Root) and then
             Aligned_64 (Virtual_Address);

   --  Using the page tables of Root, translate Virtual_Address to a
   --  Tau0_Address.  The translation is up to Level -- for a full translation,
   --  let Level = 0.  If some page on the walk is not present, then
   --  Translatable'Result.Success = False.
   function Translate
     (Root            : Types.Root_Range;
      Virtual_Address : Virtual_Address_Type;
      Level           : Level_Type)
      return Result_Type
   with
     Pre  => Invariant and then
             Data.Roots.Invariant and then
             Data.Roots.Used (Root) and then
             Data.Roots.PTP_Present (Root) and then
             Level < Data.Roots.Level (Root) and then
             Aligned_64 (Virtual_Address),
     Post => (if Translate'Result.Success
              then -- FIXME show
                   Address_Predicate (Root, Translate'Result.Address, Level) and then
                   (if Data.Roots.Kind (Root) /= Types.Root.Memory_Region or Level = 0
                     then Translate'Result.Active
                            = not Accessible (Translate'Result.Address)
                     elsif Data.Roots.Kind (Root) = Types.Root.Memory_Region
                     then Accessible (Translate'Result.Address)) and then
                   Aligned_64 (Translate'Result.Address) and then
                   (if Aligned_Page (Virtual_Address) and Level = 0
                    then Aligned_Page (Translate'Result.Address)) and then
                   (if Level > 0
                    then Correct_PT_Typization
                           (Root,
                            Get_Full_Typization (Translate'Result.Address))
                    else Get_Full_Typization (Translate'Result.Address)
                           in T.MR_Page | T.Device_Page) and then
                    Types.Typization.Level
                      (Get_Full_Typization (Translate'Result.Address))
                      = Level and then
                    Translate'Result.Path_Last = Data.Roots.Level (Root) - Level and then
                    Translate'Result.Active
                      = Full_Typization.Active (Root,
                                                Translate'Result.Address,
                                                Translate'Result.Path,
                                                Translate'Result.Path_Last) and then
                    Full_Typization.Reachable (Root,
                                               Translate'Result.Address,
                                               Translate'Result.Path,
                                               Translate'Result.Path_Last));

   --  Create a page (table) for Root at Level, using address Page to store it.
   procedure Create_Page
     (Root            :     Types.Root_Range;
      Virtual_Address :     Virtual_Address_Type;
      Level           :     Page_Level_Type;
      Page            :     Tau0_Address_Type;
      Readable        :     Boolean;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Success         : out Boolean)
   with
     Pre  => Invariant and then
             Data.Roots.Invariant and then
             Data.Memory_Region_Table.Invariant and then
             Tau0_State = Running and then
             Data.Roots.State (Root) = Types.Root.Setup and then
             Level < Data.Roots.Level (Root) and then
             (if Level = Data.Roots.Level (Root) - 1
              then not Data.Roots.PTP_Present (Root)
              else Data.Roots.PTP_Present (Root)) and then
             Get_Typization (Page) = Types.Typization.Zeroed and then
             Aligned_Page (Page) and then
             Aligned_64 (Virtual_Address) and then --  FIXME: aligned_level
             Address_Predicate (Root, Page, Level),
     Post => Invariant and
             Data.Roots.Invariant and
             Data.Memory_Region_Table.Invariant and
             Data.Roots.Roots_Unchanged
               (New_Roots => Data.Roots.Get_Roots,
                Old_Roots => Data.Roots.Get_Roots'Old,
                Except    => Root) and
             Data.Roots.State (Root) = Data.Roots.State (Root)'Old and
             Data.Roots.Kind (Root) = Data.Roots.Kind (Root)'Old and
             (if Success and Level = Data.Roots.Level (Root) - 1
              then Data.Roots.Roots_Unchanged
                     (New_Roots => Data.Roots.Get_Roots,
                      Old_Roots => Data.Roots.Get_Roots'Old,
                      Except    => Root) and
                Data.Roots.Get_Roots (Root) = (Data.Roots.Get_Roots (Root)'Old
                                               with delta
                                                     PTP_Present => True,
                                                     PTP         => Page)
              else Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old) and
              (Success = (Get_Typization (Page) = Types.Typization.Used));

   --  Activate Root's page (table) at Level
   procedure Activate_Page
     (Root            :     Types.Root_Range;
      Virtual_Address :     Virtual_Address_Type;
      Level           :     Level_Type;
      Success         : out Boolean)
   with
     Pre  => Invariant and then
             Data.Roots.Invariant and then
             Data.Memory_Region_Table.Invariant and then
             Tau0_State = Running and then
             Data.Roots.State (Root) = Types.Root.Locked and then
             not Data.Roots.PTP_Active (Root) and then
             Level < Data.Roots.Level (Root) and then
             Aligned_64 (Virtual_Address),
     Post => Invariant and
             Data.Roots.Invariant and
             Data.Memory_Region_Table.Invariant and
             Data.Roots.Roots_Unchanged
               (New_Roots => Data.Roots.Get_Roots,
                Old_Roots => Data.Roots.Get_Roots'Old,
                Except    => Root) and
             Data.Roots.Get_Roots (Root) = (Data.Roots.Get_Roots (Root)'Old
             with delta
               PTP_Active => (if Level = Data.Roots.Level (Root) - 1 and Success
                               then True else Data.Roots.PTP_Active (Root)'Old));
             --  TODO: specify that the page table's type is left unchanged, but
             --  it is now active (how?)

    --  Map a page from the memory region associated to the memory-region-table
    --  index Index into Root at Virtual_Address.  The memory region page to be
    --  mapped is determined by Offset.
   procedure Map_Page
     (Root            :     Types.Root_Range;
      Index           :     Types.Memory_Region_Table_Range;
      Offset          :     Page_Count_Type;
      Virtual_Address :     Virtual_Address_Type;
      Success         : out Boolean)
   with
     Pre => Invariant and then
            Data.Roots.Invariant and then
            Data.Memory_Region_Table.Invariant and then
            Tau0_State = Running and then
            Data.Roots.State (Root) = Types.Root.Setup and then
            Data.Roots.Kind (Root) /= Types.Root.Memory_Region and then
            Data.Roots.PTP_Present (Root) and then
            Aligned_Page (Virtual_Address) and then
            Data.Memory_Region_Table.Used (Index) and then
            Data.Memory_Region_Table.Owner (Index) = Root and then
            Data.Memory_Region_Table.Mapped_Pages (Index)
              < Data.Memory_Region_Table.Length (Index) and then
            Offset >= Data.Memory_Region_Table.Offset (Index) and then
            Offset < Data.Memory_Region_Table.Offset (Index)
                       + Data.Memory_Region_Table.Length (Index),
    Post => Invariant and
            Data.Roots.Invariant and
            Data.Memory_Region_Table.Invariant and
            (if Success and Data.Roots.Level (Root) = 1
             then Data.Roots.Roots_Unchanged
                    (New_Roots => Data.Roots.Get_Roots,
                     Old_Roots => Data.Roots.Get_Roots'Old,
                     Except    => Root) and
                  Data.Roots.Get_Roots (Root) = (Data.Roots.Get_Roots (Root)'Old
                  with delta
                    PTP_Present => True,
                    PTP         => Data.Roots.PTP (Root))
             else Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old);

   --  Append unused page at address Page to memory region Root.  Write Contents
   --  to this page.  Note that the page tables of Root must have been set up
   --  accordingly for the procedure to succeed.
   procedure Append_Page_Contents
     (Root     :     Types.Root_Range;
      Page     :     Types.Physical_Address_Type;
      Contents :     Page_Array;
      Success  : out Boolean)
   with
     Global => (Proof_In => (Data.Processors.State,
                             Devices.State,
                             Memory_Region_Table.State,
                             Tau0.State),
                Input  => (VTd_IOMMU.State),
                In_Out => (Memory.Memory_State,
                           Memory.Typization_State,
                           Roots.State)),
     Pre  => Invariant and then
             Data.Roots.Invariant and then
             Data.Memory_Region_Table.Invariant and then
             Types.Aligned_Page (Page) and then
             Tau0_State = Running and then
             Get_Typization (Page) = Types.Typization.Zeroed and then
             Data.Roots.Kind (Root) = Types.Root.Memory_Region and then
             Data.Roots.State (Root) = Types.Root.Setup and then
             (if Data.Roots.Level (Root) = 1
              then not Data.Roots.PTP_Present (Root)
              else Data.Roots.PTP_Present (Root)) and then
             Data.Roots.Length (Root) < Types.Page_Count_Type'Last,
     Post => Invariant and
             Data.Roots.Invariant and
             Data.Memory_Region_Table.Invariant and
             (if Success then Get_Full_Typization (Page) = T.MR_Page) and
             (if Success and Data.Roots.Level (Root) = 1
              then Data.Roots.Roots_Unchanged
                     (New_Roots => Data.Roots.Get_Roots,
                      Old_Roots => Data.Roots.Get_Roots'Old,
                      Except    => Root) and
                   Data.Roots.Get_Roots (Root)
                     = (Data.Roots.Get_Roots (Root)'Old with delta
                         PTP_Present => True,
                         PTP         => Page)
              else Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old);

   --  Append unused page at address Page to memory region Root.  Write no
   --  contents to this page.  Note that the page tables of Root must have been
   --  set up accordingly for the procedure to succeed.
   procedure Append_Page_Vacuous
     (Root    :     Types.Root_Range;
      Page    :     Types.Physical_Address_Type;
      Success : out Boolean)
   with
     Pre  => Invariant and then
             Data.Roots.Invariant and then
             Data.Memory_Region_Table.Invariant and then
             Types.Aligned_Page (Page) and then
             Tau0_State = Running and then
             Get_Typization (Page) in Types.Typization.Undefined
                                    | Types.Typization.Zeroed and then
             Data.Roots.Kind (Root) = Types.Root.Memory_Region and then
             Data.Roots.State (Root) = Types.Root.Setup and then
             (if Data.Roots.Level (Root) = 1
              then not Data.Roots.PTP_Present (Root)
              else Data.Roots.PTP_Present (Root)) and then
             Data.Roots.Length (Root) < Types.Page_Count_Type'Last,
     Post => Invariant and
             Data.Roots.Invariant and
             Data.Memory_Region_Table.Invariant and
             (if Success
              then Accessible (Page) and
                   Get_Full_Typization (Page) = T.MR_Page and
                   (if Data.Roots.Level (Root) = 1
                    then Data.Roots.Roots_Unchanged
                           (New_Roots => Data.Roots.Get_Roots,
                            Old_Roots => Data.Roots.Get_Roots'Old,
                            Except    => Root) and
                         Data.Roots.Get_Roots (Root)
                           = (Data.Roots.Get_Roots (Root)'Old with delta
                               PTP_Present => True,
                               PTP         => Page)
                    else Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old)
              else Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old);

   procedure Map_Device_Page
     (Root            :     Types.Root_Range;
      Device          :     Types.Device.Device_Range;
      Page            :     Types.Physical_Address_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Success         : out Boolean)
   with
     Pre => Invariant and then
            Data.Roots.Invariant and then
            Data.Memory_Region_Table.Invariant and then
            Tau0_State = Running and then
            Data.Roots.State (Root) = Types.Root.Setup and then
            Data.Roots.Kind (Root) in Types.Root.Subject | Types.Root.Kernel and then
            Data.Roots.PTP_Present (Root) and then
            Aligned_Page (Page) and then
            Aligned_Page (Virtual_Address) and then
            Data.Roots.Device_Assigned (Root, Device) and then
            Data.Devices.State (Device) = Types.Device.Active and then
            Data.Devices.Owns_Page (Device, Page),
    Post => Invariant and
            Data.Roots.Invariant and
            Data.Memory_Region_Table.Invariant and
            Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old;

   function Toplevel_Page_Table_Entries_Active
     (Root : Types.Root_Range)
      return Boolean
   with
     Pre => Data.Roots.State (Root) = Types.Root.Locked and then
            Data.Roots.Level (Root) > 1 and then
            not Data.Roots.PTP_Active (Root);

   procedure Update_Hash
     (Root       :        Types.Root_Range;
      Page_Index :        Types.Page_Count_Type;
      Context    : in out Types.Hash.Context_Type)
   with
     Pre => Invariant and then
            Data.Roots.Invariant and then
            Data.Roots.State (Root) = Types.Root.Setup and then
            Data.Roots.PTP_Present (Root) and then
            Data.Roots.Kind (Root) = Types.Root.Memory_Region and then
            Page_Index in 0 .. Data.Roots.Length (Root) - 1;

private

   subtype Page_Table_Offset_Type is Tau0_Address_Type
     range 0 .. Config.Page_Size - 1;

   subtype Page_Frame_Offset_Type is Tau0_Address_Type
     range 0 .. Config.Page_Size - 1;

   function Correct_PT_Typization
     (Root : Types.Root_Range;
      T    : Types.Typization.Full_Typization_Type)
      return Boolean
   is (case Data.Roots.Paging (Root) is
         when Types.Root.IA32e => Types.Typization.Is_IA32e_PT (T),
         when Types.Root.EPT   => Types.Typization.Is_EPT (T),
         when Types.Root.MPT   => Types.Typization.Is_MPT (T),
         when Types.Root.VTd   => Types.Typization.Is_VTd_PT (T));

   function Page_Table_Offset
     (Virtual_Address : Virtual_Address_Type;
      Level           : Page_Level_Type)
      return Page_Table_Offset_Type
   with
     Post => Aligned_64 (Page_Table_Offset'Result);

   function Page_Frame_Offset
     (Virtual_Address : Virtual_Address_Type)
      return Page_Frame_Offset_Type
   with Post => (if Aligned_64 (Virtual_Address)
                 then Aligned_64 (Page_Frame_Offset'Result)) and
                (if Aligned_Page (Virtual_Address)
                 then Page_Frame_Offset'Result = 0);

   function May_Dereference_Active_Page
     (Root : Types.Root_Range)
      return Boolean
   with Pre => Data.Roots.Used (Root);

   function From_Entry
     (Root  : Types.Root_Range;
      Entr  : Types.Page_Table.Entry_Type)
      return Types.Word64
   with
     Pre => Data.Roots.Used (Root);

   function To_Entry
     (Root  : Types.Root_Range;
      Word  : Types.Word64;
      Level : Types.Page_Level_Type)
      return Types.Page_Table.Entry_Type
   with
     Pre => Data.Roots.Used (Root);

   function Page_Table_Entries_Active
     (Root          : Types.Root_Range;
      Table_Address : Tau0_Address_Type;
      Table_Level   : Types.Page_Level_Type)
      return Boolean
   with Pre =>
     Data.Roots.Used (Root) and then
     Aligned_Page (Table_Address) and then
     Accessible (Table_Address) and then
     Types.Typization.Is_Page_Table (Get_Full_Typization (Table_Address));

   procedure Add_Mapping
     (Root            :     Types.Root_Range;
      Virtual_Address :     Virtual_Address_Type;  --  Virtual address of parent PTE
      Level           :     Level_Type;            --  Level of page we map to
      Page            :     Tau0_Address_Type;     --  The page we map to
      Readable        :     Boolean;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Active          :     Boolean                := False;
      --  Caching type only specified at leaf level
      Caching         :     Types.Opt_Caching_Type := Types.Ignore;
      Success         : out Boolean)
   with
      Global => (Proof_In => (Data.Processors.State,
                              Devices.State,
                              Memory.Typization_State,
                              Memory_Region_Table.State,
                              Tau0.State),
                 Input    => (VTd_IOMMU.State),
                 In_Out   => (Memory.Memory_State,
                              Roots.State)),
     Pre  => Invariant and then
             Data.Memory_Region_Table.Invariant and then
             Data.Roots.Invariant and then
             Tau0_State = Running and then
             Data.Roots.State (Root) = Types.Root.Setup and then
             Level < Data.Roots.Level (Root) and then
             (if Level = Data.Roots.Level (Root) - 1 then
              not Data.Roots.PTP_Present (Root)
              else Data.Roots.PTP_Present (Root)) and then
             Get_Full_Typization (Page) in
                T.Undefined | T.Zeroed | T.MR_Page | T.Device_Page and then
             Aligned_Page (Page) and then
             Aligned_64 (Virtual_Address) and then --  FIXME: aligned_level
             Address_Predicate (Root, Page, Level) and then
             (Caching in Types.Caching_Type)
                = (Level = 0 and Data.Roots.Kind (Root) /= Types.Root.Memory_Region),
     Post => Invariant and
             Data.Memory_Region_Table.Invariant and
             Data.Roots.Invariant and
             (if Success and Level = Data.Roots.Level (Root) - 1
              then Data.Roots.Roots_Unchanged
                     (New_Roots => Data.Roots.Get_Roots,
                      Old_Roots => Data.Roots.Get_Roots'Old,
                      Except    => Root) and
                   Data.Roots.Get_Roots (Root)
                     = (Data.Roots.Get_Roots (Root)'Old with delta
                         PTP_Present => True,
                         PTP         => Page)
              else Data.Roots.Get_Roots = Data.Roots.Get_Roots'Old) and
             (Success = (Get_Typization (Page) = Types.Typization.Used));

   function Translatable
     (Root            : Types.Root_Range;
      Virtual_Address : Virtual_Address_Type;
      Level           : Level_Type)
      return Boolean
   is (Translate (Root, Virtual_Address, Level).Success);

end Tau0.Data.Memory.Paging;
