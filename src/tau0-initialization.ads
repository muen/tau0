--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory;

package Tau0.Initialization
with
  Abstract_State    => State,
  Initial_Condition => not Initialized
is

   function Initialized return Boolean
   with Global => State;

   procedure Initialize (Success : out Boolean)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State),
                In_Out   => (State,
                             Data.Memory.Init_State)),
     Pre    => not Initialized and
               not Data.Memory.Initialized and
               Tau0_State = Setup,
     Post   => Success = Initialized;

end Tau0.Initialization;
