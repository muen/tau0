--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Roots.Writers.Memory_Regions;
with Tau0.Data.Memory.Paging;
with Tau0.Types.Typization;

package body Tau0.Commands.Memory_Regions
is

   package R  renames Data.Roots;
   package TC renames Types.Command;

   use type Types.Root.Kind_Type;
   use type Types.Root.State_Type;
   use type Types.Typization.Typization_Type;

   -----------------------------------------------------------------------------

   procedure Activate_Memory_Region
     (Region :     Types.Root_Range;
      Status : out TC.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Locked then
         Status := TC.Memory_Region_Not_Locked;
      elsif not R.PTP_Active (Root => Region) then
         Status := TC.Memory_Region_MPTP_Inactive;
      else
         R.Writers.Activate (Root => Region);
         Status := TC.Success;
      end if;
   end Activate_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Activate_Page
     (Region          :     Types.Root_Range;
      Virtual_Address :     Types.Virtual_Address_Type;
      Status          : out Types.Command.Status_Type)
   is
      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Locked then
         Status := TC.Memory_Region_Not_Locked;
      elsif R.PTP_Active (Root => Region) then
         Status := TC.Memory_Region_MPTP_Active;
      elsif not Types.Aligned_64 (Address => Virtual_Address) then
         Status := TC.Address_Invalid;
      else
         Data.Memory.Paging.Activate_Page
           (Root            => Region,
            Virtual_Address => Virtual_Address,
            Level           => 0,
            Success         => Success);

         if Success then
            Status := TC.Success;
         else
            Status := TC.Page_Translation_Error;
         end if;
      end if;
   end Activate_Page;

   -----------------------------------------------------------------------------

   procedure Activate_Page_Table
     (Region          :     Types.Root_Range;
      Virtual_Address :     Types.Virtual_Address_Type;
      Level           :     Types.Page_Level_Type;
      Status          : out Types.Command.Status_Type)
   is
      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Locked then
         Status := TC.Memory_Region_Not_Locked;
      elsif R.PTP_Active (Root => Region) then
         Status := TC.Memory_Region_MPTP_Active;
      elsif Level >= R.Level (Root => Region) then
         Status := TC.Level_Invalid;
      elsif not Types.Aligned_64 (Address => Virtual_Address) then
         Status := TC.Address_Invalid;
      else
         Data.Memory.Paging.Activate_Page
           (Root            => Region,
            Virtual_Address => Virtual_Address,
            Level           => Level,
            Success         => Success);

         if Success then
            Status := TC.Success;
         else
            Status := TC.Page_Translation_Error;
         end if;
      end if;
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   procedure Append_Page_Contents
     (Region   :     Types.Root_Range;
      Page     :     Types.Physical_Address_Type;
      Contents :     Types.Page_Array;
      Status   : out Types.Command.Status_Type)
   is
      use type Types.Page_Count_Type;

      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Setup then
         Status := TC.Memory_Region_Not_Setup;
      elsif
        Data.Memory.Get_Typization (Address => Page)
          /= Types.Typization.Zeroed
      then
         Status := TC.Typization_Violation;
      elsif not Types.Aligned_Page (Address => Page) then
         Status := TC.Address_Invalid;
      elsif
        R.Level (Root => Region) = 1 and R.PTP_Present (Region)
      then
         Status := TC.Memory_Region_MPTP_Present;
      elsif
        R.Level (Root => Region) > 1 and
        not R.PTP_Present (Root => Region)
      then
         Status := TC.Memory_Region_MPTP_Not_Present;
      elsif R.Length (Root => Region) = Types.Page_Count_Type'Last then
         Status := TC.Memory_Region_Length_Too_Large; --  FIXME naming
      else
         Data.Memory.Paging.Append_Page_Contents
           (Root     => Region,
            Page     => Page,
            Contents => Contents,
            Success  => Success);

         if Success then
            Status := TC.Success;
         else
            Status := TC.Page_Translation_Error;
         end if;
      end if;
   end Append_Page_Contents;

   -----------------------------------------------------------------------------

   procedure Append_Page_Vacuous
     (Region :     Types.Root_Range;
      Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   is
      use type Types.Page_Count_Type;

      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Setup then
         Status := TC.Memory_Region_Not_Setup;
      elsif
        not (Data.Memory.Get_Typization (Address => Page) in
               Types.Typization.Undefined | Types.Typization.Zeroed)
      then
         Status := TC.Typization_Violation;
      elsif not Types.Aligned_Page (Address => Page) then
         Status := TC.Address_Invalid;
      elsif
        R.Level (Root => Region) = 1 and R.PTP_Present (Region)
      then
         Status := TC.Memory_Region_MPTP_Present;
      elsif
        R.Level (Root => Region) > 1 and
        not R.PTP_Present (Root => Region)
      then
         Status := TC.Memory_Region_MPTP_Not_Present;
      elsif R.Length (Root => Region) = Types.Page_Count_Type'Last then
         Status := TC.Memory_Region_Length_Too_Large;
      else
         Data.Memory.Paging.Append_Page_Vacuous
           (Root    => Region,
            Page    => Page,
            Success => Success);

         if Success then
            Status := TC.Success;
         else
            Status := TC.Page_Translation_Error;
         end if;
      end if;
   end Append_Page_Vacuous;

   -----------------------------------------------------------------------------

   procedure Create_Memory_Region
     (Region  :     Types.Root_Range;
      Level   :     Types.Nonleaf_Level_Type;
      Caching :     Types.Caching_Type;
      Hash    :     Types.Root.Optional_Hash_Type;
      Status  : out TC.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Unused then
         Status := TC.Memory_Region_Used;
      else
         R.Writers.Memory_Regions.Create_Memory_Region
           (Root    => Region,
            Level   => Level,
            Caching => Caching,
            Hash    => Hash);
         Status := TC.Success;
      end if;
   end Create_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Create_Page_Table
     (Page            :     Types.Tau0_Private_Address_Type;
      Region          :     Types.Root_Range;
      Level           :     Types.Page_Level_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Status          : out TC.Status_Type)
   is
      Success : Boolean;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Setup then
         Status := TC.Memory_Region_Not_Setup;
      elsif Level >= R.Level (Root => Region) then
         Status := TC.Level_Invalid;
      elsif
        not Types.Aligned_64 (Address => Virtual_Address) or
        not Types.Aligned_Page (Address => Page)
      then
         Status := TC.Address_Invalid;
      elsif
        Data.Memory.Get_Typization (Address => Page)
          /= Types.Typization.Zeroed
      then
         Status := TC.Typization_Violation;
      elsif
        Level = R.Level (Root => Region) - 1 and
        R.PTP_Present (Root => Region)
      then
         Status := TC.Memory_Region_MPTP_Present;
      elsif
        Level /= R.Level (Root => Region) - 1 and
        not R.PTP_Present (Root => Region)
      then
         Status := TC.Memory_Region_MPTP_Not_Present;
      else
         Data.Memory.Paging.Create_Page
           (Root            => Region,
            Virtual_Address => Virtual_Address,
            Level           => Level,
            Page            => Page,
            Readable        => True,
            Writable        => True,
            Executable      => False,
            Success         => Success);

         if Success then
            Status := TC.Success;
         else
            Status := TC.Page_Translation_Error;
         end if;
      end if;
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   procedure Lock_Memory_Region
     (Region :     Types.Root_Range;
      Status : out TC.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Region) /= Types.Root.Memory_Region then
         Status := TC.Root_Object_Kind_Invalid;
      elsif R.State (Root => Region) /= Types.Root.Setup then
         Status := TC.Memory_Region_Not_Setup;
      elsif not R.PTP_Present (Root => Region) then
         Status := TC.Memory_Region_MPTP_Not_Present;
      elsif
        R.Must_Check_Hash (Root => Region) and then
        not R.Hash_Valid (Root => Region)
      then
         Status := TC.Memory_Region_Hash_Invalid;
      else
         R.Writers.Lock (Root => Region);
         Status := TC.Success;
      end if;
   end Lock_Memory_Region;

end Tau0.Commands.Memory_Regions;
