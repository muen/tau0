--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

separate (Tau0.Data.Memory)
package body Low_Level
with Refined_State => (State => Memory)
is

   -----------------------------------------------------------------------------

   function Get (Address : Types.Tau0_Address_Type) return Types.Word64
   is (Memory (To_Range (Address)));

   -----------------------------------------------------------------------------

   function Get (Address : Types.Tau0_Address_Type) return Types.Word128
   is (Types.Word128'(0 => Get (Address), 1 => Get (Address + 8)));

   -----------------------------------------------------------------------------

   function Get_Model return Memory_Model_Type
   is (Memory_Model_Type (Memory));

   -----------------------------------------------------------------------------

   procedure Set (Address : Types.Tau0_Address_Type; Value : Types.Word64)
   is
   begin
      Memory (To_Range (Address)) := Value;
      Accessible_Lemma (Address);
   end Set;

   -----------------------------------------------------------------------------

   procedure Set (Address : Types.Tau0_Address_Type; Value : Types.Word128)
   is
   begin
      Accessible_Lemma (Address);
      Memory (To_Range (Address))     := Value (0);
      Memory (To_Range (Address + 8)) := Value (1);
   end Set;

end Low_Level;
