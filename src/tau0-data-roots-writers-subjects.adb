--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.Accessibility;
with Tau0.Data.Memory.Bitmaps;
with Tau0.Data.Memory.Full_Typization;
with Tau0.Types.VTd_Tables;

package body Tau0.Data.Roots.Writers.Subjects
is

   -----------------------------------------------------------------------------

   procedure Activate (Root : Types.Root_Range)
   is
      use type Types.VTd_Tables.Root_Table_Model_Type;

      Old_Roots      : constant Types.Root.Root_Array
        := Roots with Ghost;
      Old_Memory     : constant Memory.Memory_Model_Type
        := Memory.Get_Memory_Model with Ghost;
      Old_Root_Table : constant Types.VTd_Tables.Root_Table_Model_Type
        := Memory.VTd_IOMMU.Root_Table_Model with Ghost;
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State                    => Types.Root.Active,
         Is_Memory_Region         => False,
         Kind                     => Types.Root.Subject,
         PTP                      => Roots (Root).PTP,
         PTP_Present              => Roots (Root).PTP_Present,
         PTP_Active               => Roots (Root).PTP_Active,
         IO_Bitmap                => Roots (Root).IO_Bitmap,
         MSR_Bitmap               => Roots (Root).MSR_Bitmap,
         Subject_Assigned_Devices => Roots (Root).Subject_Assigned_Devices,
         Assigned_IRQs            => Roots (Root).Assigned_IRQs,
         Attached_Tables          => Roots (Root).Attached_Tables,
         Paging                   => Roots (Root).Paging,
         Subject_CPU              => Roots (Root).Subject_CPU);

      Lemma_CPUs
        (Old_Roots => Old_Roots,
         New_Roots => Roots,
         CPUs      => CPUs);
      Memory_Region_Table.Lemma_Invariant
        (Old_Roots => Old_Roots,
         New_Roots => Roots);

      pragma Assert (Invariant_CPUs_Correct (Roots, CPUs));

      for I in Roots (Root).Assigned_IRQs loop
         pragma Loop_Invariant (Invariant);
         pragma Loop_Invariant
           (Memory.Interrupt_Remapping.Table_Present and then
            Memory.Interrupt_Remapping.Memory_Only_Table_Changed
              (Old_Model => Old_Memory,
               New_Model => Memory.Get_Memory_Model));
         pragma Loop_Invariant
           (if Memory.VTd_IOMMU.Root_Table_Present
            then Old_Root_Table = Memory.VTd_IOMMU.Root_Table_Model);
         pragma Loop_Invariant
           (Data.Processors.Present (ID => Roots (Root).Subject_CPU));
         pragma Loop_Variant
           (Increases => Types.Root.IRQ_Table.To_Natural (I));

         declare
            Cell : constant Types.Root.IRQ_Cell
              := Types.Root.IRQ_Table.Element
                   (Table  => Roots (Root).Assigned_IRQs,
                    Cursor => I);
         begin
            pragma Assert (Types.Root.Subject_Device_Table.Member
                             (Table => Roots (Root).Subject_Assigned_Devices,
                              Elem  => Cell.Device));

            --  TODO: announce interrupt to Muen
            Data.Memory.Interrupt_Remapping.Remap_Interrupt
              (Device    => Cell.Device,
               Interrupt => Cell.IRQ,
               CPU       => Roots (Root).Subject_CPU);

            --  Prove that VTd root table is left untouched (if present)
            --  FIXME: find easier and shorter way of doing this
            if Memory.VTd_IOMMU.Root_Table_Present then
               Memory.VTd_IOMMU.Root_Table_Entry_Lemma
                 (Old_Model => Old_Memory,
                  New_Model => Memory.Get_Memory_Model);

               pragma Assert
                 (for all Bus in Types.Device.PCI_Bus_Range =>
                  Memory.VTd_IOMMU.Model_Root_Table_Entry_Using
                    (Bus   => Bus,
                     Model => Old_Memory).Present
                    = Memory.VTd_IOMMU.Model_Root_Table_Entry_Using
                        (Bus   => Bus,
                         Model => Memory.Get_Memory_Model).Present);
            end if;

            pragma Assert
              (Invariant_Devices_Device_Domain (Roots, Memory.Get_Memory_Model));
         end;
      end loop;
   end Activate;

   -----------------------------------------------------------------------------

   procedure Assign_IRQ
     (Root      : Types.Root_Range;
      Device    : Types.Device.Device_Range;
      Interrupt : Types.Device.HW_IRQ_Type)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Types.Root.IRQ_Table.Push
        (Table => Roots (Root).Assigned_IRQs,
         Elem  => (Device => Device,
                   IRQ    => Interrupt));

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
   end Assign_IRQ;

   -----------------------------------------------------------------------------

   procedure Create_Subject
     (Root       : Types.Root_Range;
      Paging     : Types.Root.Subject_Paging_Type;
      MSR_Bitmap : Types.Physical_Address_Type;
      IO_Bitmap  : Types.Physical_Address_Type;
      CPU        : Types.CPU_Range)
   is
      package T renames Types.Typization;

      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
      IO_Bitmap_High : constant Types.Physical_Address_Type
        := Types.Successor_Page (IO_Bitmap);
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State                    => Types.Root.Setup,
         Is_Memory_Region         => False,
         Kind                     => Types.Root.Subject,
         PTP_Present              => False,
         PTP_Active               => False,
         PTP                      => 0,
         IO_Bitmap                => IO_Bitmap,
         MSR_Bitmap               => MSR_Bitmap,
         Subject_Assigned_Devices => Types.Root.Subject_Device_Table.Empty_Table,
         Assigned_IRQs            => Types.Root.IRQ_Table.Empty_Table,
         Attached_Tables          => 0,
         Paging                   => Paging,
         Subject_CPU              => CPU);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
      pragma Assert (Invariant_IDs_Device_Domain (Roots));

      Data.Memory.Use_Page (Page => MSR_Bitmap);
      pragma Assert (Data.Memory.Get_Typization_Model (IO_Bitmap) = T.Zeroed);
      Data.Memory.Use_Page (Page => IO_Bitmap);
      pragma Assert (Data.Memory.Get_Typization_Model
                       (Types.Successor_Page (IO_Bitmap)) = T.Zeroed);
      Data.Memory.Use_Page (Page => Types.Successor_Page (IO_Bitmap));

      Data.Memory.Full_Typization.IO_Bitmap_Low_Lemma (IO_Bitmap);
      Data.Memory.Full_Typization.IO_Bitmap_High_Lemma (IO_Bitmap_High);
      Data.Memory.Full_Typization.MSR_Bitmap_Lemma (MSR_Bitmap);

      Data.Memory.Accessibility.Subject_Bitmap_Lemma (Root, IO_Bitmap);
      Data.Memory.Accessibility.Subject_Bitmap_Lemma (Root, IO_Bitmap_High);
      Data.Memory.Accessibility.Subject_Bitmap_Lemma (Root, MSR_Bitmap);

      --  The bitmaps follow an inverted logic, we must set them up.
      Data.Memory.Bitmaps.Set_IO_Port_Range
        (IO_Bitmap => IO_Bitmap,
         From      => Types.IO_Port_Type'First,
         To        => Types.IO_Port_Type'Last,
         Mode      => Types.IO_Bitmap.Denied);
      Data.Memory.Bitmaps.Set_MSR_Range
        (MSR_Bitmap => MSR_Bitmap,
         From       => Types.MSR_Bitmap.MSR_Low_Range'First,
         To         => Types.MSR_Bitmap.MSR_Low_Range'Last,
         Mode       => Types.MSR_Bitmap.Denied);
      Data.Memory.Bitmaps.Set_MSR_Range
        (MSR_Bitmap => MSR_Bitmap,
         From       => Types.MSR_Bitmap.MSR_High_Range'First,
         To         => Types.MSR_Bitmap.MSR_High_Range'Last,
         Mode       => Types.MSR_Bitmap.Denied);
   end Create_Subject;

   -----------------------------------------------------------------------------

   procedure Set_IO_Port_Range
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type;
      Mode   : Types.IO_Bitmap.Mode_Type)
   is
      pragma Unreferenced (Device);
   begin
      Data.Memory.Full_Typization.IO_Bitmap_Low_Lemma (IO_Bitmap_Low (Root));
      Data.Memory.Full_Typization.IO_Bitmap_High_Lemma (IO_Bitmap_High (Root));
      Data.Memory.Accessibility.Subject_Bitmap_Lemma (Root, IO_Bitmap_Low (Root));
      Data.Memory.Accessibility.Subject_Bitmap_Lemma (Root, IO_Bitmap_High (Root));

      Data.Memory.Bitmaps.Set_IO_Port_Range
        (IO_Bitmap => IO_Bitmap_Low (Root),
         From      => From,
         To        => To,
         Mode      => Mode);
   end Set_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Set_MSR_Range
     (Root : Types.Root_Range;
      From : Types.MSR_Bitmap.MSR_Range;
      To   : Types.MSR_Bitmap.MSR_Range;
      Mode : Types.MSR_Bitmap.Mode_Type)
   is
   begin
      Data.Memory.Full_Typization.MSR_Bitmap_Lemma (MSR_Bitmap (Root));
      Data.Memory.Accessibility.Subject_Bitmap_Lemma (Root, MSR_Bitmap (Root));

      Data.Memory.Bitmaps.Set_MSR_Range
        (MSR_Bitmap => MSR_Bitmap (Root),
         From       => From,
         To         => To,
         Mode       => Mode);
   end Set_MSR_Range;

end Tau0.Data.Roots.Writers.Subjects;
