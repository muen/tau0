--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Roots.Writers.Device_Domains;
with Tau0.Data.Roots.Writers.Kernels;
with Tau0.Data.Roots.Writers.Memory_Regions;
with Tau0.Data.Roots.Writers.Subjects;

package body Tau0.Data.Roots.Writers
is

   -----------------------------------------------------------------------------

   procedure Activate (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      case Roots (Root).Kind is
         when Types.Root.Memory_Region =>
            Memory_Regions.Activate (Root => Root);
         when Types.Root.Subject =>
            Subjects.Activate (Root => Root);
         when Types.Root.Device_Domain =>
            Device_Domains.Activate (Root => Root);
         when Types.Root.Kernel =>
            Kernels.Activate (Root => Root);
      end case;

      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
   end Activate;

   -----------------------------------------------------------------------------

   procedure Activate_PTP (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root).PTP_Active := True;

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
   end Activate_PTP;

   -----------------------------------------------------------------------------

   procedure Assign_Device
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
   is
      use type Types.Device.Device_Range;

      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      case Kind (Root) is
         when Types.Root.Subject =>
            Types.Root.Subject_Device_Table.Push
              (Table => Roots (Root).Subject_Assigned_Devices,
               Elem  => Device);
         when Types.Root.Kernel =>
            Types.Root.Kernel_Device_Table.Push
              (Table => Roots (Root).Kernel_Assigned_Devices,
               Elem  => Device);
         when others =>
            pragma Assert (False, "Precluded by precondition");
      end case;

      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);

      pragma Assert
        (for all U in Roots'Range =>
        (if U /= Root and
            Roots (U).State /= Types.Root.Unused
         then (if Roots (U).Kind = Types.Root.Subject
               then (for all Device of Roots (U).Subject_Assigned_Devices =>
                     Data.Devices.State (Device) = Types.Device.Active)
               elsif Roots (U).Kind = Types.Root.Kernel
               then (for all Device of Roots (U).Kernel_Assigned_Devices =>
                     Data.Devices.State (Device) = Types.Device.Active))));

      pragma Assert
        (if Kind (Root) = Types.Root.Subject
         then (for all D of Roots (Root).Subject_Assigned_Devices =>
              (if D /= Device
               then Data.Devices.State (D) = Types.Device.Active))
         elsif Kind (Root) = Types.Root.Kernel
         then (for all D of Roots (Root).Kernel_Assigned_Devices =>
              (if D /= Device
               then Data.Devices.State (D) = Types.Device.Active)));

      pragma Assert (Invariant_Assigned_Devices (Roots));
   end Assign_Device;

   -----------------------------------------------------------------------------

   procedure Lemma_CPUs
     (Old_Roots : Types.Root.Root_Array;
      New_Roots : Types.Root.Root_Array;
      CPUs      : CPU_Array)
   is null;

   -----------------------------------------------------------------------------

   procedure Lock (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      case Roots (Root).Kind is
         when Types.Root.Memory_Region =>
            Roots (Root) := Types.Root.Root_Type'
              (State            => Types.Root.Locked,
               Is_Memory_Region => True,
               Kind             => Types.Root.Memory_Region,
               PTP              => Roots (Root).PTP,
               PTP_Present      => Roots (Root).PTP_Present,
               PTP_Active       => Roots (Root).PTP_Active,
               Region_Level     => Roots (Root).Region_Level,
               Length           => Roots (Root).Length,
               Caching          => Roots (Root).Caching,
               Hash             => Roots (Root).Hash,
               Ref_Counter      => Roots (Root).Ref_Counter);
         when Types.Root.Subject =>
            Roots (Root) := Types.Root.Root_Type'
              (State            => Types.Root.Locked,
               Is_Memory_Region         => False,
               Kind                     => Types.Root.Subject,
               PTP                      => Roots (Root).PTP,
               PTP_Present              => Roots (Root).PTP_Present,
               PTP_Active               => Roots (Root).PTP_Active,
               IO_Bitmap                => Roots (Root).IO_Bitmap,
               MSR_Bitmap               => Roots (Root).MSR_Bitmap,
               Attached_Tables          => Roots (Root).Attached_Tables,
               Subject_Assigned_Devices => Roots (Root).Subject_Assigned_Devices,
               Assigned_IRQs            => Roots (Root).Assigned_IRQs,
               Paging                   => Roots (Root).Paging,
               Subject_CPU              => Roots (Root).Subject_CPU);
         when Types.Root.Device_Domain =>
            Roots (Root) := Types.Root.Root_Type'
              (State            => Types.Root.Locked,
               Is_Memory_Region => False,
               Kind             => Types.Root.Device_Domain,
               PTP              => Roots (Root).PTP,
               PTP_Present      => Roots (Root).PTP_Present,
               PTP_Active       => Roots (Root).PTP_Active,
               Attached_Tables  => Roots (Root).Attached_Tables,
               Domain_Devices   => Roots (Root).Domain_Devices,
               Domain_Level     => Roots (Root).Domain_Level,
               Domain_ID        => Roots (Root).Domain_ID);
         when Types.Root.Kernel =>
            Roots (Root) := Types.Root.Root_Type'
              (State                   => Types.Root.Locked,
               Is_Memory_Region        => False,
               Kind                    => Types.Root.Kernel,
               PTP                     => Roots (Root).PTP,
               PTP_Present             => Roots (Root).PTP_Present,
               PTP_Active              => Roots (Root).PTP_Active,
               Attached_Tables         => Roots (Root).Attached_Tables,
               Kernel_CPU              => Roots (Root).Kernel_CPU,
               Kernel_Assigned_Devices => Roots (Root).Kernel_Assigned_Devices);
      end case;

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
   end Lock;

   -----------------------------------------------------------------------------

   procedure Increment_Attached_Region_Counter (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root).Attached_Tables := Roots (Root).Attached_Tables + 1;

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
   end Increment_Attached_Region_Counter;

   -----------------------------------------------------------------------------

   procedure Set_PTP
     (Root    : Types.Root_Range;
      Address : Types.Tau0_Address_Type)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root).PTP         := Address;
      Roots (Root).PTP_Present := True;

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots, New_Roots => Roots);
   end Set_PTP;

end Tau0.Data.Roots.Writers;
