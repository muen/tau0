--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Unchecked_Conversion;
with Tau0.Data.Memory.Full_Typization;

package body Tau0.Data.Memory.Interrupt_Remapping
with Refined_State => (State => (Table_Present_Flag, Table_Pointer))
is

   type Bit_Array is array (Natural range <>) of Types.Bit
   with Component_Size => 1;

   type Null_Array is array (Natural range <>) of Types.Null_Bit
   with Default_Component_Value => 0, Component_Size => 1;

   SID_SQ_Verification : constant Bit_Array (1 .. 2) := (1 => 1, 2 => 0);

   type IR_Entry_Type is record
      Present    : Types.Bit             := 0;
      FPD        : Types.Bit             := 0;
      DM         : Types.Bit             := 0;
      RH         : Types.Bit             := 0;
      TM         : Types.Bit             := 0;
      DLM        : Null_Array (1 .. 3);
      AVAIL      : Null_Array (1 .. 4);
      Reserved_1 : Null_Array (1 .. 4);
      V          : Types.Device.IRQ_Type := 0;
      Reserved_2 : Types.Byte            := 0;
      DST        : Types.APIC_ID_Type    := 0;
      SID        : Types.Source_ID_Type  := 0;
      SQ         : Null_Array (1 .. 2);
      SVT        : Bit_Array (1 .. 2)    := SID_SQ_Verification;
      Reserved_3 : Null_Array (1 .. 44);
   end record
   with Size => 128;

   for IR_Entry_Type use record
      Present    at 0 range  0 ..  0;
      FPD        at 0 range  1 ..  1;
      DM         at 0 range  2 ..  2;
      RH         at 0 range  3 ..  3;
      TM         at 0 range  4 ..  4;
      DLM        at 0 range  5 ..  7;
      AVAIL      at 0 range  8 .. 11;
      Reserved_1 at 0 range 12 .. 15;
      V          at 0 range 16 .. 23;
      Reserved_2 at 0 range 24 .. 31;
      DST        at 0 range 32 .. 63;
      SID        at 0 range 64 .. 79;
      SQ         at 0 range 80 .. 81;
      SVT        at 0 range 82 .. 83;
      Reserved_3 at 0 range 84 .. 127;
   end record;

   function To_Word128 is new Ada.Unchecked_Conversion
     (Source => IR_Entry_Type,
      Target => Types.Word128);

   -----------------------------------------------------------------------------

   subtype Table_Pointer_Type is Types.Physical_Address_Type
     with Dynamic_Predicate => Types.Aligned_Page (Table_Pointer_Type);

   Table_Present_Flag : Boolean            := False;
   Table_Pointer      : Table_Pointer_Type := 0;

   -----------------------------------------------------------------------------

   procedure Create_Table (Page : Types.Physical_Address_Type)
   is
   begin
      Use_Page (Page => Page);

      pragma Assert (Get_Typization_Model (Page) = T.Used);
      pragma Assert (Get_Typization (Page) = T.Used);

      Table_Present_Flag := True;
      Table_Pointer      := Page;
   end Create_Table;

   -----------------------------------------------------------------------------

   function Get_IRQ_Kind
     (Device : Types.Device.Device_Range)
      return IRQ_Kind_Type
   is (if Data.Devices.Is_PCI (Device => Device) then
        (if Data.Devices.Uses_MSI (Device => Device)
         then IRQ_PCI_MSI
         else IRQ_PCI_LSI)
       else IRQ_ISA);

   -----------------------------------------------------------------------------

   function Get_Source_ID
     (Device : Types.Device.Device_Range)
      return Types.Source_ID_Type
   is
   begin
      if Data.Devices.Uses_IOAPIC (Device => Device) then
         return Data.IO_APICs.Source_ID;
      else
         declare
            use type Tau0.Types.Source_ID_Type;

            Bus  : constant Types.Device.PCI_Bus_Range
              := Data.Devices.PCI_Bus (Device => Device);
            Dev  : constant Types.Device.PCI_Device_Range
              := Data.Devices.PCI_Dev (Device => Device);
            Func : constant Types.Device.PCI_Function_Range
              := Data.Devices.PCI_Func (Device => Device);
         begin
            return Types.Source_ID_Type (Bus) * 2 ** 8 +
                   Types.Source_ID_Type (Dev) * 2 ** 3 +
                   Types.Source_ID_Type (Func);
         end;
      end if;
   end Get_Source_ID;

   -----------------------------------------------------------------------------

   procedure Remap_Interrupt
     (Device    : Types.Device.Device_Range;
      Interrupt : Types.Device.HW_IRQ_Type;
      CPU       : Types.CPU_Range)
   is
      use type Types.Device.IRQ_Type;

      Host_IRQ_Remap_Offset : constant := 32;
      IRQ_Kind              : constant IRQ_Kind_Type
        := Get_IRQ_Kind (Device);
      Remapped_Interrupt    : constant Types.Device.IRQ_Type
        := Interrupt + Host_IRQ_Remap_Offset;
      Destination           : constant Types.APIC_ID_Type
        := Data.Processors.APIC_ID (ID => CPU);
      Source_ID             : constant Types.Source_ID_Type
        := Get_Source_ID (Device => Device);
   begin
      Set_IRTE
        (Src_Vector   => Interrupt,
         Dst_Vector   => Remapped_Interrupt,
         Destination  => Destination,
         Source_ID    => Source_ID,
         Trigger_Mode => (case IRQ_Kind is
                          when IRQ_ISA     => Edge,
                          when IRQ_PCI_MSI => Edge,
                          when IRQ_PCI_LSI => Level));
   end Remap_Interrupt;

   -----------------------------------------------------------------------------

   procedure Set_IRTE
     (Src_Vector   : Types.Device.IRQ_Type;
      Dst_Vector   : Types.Device.IRQ_Type;
      Destination  : Types.APIC_ID_Type;
      Source_ID    : Types.Source_ID_Type;
      Trigger_Mode : Trigger_Mode_Type)
   is
      subtype Page_Offset_Type is Types.Physical_Address_Type
        range 0 .. Config.Page_Size - 16;

      Offset   : constant Page_Offset_Type
        := Types.Physical_Address_Type (Src_Vector) * 16;
      Address  : constant Types.Physical_Address_Type := Table_Address + Offset;
      IR_Entry : IR_Entry_Type;
      Word     : Types.Word128;
   begin
      IR_Entry.Present := 1;
      IR_Entry.V       := Dst_Vector;
      IR_Entry.DST     := Destination;
      IR_Entry.SID     := Source_ID;
      IR_Entry.TM      := (case Trigger_Mode is
                           when Edge  => 0,
                           when Level => 1);
      Word             := To_Word128 (IR_Entry);

      Full_Typization.VTd_IR_Table_Lemma (Address);
      Low_Level.Set (Address => Address, Value => Word);
   end Set_IRTE;

   -----------------------------------------------------------------------------

   --  TODO: destructor!
   --  procedure Unset_IRTE (Src_Vector : Types.Device.IRQ_Type)
   --  is null;

   -----------------------------------------------------------------------------

   function Table_Address return Types.Physical_Address_Type
   is (Table_Pointer);

   -----------------------------------------------------------------------------

   function Table_Present return Boolean
   is (Table_Present_Flag);

end Tau0.Data.Memory.Interrupt_Remapping;
