--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;
with Tau0.Types.Device;

package Tau0.Types.VTd_Tables
with Abstract_State => null
is

   type Root_Entry_Type (Present : Boolean := False) is record
      case Present is
         when True =>
            Context_Table_Address : Types.Physical_Address_Type;
         when False =>
            null;
      end case;
   end record
   with Dynamic_Predicate =>
     (if Root_Entry_Type.Present
      then Aligned_Page (Root_Entry_Type.Context_Table_Address));

   type Root_Table_Model_Type
      is array (Types.Device.PCI_Bus_Range) of Root_Entry_Type
   with Ghost;

   function To_Word128 (Entr : Root_Entry_Type) return Types.Word128
   with
     Global => null,
     Post   => From_Word128 (To_Word128'Result) = Entr;

   function From_Word128 (Word : Word128) return Root_Entry_Type
   with
     Global => null,
     Post => (if Word = (0, 0) then not From_Word128'Result.Present);

   --  FIXME prove
   procedure Root_Entry_From_To_Lemma (Entr : Root_Entry_Type)
   with
     Ghost,
     Post => From_Word128 (To_Word128 (Entr)) = Entr;

   -----------------------------------------------------------------------------

   type Address_Width_Type is (Width_39_Bit, Width_48_Bit);

   type Domain_Identifier_Type is range  0 .. 2 ** 16 - 1;

   type Context_Entry_Type (Present : Boolean := False) is record
      case Present is
         when True =>
            SLPTPTR           : Physical_Address_Type;
            Address_Width     : Address_Width_Type;
            Domain_Identifier : Domain_Identifier_Type;
         when False =>
            null;
      end case;
   end record
   with Dynamic_Predicate =>
     (if Context_Entry_Type.Present
      then Aligned_Page (Context_Entry_Type.SLPTPTR));

   function To_Word128 (Entr : Context_Entry_Type) return Types.Word128
   with
     Global => null,
     Post   => From_Word128 (To_Word128'Result) = Entr;

   function From_Word128 (Word : Word128) return Context_Entry_Type
   with
     Global => null,
     Post   => (if Word = (0, 0) then not From_Word128'Result.Present);

private

   HAW          : constant := Config.Bits_Physical_Address;
   Present_Bit  : constant := 2 ** 0;
   Address_Mask : constant := 2 ** HAW - 2 ** 12;

   Address_Width_Mask : constant := 2 ** (66 + 1 - 64) - 2 ** (64 - 64);
   Domain_ID_Offset   : constant := 2 ** (72 - 64);
   Domain_ID_Mask     : constant := 2 ** (87 + 1 - 64) - Domain_ID_Offset;

   -----------------------------------------------------------------------------

   function Word_To_Address (Word : Word64) return Physical_Address_Type
   with Post => Aligned_Page (Word_To_Address'Result);

   -----------------------------------------------------------------------------

   function From_Word128 (Word : Word128) return Context_Entry_Type
   is (if (Word (0) and Present_Bit) = 1
       then Context_Entry_Type'
              (Present           => True,
               SLPTPTR           => Word_To_Address (Word (0)),
               Address_Width     =>
                 (case (Word (1) and Address_Width_Mask) is
                  when 2#001# => Width_39_Bit,
                  when 2#010# => Width_48_Bit,
                  when others => Width_48_Bit),  --  FIXME: handle error!
               Domain_Identifier => Domain_Identifier_Type
                 ((Word (1) and Domain_ID_Mask) / Domain_ID_Offset))
       else Context_Entry_Type'(Present => False));

   -----------------------------------------------------------------------------

   function From_Word128 (Word : Word128) return Root_Entry_Type
   is (if (Word (0) and Present_Bit) = 1
       then Root_Entry_Type'
              (Present               => True,
               Context_Table_Address => Word_To_Address (Word (0)))
       else Root_Entry_Type'(Present => False));

   -----------------------------------------------------------------------------

   procedure Root_Entry_From_To_Lemma (Entr : Root_Entry_Type)
   is null;

   -----------------------------------------------------------------------------

   function To_Word128 (Entr : Context_Entry_Type) return Types.Word128
   is (if Entr.Present
       then (0 => Word64 (Present_Bit or Entr.SLPTPTR),
             1 => Word64 (case Entr.Address_Width is
                          when Width_39_Bit => 2#001#,
                          when Width_48_Bit => 2#010#) or
                  Word64 (Entr.Domain_Identifier * Domain_ID_Offset))
       else (0 => 0, 1 => 0));

   -----------------------------------------------------------------------------

   function To_Word128 (Entr : Root_Entry_Type) return Types.Word128
   is (0 => (if Entr.Present
             then Word64 (Present_Bit or Entr.Context_Table_Address)
             else 0),
       1 => 0);

   -----------------------------------------------------------------------------

   function Word_To_Address (Word : Word64) return Physical_Address_Type
   is (Physical_Address_Type (Word and Address_Mask));

end Tau0.Types.VTd_Tables;
