--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands.Devices;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Devices
is

   -----------------------------------------------------------------------------

   procedure Activate
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Device_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Devices.Activate
           (Device => Command.Activate_Device_Params.Device,
            Status => Status);
      end if;
   end Activate;

   -----------------------------------------------------------------------------

   procedure Add_IO_Port_Range
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Add_IO_Port_Range_Device_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Devices.Add_IO_Port_Range
           (Device => Command.Add_IO_Port_Range_Device_Params.Device,
            From   => Command.Add_IO_Port_Range_Device_Params.From,
            To     => Command.Add_IO_Port_Range_Device_Params.To,
            Status => Status);
      end if;
   end Add_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Add_IRQ
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Add_IRQ_Device_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Devices.Add_IRQ
           (Device => Command.Add_IRQ_Device_Params.Device,
            IRQ    => Command.Add_IRQ_Device_Params.IRQ,
            Status => Status);
      end if;
   end Add_IRQ;

   -----------------------------------------------------------------------------

   procedure Add_Memory
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Add_Memory_Device_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Devices.Add_Memory
           (Device  => Command.Add_Memory_Device_Params.Device,
            Address => Command.Add_Memory_Device_Params.Address,
            Size    => Command.Add_Memory_Device_Params.Size,
            Caching => Command.Add_Memory_Device_Params.Caching,
            Status  => Status);
      end if;
   end Add_Memory;

   -----------------------------------------------------------------------------

   procedure Create_Legacy_Device
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Legacy_Device_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Devices.Create_Legacy_Device
           (Device => Command.Create_Legacy_Device_Params.Device,
            Status => Status);
      end if;
   end Create_Legacy_Device;

   -----------------------------------------------------------------------------

   procedure Create_PCI_Device
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_PCI_Device_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Devices.Create_PCI_Device
           (Device      => Command.Create_PCI_Device_Params.Device,
            PCI_Bus     => Command.Create_PCI_Device_Params.PCI_Bus,
            PCI_Dev     => Command.Create_PCI_Device_Params.PCI_Dev,
            PCI_Func    => Command.Create_PCI_Device_Params.PCI_Func,
            IOMMU_Group => Command.Create_PCI_Device_Params.IOMMU_Group,
            Uses_MSI    => Command.Create_PCI_Device_Params.Uses_MSI,
            Status      => Status);
      end if;
   end Create_PCI_Device;

end Tau0.Command_Processor.Devices;
