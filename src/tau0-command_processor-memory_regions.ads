--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Command;

package Tau0.Command_Processor.Memory_Regions
with Abstract_State => null
is

   use type Types.Command.Command_ID_Type;

   -----------------------------------------------------------------------------

   procedure Create_Memory_Region
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Create_Memory_Region,
     Post => Invariants;

   procedure Lock_Memory_Region
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Lock_Memory_Region,
     Post => Invariants;

   procedure Activate_Memory_Region
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Activate_Memory_Region,
     Post => Invariants;

   procedure Create_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Create_Page_Table_MR,
     Post => Invariants;

   procedure Append_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Append_Page_MR,
     Post => Invariants;

   procedure Append_Vacuous_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Append_Vacuous_Page_MR,
     Post => Invariants;

   procedure Activate_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Activate_Page_MR,
     Post => Invariants;

   procedure Activate_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Activate_Page_Table_MR,
     Post => Invariants;

end Tau0.Command_Processor.Memory_Regions;
