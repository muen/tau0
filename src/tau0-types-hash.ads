--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private with GNAT.SHA256;

package Tau0.Types.Hash
is

   subtype Digest_Type is String (1 .. 64);
   type Context_Type is private;

   Initial_Context : constant Context_Type;

   function Digest (Context : Context_Type) return Digest_Type
   with
     Global   => null,
     Annotate => (GNATprove, Terminating);

   procedure Update
     (Context : in out Context_Type;
      Input   :        Types.Page_Array)
   with
     Global   => null,
     Annotate => (GNATprove, Terminating);

private

   type Context_Type is new GNAT.SHA256.Context;

   Initial_Context : constant Context_Type
     := Context_Type (GNAT.SHA256.Initial_Context);

end Tau0.Types.Hash;
