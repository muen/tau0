--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.IO_APICs;
with Tau0.Data.Memory.Interrupt_Remapping;
with Tau0.Data.Memory.Paging;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Memory_Region_Table;

package Tau0.Data.Roots.Writers
is

   use type Types.Root.Root_Array;

   -----------------------------------------------------------------------------

   --  Set the page table pointer of Root to Address
   procedure Set_PTP
     (Root    : Types.Root_Range;
      Address : Types.Tau0_Address_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               not PTP_Present (Root) and then
               Types.Aligned_Page (Address) and then
               (if Kind (Root) = Types.Root.Memory_Region and Level (Root) > 1
                then Address in Types.Tau0_Private_Address_Type
                else Address in Types.Physical_Address_Type),
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               Get_Roots (Root) = (Get_Roots (Root) with delta
                  PTP_Present => True,
                  PTP         => Address);

   --  Activate the top-level page table of Root.  All top-level PTEs of Root
   --  (if applicable) must be active.
   procedure Activate_PTP (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               Data.Roots.State (Root) = Types.Root.Locked and then
               PTP_Present (Root) and then
               not PTP_Active (Root) and then
               (if Level (Root) > 1
                then Memory.Paging.Toplevel_Page_Table_Entries_Active (Root)),
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               Get_Roots (Root) = (Get_Roots (Root)'Old with delta PTP_Active => True);

   --  Set state of Root to Locked
   procedure Lock (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               PTP_Present (Root) and then
               (if Kind (Root) = Types.Root.Memory_Region and then
                   Must_Check_Hash (Root)
                then Hash_Valid (Root)),
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Update may not modify discriminants
               State (Root) = Types.Root.Locked and
               Kind (Root) = Kind (Root)'Old and
               PTP_Present (Root) = PTP_Present (Root)'Old and
               PTP_Active (Root) = PTP_Active (Root)'Old and
               PTP (Root) = PTP (Root)'Old;

   --  Set state of Root to Active
   procedure Activate (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Typization_State,
                             Data.Memory_Region_Table.State),
                Input    => (Data.Devices.State,
                             Data.IO_APICs.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory.Interrupt_Remapping.State,
                             Data.Processors.State),
                In_Out   => (Data.Memory.Memory_State, State)),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Locked and then
               not Active (Root) and then
               PTP_Active (Root) and then
               (if Kind (Root) = Types.Root.Device_Domain
                then VTd_Tables_Valid (Root)
                elsif Kind (Root) = Types.Root.Subject
                then (if Assigned_IRQs (Root) > 0
                      then Data.Memory.Interrupt_Remapping.Table_Present)),
     Post   => Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Update may not modify discriminants
               State (Root) = Types.Root.Active and
               Kind (Root) = Kind (Root)'Old and
               PTP_Present (Root) = PTP_Present (Root)'Old and
               PTP_Active (Root) = PTP_Active (Root)'Old and
               PTP (Root) = PTP (Root)'Old;
               --  TODO: maybe we must state more about paging

   procedure Increment_Attached_Region_Counter (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) /= Types.Root.Memory_Region and then
               Attached_Memory_Regions (Root) < Natural'Last,
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Get_Roots = (Get_Roots'Old with delta
                 Root => (Get_Roots'Old (Root) with delta
                    Attached_Tables => Attached_Memory_Regions (Root)'Old + 1));

   procedure Assign_Device
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) in Types.Root.Subject | Types.Root.Kernel and then
               not Device_Table_Full (Root) and then
               Device_Assignable (Root, Device) and then
               Data.Devices.State (Device) = Types.Device.Active,
     --  FIXME: strengthen postcondition -- 'Update for Get_Roots (Root)
     Post   => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and then
               State (Root) = State (Root)'Old and then
               Kind (Root) = Kind (Root)'Old and then
               Device_Assigned (Root, Device);

private

   --  Used to prove invariant postcondition
   procedure Lemma_CPUs
     (Old_Roots : Types.Root.Root_Array;
      New_Roots : Types.Root.Root_Array;
      CPUs      : CPU_Array)
   with
     Ghost,
     Global => null,
     Pre    => (for all Root in Types.Root_Range =>
                --  a root has been created, but it is no kernel
                (Old_Roots (Root).State = Types.Root.Unused and
                 New_Roots (Root).State = Types.Root.Setup and
                 New_Roots (Root).Kind /= Types.Root.Kernel) or
                --  the root has neither been created nor destroyed, and its kind
                --  is unchanged
                ((Old_Roots (Root).State = Types.Root.Unused)
                   = (New_Roots (Root).State = Types.Root.Unused) and
                 Old_Roots (Root).Kind = New_Roots (Root).Kind)) and then
               (for all Root in Types.Root_Range =>
                --  kernel has not moved between CPUs
                (if Old_Roots (Root).State /= Types.Root.Unused and
                    Old_Roots (Root).Kind = Types.Root.Kernel and
                    New_Roots (Root).State /= Types.Root.Unused and
                    New_Roots (Root).Kind = Types.Root.Kernel
                 then Old_Roots (Root).Kernel_CPU
                        = New_Roots (Root).Kernel_CPU)) and then
               Invariant_CPUs_Correct (Old_Roots, CPUs),
     Post   => Invariant_CPUs_Correct (New_Roots, CPUs);

end Tau0.Data.Roots.Writers;
