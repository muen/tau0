--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types;

package Debug
is

   pragma Annotate (GNATprove, Terminating, Debug);

   type Level_Type is (Info, Notice, Warning, Error);
   type Side_Type is (Left, Right, Both);

   procedure Put (Item : String; Level : Level_Type := Info)
   with Global => null;                 --  REVIEW

   procedure Put_Line (Item : String; Level : Level_Type := Info)
   with Global => null;

   procedure New_Line (Level : Level_Type := Info)
   with Global => null;

   procedure Put
     (Word      : Tau0.Types.Word64;
      Base      : Positive   := 16;
      Level     : Level_Type := Info;
      Justified : Boolean    := True)
   with Global => null;

   procedure Put
     (Address : Tau0.Types.Tau0_Address_Type;
      Level   : Level_Type := Info)
   with Global => null;

   procedure Put
     (Address : Tau0.Types.Virtual_Address_Type;
      Level   : Level_Type := Info)
   with Global => null;

   procedure Put
     (Page_Level : Tau0.Types.Level_Type;
      Level      : Level_Type := Info)
   with Global => null;

   procedure Put
     (Root  : Tau0.Types.Root_Range;
      Level : Level_Type := Info)
   with Global => null;

   function Is_Whitespace (C : Character) return Boolean;

   function Trim (Source : String; Side : Side_Type) return String
   with
     Pre  => Source'Last < Integer'Last,
     Post => (if Source = ""
              then Trim'Result = ""
              else (for some I in Source'First .. Source'Last + 1 =>
                   (for some J in Source'First - 1 .. Source'Last =>
                      (if Side = Right then I = Source'First) and
                      (if Side = Left then J = Source'Last) and
                      (for all K in Source'First .. I - 1 =>
                       Is_Whitespace (Source (K))) and
                      (for all K in J + 1 .. Source'Last =>
                       Is_Whitespace (Source (K))) and
                      (if I in Source'Range and Side in Left | Both
                       then not Is_Whitespace (Source (I))) and
                      (if J in Source'Range and Side in Right | Both
                       then not Is_Whitespace (Source (J))) and
                      Trim'Result = Source (I .. J))));

   --  TODO: extend

private

   function Get_Current_Level return Level_Type;

end Debug;
