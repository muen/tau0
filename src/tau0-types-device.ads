--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Bounded_Table;
with Tau0.Config;
with IO_Port_Set;

package Tau0.Types.Device
with Abstract_State => null
is

   type PCI_Bus_Range      is range 0 .. 255;
   type PCI_Device_Range   is range 0 .. 31;
   type PCI_Function_Range is range 0 .. 7;

   type IOMMU_Group_Type is new Natural; --  FIXME
   No_IOMMU_Group : constant IOMMU_Group_Type := IOMMU_Group_Type'Last;

   type Classcode_Type is range 0 .. 2 ** 16 - 1;
   type Vendor_ID_Type is range 0 .. 2 ** 16 - 1;
   type Device_ID_Type is range 0 .. 2 ** 16 - 1;
   type Revision_ID_Type is range 0 .. 2 ** 8 - 1;

   type PCI_Type is record
      Bus         : PCI_Bus_Range;
      Device      : PCI_Device_Range;
      Func        : PCI_Function_Range;
      IOMMU_Group : IOMMU_Group_Type;
      Classcode   : Classcode_Type;
      Vendor_ID   : Vendor_ID_Type;
      Device_ID   : Device_ID_Type;
      Revision_ID : Revision_ID_Type;
      Uses_MSI    : Boolean;
   end record;

   -----------------------------------------------------------------------------

   type IRQ_Type is range 0 .. 255;
   subtype HW_IRQ_Type is IRQ_Type range 0 .. 220; --  REVIEW

   package IRQ_Table is new Bounded_Table
     (Max       => Config.Max_IRQs_Per_Device,
      Elem_Type => HW_IRQ_Type,
      Null_Elem => 0);

   -----------------------------------------------------------------------------

   type Memory_Type is record
      Physical_Address : Physical_Address_Type;
      Size             : Physical_Address_Type;
      Caching          : Caching_Type;
   end record
   with Dynamic_Predicate =>
     Aligned_Page (Physical_Address) and Aligned_Page (Size);

   package Memory_Table is new Bounded_Table
     (Max       => Config.Max_Memory_Regions_Per_Device,
      Elem_Type => Memory_Type,
      Null_Elem => Memory_Type'
        (Physical_Address => 0,
         Size             => 0,
         Caching          => UC));

   -----------------------------------------------------------------------------

   type State_Type is (Unused, Setup, Active);

   type Device_Type (State : State_Type := Unused; Is_PCI : Boolean := False) is
      record
         case State is
            when Unused =>
               null;
            when Setup | Active =>
               IRQs     : IRQ_Table.T;
               Memory   : Memory_Table.T;
               IO_Ports : IO_Port_Set.T;

               case Is_PCI is
                  when True  => PCI : PCI_Type;
                  when False => null;
               end case;
         end case;
      end record;

   Null_Device : constant Device_Type := Device_Type'
     (State  => Unused,
      Is_PCI => False);

   --  True iff L and R have distinct PCI B/D/Fs (where applicable)
   function PCI_BDFs_Distinct (L, R : Device_Type) return Boolean
   is (L.State = Unused or else
       R.State = Unused or else
       not L.Is_PCI or else
       not R.Is_PCI or else
       not (L.PCI.Bus    = R.PCI.Bus and
            L.PCI.Device = R.PCI.Device and
            L.PCI.Func   = R.PCI.Func));

   --  True iff L and R have no interrupt in common
   function IRQs_Disjoint (L, R : Device_Type) return Boolean
   is (L.State = Unused or else
       R.State = Unused or else
         (for all I of L.IRQs =>
         (for all J of R.IRQs =>
            I /= J)));

   --  True iff L and R have no IO port in common
   function IO_Ports_Disjoint (L, R : Device_Type) return Boolean
   is (L.State = Unused or else
       R.State = Unused or else
       (for all I in IO_Port_Type =>
        not (IO_Port_Set.Element (Elem => I, Set => L.IO_Ports) and
             IO_Port_Set.Element (Elem => I, Set => R.IO_Ports))));

   --  True iff memory intervals L and R overlap
   function Memory_Overlapping (L, R : Memory_Type) return Boolean
   is (for some P in Tau0_Address_Type =>
       P in L.Physical_Address .. L.Physical_Address + L.Size - 1 and
       P in R.Physical_Address .. R.Physical_Address + R.Size - 1);

   --  True iff L and R have no memory cell in common
   function Memory_Disjoint (L, R : Device_Type) return Boolean
   is (L.State = Unused or else
       R.State = Unused or else
         (for all LM of L.Memory =>
         (for all RM of R.Memory =>
         (not Memory_Overlapping (LM, RM)))));

   type Device_Base_Range is range 0 .. 2 ** 8 - 1;
   subtype Device_Range is Device_Base_Range range 0 .. Config.Max_Devices - 1;

   type Device_Array_Base is array (Device_Range) of Device_Type;

   --  Ensure that no two distinct devices in array Arr have
   --  - the same PCI B/D/F (if applicable),
   --  - a common IRQ,
   --  - a common IO port, or
   --  - common device memory
   function Resources_Pairwise_Disjoint (Arr : Device_Array_Base) return Boolean
   is (for all L in Arr'Range =>
      (for all R in Arr'Range =>
      (if not PCI_BDFs_Distinct (Arr (L), Arr (R)) or
          not IRQs_Disjoint (Arr (L), Arr (R)) or
          not IO_Ports_Disjoint (Arr (L), Arr (R)) or
          not Memory_Disjoint (Arr (L), Arr (R))
       then L = R)));

   subtype Device_Array is Device_Array_Base
   with Dynamic_Predicate => Resources_Pairwise_Disjoint (Device_Array);

end Tau0.Types.Device;
