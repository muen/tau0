--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.Full_Typization;

package body Tau0.Data.Memory.VTd_IOMMU.Writers
is

   -----------------------------------------------------------------------------

   procedure Add_Device
     (Device : Types.Device.Device_Range;
      Domain : Types.Root_Range)
   is
      AW : constant Address_Width_Type
        := (if Data.Roots.Level (Root => Domain) - 1 = 3 then Width_39_Bit
            else Width_48_Bit);
      E : constant Context_Entry_Type := Context_Entry_Type'
        (Present                  => True,
         SLPTPTR                  => Data.Roots.PTP (Root => Domain),
         Address_Width            => AW,
         Domain_Identifier        => Data.Roots.Domain_ID (Domain));
   begin
      Set_Context_Table_Entry
        (Bus  => Data.Devices.PCI_Bus (Device),
         Dev  => Data.Devices.PCI_Dev (Device),
         Func => Data.Devices.PCI_Func (Device),
         E    => E);
   end Add_Device;

   -----------------------------------------------------------------------------

   procedure Create_Context_Table
     (Bus     : Types.Device.PCI_Bus_Range;
      Address : Types.Physical_Address_Type)
   is
      E : constant Root_Entry_Type := Root_Entry_Type'
        (Present               => True,
         Context_Table_Address => Address);
   begin
      pragma Assert (not Page.Within (Address, Root_Table_Pointer));
      pragma Assert (not Context_Table_Present (Bus));
      pragma Assert
        (for all Bus in Types.Device.PCI_Bus_Range =>
        (if Context_Table_Present (Bus)
         then not Page.Within (Context_Table_Address (Bus), Address)));

      Typization.Set (Address    => Address,
                      Typization => Types.Typization.Used);

      pragma Assert (Invariant);

      Set_Root_Table_Entry (Bus => Bus, E => E);
   end Create_Context_Table;

   -----------------------------------------------------------------------------

   procedure Create_Root_Table (Address : Types.Physical_Address_Type)
   is
   begin
      Typization_Lemma;
      pragma Assert
        (for all Addr in Address .. Page.Last (Address) =>
         Get_Typization_Model (Addr) = T.Zeroed);

      Typization.Set (Address    => Address,
                      Typization => Types.Typization.Used);

      Root_Table_Pointer      := Address;
      Root_Table_Present_Flag := True;

      pragma Assert (Get_Typization_Model (Address) = Types.Typization.Used);
      pragma Assert (Data.Memory.Invariant);
   end Create_Root_Table;

   -----------------------------------------------------------------------------

   procedure Set_Context_Table_Entry
     (Bus  : Types.Device.PCI_Bus_Range;
      Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range;
      E    : Context_Entry_Type)
   is
      Old_Table : constant Types.VTd_Tables.Root_Table_Model_Type
        := Root_Table_Model with Ghost;

      Word          : constant Types.Word128 := To_Word128 (E);
      Base_Address  : constant Types.Physical_Address_Type
        := Context_Table_Address (Bus => Bus);
      Entry_Address : constant Types.Physical_Address_Type
        := Base_Address + Context_Table_Offset (Dev => Dev, Func => Func);
   begin
      Full_Typization.VTd_Context_Table_Lemma
        (Address => Entry_Address,
         Bus     => Bus);
      Accessible_Lemma (Entry_Address);

      Low_Level.Set (Address => Entry_Address, Value => Word);

      --  FIXME prove
      pragma Assert (Root_Table_Model = Old_Table);
      pragma Assert (Data.Memory.Invariant);
      pragma Assert (Context_Table_Present (Bus));
      pragma Assert (Get_Context_Table_Entry (Bus, Dev, Func) = E);
   end Set_Context_Table_Entry;

   -----------------------------------------------------------------------------

   procedure Set_Root_Table_Entry
     (Bus : Types.Device.PCI_Bus_Range;
      E   : Root_Entry_Type)
   is
      Word          : constant Types.Word128 := To_Word128 (E);
      Entry_Address : constant Types.Physical_Address_Type
        := Root_Table_Address + Root_Table_Offset (Bus => Bus);
   begin
      Full_Typization.VTd_Root_Table_Lemma (Address => Entry_Address);
      Accessible_Lemma (Entry_Address);

      Low_Level.Set (Address => Entry_Address, Value => Word);

      pragma Assert (Data.Memory.Invariant);
   end Set_Root_Table_Entry;

end Tau0.Data.Memory.VTd_IOMMU.Writers;
