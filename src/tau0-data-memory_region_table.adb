--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Roots.Writers.Memory_Regions;

package body Tau0.Data.Memory_Region_Table
with Refined_State => (State => Table)
is

   use Tau0.Types;
   use Tau0.Types.Memory_Region_Table;

   -----------------------------------------------------------------------------

   procedure Add_Page
     (Index   : Memory_Region_Table_Range;
      Offset  : Page_Count_Type;
      Address : Virtual_Address_Type)
   is
   begin
      Table (Index).Mapped_Pages := Table (Index).Mapped_Pages + 1;

      if Table (Index).Has_Base_Address then
         declare
            Table_Offset  : constant Page_Count_Type
              := Table (Index).Offset;
            Table_Address : constant Virtual_Address_Type
              := Table (Index).Base_Address;
            Is_Shifted    : constant Boolean
              := From_Page_Count (Offset - Table_Offset) /= Address - Table_Address;
         begin
            if Is_Shifted then
               Table (Index).Shifted_Pages := Table (Index).Shifted_Pages + 1;
            end if;
         end;
      end if;
   end Add_Page;

   -----------------------------------------------------------------------------

   procedure Add_Region
     (Index            : Memory_Region_Table_Range;
      Owner            : Root_Range;
      Region           : Root_Range;
      Use_Base_Address : Boolean;
      Base_Address     : Virtual_Address_Type := 0;
      Offset           : Page_Count_Type;
      Length           : Page_Count_Type;
      Writable         : Boolean;
      Executable       : Boolean)
   is
   begin
      if Use_Base_Address then
         Table (Index) := Cell_Type'
           (Used             => True,
            Has_Base_Address => True,
            Owner            => Owner,
            Region           => Region,
            Base_Address     => Base_Address,
            Offset           => Offset,
            Length           => Length,
            Mapped_Pages     => 0,
            Shifted_Pages    => 0,
            Writable         => Writable,
            Executable       => Executable);
      else
         Table (Index) := Cell_Type'
           (Used             => True,
            Has_Base_Address => False,
            Owner            => Owner,
            Region           => Region,
            Offset           => Offset,
            Length           => Length,
            Mapped_Pages     => 0,
            Writable         => Writable,
            Executable       => Executable);
      end if;

      Roots.Writers.Increment_Attached_Region_Counter (Owner);
      Roots.Writers.Memory_Regions.Increment_Reference_Counter (Region);
   end Add_Region;

   -----------------------------------------------------------------------------

   function Executable
     (Index : Types.Memory_Region_Table_Range)
      return Boolean
   is (Table (Index).Executable);

   -----------------------------------------------------------------------------

   function Has_Base_Address (Index : Memory_Region_Table_Range) return Boolean
   is (Table (Index).Has_Base_Address);

   -----------------------------------------------------------------------------

   procedure Lemma_Invariant (Old_Roots, New_Roots : Types.Root.Root_Array)
   is null;

   -----------------------------------------------------------------------------

   function Length (Index : Memory_Region_Table_Range) return Page_Count_Type
   is (Table (Index).Length);

   -----------------------------------------------------------------------------

   function Mapped_Pages (Index : Memory_Region_Table_Range) return Page_Count_Type
   is (Table (Index).Mapped_Pages);

   -----------------------------------------------------------------------------

   function Offset (Index : Memory_Region_Table_Range) return Types.Page_Count_Type
   is (Table (Index).Offset);

   -----------------------------------------------------------------------------

   function Owner (Index : Memory_Region_Table_Range) return Root_Range
   is (Table (Index).Owner);

   -----------------------------------------------------------------------------

   function Region (Index : Memory_Region_Table_Range) return Root_Range
   is (Table (Index).Region);

   -----------------------------------------------------------------------------

   function Shifted_Pages
     (Index : Memory_Region_Table_Range)
      return Page_Count_Type
   is (Table (Index).Shifted_Pages);

   -----------------------------------------------------------------------------

   function Used (Index : Memory_Region_Table_Range) return Boolean
   is (Table (Index).Used);

   -----------------------------------------------------------------------------

   function Writable
     (Index : Types.Memory_Region_Table_Range)
      return Boolean
   is (Table (Index).Writable);

end Tau0.Data.Memory_Region_Table;
