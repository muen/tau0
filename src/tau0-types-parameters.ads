--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Device;
with Tau0.Types.IO_Bitmap;
with Tau0.Types.MSR_Bitmap;
with Tau0.Types.Root;
with Tau0.Types.VTd_Tables;

package Tau0.Types.Parameters
with Abstract_State => null
is

   --  The following records contain the parameters for the commands transmitted
   --  to Tau0 through an SHM input stream.  To avoid transmission of data
   --  invalid for its type, the records use 'Base classes.  We provide
   --  validation functions and valid subtypes.

   --  ASSUMPTION: For every type X, there are no invalid values within type
   --              X'Base.

   pragma Warnings (Off, "redundant attribute, * is its own base type",
                    Reason => "The type definitions might change");
   pragma Warnings (Off, "explicit membership test may be optimized away",
                    Reason => "This only applies to type which are valid anyway");

   -----------------------------------------------------------------------------

   type Clear_Page_Type is record
      Page : Types.Tau0_Address_Type'Base;
   end record;

   function Valid (Value : Clear_Page_Type) return Boolean
   is (Value.Page in Types.Tau0_Address_Type);

   subtype Valid_Clear_Page_Type is Clear_Page_Type
   with Dynamic_Predicate => Valid (Valid_Clear_Page_Type);

   -----------------------------------------------------------------------------

   type Create_VTd_Root_Table_Type is record
      Page : Types.Physical_Address_Type'Base;
   end record;

   function Valid (Value : Create_VTd_Root_Table_Type) return Boolean
   is (Value.Page in Types.Physical_Address_Type);

   subtype Valid_Create_VTd_Root_Table_Type is Create_VTd_Root_Table_Type
   with Dynamic_Predicate => Valid (Valid_Create_VTd_Root_Table_Type);

   -----------------------------------------------------------------------------

   type Create_VTd_Context_Table_Type is record
      Bus  : Types.Device.PCI_Bus_Range'Base;
      Page : Types.Physical_Address_Type'Base;
   end record;

   function Valid (Value : Create_VTd_Context_Table_Type) return Boolean
   is (Value.Bus  in Types.Device.PCI_Bus_Range and
       Value.Page in Types.Physical_Address_Type);

   subtype Valid_Create_VTd_Context_Table_Type is Create_VTd_Context_Table_Type
   with Dynamic_Predicate => Valid (Valid_Create_VTd_Context_Table_Type);

   -----------------------------------------------------------------------------

   type Create_VTd_IRQ_Remap_Table_Type is record
      Page : Types.Physical_Address_Type'Base;
   end record;

   function Valid (Value : Create_VTd_IRQ_Remap_Table_Type) return Boolean
   is (Value.Page in Types.Physical_Address_Type);

   subtype Valid_Create_VTd_IRQ_Remap_Table_Type is Create_VTd_IRQ_Remap_Table_Type
   with Dynamic_Predicate => Valid (Valid_Create_VTd_IRQ_Remap_Table_Type);

   -----------------------------------------------------------------------------

   type Add_Memory_Block_Type is record
      Address : Types.Physical_Address_Type'Base;
      Size    : Types.Physical_Page_Count_Type'Base;
   end record;

   function Valid (Value : Add_Memory_Block_Type) return Boolean
   is (Value.Address in Types.Physical_Address_Type and
       Value.Size    in Types.Physical_Page_Count_Type);

   subtype Valid_Add_Memory_Block_Type is Add_Memory_Block_Type
   with Dynamic_Predicate => Valid (Valid_Add_Memory_Block_Type);

   -----------------------------------------------------------------------------

   type Add_Processor_Type is record
      ID      : Types.CPU_Range'Base;
      APIC_ID : Types.APIC_ID_Type'Base;
   end record;

   function Valid (Value : Add_Processor_Type) return Boolean
   is (Value.ID      in Types.CPU_Range and
       Value.APIC_ID in Types.APIC_ID_Type);

   subtype Valid_Add_Processor_Type is Add_Processor_Type
   with Dynamic_Predicate => Valid (Valid_Add_Processor_Type);

   -----------------------------------------------------------------------------

   type Add_Ioapic_Type is record
      Source_ID : Types.Source_ID_Type'Base;
   end record;

   function Valid (Value : Add_Ioapic_Type) return Boolean
   is (Value.Source_ID in Types.Source_ID_Type);

   subtype Valid_Add_Ioapic_Type is Add_Ioapic_Type
   with Dynamic_Predicate => Valid (Valid_Add_Ioapic_Type);

   -----------------------------------------------------------------------------

   type Create_Memory_Region_Type is record
      Region  : Types.Root_Range'Base;
      Level   : Types.Nonleaf_Level_Type'Base;
      Caching : Types.Caching_Type'Base;
      Hash    : Types.Root.Optional_Hash_Type;
   end record;

   function Valid (Value : Create_Memory_Region_Type) return Boolean
   is (Value.Region  in Types.Root_Range and
       Value.Level   in Types.Nonleaf_Level_Type and
       Value.Caching in Types.Caching_Type);

   subtype Valid_Create_Memory_Region_Type is Create_Memory_Region_Type
   with Dynamic_Predicate => Valid (Valid_Create_Memory_Region_Type);

   -----------------------------------------------------------------------------

   type Lock_Memory_Region_Type is record
      Region : Types.Root_Range'Base;
   end record;

   function Valid (Value : Lock_Memory_Region_Type) return Boolean
   is (Value.Region in Types.Root_Range);

   subtype Valid_Lock_Memory_Region_Type is Lock_Memory_Region_Type
   with Dynamic_Predicate => Valid (Valid_Lock_Memory_Region_Type);

   -----------------------------------------------------------------------------

   type Activate_Memory_Region_Type is record
      Region : Types.Root_Range'Base;
   end record;

   function Valid (Value : Activate_Memory_Region_Type) return Boolean
   is (Value.Region in Types.Root_Range);

   subtype Valid_Activate_Memory_Region_Type is Activate_Memory_Region_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Memory_Region_Type);

   -----------------------------------------------------------------------------

   type Deactivate_Memory_Region_Type is record
      Region : Types.Root_Range'Base;
   end record;

   function Valid (Value : Deactivate_Memory_Region_Type) return Boolean
   is (Value.Region in Types.Root_Range);

   subtype Valid_Deactivate_Memory_Region_Type is Deactivate_Memory_Region_Type
   with Dynamic_Predicate => Valid (Valid_Deactivate_Memory_Region_Type);

   -----------------------------------------------------------------------------

   type Unlock_Memory_Region_Type is record
      Region : Types.Root_Range'Base;
   end record;

   function Valid (Value : Unlock_Memory_Region_Type) return Boolean
   is (Value.Region in Types.Root_Range);

   subtype Valid_Unlock_Memory_Region_Type is Unlock_Memory_Region_Type
   with Dynamic_Predicate => Valid (Valid_Unlock_Memory_Region_Type);

   -----------------------------------------------------------------------------

   type Destroy_Memory_Region_Type is record
      Region : Types.Root_Range'Base;
   end record;

   function Valid (Value : Destroy_Memory_Region_Type) return Boolean
   is (Value.Region in Types.Root_Range);

   subtype Valid_Destroy_Memory_Region_Type is Destroy_Memory_Region_Type
   with Dynamic_Predicate => Valid (Valid_Destroy_Memory_Region_Type);

   -----------------------------------------------------------------------------

   type Create_Page_Table_MR_Type is record
      Page            : Types.Tau0_Private_Address_Type'Base;
      Region          : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Create_Page_Table_MR_Type) return Boolean
   is (Value.Page            in Types.Tau0_Private_Address_Type and
       Value.Region          in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Create_Page_Table_MR_Type is Create_Page_Table_MR_Type
   with Dynamic_Predicate => Valid (Valid_Create_Page_Table_MR_Type);

   -----------------------------------------------------------------------------

   type Destroy_Page_Table_MR_Type is record
      Region          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Level           : Types.Page_Level_Type'Base;
   end record;

   function Valid (Value : Destroy_Page_Table_MR_Type) return Boolean
   is (Value.Region          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Level           in Types.Page_Level_Type);

   subtype Valid_Destroy_Page_Table_MR_Type is Destroy_Page_Table_MR_Type
   with Dynamic_Predicate => Valid (Valid_Destroy_Page_Table_MR_Type);

   -----------------------------------------------------------------------------

   type Append_Page_MR_Type is record
      Region   : Types.Root_Range'Base;
      Page     : Types.Physical_Address_Type'Base;
      Contents : Types.Page_Array;
   end record;

   function Valid (Value : Append_Page_MR_Type) return Boolean
   is (Value.Region in Types.Root_Range and
       Value.Page   in Types.Physical_Address_Type);

   subtype Valid_Append_Page_MR_Type is Append_Page_MR_Type
   with Dynamic_Predicate => Valid (Valid_Append_Page_MR_Type);

   -----------------------------------------------------------------------------

   type Append_Vacuous_Page_MR_Type is record
      Region : Types.Root_Range'Base;
      Page   : Types.Physical_Address_Type'Base;
   end record;

   function Valid (Value : Append_Vacuous_Page_MR_Type) return Boolean
   is (Value.Region in Types.Root_Range and
       Value.Page   in Types.Physical_Address_Type);

   subtype Valid_Append_Vacuous_Page_MR_Type is Append_Vacuous_Page_MR_Type
   with Dynamic_Predicate => Valid (Valid_Append_Vacuous_Page_MR_Type);

   -----------------------------------------------------------------------------

   type Truncate_Page_MR_Type is record
      Region : Types.Root_Range'Base;
   end record;

   function Valid (Value : Truncate_Page_MR_Type) return Boolean
   is (Value.Region in Types.Root_Range);

   subtype Valid_Truncate_Page_MR_Type is Truncate_Page_MR_Type
   with Dynamic_Predicate => Valid (Valid_Truncate_Page_MR_Type);

   -----------------------------------------------------------------------------

   type Activate_Page_Table_MR_Type is record
      Region          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Level           : Types.Level_Type'Base;
   end record;

   function Valid (Value : Activate_Page_Table_MR_Type) return Boolean
   is (Value.Region          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Level           in Types.Page_Level_Type);

   subtype Valid_Activate_Page_Table_MR_Type is Activate_Page_Table_MR_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Page_Table_MR_Type);

   -----------------------------------------------------------------------------

   type Activate_Page_MR_Type is record
      Region          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Activate_Page_MR_Type) return Boolean
   is (Value.Region          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Activate_Page_MR_Type is Activate_Page_MR_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Page_MR_Type);

   -----------------------------------------------------------------------------

   type Deactivate_Page_MR_Type is record
      Region          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Deactivate_Page_MR_Type) return Boolean
   is (Value.Region          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Deactivate_Page_MR_Type is Deactivate_Page_MR_Type
   with Dynamic_Predicate => Valid (Valid_Deactivate_Page_MR_Type);

   -----------------------------------------------------------------------------

   type Deactivate_Page_Table_MR_Type is record
      Region          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Level           : Types.Level_Type'Base;
   end record;

   function Valid (Value : Deactivate_Page_Table_MR_Type) return Boolean
   is (Value.Region          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Level           in Types.Page_Level_Type);

   subtype Valid_Deactivate_Page_Table_MR_Type is Deactivate_Page_Table_MR_Type
   with Dynamic_Predicate => Valid (Valid_Deactivate_Page_Table_MR_Type);

   -----------------------------------------------------------------------------

   type Create_Subject_Type is record
      Subject    : Types.Root_Range'Base;
      Paging     : Types.Root.Subject_Paging_Type'Base;
      IO_Bitmap  : Types.Physical_Address_Type'Base;
      MSR_Bitmap : Types.Physical_Address_Type'Base;
      CPU        : Types.CPU_Range'Base;
   end record;

   function Valid (Value : Create_Subject_Type) return Boolean
   is (Value.Subject    in Types.Root_Range and
       Value.Paging     in Types.Root.Subject_Paging_Type and
       Value.IO_Bitmap  in Types.Physical_Address_Type and
       Value.MSR_Bitmap in Types.Physical_Address_Type and
       Value.CPU        in Types.CPU_Range);

   subtype Valid_Create_Subject_Type is Create_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Create_Subject_Type);

   -----------------------------------------------------------------------------

   type Destroy_Subject_Type is record
      Subject : Types.Root_Range'Base;
   end record;

   function Valid (Value : Destroy_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range);

   subtype Valid_Destroy_Subject_Type is Destroy_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Destroy_Subject_Type);

   -----------------------------------------------------------------------------

   type Lock_Subject_Type is record
      Subject : Types.Root_Range'Base;
   end record;

   function Valid (Value : Lock_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range);

   subtype Valid_Lock_Subject_Type is Lock_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Lock_Subject_Type);

   -----------------------------------------------------------------------------

   type Activate_Subject_Type is record
      Subject : Types.Root_Range'Base;
   end record;

   function Valid (Value : Activate_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range);

   subtype Valid_Activate_Subject_Type is Activate_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Subject_Type);

   -----------------------------------------------------------------------------

   type Deactivate_Subject_Type is record
      Subject : Types.Root_Range'Base;
   end record;

   function Valid (Value : Deactivate_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range);

   subtype Valid_Deactivate_Subject_Type is Deactivate_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Deactivate_Subject_Type);

   -----------------------------------------------------------------------------

   type Attach_Memory_Region_Subject_Type is record
      Subject          : Types.Root_Range'Base;
      Region           : Types.Root_Range'Base;
      Use_Base_Address : Boolean;
      Base_Address     : Types.Virtual_Address_Type'Base;
      Offset           : Types.Page_Count_Type'Base;
      Length           : Types.Page_Count_Type'Base;
      Index            : Types.Memory_Region_Table_Range'Base;
      Writable         : Boolean;
      Executable       : Boolean;
   end record;

   function Valid (Value : Attach_Memory_Region_Subject_Type) return Boolean
   is (Value.Subject      in Types.Root_Range and
       Value.Region       in Types.Root_Range and
       Value.Base_Address in Types.Virtual_Address_Type and
       Value.Offset       in Types.Page_Count_Type and
       Value.Length       in Types.Page_Count_Type and
       Value.Index        in Types.Memory_Region_Table_Range);

   subtype Valid_Attach_Memory_Region_Subject_Type is Attach_Memory_Region_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Attach_Memory_Region_Subject_Type);

   -----------------------------------------------------------------------------

   type Detach_Memory_Region_Subject_Type is record
      Subject : Types.Root_Range'Base;
      Index   : Types.Memory_Region_Table_Range'Base;
   end record;

   function Valid (Value : Detach_Memory_Region_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range and
       Value.Index   in Types.Memory_Region_Table_Range);

   subtype Valid_Detach_Memory_Region_Subject_Type is Detach_Memory_Region_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Detach_Memory_Region_Subject_Type);

   -----------------------------------------------------------------------------

   type Assign_Device_Subject_Type is record
      Subject : Types.Root_Range'Base;
      Device  : Types.Device.Device_Range'Base;
   end record;

   function Valid (Value : Assign_Device_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range and
       Value.Device  in Types.Device.Device_Range);

   subtype Valid_Assign_Device_Subject_Type is Assign_Device_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Assign_Device_Subject_Type);

   -----------------------------------------------------------------------------

   type Assign_IRQ_Subject_Type is record
      Subject : Types.Root_Range'Base;
      Device  : Types.Device.Device_Range'Base;
      IRQ     : Types.Device.HW_IRQ_Type'Base;
   end record;

   function Valid (Value : Assign_IRQ_Subject_Type) return Boolean
   is (Value.Subject in Types.Root_Range and
       Value.Device  in Types.Device.Device_Range and
       Value.IRQ     in Types.Device.HW_IRQ_Type);

   subtype Valid_Assign_IRQ_Subject_Type is Assign_IRQ_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Assign_IRQ_Subject_Type);

   -----------------------------------------------------------------------------

   type Set_IO_Port_Range_Type is record
      Subject : Types.Root_Range'Base;
      Device  : Types.Device.Device_Range'Base;
      From    : Types.IO_Port_Type'Base;
      To      : Types.IO_Port_Type'Base;
      Mode    : Types.IO_Bitmap.Mode_Type'Base;
   end record;

   function Valid (Value : Set_IO_Port_Range_Type) return Boolean
   is (Value.Subject in Types.Root_Range and
       Value.Device  in Types.Device.Device_Range and
       Value.From    in Types.IO_Port_Type and
       Value.To      in Types.IO_Port_Type and
       Value.Mode    in Types.IO_Bitmap.Mode_Type);

   subtype Valid_Set_IO_Port_Range_Type is Set_IO_Port_Range_Type
   with Dynamic_Predicate => Valid (Valid_Set_IO_Port_Range_Type);

   -----------------------------------------------------------------------------

   type Set_MSR_Range_Type is record
      Subject : Types.Root_Range'Base;
      From    : Types.MSR_Bitmap.MSR_Range'Base;
      To      : Types.MSR_Bitmap.MSR_Range'Base;
      Mode    : Types.MSR_Bitmap.Mode_Type'Base;
   end record;

   function Valid (Value : Set_MSR_Range_Type) return Boolean
   is (Value.Subject in Types.Root_Range and
       Value.From    in Types.MSR_Bitmap.MSR_Range and
       Value.To      in Types.MSR_Bitmap.MSR_Range and
       Value.Mode    in Types.MSR_Bitmap.Mode_Type);

   subtype Valid_Set_MSR_Range_Type is Set_MSR_Range_Type
   with Dynamic_Predicate => Valid (Valid_Set_MSR_Range_Type);

   -----------------------------------------------------------------------------

   type Create_Page_Table_Subject_Type is record
      Page            : Types.Physical_Address_Type'Base;
      Subject         : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Readable        : Boolean;
      Writable        : Boolean;
      Executable      : Boolean;
   end record;

   function Valid (Value : Create_Page_Table_Subject_Type) return Boolean
   is (Value.Page            in Types.Physical_Address_Type and
       Value.Subject         in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Create_Page_Table_Subject_Type is Create_Page_Table_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Create_Page_Table_Subject_Type);

   -----------------------------------------------------------------------------

   type Destroy_Page_Table_Subject_Type is record
      Subject         : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Destroy_Page_Table_Subject_Type) return Boolean
   is (Value.Subject         in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Destroy_Page_Table_Subject_Type is Destroy_Page_Table_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Destroy_Page_Table_Subject_Type);

   -----------------------------------------------------------------------------

   type Activate_Page_Table_Subject_Type is record
      Subject         : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Activate_Page_Table_Subject_Type) return Boolean
   is (Value.Subject         in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Activate_Page_Table_Subject_Type is Activate_Page_Table_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Page_Table_Subject_Type);

   -----------------------------------------------------------------------------

   type Deactivate_Page_Table_Subject_Type is record
      Subject         : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Deactivate_Page_Table_Subject_Type) return Boolean
   is (Value.Subject         in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Deactivate_Page_Table_Subject_Type is Deactivate_Page_Table_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Deactivate_Page_Table_Subject_Type);

   -----------------------------------------------------------------------------

   type Map_Page_Subject_Type is record
      Subject         : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Table_Index     : Types.Memory_Region_Table_Range'Base;
      Offset          : Types.Page_Count_Type'Base;
   end record;

   function Valid (Value : Map_Page_Subject_Type) return Boolean
   is (Value.Subject         in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Table_Index     in Types.Memory_Region_Table_Range and
       Value.Offset          in Types.Page_Count_Type);

   subtype Valid_Map_Page_Subject_Type is Map_Page_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Map_Page_Subject_Type);

   -----------------------------------------------------------------------------

   type Map_Device_Page_Subject_Type is record
      Subject         : Types.Root_Range'Base;
      Device          : Types.Device.Device_Range'Base;
      Page            : Types.Physical_Address_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Writable        : Boolean;
      Executable      : Boolean;
   end record;

   function Valid (Value : Map_Device_Page_Subject_Type) return Boolean
   is (Value.Subject         in Types.Root_Range and
       Value.Device          in Types.Device.Device_Range and
       Value.Page            in Types.Physical_Address_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Map_Device_Page_Subject_Type is Map_Device_Page_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Map_Device_Page_Subject_Type);

   -----------------------------------------------------------------------------

   type Unmap_Page_Subject_Type is record
      Subject         : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Table_Index     : Types.Memory_Region_Table_Range'Base;
      Offset          : Types.Page_Count_Type'Base;
   end record;

   function Valid (Value : Unmap_Page_Subject_Type) return Boolean
   is (Value.Subject         in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Table_Index     in Types.Memory_Region_Table_Range and
       Value.Offset          in Types.Page_Count_Type);

   subtype Valid_Unmap_Page_Subject_Type is Unmap_Page_Subject_Type
   with Dynamic_Predicate => Valid (Valid_Unmap_Page_Subject_Type);

   -----------------------------------------------------------------------------

   type Create_Device_Domain_Type is record
      Domain : Types.Root_Range'Base;
      Level  : Types.Nonleaf_Level_Type'Base;
      ID     : Types.VTd_Tables.Domain_Identifier_Type'Base;
   end record;

   function Valid (Value : Create_Device_Domain_Type) return Boolean
   is (Value.Domain in Types.Root_Range and
       Value.Level  in Types.Nonleaf_Level_Type and
       Value.ID     in Types.VTd_Tables.Domain_Identifier_Type);

   subtype Valid_Create_Device_Domain_Type is Create_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Create_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Lock_Device_Domain_Type is record
      Domain : Types.Root_Range'Base;
   end record;

   function Valid (Value : Lock_Device_Domain_Type) return Boolean
   is (Value.Domain in Types.Root_Range);

   subtype Valid_Lock_Device_Domain_Type is Lock_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Lock_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Activate_Device_Domain_Type is record
      Domain : Types.Root_Range'Base;
   end record;

   function Valid (Value : Activate_Device_Domain_Type) return Boolean
   is (Value.Domain in Types.Root_Range);

   subtype Valid_Activate_Device_Domain_Type is Activate_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Add_Device_To_Device_Domain_Type is record
      Domain : Types.Root_Range'Base;
      Device : Types.Device.Device_Range'Base;
   end record;

   function Valid (Value : Add_Device_To_Device_Domain_Type) return Boolean
   is (Value.Domain in Types.Root_Range and
       Value.Device in Types.Device.Device_Range);

   subtype Valid_Add_Device_To_Device_Domain_Type is Add_Device_To_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Add_Device_To_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Attach_Memory_Region_Domain_Type is record
      Domain           : Types.Root_Range'Base;
      Region           : Types.Root_Range'Base;
      Use_Base_Address : Boolean;
      Base_Address     : Types.Virtual_Address_Type'Base;
      Offset           : Types.Page_Count_Type'Base;
      Length           : Types.Page_Count_Type'Base;
      Index            : Types.Memory_Region_Table_Range'Base;
      Writable         : Boolean;
      Executable       : Boolean;
   end record;

   function Valid (Value : Attach_Memory_Region_Domain_Type) return Boolean
   is (Value.Domain       in Types.Root_Range and
       Value.Region       in Types.Root_Range and
       Value.Base_Address in Types.Virtual_Address_Type and
       Value.Offset       in Types.Page_Count_Type and
       Value.Length       in Types.Page_Count_Type and
       Value.Index        in Types.Memory_Region_Table_Range);

   subtype Valid_Attach_Memory_Region_Domain_Type
      is Attach_Memory_Region_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Attach_Memory_Region_Domain_Type);

   -----------------------------------------------------------------------------

   type Create_Page_Table_Device_Domain_Type is record
      Page            : Types.Physical_Address_Type'Base;
      Domain          : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Readable        : Boolean;
      Writable        : Boolean;
      Executable      : Boolean;
   end record;

   function Valid (Value : Create_Page_Table_Device_Domain_Type) return Boolean
   is (Value.Page            in Types.Physical_Address_Type and
       Value.Domain          in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Create_Page_Table_Device_Domain_Type
      is Create_Page_Table_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Create_Page_Table_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Activate_Page_Table_Device_Domain_Type is record
      Domain          : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Activate_Page_Table_Device_Domain_Type) return Boolean
   is (Value.Domain          in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Activate_Page_Table_Device_Domain_Type
      is Activate_Page_Table_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Page_Table_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Map_Page_Device_Domain_Type is record
      Domain          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Table_Index     : Types.Memory_Region_Table_Range'Base;
      Offset          : Types.Page_Count_Type'Base;
   end record;

   function Valid (Value : Map_Page_Device_Domain_Type) return Boolean
   is (Value.Domain          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Table_Index     in Types.Memory_Region_Table_Range and
       Value.Offset          in Types.Page_Count_Type);

   subtype Valid_Map_Page_Device_Domain_Type is Map_Page_Device_Domain_Type
   with Dynamic_Predicate => Valid (Valid_Map_Page_Device_Domain_Type);

   -----------------------------------------------------------------------------

   type Create_Kernel_Type is record
      Kernel : Types.Root_Range'Base;
      CPU    : Types.CPU_Range'Base;
   end record;

   function Valid (Value : Create_Kernel_Type) return Boolean
   is (Value.Kernel in Types.Root_Range and
       Value.CPU    in Types.CPU_Range);

   subtype Valid_Create_Kernel_Type is Create_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Create_Kernel_Type);

   -----------------------------------------------------------------------------

   type Lock_Kernel_Type is record
      Kernel : Types.Root_Range'Base;
   end record;

   function Valid (Value : Lock_Kernel_Type) return Boolean
   is (Value.Kernel in Types.Root_Range);

   subtype Valid_Lock_Kernel_Type is Lock_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Lock_Kernel_Type);

   -----------------------------------------------------------------------------

   type Activate_Kernel_Type is record
      Kernel : Types.Root_Range'Base;
   end record;

   function Valid (Value : Activate_Kernel_Type) return Boolean
   is (Value.Kernel in Types.Root_Range);

   subtype Valid_Activate_Kernel_Type is Activate_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Kernel_Type);

   -----------------------------------------------------------------------------

   type Attach_Memory_Region_Kernel_Type is record
      Kernel           : Types.Root_Range'Base;
      Region           : Types.Root_Range'Base;
      Use_Base_Address : Boolean;
      Base_Address     : Types.Virtual_Address_Type'Base;
      Offset           : Types.Page_Count_Type'Base;
      Length           : Types.Page_Count_Type'Base;
      Index            : Types.Memory_Region_Table_Range'Base;
      Writable         : Boolean;
      Executable       : Boolean;
   end record;

   function Valid (Value : Attach_Memory_Region_Kernel_Type) return Boolean
   is (Value.Kernel       in Types.Root_Range and
       Value.Region       in Types.Root_Range and
       Value.Base_Address in Types.Virtual_Address_Type and
       Value.Offset       in Types.Page_Count_Type and
       Value.Length       in Types.Page_Count_Type and
       Value.Index        in Types.Memory_Region_Table_Range);

   subtype Valid_Attach_Memory_Region_Kernel_Type
      is Attach_Memory_Region_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Attach_Memory_Region_Kernel_Type);

   -----------------------------------------------------------------------------

   type Create_Page_Table_Kernel_Type is record
      Page            : Types.Physical_Address_Type'Base;
      Kernel          : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Readable        : Boolean;
      Writable        : Boolean;
      Executable      : Boolean;
   end record;

   function Valid (Value : Create_Page_Table_Kernel_Type) return Boolean
   is (Value.Page            in Types.Physical_Address_Type and
       Value.Kernel          in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Create_Page_Table_Kernel_Type is Create_Page_Table_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Create_Page_Table_Kernel_Type);

   -----------------------------------------------------------------------------

   type Activate_Page_Table_Kernel_Type is record
      Kernel          : Types.Root_Range'Base;
      Level           : Types.Page_Level_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
   end record;

   function Valid (Value : Activate_Page_Table_Kernel_Type) return Boolean
   is (Value.Kernel          in Types.Root_Range and
       Value.Level           in Types.Page_Level_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Activate_Page_Table_Kernel_Type is Activate_Page_Table_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Page_Table_Kernel_Type);

   -----------------------------------------------------------------------------

   type Map_Page_Kernel_Type is record
      Kernel          : Types.Root_Range'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Table_Index     : Types.Memory_Region_Table_Range'Base;
      Offset          : Types.Page_Count_Type'Base;
   end record;

   function Valid (Value : Map_Page_Kernel_Type) return Boolean
   is (Value.Kernel          in Types.Root_Range and
       Value.Virtual_Address in Types.Virtual_Address_Type and
       Value.Table_Index     in Types.Memory_Region_Table_Range and
       Value.Offset          in Types.Page_Count_Type);

   subtype Valid_Map_Page_Kernel_Type is Map_Page_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Map_Page_Kernel_Type);

   -----------------------------------------------------------------------------

   type Map_Device_Page_Kernel_Type is record
      Kernel          : Types.Root_Range'Base;
      Device          : Types.Device.Device_Range'Base;
      Page            : Types.Physical_Address_Type'Base;
      Virtual_Address : Types.Virtual_Address_Type'Base;
      Writable        : Boolean;
      Executable      : Boolean;
   end record;

   function Valid (Value : Map_Device_Page_Kernel_Type) return Boolean
   is (Value.Kernel          in Types.Root_Range and
       Value.Device          in Types.Device.Device_Range and
       Value.Page            in Types.Physical_Address_Type and
       Value.Virtual_Address in Types.Virtual_Address_Type);

   subtype Valid_Map_Device_Page_Kernel_Type is Map_Device_Page_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Map_Device_Page_Kernel_Type);

   -----------------------------------------------------------------------------

   type Assign_Device_Kernel_Type is record
      Kernel : Types.Root_Range'Base;
      Device : Types.Device.Device_Range'Base;
   end record;

   function Valid (Value : Assign_Device_Kernel_Type) return Boolean
   is (Value.Kernel in Types.Root_Range and
       Value.Device  in Types.Device.Device_Range);

   subtype Valid_Assign_Device_Kernel_Type is Assign_Device_Kernel_Type
   with Dynamic_Predicate => Valid (Valid_Assign_Device_Kernel_Type);

   -----------------------------------------------------------------------------

   type Create_Legacy_Device_Type is record
      Device : Types.Device.Device_Range'Base;
   end record;

   function Valid (Value : Create_Legacy_Device_Type) return Boolean
   is (Value.Device in Types.Device.Device_Range);

   subtype Valid_Create_Legacy_Device_Type is Create_Legacy_Device_Type
   with Dynamic_Predicate => Valid (Valid_Create_Legacy_Device_Type);

   -----------------------------------------------------------------------------

   type Create_PCI_Device_Type is record
      Device      : Types.Device.Device_Range'Base;
      PCI_Bus     : Types.Device.PCI_Bus_Range'Base;
      PCI_Dev     : Types.Device.PCI_Device_Range'Base;
      PCI_Func    : Types.Device.PCI_Function_Range'Base;
      IOMMU_Group : Types.Device.IOMMU_Group_Type'Base;
      Uses_MSI    : Boolean;
   end record;

   function Valid (Value : Create_PCI_Device_Type) return Boolean
   is (Value.Device      in Types.Device.Device_Range and
       Value.PCI_Bus     in Types.Device.PCI_Bus_Range and
       Value.PCI_Dev     in Types.Device.PCI_Device_Range and
       Value.PCI_Func    in Types.Device.PCI_Function_Range and
       Value.IOMMU_Group in Types.Device.IOMMU_Group_Type);

   subtype Valid_Create_PCI_Device_Type is Create_PCI_Device_Type
   with Dynamic_Predicate => Valid (Valid_Create_PCI_Device_Type);

   -----------------------------------------------------------------------------

   type Add_IO_Port_Range_Device_Type is record
      Device : Types.Device.Device_Range'Base;
      From   : Types.IO_Port_Type'Base;
      To     : Types.IO_Port_Type'Base;
   end record;

   function Valid (Value : Add_IO_Port_Range_Device_Type) return Boolean
   is (Value.Device in Types.Device.Device_Range and
       Value.From   in Types.IO_Port_Type and
       Value.To     in Types.IO_Port_Type);

   subtype Valid_Add_IO_Port_Range_Device_Type is Add_IO_Port_Range_Device_Type
   with Dynamic_Predicate => Valid (Valid_Add_IO_Port_Range_Device_Type);

   -----------------------------------------------------------------------------

   type Add_IRQ_Device_Type is record
      Device : Types.Device.Device_Range'Base;
      IRQ    : Types.Device.HW_IRQ_Type'Base;
   end record;

   function Valid (Value : Add_IRQ_Device_Type) return Boolean
   is (Value.Device in Types.Device.Device_Range and
       Value.IRQ    in Types.Device.HW_IRQ_Type);

   subtype Valid_Add_IRQ_Device_Type is Add_IRQ_Device_Type
   with Dynamic_Predicate => Valid (Valid_Add_IRQ_Device_Type);

   -----------------------------------------------------------------------------

   type Add_Memory_Device_Type is record
      Device  : Types.Device.Device_Range'Base;
      Address : Types.Physical_Address_Type'Base;
      Size    : Types.Physical_Page_Count_Type'Base;
      Caching : Types.Caching_Type'Base;
   end record;

   function Valid (Value : Add_Memory_Device_Type) return Boolean
   is (Value.Device  in Types.Device.Device_Range and
       Value.Address in Types.Physical_Address_Type and
       Value.Size    in Types.Physical_Page_Count_Type and
       Value.Caching in Types.Caching_Type);

   subtype Valid_Add_Memory_Device_Type is Add_Memory_Device_Type
   with Dynamic_Predicate => Valid (Valid_Add_Memory_Device_Type);

   -----------------------------------------------------------------------------

   type Activate_Device_Type is record
      Device : Types.Device.Device_Range'Base;
   end record;

   function Valid (Value : Activate_Device_Type) return Boolean
   is (Value.Device in Types.Device.Device_Range);

   subtype Valid_Activate_Device_Type is Activate_Device_Type
   with Dynamic_Predicate => Valid (Valid_Activate_Device_Type);

   -----------------------------------------------------------------------------

   --  TODO: This command is only useful in static mode.
   type Write_Image_Commands_Type is record
      Entry_Point : Types.Physical_Address_Type'Base;
   end record;

   function Valid (Value : Write_Image_Commands_Type) return Boolean
   is (Value.Entry_Point in Types.Physical_Address_Type);

   subtype Valid_Write_Image_Commands_Type is Write_Image_Commands_Type
   with Dynamic_Predicate => Valid (Valid_Write_Image_Commands_Type);

   pragma Warnings (On, "redundant attribute, * is its own base type");
   pragma Warnings (On, "explicit membership test may be optimized away");

end Tau0.Types.Parameters;
