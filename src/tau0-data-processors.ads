--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types;

package Tau0.Data.Processors
with Abstract_State => State
is

   function Present (ID : Types.CPU_Range) return Boolean;

   function APIC_ID_Used (APIC_ID : Types.APIC_ID_Type) return Boolean;

   function APIC_ID (ID : Types.CPU_Range) return Types.APIC_ID_Type
   with
     Global => (Input => State),
     Pre    => Present (ID);

   procedure Add_Processor (ID : Types.CPU_Range; APIC_ID : Types.APIC_ID_Type)
   with
     Global => (Proof_In => Tau0.State,
                In_Out   => State),
     Pre    => Tau0_State = Setup and
               not Present (ID) and
               not APIC_ID_Used (APIC_ID),
     Post   => Present (ID);

end Tau0.Data.Processors;
