--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

--  Store a fixed-size array together with a fill-counter.
--  One can push and pop elements to/from the back of the array.
--  Elements can be accessed using a Cursor type.
generic
   type Elem_Type is private;
   Null_Elem : Elem_Type;
   Max       : Positive;
package Bounded_Table
with Abstract_State => null
is

   pragma Annotate (GNATprove, Terminating, Bounded_Table);

   type Cursor_Type is private;

   type T is private
   with Iterable => (First       => First,
                     Next        => Next,
                     Has_Element => Has_Element,
                     Element     => Element);

   type Model_Type is array (Natural range <>) of Elem_Type
   with Ghost;

   --  Return array model for table
   function Model (Table : T) return Model_Type
   with
     Ghost,
     Post => (for all C in Table =>
              Model'Result (To_Natural (C)) = Element (Table, C));

   --  Cast Cursor to Natural -- allows loop termination proofs
   function To_Natural (Cursor : Cursor_Type) return Natural
   with Ghost;

   --  Return an empty table
   function Empty_Table return T
   with Post => Model (Empty_Table'Result)'Length = 0;

   --  Number of elements in the Table
   function Length (Table : T) return Natural
   with Post => Length'Result = Model (Table)'Length;

   --  True iff no elements can be pushed
   function Full (Table : T) return Boolean
   is (Length (Table) = Max);

   --  Cursor pointing to first element of Table
   function First (Table : T) return Cursor_Type;

   --  Does Cursor point to an element in Table?
   function Has_Element (Table : T; Cursor : Cursor_Type) return Boolean;

   --  Return cursor to next element in Table
   function Next (Table : T; Cursor : Cursor_Type) return Cursor_Type
   with
     Pre  => Has_Element (Table, Cursor),
     Post => To_Natural (Next'Result) > To_Natural (Cursor);

   --  Return element at Cursor in Table
   function Element (Table : T; Cursor : Cursor_Type) return Elem_Type
   with Pre => Has_Element (Table, Cursor);

   --  Is there an element equal to Elem in Table?  O(n) lookup.
   function Member (Table : T; Elem : Elem_Type) return Boolean
   with Post => Member'Result = (for some E of Table => E = Elem);
   pragma Annotate (GNATprove, Iterable_For_Proof, "Contains", Member);

   --  Append Elem to end of Table
   procedure Push
     (Table : in out T;
      Elem  :        Elem_Type)
   with
     Pre  => Length (Table) < Max,
     Post => Length (Table) = Length (Table'Old) + 1 and then
             Member (Table, Elem) and then
             --  more generally:
             (for all E of Table'Old => Member (Table, E)) and then
             (for all E of Table => E = Elem or Member (Table'Old, E));

   --  Pop Elem from end of Table
   procedure Pop
     (Table : in out T;
      Elem  :    out Elem_Type)
   with
     Pre  => Length (Table) > 0,
     Post => Length (Table) = Length (Table'Old) - 1 and then
             (for all E of Table => Member (Table'Old, E)) and then
             (for all E of Table'Old => E = Elem or Member (Table, E));

private

   subtype Opt_Index_Type is Natural range 0 .. Max;
   subtype Index_Type is Opt_Index_Type range 1 .. Opt_Index_Type (Max);

   --  KLUDGE: avoid complaints about non-static types
   subtype Index_With_Sentinel is Natural range 1 .. Opt_Index_Type (Max) + 1;
   type Cursor_Type is new Index_With_Sentinel;

   type Elem_Array is array (Index_Type) of Elem_Type;

   type T is record
      Elems : Elem_Array;
      Last  : Opt_Index_Type;
   end record;

end Bounded_Table;
