--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands.Device_Domains.Paging;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Device_Domains
is

   -----------------------------------------------------------------------------

   procedure Activate_Page_Table_Device_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Activate_Page_Table_Device_Domain_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Paging.Activate_Page_Table
           (Domain          =>
              Command.Activate_Page_Table_Device_Domain_Params.Domain,
            Level           =>
              Command.Activate_Page_Table_Device_Domain_Params.Level,
            Virtual_Address =>
              Command.Activate_Page_Table_Device_Domain_Params.Virtual_Address,
            Status          => Status);
      end if;
   end Activate_Page_Table_Device_Domain;

   -----------------------------------------------------------------------------

   procedure Activate_Device_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Device_Domain_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Activate_Domain
           (Domain => Command.Activate_Device_Domain_Params.Domain,
            Status => Status);
      end if;
   end Activate_Device_Domain;

   -----------------------------------------------------------------------------

   procedure Add_Device
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Add_Device_To_Device_Domain_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Add_Device
           (Domain => Command.Add_Device_To_Device_Domain_Params.Domain,
            Device => Command.Add_Device_To_Device_Domain_Params.Device,
            Status => Status);
      end if;
   end Add_Device;

   -----------------------------------------------------------------------------

   procedure Attach_Memory_Region_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Attach_Memory_Region_Domain_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Attach_Memory_Region
           (Domain           =>
              Command.Attach_Memory_Region_Domain_Params.Domain,
            Region           =>
              Command.Attach_Memory_Region_Domain_Params.Region,
            Use_Base_Address =>
              Command.Attach_Memory_Region_Domain_Params.Use_Base_Address,
            Base_Address     =>
              Command.Attach_Memory_Region_Domain_Params.Base_Address,
            Offset           =>
              Command.Attach_Memory_Region_Domain_Params.Offset,
            Length           =>
              Command.Attach_Memory_Region_Domain_Params.Length,
            Index            =>
              Command.Attach_Memory_Region_Domain_Params.Index,
            Writable         =>
              Command.Attach_Memory_Region_Domain_Params.Writable,
            Executable       =>
              Command.Attach_Memory_Region_Domain_Params.Executable,
            Status           => Status);
      end if;
   end Attach_Memory_Region_Domain;

   -----------------------------------------------------------------------------

   procedure Create_Page_Table_Device_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Page_Table_Device_Domain_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         pragma Assert (Data.Memory.Invariant);

         Commands.Device_Domains.Paging.Create_Page_Table
           (Page            =>
              Command.Create_Page_Table_Device_Domain_Params.Page,
            Domain          =>
              Command.Create_Page_Table_Device_Domain_Params.Domain,
            Level           =>
              Command.Create_Page_Table_Device_Domain_Params.Level,
            Virtual_Address =>
              Command.Create_Page_Table_Device_Domain_Params.Virtual_Address,
            Readable        =>
              Command.Create_Page_Table_Device_Domain_Params.Readable,
            Writable        =>
              Command.Create_Page_Table_Device_Domain_Params.Writable,
            Executable      =>
              Command.Create_Page_Table_Device_Domain_Params.Executable,
            Status          => Status);

         pragma Assert (Data.Memory.Invariant);
      end if;
   end Create_Page_Table_Device_Domain;

   -----------------------------------------------------------------------------

   procedure Create_Device_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Device_Domain_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Create_Domain
           (Domain => Command.Create_Device_Domain_Params.Domain,
            Level  => Command.Create_Device_Domain_Params.Level,
            ID     => Command.Create_Device_Domain_Params.ID,
            Status => Status);
      end if;
   end Create_Device_Domain;

   -----------------------------------------------------------------------------

   procedure Lock_Device_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Lock_Device_Domain_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Lock_Domain
           (Domain => Command.Lock_Device_Domain_Params.Domain,
            Status => Status);
      end if;
   end Lock_Device_Domain;

   -----------------------------------------------------------------------------

   procedure Map_Page_Device_Domain
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Map_Page_Device_Domain_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Device_Domains.Paging.Map_Page
           (Domain          => Command.Map_Page_Device_Domain_Params.Domain,
            Virtual_Address => Command.Map_Page_Device_Domain_Params.Virtual_Address,
            Table_Index     => Command.Map_Page_Device_Domain_Params.Table_Index,
            Offset          => Command.Map_Page_Device_Domain_Params.Offset,
            Status          => Status);
      end if;
   end Map_Page_Device_Domain;

end Tau0.Command_Processor.Device_Domains;
