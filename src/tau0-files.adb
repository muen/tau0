--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Streams.Stream_IO;

package body Tau0.Files
with
  SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   procedure Read_Page
     (Filename :     String;
      Offset   :     Types.Word64;
      Contents : out Types.Page_Array)
   is
      use Ada.Streams;

      File      : Stream_IO.File_Type;
      Last      : Stream_Element_Offset;
      File_Size : Stream_IO.Count;
      Overlay   : Stream_Element_Array
        (Stream_Element_Offset range 0 .. Contents'Size / Stream_Element'Size - 1)
      with Import, Address => Contents'Address;
   begin
      Stream_IO.Open
        (File => File,
         Mode => Stream_IO.In_File,
         Name => Filename);

      File_Size := Stream_IO.Size (File => File);
      if Offset >= Types.Word64 (File_Size) then
         raise Program_Error with "Read_Page: Offset" & Offset'Img
           & " is out of bounds (>" & File_Size'Img & ")";
      end if;

      Stream_IO.Read
        (File => File,
         Item => Overlay,
         Last => Last,
         From => Stream_IO.Positive_Count (Offset + 1));

      --  Add padding (if file segment shorter than page)
      for Q of Overlay (Last + 1 .. Overlay'Last) loop
         Q := 0;
      end loop;

      Stream_IO.Close (File => File);
   end Read_Page;

end Tau0.Files;
