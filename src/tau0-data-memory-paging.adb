--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug;
with Tau0.Data.Memory.Accessibility;
with Tau0.Data.Roots.Writers.Memory_Regions;

--  TODO: Add Translatable as precondition to procedures -> should obviate need
--  for Success out parameter

package body Tau0.Data.Memory.Paging
is

   --  TODO: Move?
   procedure Lemma_Distinct (A, B : Tau0_Address_Type)
   with
     Ghost,
     Pre  => Get_Full_Typization (A) /= Get_Full_Typization (B),
     Post => not Page.Within (A, B) and not Page.Within (B, A);

   procedure Lemma_VTd
     (Old_Memory, New_Memory : Memory_Model_Type;
      Modified               : Types.Tau0_Address_Type)
   with
     Ghost,
     Pre  => Data.Roots.Invariant (Old_Memory) and
             Get_Full_Typization (Modified) /= T.VTd_Root_Table and
             (for all A in Types.Tau0_Address_Type =>
             (if not Page.Within (Address => A, Page => Modified)
              then New_Memory (To_Range (A)) =  Old_Memory (To_Range (A)))),
     Post => Data.Roots.Invariant (New_Memory);

   -----------------------------------------------------------------------------

   --  TODO (dynamic mode): add unmapping from Tau0!
   procedure Activate_Page
     (Root            :     Types.Root_Range;
      Virtual_Address :     Virtual_Address_Type;
      Level           :     Level_Type;
      Success         : out Boolean)
   is
      Translation : Result_Type;
      PTE         : Page_Table.Entry_Type;
   begin
      if Level = Data.Roots.Level (Root) - 1 then
         Success := Level = 0 or else
                    Toplevel_Page_Table_Entries_Active (Root => Root);

         if Success then
            Data.Roots.Writers.Activate_PTP (Root => Root);
         else
            pragma Debug
              (Debug.Put_Line
                 ("Activate_Page: not all inferiors are active. Aborting.",
                  Level => Debug.Error));
         end if;
      else
         Translation := Translate (Root            => Root,
                                   Virtual_Address => Virtual_Address,
                                   Level           => Level + 1);

         Success := Translation.Success and then not Translation.Active;

         if not Success then
            pragma Debug
              (Debug.Put_Line ("Activate_Page: PTE pointing to page is active"));

            return;
         end if;

         PTE     := To_Entry (Root  => Root,
                              Word  => Low_Level.Get (Address => Translation.Address),
                              Level => Level + 1);
         Success := not PTE.Active and PTE.Present;

         if not Success then
            pragma Debug
              (not PTE.Present,
               Debug.Put_Line ("Activate_Page: Page not present",
                               Level => Debug.Warning));
            pragma Debug
              (PTE.Active,
               Debug.Put_Line ("Activate_Page: Page already active",
                               Level => Debug.Warning));
            pragma Debug (Debug.Put_Line ("Activate_Page: Error. Aborting."));

            return;
         end if;

         if Level > 0 then  --  Check whether inferior PTEs are active
            declare
               package FT renames Full_Typization;
               Extension : FT.Path_Type with Ghost;
               Last      : FT.Path_Index with Ghost;
            begin
               Last             := Translation.Path_Last + 1;
               Extension        := Translation.Path;
               Extension (Last) := FT.Cell_Type'(PTE.Address, 0);

               FT.Reachable_Step (Root, Extension, PTE.Address, Last);
               FT.Active_Cong (Root, Translation.Path, Extension, Last - 1);
               FT.Paging_Typization_Lemma (Root, PTE.Address, Extension, Last);
               Accessibility.Paging_Lemma (Root, PTE.Address, Extension, Last);

               Success := Page_Table_Entries_Active
                 (Root          => Root,
                  Table_Address => PTE.Address,
                  Table_Level   => Level);

               if not Success then
                  pragma Debug
                    (Debug.Put_Line
                       ("Activate_Page: not all inferior PTEs are active. Aborting.",
                        Level => Debug.Error));

                  return;
               end if;
            end;
         end if;

         PTE.Active := True;

         Low_Level.Set (Address => Translation.Address,
                        Value   => From_Entry (Root => Root, Entr => PTE));
      end if;
   end Activate_Page;

   -----------------------------------------------------------------------------

   procedure Add_Mapping
     (Root            :     Types.Root_Range;
      Virtual_Address :     Virtual_Address_Type;
      Level           :     Level_Type;
      Page            :     Tau0_Address_Type;
      Readable        :     Boolean;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Active          :     Boolean                := False;
      Caching         :     Types.Opt_Caching_Type := Types.Ignore;
      Success         : out Boolean)
   is
      Old_Memory  : Memory_Model_Type with Ghost;
      Translation : Result_Type;
      PTE         : Page_Table.Entry_Type;
   begin
      if Level = Data.Roots.Level (Root) - 1 then  --  We must modify the root PTP
         Data.Roots.Writers.Set_PTP (Root => Root, Address => Page);
         Success := True;
      else
         Translation := Translate (Root            => Root,
                                   Virtual_Address => Virtual_Address,
                                   Level           => Level + 1);

         Success := Translation.Success and then not Translation.Active;

         if not Success then
            pragma Debug (Debug. Put_Line
                            ("Add_Mapping: PTE pointing to page is active"));

            return;
         end if;

         --  FIXME: add precondition on "Get_Entry"
         PTE     := To_Entry (Root  => Root,
                              Word  => Low_Level.Get (Address => Translation.Address),
                              Level => Level + 1);
         Success := not PTE.Present;

         if not Success then
            pragma Debug
              (PTE.Present,
               Debug.Put_Line
                 ("Add_Mapping: A mapping for this virtual address already exists.",
                  Level => Debug.Error));
            pragma Debug (Debug.Put_Line ("Create_Page: Aborting"));

            return;
         end if;

         PTE.Readable   := Readable;
         PTE.Writable   := Writable;
         PTE.Executable := Executable;
         PTE.Address    := Page;
         PTE.Present    := True;
         PTE.Active     := Active;

         if Caching /= Types.Ignore then
            PTE.Caching := Caching;
         end if;

         Old_Memory := Get_Memory_Model;

         Low_Level.Set (Address => Translation.Address,
                        Value   => From_Entry (Root => Root, Entr => PTE));

         Lemma_VTd (Old_Memory => Old_Memory,
                    New_Memory => Get_Memory_Model,
                    Modified   => Translation.Address);
      end if;

      pragma Assert (Invariant);
   end Add_Mapping;

   -----------------------------------------------------------------------------

   procedure Append_Page_Contents
     (Root     :     Types.Root_Range;
      Page     :     Types.Physical_Address_Type;
      Contents :     Page_Array;
      Success  : out Boolean)
   is
      Old_Memory : Memory_Model_Type with Ghost;
   begin
      Append_Page_Vacuous (Root => Root, Page => Page, Success => Success);

      if not Success then
         return;
      end if;

      Old_Memory := Get_Memory_Model;

      Write_Page
        (Page     => Page,
         Contents => Contents);

      Lemma_VTd (Old_Memory => Old_Memory,
                 New_Memory => Get_Memory_Model,
                 Modified   => Page);
   end Append_Page_Contents;

   -----------------------------------------------------------------------------

   procedure Append_Page_Vacuous
     (Root    :     Types.Root_Range;
      Page    :     Types.Physical_Address_Type;
      Success : out Boolean)
   is
   begin
      Add_Mapping
        (Root            => Root,
         Virtual_Address => Types.From_Page_Count
           (Count => Data.Roots.Length (Root => Root)),
         Level           => 0,
         Page            => Page,
         Readable        => True,
         Writable        => True,
         Executable      => False,
         Success         => Success);

      if not Success then
         return;
      end if;

      Data.Roots.Writers.Memory_Regions.Increment_Length (Root => Root);

      Typization.Set (Page, Types.Typization.Used);
   end Append_Page_Vacuous;

   -----------------------------------------------------------------------------

   --  TODO: precondition in EPT instantiation that Readable ∨ Writable ∨ Executable
   procedure Create_Page
     (Root            :     Types.Root_Range;
      Virtual_Address :     Virtual_Address_Type;
      Level           :     Page_Level_Type;
      Page            :     Tau0_Address_Type;
      Readable        :     Boolean;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Success         : out Boolean)
   is
   begin
      Add_Mapping
        (Root            => Root,
         Virtual_Address => Virtual_Address,
         Level           => Level,
         Page            => Page,
         Readable        => Readable,
         Writable        => Writable,
         Executable      => Executable,
         Success         => Success);

      if Success then
         pragma Assert (Get_Typization (Page) = Types.Typization.Zeroed);
         Typization.Set (Address => Page, Typization => Types.Typization.Used);
      end if;
   end Create_Page;

   -----------------------------------------------------------------------------

   function From_Entry
     (Root  : Types.Root_Range;
      Entr  : Types.Page_Table.Entry_Type)
      return Types.Word64
   is (Types.Page_Table.From_Entry
         (Paging => Data.Roots.Paging (Root),
          Entr   => Entr));

   -----------------------------------------------------------------------------

   procedure Lemma_Distinct (A, B : Tau0_Address_Type)
   is
      pragma Unreferenced (A);
      pragma Unreferenced (B);
   begin
      Full_Typization.Full_Typization_Page;
   end Lemma_Distinct;

   -----------------------------------------------------------------------------

   procedure Lemma_VTd
     (Old_Memory, New_Memory : Memory_Model_Type;
      Modified               : Types.Tau0_Address_Type)
   is
   begin
      if VTd_IOMMU.Root_Table_Present then
         Full_Typization.VTd_Root_Table_Lemma (VTd_IOMMU.Root_Table_Address);
         Lemma_Distinct (VTd_IOMMU.Root_Table_Address, Modified);

         Data.Roots.Lemma_Invariant
           (Old_Memory => Old_Memory,
            New_Memory => New_Memory);
      end if;
   end Lemma_VTd;

   -----------------------------------------------------------------------------

   function May_Dereference_Active_Page
     (Root : Types.Root_Range)
      return Boolean
   is (Data.Roots.Kind (Root) = Types.Root.Memory_Region);

   -----------------------------------------------------------------------------

   procedure Map_Device_Page
     (Root            :     Types.Root_Range;
      Device          :     Types.Device.Device_Range;
      Page            :     Types.Physical_Address_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Writable        :     Boolean;
      Executable      :     Boolean;
      Success         : out Boolean)
   is
   begin
      Full_Typization.Device_Page_Lemma
        (Device  => Device,
         Address => Page);

      Add_Mapping
        (Root            => Root,
         Virtual_Address => Virtual_Address,
         Level           => 0,
         Page            => Page,
         Readable        => True,
         Writable        => Writable,
         Executable      => Executable,
         Caching         => Data.Devices.Caching (Device  => Device,
                                                  Address => Page),
         Active          => True,
         Success         => Success);
   end Map_Device_Page;

   -----------------------------------------------------------------------------

   procedure Map_Page
     (Root            :     Types.Root_Range;
      Index           :     Types.Memory_Region_Table_Range;
      Offset          :     Page_Count_Type;
      Virtual_Address :     Virtual_Address_Type;
      Success         : out Boolean)
   is
      Translation : Result_Type;
   begin
      Translation := Translate
        (Root            => Data.Memory_Region_Table.Region (Index => Index),
         Virtual_Address => Types.From_Page_Count (Count => Offset),
         Level           => 0);

      if not Translation.Success then
         Success := False;
         return;
      end if;

      Add_Mapping
        (Root            => Root,
         Virtual_Address => Virtual_Address,
         Level           => 0,
         Page            => Translation.Address,
         Readable        => True,
         Writable        => Data.Memory_Region_Table.Writable (Index => Index),
         Executable      => Data.Memory_Region_Table.Executable (Index => Index),
         Caching         => Data.Roots.Caching
           (Root => Data.Memory_Region_Table.Region (Index => Index)),
         Active          => True,
         Success         => Success);

      pragma Assert (Memory_Region_Table.Invariant);

      if Success then
         Data.Memory_Region_Table.Add_Page
           (Index   => Index,
            Offset  => Offset,
            Address => Virtual_Address);
      end if;
   end Map_Page;

   -----------------------------------------------------------------------------

   function Page_Table_Offset
     (Virtual_Address : Virtual_Address_Type;
      Level           : Page_Level_Type)
      return Page_Table_Offset_Type
   is
      --      ---------------------------------------------
      --      |  9   |   9    |  9    |   9    |    12    |  Virtual_Address
      --      ---------------------------------------------
      --  Lvl    4       3       2        1

      --  We use the modular type Word64 for better proofs
      Address_W     : constant Word64  := Word64 (Virtual_Address);
      Mask_Bits     : constant         := 9;
      Bytes_Per_PTE : constant         := 8;
      Mask          : constant         := 2 ** Mask_Bits - 1;
      Shift         : constant Integer := 12 + (Level - 1) * 9;
      PTE_Index     : constant Word64  := (Address_W / 2 ** Shift) and Mask;
      PTE_Offset    : constant Word64  := PTE_Index * Bytes_Per_PTE;
   begin
      pragma Assert (PTE_Offset mod 8 = 0);

      return Tau0_Address_Type (PTE_Offset);
   end Page_Table_Offset;

   -----------------------------------------------------------------------------

   function Page_Frame_Offset
     (Virtual_Address : Virtual_Address_Type)
      return Page_Frame_Offset_Type
   is
      Mask   : constant Word64 := Config.Page_Size - 1;
      Result : constant Word64 := Word64 (Virtual_Address) and Mask;
   begin
      pragma Assert (Result mod 8 = Word64 (Virtual_Address) mod 8);
      pragma Assert (if Virtual_Address mod Config.Page_Size = 0
                     then Result = 0);

      return Page_Frame_Offset_Type (Result);
   end Page_Frame_Offset;

   -----------------------------------------------------------------------------

   function Page_Table_Entries_Active
     (Root          : Types.Root_Range;
      Table_Address : Tau0_Address_Type;
      Table_Level   : Types.Page_Level_Type)
      return Boolean
   is
   begin
      for Offset in Tau0_Address_Type
        range 0 .. Page_Table.Entries_Per_Page - 1
      loop
         declare
            Current_Address : constant Tau0_Address_Type
              := Table_Address + Offset * Page_Table.Bytes_PTE;
         begin
            Accessible_Lemma (Table_Address, Current_Address);

            declare
               Current_Entry : constant Page_Table.Entry_Type
                 := To_Entry (Root  => Root,
                              Word  => Low_Level.Get (Current_Address),
                              Level => Table_Level);
            begin
               if Current_Entry.Present and not Current_Entry.Active then
                  return False;
               end if;
            end;
         end;
      end loop;

      return True;
   end Page_Table_Entries_Active;

   -----------------------------------------------------------------------------

   function To_Entry
     (Root  : Types.Root_Range;
      Word  : Types.Word64;
      Level : Types.Page_Level_Type)
      return Types.Page_Table.Entry_Type
   is (Types.Page_Table.To_Entry
         (Paging => Data.Roots.Paging (Root),
          Word   => Word,
          Level  => Level));

   -----------------------------------------------------------------------------

   function Toplevel_Page_Table_Entries_Active
     (Root : Types.Root_Range)
      return Boolean
   is
      Table_Address : constant Tau0_Address_Type
        := Data.Roots.PTP (Root => Root);
      Path : constant Full_Typization.Path_Type
        := (1      => (Page   => Table_Address,
                       Offset => 0),
            others => (0, 0))
      with Ghost;
   begin
      Full_Typization.Paging_Typization_Lemma
        (Root      => Root,
         Address   => Table_Address,
         Path      => Path,
         Path_Last => 1);
      Accessibility.Paging_Lemma
        (Root      => Root,
         Address   => Table_Address,
         Path      => Path,
         Path_Last => 1);

      return Page_Table_Entries_Active
        (Root          => Root,
         Table_Address => Table_Address,
         Table_Level   => Data.Roots.Level (Root) - 1);
   end Toplevel_Page_Table_Entries_Active;

   -----------------------------------------------------------------------------

   function Translate
     (Root            : Types.Root_Range;
      Virtual_Address : Virtual_Address_Type;
      Level           : Level_Type) -- 0 .. 4 at most
      return Result_Type
   is
      package FT renames Full_Typization;

      Result        : Result_Type;
      Current_Entry : Page_Table.Entry_Type;
      Current_Level : Natural := Data.Roots.Level (Root) - 1;

      --  FIXME: Storing Path in Result leads to some failed VCs
      Path      : FT.Path_Type := FT.Null_Path;
      Path_Last : Positive     := Path'First;
      Old_Path  : FT.Path_Type with Ghost;

      function Current_Offset return Tau0_Address_Type
      is (if Current_Level = 0
          then Page_Frame_Offset (Virtual_Address)
          else Page_Table_Offset (Virtual_Address, Current_Level))
      with
         Pre  => Current_Level <= 4,
         Post => (if Aligned_64 (Virtual_Address)
                  then Aligned_64 (Current_Offset'Result)) and
                 (if Current_Level = 0 and Types.Aligned_Page (Virtual_Address)
                  then Current_Offset'Result = 0);

   begin
      Result := Result_Type'
        (Success   => True,
         Active    => Data.Roots.PTP_Active (Root),
         Readable  => True,
         Writable  => True,
         Address   => Data.Roots.PTP (Root) + Current_Offset,
         Path      => Path,
         Path_Last => Path_Last);
      Path (Path_Last) := FT.Cell_Type'
        (Page   => Data.Roots.PTP (Root),
         Offset => Current_Offset);

      while Current_Level > Level loop
         pragma Loop_Invariant (FT.Reachable (Root        => Root,
                                              Destination => Result.Address,
                                              Path        => Path,
                                              Path_Last   => Path_Last));
         pragma Loop_Invariant (Current_Level < Data.Roots.Level (Root));
         pragma Loop_Invariant (Aligned_64 (Result.Address));
         pragma Loop_Invariant (Path_Last = Data.Roots.Level (Root) - Current_Level);
         pragma Loop_Invariant (Result.Active =
                                FT.Active (Root, Result.Address, Path, Path_Last));
         pragma Loop_Variant (Decreases => Current_Level);

         if Result.Active and not May_Dereference_Active_Page (Root) then
            return Result_Type'(Success => False);
         end if;

         Accessibility.Paging_Lemma (Root, Result.Address, Path, Path_Last);

         Current_Entry := To_Entry
           (Root  => Root,
            Word  => Low_Level.Get (Result.Address),
            Level => Current_Level);

         if not Current_Entry.Present then
            return Result_Type'(Success => False);
         end if;

         Current_Level    := Current_Level - 1;
         Old_Path         := Path;
         Path_Last        := Path_Last + 1;
         Path (Path_Last) := FT.Cell_Type'(Page   => Current_Entry.Address,
                                           Offset => Current_Offset);
         Result.Active    := Result.Active   or  Current_Entry.Active;
         Result.Readable  := Result.Readable and Current_Entry.Readable;
         Result.Writable  := Result.Writable and Current_Entry.Writable;
         Result.Address   := Current_Entry.Address + Current_Offset;

         pragma Assert (Aligned_64 (Result.Address));

         FT.Reachable_Step (Root, Path, Result.Address, Path_Last);
         FT.Active_Cong (Root, Old_Path, Path, Path_Last - 1);
      end loop;

      Accessibility.Paging_Lemma (Root, Result.Address, Path, Path_Last);
      pragma Assert (if not May_Dereference_Active_Page (Root) or Level = 0
                     then Result.Active = not Accessible (Result.Address)
                     elsif May_Dereference_Active_Page (Root)
                     then Accessible (Result.Address));
      pragma Assert (Aligned_64 (Result.Address));
      pragma Assert (if Aligned_Page (Virtual_Address) and Level = 0
                     then Aligned_Page (Result.Address));

      FT.Paging_Typization_Lemma (Root, Result.Address, Path, Path_Last);

      Result.Path      := Path;
      Result.Path_Last := Path_Last;

      return Result;
   end Translate;

   -----------------------------------------------------------------------------

   procedure Update_Hash
     (Root       :        Types.Root_Range;
      Page_Index :        Types.Page_Count_Type;
      Context    : in out Types.Hash.Context_Type)
   is
      Page        : Types.Page_Array;
      Translation : constant Result_Type
        := Translate (Root            => Root,
                      Virtual_Address => From_Page_Count (Page_Index),
                      Level           => 0);
   begin
      pragma Assert (Translation.Success);    --  TODO: prove
      pragma Assert (not Translation.Active); --  TODO: prove

      --  Read page into array
      for I in Page'Range loop
         declare
            Address : constant Types.Physical_Address_Type
              := Translation.Address + Tau0_Address_Type (I - Page'First) * 8;
         begin
            pragma Assert (Accessible (Address));
            Page (I) := Low_Level.Get (Address => Address);
         end;
      end loop;

      Types.Hash.Update (Context => Context,
                         Input   => Page);
   end Update_Hash;

end Tau0.Data.Memory.Paging;
