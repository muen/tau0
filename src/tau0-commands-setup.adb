--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.VTd_IOMMU.Writers;
with Tau0.Types.Typization;

package body Tau0.Commands.Setup
is

   package TC renames Types.Command;
   package TT renames Types.Typization;

   -----------------------------------------------------------------------------

   procedure Activate_Tau0 (Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Tau0.Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif not Tau0.Setup_Phase_Complete then
         Status := Types.Command.Setup_Phase_Incomplete;
      else
         Set_Tau0_State (State => Running);

         Status := TC.Success;
      end if;
   end Activate_Tau0;

   -----------------------------------------------------------------------------

   procedure Add_Memory_Block
     (Address :     Types.Physical_Address_Type;
      Size    :     Types.Physical_Page_Count_Type;
      Status  : out Types.Command.Status_Type)
   is
      use type Types.Page_Count_Type;
      use type Types.Tau0_Address_Type;
   begin
      if Tau0_State /= Tau0.Setup then
         Status := TC.Tau0_Mode_Invalid;
      elsif Data.Memory.Memory_Blocks_Full then
         Status := TC.Memory_Blocks_Full;
      elsif not Types.Aligned_Page (Address) then
         Status := TC.Address_Invalid;
      elsif
        Address + Types.From_Page_Count (Size) > Types.Physical_Address_Type'Last
      then
         Status := TC.Address_Invalid;
      elsif Size = 0 then
         Status := TC.Invalid_Parameters;
      else
         Data.Memory.Add_Memory_Block (Address => Address, Size => Size);
         Status := TC.Success;
      end if;
   end Add_Memory_Block;

   -----------------------------------------------------------------------------

   procedure Add_Processor
     (ID      :     Types.CPU_Range;
      APIC_ID :     Types.APIC_ID_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Tau0.Setup then
         Status := TC.Tau0_Mode_Invalid;
      elsif Data.Processors.Present (ID => ID) then
         Status := TC.Processor_Present;
      elsif Data.Processors.APIC_ID_Used (APIC_ID => APIC_ID) then
         Status := TC.APIC_ID_Used;
      else
         Data.Processors.Add_Processor
           (ID      => ID,
            APIC_ID => APIC_ID);
         Status := TC.Success;
      end if;
   end Add_Processor;

   -----------------------------------------------------------------------------

   procedure Add_Ioapic
     (Source_ID :     Types.Source_ID_Type;
      Status    : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Tau0.Setup then
         Status := TC.Tau0_Mode_Invalid;
      else
         Data.IO_APICs.Set_Source_ID (ID => Source_ID);
         Status := TC.Success;
      end if;
   end Add_Ioapic;

   -----------------------------------------------------------------------------

   procedure Create_VTd_Context_Table
     (Bus    :     Types.Device.PCI_Bus_Range;
      Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   is
      use type TT.Typization_Type;
   begin
      if Tau0_State /= Tau0.Setup then
         Status := TC.Tau0_Mode_Invalid;
      elsif not Types.Aligned_Page (Address => Page) then
         Status := TC.Address_Invalid;
      elsif Data.Memory.Get_Typization (Page) /= TT.Zeroed then
         Status := TC.Typization_Violation;
      elsif not Data.Memory.VTd_IOMMU.Root_Table_Present then
         Status := TC.VTd_Root_Table_Not_Present;
      elsif Data.Memory.VTd_IOMMU.Context_Table_Present (Bus => Bus) then
         Status := TC.VTd_Context_Table_Present;
      else
         Data.Memory.VTd_IOMMU.Writers.Create_Context_Table
           (Address => Page,
            Bus     => Bus);
         Status := TC.Success;
      end if;
   end Create_VTd_Context_Table;

   -----------------------------------------------------------------------------

   procedure Create_VTd_IRQ_Remap_Table
     (Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   is
      use type TT.Typization_Type;
   begin
      if Tau0_State /= Tau0.Setup then
         Status := TC.Tau0_Mode_Invalid;
      elsif not Types.Aligned_Page (Address => Page) then
         Status := TC.Address_Invalid;
      elsif Data.Memory.Get_Typization (Page) /= TT.Zeroed then
         Status := TC.Typization_Violation;
      elsif Data.Memory.Interrupt_Remapping.Table_Present then
         Status := TC.VTd_IRQ_Remap_Table_Present;
      else
         Data.Memory.Interrupt_Remapping.Create_Table (Page => Page);
         Status := TC.Success;
      end if;
   end Create_VTd_IRQ_Remap_Table;

   -----------------------------------------------------------------------------

   procedure Create_VTd_Root_Table
     (Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   is
      use type TT.Typization_Type;
   begin
      if Tau0_State /= Tau0.Setup then
         Status := TC.Tau0_Mode_Invalid;
      elsif not Types.Aligned_Page (Address => Page) then
         Status := TC.Address_Invalid;
      elsif Data.Memory.Get_Typization (Page) /= TT.Zeroed then
         Status := TC.Typization_Violation;
      elsif Data.Memory.VTd_IOMMU.Root_Table_Present then
         Status := TC.VTd_Root_Table_Present;
      else
         Data.Memory.VTd_IOMMU.Writers.Create_Root_Table (Address => Page);
         Status := TC.Success;
      end if;
   end Create_VTd_Root_Table;

end Tau0.Commands.Setup;
