--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with IO_Port_Set;
with Tau0.Types.Device;

--  Manage device information
package Tau0.Data.Devices
with Abstract_State => State
is

   pragma Elaborate_Body;

   use type Types.Device.State_Type;
   use type Types.IO_Port_Type;

   -----------------------------------------------------------------------------

   function State
     (Device : Types.Device.Device_Range)
      return Types.Device.State_Type
   with Global => State;

   function Used (Device : Types.Device.Device_Range) return Boolean
   with
     Global => State,
     Post   => Used'Result = (State (Device) /= Types.Device.Unused);

   --  True iff Device is a PCI device
   function Is_PCI (Device : Types.Device.Device_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Device);

   function Uses_MSI (Device : Types.Device.Device_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Device) and then Is_PCI (Device);

   function Uses_IOAPIC (Device : Types.Device.Device_Range) return Boolean
   with
      Global => State,
      Pre    => Used (Device);

   function PCI_Bus
     (Device : Types.Device.Device_Range)
      return Types.Device.PCI_Bus_Range
   with
     Global => State,
     Pre    => Used (Device) and then Is_PCI (Device);

   function PCI_Dev
     (Device : Types.Device.Device_Range)
      return Types.Device.PCI_Device_Range
   with
     Global => State,
     Pre    => Used (Device) and then Is_PCI (Device);

   function PCI_Func
     (Device : Types.Device.Device_Range)
      return Types.Device.PCI_Function_Range
   with
     Global => State,
     Pre    => Used (Device) and then Is_PCI (Device);

   --  Check whether Device owns the IO port interval [From, To]
   function Owns_IO_Port_Range
     (Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type)
      return Boolean
   with
     Global => State,
     Pre    => Used (Device) and
               From <= To;

   --  Check whether Device owns IRQ
   function Owns_IRQ
     (Device : Types.Device.Device_Range;
      IRQ    : Types.Device.IRQ_Type)
      return Boolean
   with
     Global => State,
     Pre    => Used (Device);

   --  Check whether Device owns Address
   function Owns_Page
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type)
      return Boolean
   with
     Global => State,
     Pre    => Used (Device);

   function IRQ_Table_Full (Device : Types.Device.Device_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Device);

   function Memory_Table_Full (Device : Types.Device.Device_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Device);

   function IO_Port_Set_Full (Device : Types.Device.Device_Range) return Boolean
   with
     Global => State,
     Pre    => Used (Device);

   --  True iff there is a device that owns IRQ
   function IRQ_Used (IRQ : Types.Device.HW_IRQ_Type) return Boolean
   with
     Global => State;

   --  True if there is a device that owns some page in the interval
   --  [Address, Address + Size - 1]
   function Memory_Used
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
      return Boolean
   with Global => State;

   --  True iff there is a device that owns some IO port in the range [From, To]
   function Some_IO_Port_Used
     (From : Types.IO_Port_Type;
      To   : Types.IO_Port_Type)
      return Boolean
   with
     Global => State,
     Pre    => From <= To;

   --  Get caching mode of memory range that contains Address
   function Caching
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type)
      return Types.Caching_Type
   with
     Global => State,
     Pre    => Used (Device) and then Owns_Page (Device, Address);

private

   use type Types.Device.IRQ_Type;
   use type Types.Physical_Address_Type;

   Devices : Types.Device.Device_Array := (others => Types.Device.Null_Device)
   with Part_Of => State;

   function Overlaps
     (L_Address, R_Address : Types.Physical_Address_Type;
      L_Size,    R_Size    : Types.Physical_Address_Type)
      return Boolean;

   -----------------------------------------------------------------------------

   function IO_Port_Set_Full (Device : Types.Device.Device_Range) return Boolean
   is (IO_Port_Set.Full (Set => Devices (Device).IO_Ports));

   -----------------------------------------------------------------------------

   function IRQ_Used (IRQ : Types.Device.HW_IRQ_Type) return Boolean
   is (for some Dev of Devices =>
       Dev.State /= Types.Device.Unused and then
       Types.Device.IRQ_Table.Member (Table => Dev.IRQs, Elem => IRQ));

   -----------------------------------------------------------------------------

   function IRQ_Table_Full (Device : Types.Device.Device_Range) return Boolean
   is (Types.Device.IRQ_Table.Full (Table => Devices (Device).IRQs));

   -----------------------------------------------------------------------------

   function Is_PCI (Device : Types.Device.Device_Range) return Boolean
   is (Devices (Device).Is_PCI);

   -----------------------------------------------------------------------------

   function Memory_Used
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
      return Boolean
   is (for some Dev of Devices =>
       Dev.State /= Types.Device.Unused and then
       (for some Mem of Dev.Memory =>
        Overlaps (L_Address => Mem.Physical_Address,
                  L_Size    => Mem.Size,
                  R_Address => Address,
                  R_Size    => Types.From_Page_Count (Size))));

   -----------------------------------------------------------------------------

   function Memory_Table_Full (Device : Types.Device.Device_Range) return Boolean
   is (Types.Device.Memory_Table.Full (Table => Devices (Device).Memory));

   -----------------------------------------------------------------------------

   function Overlaps
     (L_Address, R_Address : Types.Physical_Address_Type;
      L_Size,    R_Size    : Types.Physical_Address_Type)
      return Boolean
   is ((L_Address in R_Address .. R_Address + R_Size - 1) or
       (R_Address in L_Address .. L_Address + L_Size - 1) or
       (R_Address + R_Size - 1 in L_Address .. L_Address + L_Size - 1));

   -----------------------------------------------------------------------------

   function Owns_IO_Port_Range
     (Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type)
      return Boolean
   is (IO_Port_Set.Contains
           (Set  => Devices (Device).IO_Ports,
            From => From,
            To   => To));

   -----------------------------------------------------------------------------

   function Owns_IRQ
     (Device : Types.Device.Device_Range;
      IRQ    : Types.Device.IRQ_Type)
      return Boolean
   is (for some X of Devices (Device).IRQs => IRQ = X);

   -----------------------------------------------------------------------------

   function Owns_Page
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type)
      return Boolean
   is (for some M of Devices (Device).Memory =>
       Address in M.Physical_Address .. M.Physical_Address + M.Size - 1);

   -----------------------------------------------------------------------------

   function PCI_Bus
     (Device : Types.Device.Device_Range)
      return Types.Device.PCI_Bus_Range
   is (Devices (Device).PCI.Bus);

   -----------------------------------------------------------------------------

   function PCI_Dev
     (Device : Types.Device.Device_Range)
      return Types.Device.PCI_Device_Range
   is (Devices (Device).PCI.Device);

   -----------------------------------------------------------------------------

   function PCI_Func
     (Device : Types.Device.Device_Range)
      return Types.Device.PCI_Function_Range
   is (Devices (Device).PCI.Func);

   -----------------------------------------------------------------------------

   --  OPTIMIZE: - Use Boolean array (8 KB)
   --            - Use IO_Port_Set 'Used' (Con: what size?)
   function Some_IO_Port_Used
     (From : Types.IO_Port_Type;
      To   : Types.IO_Port_Type)
      return Boolean
   is (for some Dev of Devices =>
       Dev.State /= Types.Device.Unused and then
       IO_Port_Set.Contains_Some (Set  => Dev.IO_Ports,
                                  From => From,
                                  To   => To));

   -----------------------------------------------------------------------------

   function State
     (Device : Types.Device.Device_Range)
      return Types.Device.State_Type
   is (Devices (Device).State);

   -----------------------------------------------------------------------------

   function Used (Device : Types.Device.Device_Range) return Boolean
   is (Devices (Device).State /= Types.Device.Unused);

   -----------------------------------------------------------------------------

   function Uses_MSI (Device : Types.Device.Device_Range) return Boolean
   is (Devices (Device).PCI.Uses_MSI);

   -----------------------------------------------------------------------------

   function Uses_IOAPIC (Device : Types.Device.Device_Range) return Boolean
   is (not (Is_PCI (Device => Device) and then Uses_MSI (Device => Device)));

end Tau0.Data.Devices;
