--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Bounded_Interval_Union.Lemmas
is

   -----------------------------------------------------------------------------

   procedure Coalesce_Lemma
     (Old_Set, New_Set     : T;
      Old_Right, New_Right : Cell_Range;
      To                   : Elem_Type)
   is
   begin
      pragma Assert
        (for all I in Elem_Type =>
        (if To < I and I < Old_Set.Elems (Old_Right + 1).From
         then Model (New_Set) (I) = Model (Old_Set) (I)));

      for Idx in Old_Right + 1 .. Old_Set.Last - 1 loop
         pragma Assert
           (Old_Set.Elems (Idx) = New_Set.Elems (Idx - Old_Right + New_Right));
         pragma Assert
           (for all I in Elem_Type =>
           (if Old_Set.Elems (Idx).From <= I and I < Old_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));
         pragma Loop_Invariant
           (for all I in Elem_Type =>
           (if To < I and I < Old_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));
      end loop;

      pragma Assert
        (Old_Set.Elems (Old_Set.Last) = New_Set.Elems (New_Set.Last));
      pragma Assert
        (for all I in Old_Set.Elems (Old_Set.Last).From .. Elem_Type'Last =>
           Model (New_Set) (I) = Model (Old_Set) (I));
   end Coalesce_Lemma;

   -----------------------------------------------------------------------------

   procedure Delete_Lemma
     (Old_Set, New_Set       : T;
      Delete_From, Delete_To : Cell_Range)
   is
      To : constant Elem_Type := Old_Set.Elems (Delete_To).To;
   begin
      if Delete_To = Old_Set.Last then
         pragma Assert
           (for all I in Elem_Type =>
           (if I > To then Model (New_Set) (I) = Model (Old_Set) (I)));
         return;
      end if;

      pragma Assert
        (for all I in Elem_Type =>
        (if To < I and I < Old_Set.Elems (Delete_To + 1).From
         then Model (New_Set) (I) = Model (Old_Set) (I)));

      for Idx in Delete_From .. New_Set.Last - 1 loop
         pragma Assert
           (New_Set.Elems (Idx) = Old_Set.Elems (Idx + Delete_To - Delete_From + 1));

         pragma Assert
           (for all I in Elem_Type =>
           (if New_Set.Elems (Idx).From <= I and
               I < New_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));

         pragma Loop_Invariant
           (for all I in Elem_Type =>
           (if To < I and I < New_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));
      end loop;

      pragma Assert
        (for all I in Elem_Type =>
        (if I > Old_Set.Elems (Delete_To).To
         then Model (New_Set) (I) = Model (Old_Set) (I)));
   end Delete_Lemma;

   -----------------------------------------------------------------------------

   procedure Insert_Lemma
     (Old_Set, New_Set   : T;
      Old_Cell, New_Cell : Cell_Range;
      To                 : Elem_Type)
   is
   begin
      if Old_Cell = Old_Set.Last then
         pragma Assert
           (for all I in Old_Set.Elems (Old_Set.Last).From .. Elem_Type'Last =>
            Model (New_Set) (I) = Model (Old_Set) (I));
         return;
      end if;

      pragma Assert
        (for all I in Elem_Type =>
        (if To < I and I < Old_Set.Elems (Old_Cell).From
         then Model (New_Set) (I) = Model (Old_Set) (I)));

      for Idx in Old_Cell .. Old_Set.Last - 1 loop
         pragma Assert
           (Old_Set.Elems (Idx) = New_Set.Elems (Idx - Old_Cell + New_Cell));
         pragma Assert
           (for all I in Elem_Type =>
           (if Old_Set.Elems (Idx).From <= I and I < Old_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));
         pragma Loop_Invariant
           (for all I in Elem_Type =>
           (if To < I and I < Old_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));
      end loop;

      pragma Assert
        (Old_Set.Elems (Old_Set.Last) = New_Set.Elems (New_Set.Last));
      pragma Assert
        (for all I in Old_Set.Elems (Old_Set.Last).From .. Elem_Type'Last =>
           Model (New_Set) (I) = Model (Old_Set) (I));
   end Insert_Lemma;

   -----------------------------------------------------------------------------

   procedure Split_Lemma
     (Old_Set, New_Set   : T;
      Old_Cell, New_Cell : Cell_Range)
   is
      To : constant Elem_Type := New_Set.Elems (New_Cell).To;
   begin
      if New_Cell = New_Set.Last then
         pragma Assert
           (for all I in Elem_Type =>
           (if I > New_Set.Elems (New_Cell).To
            then Model (New_Set) (I) = Model (Old_Set) (I)));
         return;
      end if;

      pragma Assert
        (for all I in Elem_Type =>
        (if To < I and I < New_Set.Elems (New_Cell + 1).From
         then Model (New_Set) (I) = Model (Old_Set) (I)));

      for Idx in New_Cell + 1 .. New_Set.Last - 1 loop
         pragma Assert
           (New_Set.Elems (Idx) = Old_Set.Elems (Idx - New_Cell + Old_Cell));

         pragma Assert
           (for all I in Elem_Type =>
           (if New_Set.Elems (Idx).From <= I and
               I < New_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));

         pragma Loop_Invariant
           (for all I in Elem_Type =>
           (if To < I and I < New_Set.Elems (Idx + 1).From
            then Model (New_Set) (I) = Model (Old_Set) (I)));
      end loop;

      pragma Assert (for all I in Elem_Type =>
                    (if I > New_Set.Elems (New_Cell).To
                     then Model (New_Set) (I) = Model (Old_Set) (I)));
   end Split_Lemma;

end Bounded_Interval_Union.Lemmas;
