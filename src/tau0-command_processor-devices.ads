--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Command;

package Tau0.Command_Processor.Devices
with Abstract_State => null
is

   use type Types.Command.Command_ID_Type;

   -----------------------------------------------------------------------------

   procedure Create_Legacy_Device
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Create_Legacy_Device,
     Post => Invariants;

   procedure Create_PCI_Device
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Create_PCI_Device,
     Post => Invariants;

   procedure Add_IO_Port_Range
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Add_IO_Port_Range_Device,
     Post => Invariants;

   procedure Add_IRQ
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Add_IRQ_Device,
     Post => Invariants;

   procedure Add_Memory
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Add_Memory_Device,
     Post => Invariants;

   procedure Activate
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Activate_Device,
     Post => Invariants;

end Tau0.Command_Processor.Devices;
