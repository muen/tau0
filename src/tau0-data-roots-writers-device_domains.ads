--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Types.VTd_Tables;

package Tau0.Data.Roots.Writers.Device_Domains
is

   function Domain_ID_Used
     (ID : Types.VTd_Tables.Domain_Identifier_Type)
      return Boolean;

   procedure Activate (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Typization_State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State),
                In_Out   => (Data.Memory.Memory_State, State)),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Locked and then
               Kind (Root) = Types.Root.Device_Domain and then
               not Active (Root) and then
               (if Paging (Root) in Types.Root.Paging_Type
                then PTP_Active (Root)) and then
               VTd_Tables_Valid (Root),
     Post   => Invariant and
               Data.Memory.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Update may not modify discriminants
               State (Root) = Types.Root.Active and
               Kind (Root) = Kind (Root)'Old and
               PTP_Present (Root) = PTP_Present (Root)'Old and
               PTP_Active (Root) = PTP_Active (Root)'Old and
               PTP (Root) = PTP (Root)'Old;
               --  TODO: maybe we must state more about paging

   procedure Create_Device_Domain
     (Root  : Types.Root_Range;
      Level : Types.Nonleaf_Level_Type;
      ID    : Types.VTd_Tables.Domain_Identifier_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and
               Data.Memory_Region_Table.Invariant and
               Tau0_State = Running and
               State (Root) = Types.Root.Unused and
               Level in 4 | 5 and
               not Domain_ID_Used (ID),
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               State (Root) = Types.Root.Setup and
               Kind (Root) = Types.Root.Device_Domain and
               not PTP_Present (Root) and
               not PTP_Active (Root);

   procedure Add_Device
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Device_Domain and then
               Data.Devices.Used (Device) and then
               Data.Devices.Is_PCI (Device) and then
               Memory.VTd_IOMMU.Root_Table_Present and then
               Memory.VTd_IOMMU.Context_Table_Present
                 (Devices.PCI_Bus (Device)) and then
               not Device_Table_Full (Root) and then
               not Device_Included (Device) and then
               Data.Devices.State (Device) = Types.Device.Active and then
               Data.Devices.Is_PCI (Device),
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Devices updated, everything else unchanged
               Get_Roots (Root) = (Get_Roots (Root)'Old with delta
                 Domain_Devices => Get_Roots (Root).Domain_Devices) and
               Includes_Device (Root, Device);

private

   function Domain_ID_Used
     (Roots : Types.Root.Root_Array;
      ID    : Types.VTd_Tables.Domain_Identifier_Type)
      return Boolean
   with Global => null;

   procedure Lemma_Domain_ID
     (Old_Roots  : Types.Root.Root_Array;
      New_Roots  : Types.Root.Root_Array;
      New_Domain : Types.Root_Range;
      ID         : Types.VTd_Tables.Domain_Identifier_Type)
   with
     Ghost,
     Global => null,
     Pre    => Invariant_IDs_Device_Domain (Old_Roots) and then
               not Domain_ID_Used (Old_Roots, ID) and then
               (for all Root in Old_Roots'Range =>
               (if Root /= New_Domain
                then New_Roots (Root) = Old_Roots (Root))) and then
               New_Roots (New_Domain).State /= Types.Root.Unused and then
               New_Roots (New_Domain).Kind = Types.Root.Device_Domain and then
               New_Roots (New_Domain).Domain_ID = ID,
     Post   => Invariant_IDs_Device_Domain (New_Roots);

   -----------------------------------------------------------------------------

   function Domain_ID_Used
     (Roots : Types.Root.Root_Array;
      ID    : Types.VTd_Tables.Domain_Identifier_Type)
      return Boolean
   is (for some Root in Roots'Range =>
         Roots (Root).State /= Types.Root.Unused and then
         Roots (Root).Kind = Types.Root.Device_Domain and then
         Roots (Root).Domain_ID = ID);

   function Domain_ID_Used
     (ID : Types.VTd_Tables.Domain_Identifier_Type)
      return Boolean
   is (Domain_ID_Used (Roots => Roots, ID => ID));

end Tau0.Data.Roots.Writers.Device_Domains;
