--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

private generic
package Bounded_Interval_Union.Lemmas
is

   --  Prove in Add_Interval (branch "coalesce") that intervals "to the right"
   --  of inserted intervals stay constant.
   procedure Coalesce_Lemma
     (Old_Set, New_Set     : T;
      Old_Right, New_Right : Cell_Range;
      To                   : Elem_Type)
   with
     Ghost,
     Global => null,
     Pre    => Invariant (Old_Set.Last, Old_Set.Elems) and then
               Invariant (New_Set.Last, New_Set.Elems) and then
               Old_Right < Old_Set.Last and then
               Old_Set.Elems (Old_Right).From <= Clamped_Succ (To) and then
               Clamped_Succ (To) < Old_Set.Elems (Old_Right + 1).From and then
               New_Set.Elems (New_Right).From <= Old_Set.Elems (Old_Right).From and then
               New_Set.Elems (New_Right).To = Elem_Type'Max
                 (Old_Set.Elems (Old_Right).To,
                  To) and then
               New_Set.Last - New_Right = Old_Set.Last - Old_Right and then
               (for all I in New_Right + 1 .. New_Set.Last =>
                New_Set.Elems (I) = Old_Set.Elems (I - New_Right + Old_Right)),
     Post   => (for all I in Elem_Type =>
                  (if I > To then Model (New_Set) (I) = Model (Old_Set) (I)));

   --  Prove in Add_Interval (branch "insert") that intervals "to the right"
   --  of inserted intervals stay constant.
   --  FIXME: Share proof code with Coalesce_Lemma
   procedure Insert_Lemma
     (Old_Set, New_Set   : T;
      Old_Cell, New_Cell : Cell_Range;
      To                 : Elem_Type)
   with
     Ghost,
     Global => null,
     Pre    => Invariant (Old_Set.Last, Old_Set.Elems) and then
               Invariant (New_Set.Last, New_Set.Elems) and then
               Old_Cell <= Old_Set.Last and then
               New_Cell <= New_Set.Last and then
               New_Set.Last - New_Cell = Old_Set.Last - Old_Cell and then
               (for all I in New_Cell .. New_Set.Last =>
                New_Set.Elems (I) = Old_Set.Elems (I - New_Cell + Old_Cell)) and then
               To < Old_Set.Elems (Old_Cell).From and then
               (if Old_Cell > Cell_Range'First
                then Old_Set.Elems (Old_Cell - 1).To < To) and then
               (if New_Cell > Cell_Range'First
                then New_Set.Elems (New_Cell - 1).To <= To),
     Post   => (for all I in Elem_Type =>
                  (if I > To then Model (New_Set) (I) = Model (Old_Set) (I)));

   procedure Split_Lemma
     (Old_Set, New_Set   : T;
      Old_Cell, New_Cell : Cell_Range)
   with
     Ghost,
     Global => null,
     Pre    => Invariant (Old_Set.Last, Old_Set.Elems) and then
               Invariant (New_Set.Last, New_Set.Elems) and then
               Old_Cell <= Old_Set.Last and then
               New_Cell <= New_Set.Last and then
               New_Set.Last - New_Cell = Old_Set.Last - Old_Cell and then
               (for all I in New_Cell + 1 .. New_Set.Last =>
                New_Set.Elems (I) = Old_Set.Elems (I - New_Cell + Old_Cell)) and then
               New_Set.Elems (New_Cell).To = Old_Set.Elems (Old_Cell).To,
     Post   => (for all I in Elem_Type =>
                  (if I > New_Set.Elems (New_Cell).To
                   then Model (New_Set) (I) = Model (Old_Set) (I)));

   procedure Delete_Lemma
     (Old_Set, New_Set       : T;
      Delete_From, Delete_To : Cell_Range)
   with
     Ghost,
     Global => null,
     Pre    => Invariant (Old_Set.Last, Old_Set.Elems) and then
               Invariant (New_Set.Last, New_Set.Elems) and then
               Delete_From <= Old_Set.Last and then
               Delete_To <= Old_Set.Last and then
               Delete_From <= Delete_To and then
               New_Set.Last = Old_Set.Last - (Delete_To - Delete_From + 1) and then
               (if Delete_From > Cell_Range'First
                then New_Set.Elems (Delete_From - 1)
                       = Old_Set.Elems (Delete_From - 1)) and then
               (for all I in Delete_From .. New_Set.Last =>
                New_Set.Elems (I) = Old_Set.Elems (I + Delete_To - Delete_From + 1)),
     Post   => (for all I in Elem_Type =>
                  (if I > Old_Set.Elems (Delete_To).To
                   then Model (New_Set) (I) = Model (Old_Set) (I)));

end Bounded_Interval_Union.Lemmas;
