--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;
with Tau0.Page;
with Tau0.Types.Typization;

private with System;

package Tau0.Data.Memory
with
  Abstract_State => (Init_State, Memory_State, Typization_State)
  --  FIXME: state is indeed not initialized.  Moreover we use remote abstract
  --  state without preelaboration, which is forbidden.  We cannot preelaborate
  --  though, since this would lead to a cycle.
is

   package T renames Types.Typization;

   use type T.Full_Typization_Type;
   use type T.Typization_Model_Type;
   use type T.Typization_Type;
   use type Types.Page_Count_Type;
   use type Types.Tau0_Address_Type;
   use type Types.Word64;

   --  Index over Word64 cells of memory
   type Memory_Range is new Types.Tau0_Address_Type
     range 0 .. 2 ** (Config.Bits_Tau0_Address - 3) - 1;

   function To_Range (Address : Types.Tau0_Address_Type) return Memory_Range
   with Global => null;

   function To_Range (Address : Types.Tau0_Address_Type) return Memory_Range
   is (Memory_Range (Address / (Types.Word64'Size / 8)));

   type Memory_Model_Type is array (Memory_Range) of Types.Word64
   with Ghost;

   -----------------------------------------------------------------------------
   --  Verification functions
   -----------------------------------------------------------------------------

   function Initial_Condition return Boolean
   with
     Ghost,
     Global => (Input => Typization_State);

   --  The central invariant for the memory model, to be implemented in
   --  Isabelle.  It shall ensure, i.a., that
   --  - the virtual address spaces of root objects do not overlap, except when
   --    specified by means of a mapped memory region,
   --  - each page has one unique full typization (according to the conditions
   --    described in the README).
   function Invariant return Boolean
   with
     Ghost,
     Global => (Input => (Memory_State, Typization_State)),
     Post   => (if Invariant'Result
                then (for all Addr in Types.Tau0_Address_Type =>
                     (if Get_Typization_Model (Addr) = T.Zeroed
                      then Get_Memory_Model (To_Range (Addr)) = 0)));  --  TODO: extend

   function Get_Memory_Model return Memory_Model_Type
   with
     Ghost,
     Global => (Input => Memory_State);

   function Get_Typization_Model return T.Typization_Model_Type
   with
     Ghost,
     Global => (Input => Typization_State),
     Post   => (for all Addr in Types.Tau0_Address_Type =>
                Get_Typization_Model'Result (Addr) = Get_Typization (Addr));

   function Accessible (Address : Types.Tau0_Address_Type) return Boolean
   with
     Ghost,
     Import,
     Global => (Input => Typization_State),
     Post   => (if Get_Full_Typization (Address) in T.Reserved | T.Device_Page
                then not Accessible'Result) and
               (if Get_Full_Typization (Address) in
                T.Undefined | T.Zeroed | T.VTd_Root_Table
                  | T.VTd_Context_Table | T.VTd_IR_Table
                then Accessible'Result);

   function Get_Full_Typization
     (Address : Types.Tau0_Address_Type)
      return T.Full_Typization_Type
   with
     Ghost,
     Import,
     Global => (Input => Typization_State),
     Post   => (Get_Full_Typization'Result = T.Reserved)
                  = (Get_Typization (Address) = T.Reserved) and
               (Get_Full_Typization'Result = T.Undefined)
                  = (Get_Typization (Address) = T.Undefined) and
               (Get_Full_Typization'Result = T.Zeroed)
                  = (Get_Typization (Address) = T.Zeroed) and
               ((Get_Full_Typization'Result in T.Used_Typization_Type)
                  = (Get_Typization (Address) = T.Used));

   -----------------------------------------------------------------------------

   function Initialized return Boolean
   with
     Global => (Input => Init_State);

   function Get_Typization
     (Address : Types.Tau0_Address_Type)
      return T.Typization_Type
   with
     Global => (Input => Typization_State);

   -----------------------------------------------------------------------------

   --  Initialize memory and typizations.  After initialization, the memory
   --  invariant holds (TODO: prove in Isabelle!)
   procedure Initialize (Success : out Boolean)
   with
     Global => (Proof_In => (Tau0.State, Memory_State, Typization_State),
                In_Out   => (Init_State)),
     Pre    => Tau0_State = Setup and not Initialized and Initial_Condition,
     Post   => (Success = Initialized) and (if Success then Invariant);

   --  Zero Page, set its typization accordingly
   procedure Clear_Page (Page : Types.Tau0_Address_Type)
   with
     Global => (In_Out => (Memory_State, Typization_State)),
     Pre    => Types.Aligned_Page (Page) and
               (Get_Typization (Page) in T.Undefined | T.Zeroed) and
               not Content_Regions_Full and
               Invariant,
     Post   => Get_Typization_Model = (Get_Typization_Model'Old with delta
                Page .. Tau0.Page.Last (Page) => T.Zeroed) and
               Get_Memory_Model = (Get_Memory_Model'Old with delta
                 To_Range (Page) .. To_Range (Tau0.Page.Last (Page)) => 0) and
               Invariant;

   --  Set typization of a page to Used.
   procedure Use_Page (Page : Types.Tau0_Address_Type)
   with
     Global => (Proof_In => Memory_State,
                In_Out   => Typization_State),
     Pre    => Types.Aligned_Page (Page) and
               Get_Typization (Page) in T.Undefined | T.Zeroed and
               Invariant,
     Post   => Get_Typization_Model = (Get_Typization_Model'Old with delta
                 Page .. Tau0.Page.Last (Page) => T.Used) and
               Invariant;

   --  True iff no more memory blocks can be added
   function Memory_Blocks_Full return Boolean;
   --  True iff no more device blocks can be added
   function Device_Blocks_Full return Boolean;
   --  True iff no more content regions can be declared.
   function Content_Regions_Full return Boolean;

   procedure Typization_Lemma
   with
     Ghost,
     Global => (Proof_In => Typization_State),
     Post   =>
       (for all A in Types.Tau0_Address_Type =>
       (for all B in Page.First (A) .. Page.Last (A) =>
        Get_Typization (A) = Get_Typization (B)));

   procedure Add_Memory_Block
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
   with
     Global => (Proof_In => (Tau0.State, Memory_State),
                In_Out   => Typization_State),
     -- REVIEW: demand disjointness with devices?
     Pre    => Invariant and
               Tau0_State = Setup and
               not Memory_Blocks_Full and
               Types.Aligned_Page (Address) and
               Size > 0 and
               Address + Types.From_Page_Count (Size) - 1
                 <= Types.Physical_Address_Type'Last,
     Post   => Invariant and
               Get_Typization_Model = (Get_Typization_Model'Old with delta
                 Address .. Address + Types.From_Page_Count (Size) - 1 => T.Undefined);

   procedure Add_Device_Block
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
   with
     Global => (Proof_In => (Tau0.State, Memory_State),
                In_Out   => Typization_State),
     -- REVIEW: demand disjointness with memory blocks?
     Pre    => Invariant and
               Tau0_State = Setup and
               not Device_Blocks_Full and
               Types.Aligned_Page (Address) and
               Size > 0 and
               Address + Types.From_Page_Count (Size) - 1
                 <= Types.Physical_Address_Type'Last,
     Post   => Invariant and
               Get_Typization_Model = (Get_Typization_Model'Old with delta
                 Address .. Address + Types.From_Page_Count (Size) - 1 =>
                    T.Device_Page);

   pragma Warnings (Off, "Subprogram * has no effect",
                    Reason => "Effect is file output");

   procedure Write_Image_Commands
     (Filename    : String;
      Entry_Point : Types.Tau0_Address_Type);

   pragma Warnings (On, "Subprogram * has no effect");

private

   --  NOTE: We must declare the private child packages Data.Memory.Typization
   --  and Data.Memory.Low_Level as nested packages, as otherwise we cannot
   --  prove identities like
   --    Data.Memory.Get_Typization = Data.Memory.Typization.Get.

   --  Store typization of pages.  "Full" typization is determined as (ghost)
   --  postcondition of a page walk, see Data.Memory.Full_Typization.
   package Typization
   with
     Abstract_State    => (State with Part_Of => Data.Memory.Typization_State),
     Initial_Condition => (for all A in Types.Tau0_Address_Type =>
                           (if A in Types.Tau0_Private_Address_Type
                            then Get_Model (A) = Types.Typization.Undefined
                            else Get_Model (A) = Types.Typization.Reserved))
   is

      Typizations_Per_Byte : constant := 4;

      --  Return Ghost model.  Used for Data.Memory.Invariant
      function Get_Model return T.Typization_Model_Type
      with
        Ghost,
        Post => (for all Addr in Types.Tau0_Address_Type =>
                 Get_Model'Result (Addr) = Get (Addr));

      --  Return typization for page at Address
      function Get (Address : Types.Tau0_Address_Type) return T.Typization_Type;

      --  Typization is only up to pages.
      procedure Get_Lemma
      with
        Ghost,
        Post =>
          (for all A in Types.Tau0_Address_Type =>
          (for all B in Page.First (A) .. Page.Last (A) =>
           Get (A) = Get (B)));

      --  Set typization of page at Address to Typization
      --  REVIEW: For now, we do not allow setting Typization to Reserved.
      --  Is this a problem?
      procedure Set
        (Address    : Types.Tau0_Address_Type;
         Typization : T.Typization_Type)
      with
        Global => (In_Out => Typization_State),
        Pre  => Typization /= T.Reserved and
                T.Transition_Valid (Get_Model (Address), Typization),
        Post => Get_Model = (Get_Model'Old with delta
                  Page.First (Address) .. Page.Last (Address) => Typization);

      --  True iff no more memory blocks can be added
      function Memory_Blocks_Full return Boolean;
      --  True iff no more device blocks can be added
      function Device_Blocks_Full return Boolean;
      --  True iff no more content regions can be declared
      function Content_Regions_Full return Boolean;

      procedure Add_Memory_Block
        (Address : Types.Physical_Address_Type;
         Size    : Types.Physical_Page_Count_Type)
      with
        -- REVIEW: demand disjointness with devices?
        Pre  => Tau0_State = Setup and
                not Memory_Blocks_Full and
                Types.Aligned_Page (Address) and
                Size > 0 and
                Address + Types.From_Page_Count (Size) - 1
                  <= Types.Physical_Address_Type'Last,
        Post => Get_Model = (Get_Model'Old with delta
                  Address .. Address + Types.From_Page_Count (Size) - 1 => T.Undefined);

      procedure Add_Device_Block
        (Address : Types.Physical_Address_Type;
         Size    : Types.Physical_Page_Count_Type)
      with
        -- REVIEW: demand disjointness with memory blocks?
        Pre  => Tau0_State = Setup and
                not Device_Blocks_Full and
                Types.Aligned_Page (Address) and
                Size > 0 and
                Address + Types.From_Page_Count (Size) - 1
                  <= Types.Physical_Address_Type'Last,
        Post => Get_Model = (Get_Model'Old with delta
                  Address .. Address + Types.From_Page_Count (Size) - 1 =>
                     T.Device_Page);

      procedure Declare_Content
        (Page : Types.Physical_Page_Count_Type)
      with
        -- REVIEW: demand subset of Memory_Blocks
        Pre  => not Typization.Content_Regions_Full,
        Post => Get_Model = Get_Model'Old;

      --  Returns True iff the given physical page contains content.
      function Has_Content
        (Page : Types.Physical_Page_Count_Type)
         return Boolean;

   end Typization;

   -----------------------------------------------------------------------------

   package Low_Level
   with
     Abstract_State => (State with Part_Of => Data.Memory.Memory_State),
     Initializes    => State               --  FIXME: it does not, nor should it!
   is

      use type Types.Word128;

      type Memory_Type is array (Memory_Range) of Types.Word64;

      function Get_Model return Memory_Model_Type
      with Ghost;

      function Get (Address : Types.Tau0_Address_Type) return Types.Word64
      with
        --  FIXME: Write reproducer
        Global => (Proof_In => Typization_State,
                   Input    => Memory_State),
        Pre    => Types.Aligned_64 (Address) and then Accessible (Address),
        Post   => Get'Result = Get_Model (To_Range (Address));

      function Get (Address : Types.Tau0_Address_Type) return Types.Word128
      with
        Global => (Proof_In => Typization_State,
                   Input    => Memory_State),
        Pre    => Types.Aligned_128 (Address) and then
                  Accessible (Address) and then
                  Accessible (Address + 8),
        Post   => Get'Result = (0 => Get_Model (To_Range (Address)),
                                1 => Get_Model (To_Range (Address + 8)));

      procedure Set (Address : Types.Tau0_Address_Type; Value : Types.Word64)
      with
        Global => (Proof_In => Typization_State,
                   In_Out   => Memory_State),
        Pre    => Types.Aligned_64 (Address) and then Accessible (Address),
        Post   => Get_Model = (Get_Model'Old with delta To_Range (Address) => Value) and
                  (for all A in Page.First (Address) .. Page.Last (Address) =>
                   Accessible (A));

      procedure Set (Address : Types.Tau0_Address_Type; Value : Types.Word128)
      with
        Global => (Proof_In => Typization_State,
                   In_Out   => Memory_State),
        Pre    => Types.Aligned_128 (Address) and then
                  Accessible (Address),
        Post   => Get_Model = (Get_Model'Old with delta
                    To_Range (Address)     => Value (0),
                    To_Range (Address + 8) => Value (1)) and
                  (for all A in Page.First (Address) .. Page.Last (Address) =>
                   Accessible (A));

      procedure Write_Image_Commands
        (Filename    : String;
         Entry_Point : Types.Tau0_Address_Type);

   private

      pragma Warnings
        (GNATprove, Off,
         "indirect writes to * through a potential alias are ignored",
         Reason => "Non-overlap with Typization is checked by " &
           "Config.Initial_Condition");
      pragma Warnings
        (GNATprove, Off,
         "writing *is assumed to have no effects on other non-volatile objects",
         Reason => "Non-overlap with Typization is checked by " &
           "Config.Initial_Condition");
      Memory : Memory_Type
      with
        Part_Of => State,
        Import,
        Address => System'To_Address (Config.Memory_Map_Base);
      pragma Warnings
        (GNATprove, On,
         "writing *is assumed to have no effects on other non-volatile objects");
      pragma Warnings
        (GNATprove, On,
         "indirect writes to * through a potential alias are ignored");

      function Get_Model return Memory_Model_Type
      is (Memory_Model_Type (Memory));

   end Low_Level;

   -----------------------------------------------------------------------------

   --  TODO (?): Alternatively, we may state as a postcondition of
   --  Translate_Address that its out-parameter Active respects page-boundaries.
   procedure Accessible_Lemma (A, B : Types.Tau0_Address_Type)
   with
     Ghost,
     Global => (Proof_In => Typization_State),
     Pre    => Page.Within (Address => B, Page => A),
     Post   => Accessible (A) = Accessible (B);

   procedure Accessible_Lemma (A : Types.Tau0_Address_Type)
   with
     Ghost,
     Global => (Proof_In => Typization_State),
     Post   => (for all B in Page.First (A) .. Page.Last (A) =>
                Accessible (A) = Accessible (B));

   --  Write Contents to Page and set its typization.
   procedure Write_Page
     (Page     : Types.Tau0_Address_Type;
      Contents : Types.Page_Array)
   with
     Global => (Proof_In => Typization_State,
                In_Out   => Memory_State),
     Pre    => Data.Memory.Invariant and
               Types.Aligned_Page (Page) and
               Accessible (Page) and
               Get_Full_Typization (Page) in T.Undefined | T.Zeroed | T.MR_Page,
     Post   => Data.Memory.Invariant and
               Accessible (Page) and
               --  Memory is written to page.
               (for all M in Memory_Range =>
               (if M in To_Range (Page) .. To_Range (Tau0.Page.Last (Page))
                then Get_Memory_Model (M)
                       = Contents (Types.Page_Array_Range'First
                                     + Natural (M - To_Range (Page)))
                else Get_Memory_Model (M) = Get_Memory_Model'Old (M)));

   -----------------------------------------------------------------------------

   function Device_Blocks_Full return Boolean
   is (Typization.Device_Blocks_Full);

   -----------------------------------------------------------------------------

   function Get_Typization
     (Address : Types.Tau0_Address_Type)
      return T.Typization_Type
   is (Typization.Get (Address => Address));

   -----------------------------------------------------------------------------

   function Get_Typization_Model return T.Typization_Model_Type
   is (Typization.Get_Model);

   -----------------------------------------------------------------------------

   function Get_Memory_Model return Memory_Model_Type
   is (Low_Level.Get_Model);

   -----------------------------------------------------------------------------

   function Memory_Blocks_Full return Boolean
   is (Typization.Memory_Blocks_Full);

   -----------------------------------------------------------------------------

   function Content_Regions_Full return Boolean
   is (Typization.Content_Regions_Full);

end Tau0.Data.Memory;
