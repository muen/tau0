--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.Paging;
with Tau0.Types.Hash;

package body Tau0.Data.Roots
with Refined_State => (State => (Roots, CPUs))
is

   -----------------------------------------------------------------------------

   --  TODO: Check system policy
   --  NOTE: A device may be assigned to several subjects, e.g. Linux SMP
   --  subjects
   function Device_Assignable
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
      return Boolean
   is
      pragma Unreferenced (Root);
      pragma Unreferenced (Device);
   begin
      return True;
   end Device_Assignable;

   -----------------------------------------------------------------------------

   function Hash_Valid (Root : Types.Root_Range) return Boolean
   is
      Context : Types.Hash.Context_Type := Types.Hash.Initial_Context;
   begin
      for Idx in 0 .. Length (Root) - 1 loop
         pragma Loop_Invariant
           (Invariant and Data.Memory.Invariant and PTP_Present (Root));

         Memory.Paging.Update_Hash (Root       => Root,
                                    Page_Index => Idx,
                                    Context    => Context);
      end loop;

      return Types.Hash.Digest (Context => Context) = Roots (Root).Hash.Digest;
   end Hash_Valid;

   -----------------------------------------------------------------------------

   procedure Lemma_Invariant (Old_Memory, New_Memory : Data.Memory.Memory_Model_Type)
   is
      use Data.Memory.VTd_IOMMU;
      use type Types.VTd_Tables.Root_Table_Model_Type;
   begin
      Root_Table_Entry_Lemma
        (Old_Model => Old_Memory,
         New_Model => New_Memory);

      pragma Assert
        (for all B in Types.Device.PCI_Bus_Range =>
         (Root_Table_Model_Using (Model => Old_Memory) (B).Present
            = Root_Table_Model_Using (Model => New_Memory) (B).Present));

      pragma Assert (Invariant_Devices_Device_Domain (Roots        => Roots,
                                                      Memory_Model => New_Memory));
   end Lemma_Invariant;

end Tau0.Data.Roots;
