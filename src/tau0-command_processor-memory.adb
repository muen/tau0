--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Memory
is

   -----------------------------------------------------------------------------

   procedure Clear_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Clear_Page_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Clear_Page
           (Page   => Command.Clear_Page_Params.Page,
            Status => Status);

         pragma Assert (Data.Roots.Invariant);
      end if;
   end Clear_Page;

   -----------------------------------------------------------------------------

   procedure Write_Image_Commands
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      Commands.Write_Image_Commands
        (Entry_Point => Command.Write_Image_Commands_Param.Entry_Point,
         Status      => Status);
   end Write_Image_Commands;

end Tau0.Command_Processor.Memory;
