--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Devices
with Refined_State => (State => Devices)
is

   function Caching
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type)
      return Types.Caching_Type
   is
   begin
      for I in Devices (Device).Memory loop
         declare
            Mem : constant Types.Device.Memory_Type
              := Types.Device.Memory_Table.Element
                   (Table  => Devices (Device).Memory,
                    Cursor => I);
         begin
            if Address in Mem.Physical_Address
                       .. Mem.Physical_Address + Mem.Size - 1
            then
               return Mem.Caching;
            end if;
         end;
      end loop;

      pragma Assert (False, "This point is not reached");
      pragma Warnings
        (GNATprove, Off, "This statement is never reached",
         Reason => "Unlike the compiler, SPARK deduces that this return is "
         & "never reached.");
      return Types.UC;
   end Caching;

end Tau0.Data.Devices;
