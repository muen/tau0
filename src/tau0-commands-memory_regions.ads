--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Memory.Interrupt_Remapping;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Memory_Region_Table;
with Tau0.Data.Processors;
with Tau0.Data.Roots;
with Tau0.Types.Command;
with Tau0.Types.Root;

package Tau0.Commands.Memory_Regions
is

   procedure Create_Memory_Region
     (Region  :     Types.Root_Range;
      Level   :     Types.Nonleaf_Level_Type;
      Caching :     Types.Caching_Type;
      Hash    :     Types.Root.Optional_Hash_Type;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => Data.Roots.State),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   procedure Create_Page_Table
     (Page            :     Types.Tau0_Private_Address_Type;
      Region          :     Types.Root_Range;
      Level           :     Types.Page_Level_Type;
      Virtual_Address :     Types.Virtual_Address_Type;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Roots.State)),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   procedure Lock_Memory_Region
     (Region :     Types.Root_Range;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Tau0.State,
                             Data.Memory.Memory_State),
                In_Out   => Data.Roots.State),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   procedure Activate_Memory_Region
     (Region :     Types.Root_Range;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Typization_State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory.Interrupt_Remapping.State),
                In_Out   => (Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   procedure Activate_Page
     (Region          :     Types.Root_Range;
      Virtual_Address :     Types.Virtual_Address_Type;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   procedure Activate_Page_Table
     (Region          :     Types.Root_Range;
      Virtual_Address :     Types.Virtual_Address_Type;
      Level           :     Types.Page_Level_Type;
      Status          : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   procedure Append_Page_Contents
     (Region   :     Types.Root_Range;
      Page     :     Types.Physical_Address_Type;
      Contents :     Types.Page_Array;
      Status   : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Typization_State,
                             Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

   --  REVIEW: Allow also in dynamic mode?
   procedure Append_Page_Vacuous
     (Region :     Types.Root_Range;
      Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Typization_State,
                             Data.Memory.Memory_State,
                             Data.Roots.State)),
     Pre  => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant,
     Post => Data.Roots.Invariant and
             Data.Memory.Invariant and
             Data.Memory_Region_Table.Invariant;

end Tau0.Commands.Memory_Regions;
