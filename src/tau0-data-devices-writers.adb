--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Devices.Writers
is

   -----------------------------------------------------------------------------

   procedure Activate (Device : Types.Device.Device_Range)
   is
   begin
      case Devices (Device).Is_PCI is
         when True =>
            Devices (Device) := Types.Device.Device_Type'
              (State    => Types.Device.Active,
               Is_PCI   => True,
               PCI      => Devices (Device).PCI,
               IRQs     => Devices (Device).IRQs,
               Memory   => Devices (Device).Memory,
               IO_Ports => Devices (Device).IO_Ports);
         when False =>
            Devices (Device) := Types.Device.Device_Type'
              (State    => Types.Device.Active,
               Is_PCI   => False,
               IRQs     => Devices (Device).IRQs,
               Memory   => Devices (Device).Memory,
               IO_Ports => Devices (Device).IO_Ports);
      end case;
   end Activate;

   -----------------------------------------------------------------------------

   procedure Add_IO_Port_Range
     (Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type)
   is
   begin
      pragma Assert
        (for all Dev of Devices =>
        Dev.State = Types.Device.Unused or else
        (for all I in Types.IO_Port_Type =>
         not (I in From .. To and
              IO_Port_Set.Element (Elem => I, Set => Dev.IO_Ports))));

      IO_Port_Set.Add_Interval
        (Set  => Devices (Device).IO_Ports,
         From => From,
         To   => To);
   end Add_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Add_IRQ
     (Device : Types.Device.Device_Range;
      IRQ    : Types.Device.HW_IRQ_Type)
   is
   begin
      Types.Device.IRQ_Table.Push
        (Table => Devices (Device).IRQs,
         Elem  => IRQ);
   end Add_IRQ;

   -----------------------------------------------------------------------------

   procedure Add_Memory
     (Device  : Types.Device.Device_Range;
      Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type;
      Caching : Types.Caching_Type)
   is
   begin
      Data.Memory.Add_Device_Block
        (Address => Address,
         Size    => Size);

      Types.Device.Memory_Table.Push
        (Table => Devices (Device).Memory,
         Elem  => Types.Device.Memory_Type'
           (Physical_Address => Address,
            Size             => Types.From_Page_Count (Size),
            Caching          => Caching));

      pragma Assert
        (for all A in Types.Tau0_Address_Type =>
        (if Page.Within (Address => A, Base => Address, Length => Size)
         then Data.Memory.Get_Typization_Model (A) = Types.Typization.Device_Page));
   end Add_Memory;

   -----------------------------------------------------------------------------

   procedure Create_Legacy_Device (Device : Types.Device.Device_Range)
   is
   begin
      Devices (Device) := Types.Device.Device_Type'
        (State    => Types.Device.Setup,
         Is_PCI   => False,
         IRQs     => Types.Device.IRQ_Table.Empty_Table,
         Memory   => Types.Device.Memory_Table.Empty_Table,
         IO_Ports => IO_Port_Set.Empty_Set);
   end Create_Legacy_Device;

   -----------------------------------------------------------------------------

   procedure Create_PCI_Device
     (Device      : Types.Device.Device_Range;
      PCI_Bus     : Types.Device.PCI_Bus_Range;
      PCI_Dev     : Types.Device.PCI_Device_Range;
      PCI_Func    : Types.Device.PCI_Function_Range;
      IOMMU_Group : Types.Device.IOMMU_Group_Type;
      Uses_MSI    : Boolean;
      Classcode   : Types.Device.Classcode_Type;
      Vendor_ID   : Types.Device.Vendor_ID_Type;
      Device_ID   : Types.Device.Device_ID_Type;
      Revision_ID : Types.Device.Revision_ID_Type)
   is
   begin
      Devices (Device) := Types.Device.Device_Type'
        (State    => Types.Device.Setup,
         Is_PCI   => True,
         PCI      => Types.Device.PCI_Type'(Bus         => PCI_Bus,
                                            Device      => PCI_Dev,
                                            Func        => PCI_Func,
                                            IOMMU_Group => IOMMU_Group,
                                            Classcode   => Classcode,
                                            Vendor_ID   => Vendor_ID,
                                            Device_ID   => Device_ID,
                                            Revision_ID => Revision_ID,
                                            Uses_MSI    => Uses_MSI),
         IRQs     => Types.Device.IRQ_Table.Empty_Table,
         Memory   => Types.Device.Memory_Table.Empty_Table,
         IO_Ports => IO_Port_Set.Empty_Set);
   end Create_PCI_Device;

end Tau0.Data.Devices.Writers;
