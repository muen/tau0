--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Page_Table.EPT;
with Tau0.Types.Page_Table.IA32e;
with Tau0.Types.Page_Table.MPT;

package body Tau0.Types.Page_Table
is

   -----------------------------------------------------------------------------

   function From_Entry
     (Paging : Root.Paging_Type;
      Entr   : Entry_Type)
      return Types.Word64
   is (case Paging is
         when Types.Root.IA32e =>
           IA32e.From_Entry (Entr => Entr),
         when Types.Root.EPT =>
           EPT.From_Entry (Entr => Entr),
         when Types.Root.MPT =>
           MPT.From_Entry (Entr => Entr),
         when Types.Root.VTd =>
           EPT.From_Entry (Entr => Entr));

   -----------------------------------------------------------------------------

   function To_Entry
     (Paging : Root.Paging_Type;
      Word   : Word64;
      Level  : Page_Level_Type)
      return Entry_Type
   is (case Paging is
         when Types.Root.IA32e =>
           IA32e.To_Entry (Word => Word, Level => Level),
         when Types.Root.EPT =>
           EPT.To_Entry (Word => Word, Level => Level),
         when Types.Root.MPT =>
           MPT.To_Entry (Word => Word, Level => Level),
         when Types.Root.VTd =>
           EPT.To_Entry (Word => Word, Level => Level));

end Tau0.Types.Page_Table;
