--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.Full_Typization;
with Tau0.Data.Roots;
with Tau0.Types.Root;

package Tau0.Data.Memory.Accessibility
is

   use type Types.Root.Kind_Type;

   --  NOTE: Using distinct paths to a page, we might "prove" that it is at the
   --  same time owned and not owned.  Hence we must show for the Isabelle model
   --  that such paths cannot exist: being active is independent from paths (by
   --  construction)
   procedure Paging_Lemma
     (Root      : Types.Root_Range;
      Address   : Types.Tau0_Address_Type;
      Path      : Full_Typization.Path_Type;
      Path_Last : Full_Typization.Path_Index)
   with
     Ghost,
     Import,
     Pre  => Data.Roots.Used (Root) and then
             Full_Typization.Reachable (Root, Address, Path, Path_Last),
     Post => (if Data.Roots.Kind (Root) = Types.Root.Memory_Region and
                 Path_Last < Data.Roots.Level (Root)
              then Accessible (Address)  --  Mem. region PTs are always accessible
              else Accessible (Address)
                     = not Full_Typization.Active (Root, Address, Path, Path_Last));

   procedure Subject_Bitmap_Lemma
     (Root    : Types.Root_Range;
      Address : Types.Tau0_Address_Type)
   with
     Ghost,
     Import,
     Pre  => Data.Roots.Used (Root) and then
             Roots.Kind (Root) = Types.Root.Subject and then
             not Roots.Active (Root) and then
             Address in Roots.IO_Bitmap_Low (Root)
                      | Roots.IO_Bitmap_High (Root)
                      | Roots.MSR_Bitmap (Root),
     Post => Accessible (Address);

end Tau0.Data.Memory.Accessibility;
