--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands.Memory_Regions;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Memory_Regions
is

   -----------------------------------------------------------------------------

   procedure Activate_Memory_Region
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Memory_Region_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Activate_Memory_Region
           (Region => Command.Activate_Memory_Region_Params.Region,
            Status => Status);
      end if;
   end Activate_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Activate_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Page_MR_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Activate_Page
           (Region          => Command.Activate_Page_MR_Params.Region,
            Virtual_Address => Command.Activate_Page_MR_Params.Virtual_Address,
            Status          => Status);
      end if;
   end Activate_Page;

   -----------------------------------------------------------------------------

   procedure Activate_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Page_Table_MR_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Activate_Page_Table
           (Region          => Command.Activate_Page_Table_MR_Params.Region,
            Virtual_Address => Command.Activate_Page_Table_MR_Params.Virtual_Address,
            Level           => Command.Activate_Page_Table_MR_Params.Level,
            Status          => Status);
      end if;
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   procedure Append_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Append_Page_MR_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Append_Page_Contents
           (Region   => Command.Append_Page_MR_Params.Region,
            Page     => Command.Append_Page_MR_Params.Page,
            Contents => Command.Append_Page_MR_Params.Contents,
            Status   => Status);
      end if;
   end Append_Page;

   -----------------------------------------------------------------------------

   procedure Append_Vacuous_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Append_Vacuous_Page_MR_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Append_Page_Vacuous
           (Region => Command.Append_Vacuous_Page_MR_Params.Region,
            Page   => Command.Append_Vacuous_Page_MR_Params.Page,
            Status => Status);
      end if;
   end Append_Vacuous_Page;

   -----------------------------------------------------------------------------

   procedure Create_Memory_Region
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Memory_Region_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Create_Memory_Region
           (Region  => Command.Create_Memory_Region_Params.Region,
            Level   => Command.Create_Memory_Region_Params.Level,
            Caching => Command.Create_Memory_Region_Params.Caching,
            Hash    => Command.Create_Memory_Region_Params.Hash,
            Status  => Status);
      end if;
   end Create_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Create_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Page_Table_MR_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Create_Page_Table
           (Page            => Command.Create_Page_Table_MR_Params.Page,
            Region          => Command.Create_Page_Table_MR_Params.Region,
            Level           => Command.Create_Page_Table_MR_Params.Level,
            Virtual_Address => Command.Create_Page_Table_MR_Params.Virtual_Address,
            Status          => Status);
      end if;
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   procedure Lock_Memory_Region
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Lock_Memory_Region_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Memory_Regions.Lock_Memory_Region
           (Region => Command.Lock_Memory_Region_Params.Region,
            Status => Status);
      end if;
   end Lock_Memory_Region;

end Tau0.Command_Processor.Memory_Regions;
