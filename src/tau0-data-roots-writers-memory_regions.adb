--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Roots.Writers.Memory_Regions
is

   -----------------------------------------------------------------------------

   procedure Activate (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State            => Types.Root.Active,
         Is_Memory_Region => True,
         Kind             => Types.Root.Memory_Region,
         PTP              => Roots (Root).PTP,
         PTP_Present      => Roots (Root).PTP_Present,
         PTP_Active       => Roots (Root).PTP_Active,
         Region_Level     => Roots (Root).Region_Level,
         Length           => Roots (Root).Length,
         Caching          => Roots (Root).Caching,
         Hash             => Roots (Root).Hash,
         Ref_Counter      => Roots (Root).Ref_Counter);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Activate;

   -----------------------------------------------------------------------------

   procedure Create_Memory_Region
     (Root    : Types.Root_Range;
      Level   : Types.Nonleaf_Level_Type;
      Caching : Types.Caching_Type;
      Hash    : Types.Root.Optional_Hash_Type := Types.Root.Dont_Check)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State            => Types.Root.Setup,
         Is_Memory_Region => True,
         Kind             => Types.Root.Memory_Region,
         PTP_Present      => False,
         PTP_Active       => False,
         PTP              => 0,
         Length           => 0,
         Region_Level     => Level,
         Caching          => Caching,
         Hash             => Hash,
         Ref_Counter      => 0);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Create_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Increment_Length  (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root).Length := Roots (Root).Length + 1;

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Increment_Length;

   -----------------------------------------------------------------------------

   procedure Increment_Reference_Counter (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root).Ref_Counter := Roots (Root).Ref_Counter + 1;

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
   end Increment_Reference_Counter;

end Tau0.Data.Roots.Writers.Memory_Regions;
