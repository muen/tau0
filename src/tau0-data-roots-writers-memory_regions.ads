--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Data.Roots.Writers.Memory_Regions
is

   procedure Activate (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Locked and then
               Kind (Root) = Types.Root.Memory_Region and then
               not Active (Root) and then
               (if Paging (Root) in Types.Root.Paging_Type
                then PTP_Active (Root)),
     Post   => Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Update may not modify discriminants
               State (Root) = Types.Root.Active and
               Kind (Root) = Kind (Root)'Old and
               PTP_Present (Root) = PTP_Present (Root)'Old and
               PTP_Active (Root) = PTP_Active (Root)'Old and
               PTP (Root) = PTP (Root)'Old;
               --  TODO: maybe we must state more about paging

   procedure Create_Memory_Region
     (Root    : Types.Root_Range;
      Level   : Types.Nonleaf_Level_Type;
      Caching : Types.Caching_Type;
      Hash    : Types.Root.Optional_Hash_Type := Types.Root.Dont_Check)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and
               Data.Memory_Region_Table.Invariant and
               Tau0_State = Running and
               State (Root) = Types.Root.Unused,
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               State (Root) = Types.Root.Setup and
               Kind (Root) = Types.Root.Memory_Region and
               not PTP_Present (Root) and
               not PTP_Active (Root) and
               Reference_Count (Root) = 0;

   procedure Increment_Length (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State, Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Memory_Region and then
               Length (Root) < Types.Page_Count_Type'Last,
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Get_Roots = (Get_Roots'Old with delta
                 Root => (Get_Roots'Old (Root) with delta
                    Length => Length (Root)'Old + 1));

   procedure Increment_Reference_Counter (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Active and then
               Kind (Root) = Types.Root.Memory_Region and then
               Reference_Count (Root) < Types.Root.Counter_Type'Last,
     Post   => Invariant and
               Data.Memory_Region_Table.Invariant and
               Get_Roots = (Get_Roots'Old with delta
                 Root => (Get_Roots'Old (Root) with delta
                    Ref_Counter => Reference_Count (Root)'Old + 1));

end Tau0.Data.Roots.Writers.Memory_Regions;
