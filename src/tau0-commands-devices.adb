--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices.Writers;

package body Tau0.Commands.Devices
is

   --  TODO: allow only transition from Tau0.Setup to Tau0.Running if all used
   --  devices are active!

   use type Types.Device.State_Type;
   use type Types.IO_Port_Type;

   -----------------------------------------------------------------------------

   procedure Activate
     (Device :     Types.Device.Device_Range;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Setup then
         Status := Types.Command.Device_Not_Setup;
      else
         Data.Devices.Writers.Activate (Device => Device);
         Status := Types.Command.Success;
      end if;
   end Activate;

   -----------------------------------------------------------------------------

   procedure Add_IO_Port_Range
     (Device :     Types.Device.Device_Range;
      From   :     Types.IO_Port_Type;
      To     :     Types.IO_Port_Type;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Setup then
         Status := Types.Command.Device_Not_Setup;
      elsif Data.Devices.IO_Port_Set_Full (Device => Device) then
         Status := Types.Command.Device_IO_Port_Set_Full;
      elsif From > To then
         Status := Types.Command.Invalid_Parameters;
      elsif Data.Devices.Some_IO_Port_Used (From => From, To => To) then
         Status := Types.Command.Device_IO_Port_Used;
      else
         Data.Devices.Writers.Add_IO_Port_Range
           (Device => Device,
            From   => From,
            To     => To);

         Status := Types.Command.Success;
      end if;
   end Add_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Add_IRQ
     (Device :     Types.Device.Device_Range;
      IRQ    :     Types.Device.HW_IRQ_Type;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Setup then
         Status := Types.Command.Device_Not_Setup;
      elsif Data.Devices.IRQ_Table_Full (Device => Device) then
         Status := Types.Command.Device_IO_Port_Set_Full;
      elsif Data.Devices.IRQ_Used (IRQ => IRQ) then
         Status := Types.Command.Device_IRQ_Used;
      else
         Data.Devices.Writers.Add_IRQ (Device => Device, IRQ => IRQ);
         Status := Types.Command.Success;
      end if;
   end Add_IRQ;

   -----------------------------------------------------------------------------

   procedure Add_Memory
     (Device  :     Types.Device.Device_Range;
      Address :     Types.Physical_Address_Type;
      Size    :     Types.Physical_Page_Count_Type;
      Caching :     Types.Caching_Type;
      Status  : out Types.Command.Status_Type)
   is
      use Types;
   begin
      if Tau0_State /= Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Setup then
         Status := Types.Command.Device_Not_Setup;
      elsif Data.Devices.Memory_Table_Full (Device => Device) then
         Status := Types.Command.Device_Memory_Table_Full;
      elsif
        not Types.Aligned_Page (Address => Address) or
        not (Address + Types.From_Page_Count (Size) - 1
               in Types.Physical_Address_Type)
      then
         Status := Types.Command.Address_Invalid;
      elsif Data.Memory.Device_Blocks_Full then
         Status := Types.Command.Reserved_Memory_Set_Full;
      elsif
        Size = 0 or
        not (Address + Types.From_Page_Count (Size) - 1 in Physical_Address_Type)
      then
         Status := Types.Command.Invalid_Parameters;
      elsif Data.Devices.Memory_Used (Address => Address, Size => Size) then
         Status := Types.Command.Device_Memory_Used;
      else
         Data.Devices.Writers.Add_Memory
           (Device  => Device,
            Address => Address,
            Size    => Size,
            Caching => Caching);

         Status := Types.Command.Success;
      end if;
   end Add_Memory;

   -----------------------------------------------------------------------------

   procedure Create_Legacy_Device
     (Device :     Types.Device.Device_Range;
      Status : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Unused then
         Status := Types.Command.Device_Used;
      else
         Data.Devices.Writers.Create_Legacy_Device (Device => Device);
         Status := Types.Command.Success;
      end if;
   end Create_Legacy_Device;

   -----------------------------------------------------------------------------

   procedure Create_PCI_Device
     (Device      :     Types.Device.Device_Range;
      PCI_Bus     :     Types.Device.PCI_Bus_Range;
      PCI_Dev     :     Types.Device.PCI_Device_Range;
      PCI_Func    :     Types.Device.PCI_Function_Range;
      IOMMU_Group :     Types.Device.IOMMU_Group_Type;
      Uses_MSI    :     Boolean;
      Status      : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Setup then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Unused then
         Status := Types.Command.Device_Used;
      elsif Data.Devices.Writers.PCI_BDF_Used
        (PCI_Bus  => PCI_Bus,
         PCI_Dev  => PCI_Dev,
         PCI_Func => PCI_Func)
      then
         Status := Types.Command.Device_BDF_Used;
      else
         Data.Devices.Writers.Create_PCI_Device
           (Device      => Device,
            PCI_Bus     => PCI_Bus,
            PCI_Dev     => PCI_Dev,
            PCI_Func    => PCI_Func,
            IOMMU_Group => IOMMU_Group,
            Uses_MSI    => Uses_MSI,
            Classcode   => 0,           --  FIXME!  Necessary?
            Vendor_ID   => 0,
            Device_ID   => 0,
            Revision_ID => 0);

         Status := Types.Command.Success;
      end if;
   end Create_PCI_Device;

end Tau0.Commands.Devices;
