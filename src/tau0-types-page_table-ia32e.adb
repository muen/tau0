--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Types.Page_Table.IA32e
is

   Present_Mask : constant Word64 := 2 ** 0;
   RW_Mask      : constant Word64 := 2 ** 1;
   US_Mask      : constant Word64 := 2 ** 2;
   PWT_Mask     : constant Word64 := 2 ** 3;
   PCD_Mask     : constant Word64 := 2 ** 4;
   PAT_PTE_Mask : constant Word64 := 2 ** 7;  --  Location of PAT flag depends
   PAT_PDE_Mask : constant Word64 := 2 ** 12; --  on level.
   Active_Mask  : constant Word64 := 2 ** 11; --  Within ignored block.
   XD_Mask      : constant Word64 := 2 ** 63;

   Page_Mask    : constant Word64 := Config.Page_Size - 1;
   Address_Mask : constant Word64
     := (2 ** Config.Max_Phys_Address - 1) and not Page_Mask;

   function From_Caching
     (Caching   : Caching_Type;
      Level     : Page_Level_Type)
      return Word64;

   function To_Caching
     (Word  : Word64;
      Level : Page_Level_Type)
      return Caching_Type;

   function PAT_Mask (Level : Page_Level_Type) return Word64
   is (if Level = 1 then PAT_PTE_Mask else PAT_PDE_Mask);

   -----------------------------------------------------------------------------

   function From_Caching
     (Caching   : Caching_Type;
      Level     : Page_Level_Type)
      return Word64
   is
      --  See Intel SDM, Vol. 3A, Sections 11.12.3 - 11.12.4
      PAT : constant Boolean := Caching = WP;
      PCD : constant Boolean := Caching = WC or Caching = UC;
      PWT : constant Boolean := Caching = WT or Caching = UC;

      Result : constant Word64
        := Inject (PCD) * PCD_Mask or
           Inject (PWT) * PWT_Mask or
           Inject (PAT) * PAT_Mask (Level);
   begin
      return Result;
   end From_Caching;

   -----------------------------------------------------------------------------

   function From_Entry (Entr : Entry_Type) return Word64
   is (Inject (Entr.Present) * Present_Mask or
       Inject (Entr.Readable) * US_Mask or
       Inject (Entr.Writable) * RW_Mask or
       Inject (not Entr.Executable) * XD_Mask or
       Inject (Entr.Active) * Active_Mask or
       Word64 (Entr.Address) or
       (if Entr.Maps_Page
        then From_Caching (Caching => Entr.Caching, Level => Entr.Level)
        else 0));

   -----------------------------------------------------------------------------

   function To_Caching
     (Word  : Word64;
      Level : Page_Level_Type)
      return Caching_Type
   is
      PCD : constant Boolean := (Word and PCD_Mask) /= 0;
      PWT : constant Boolean := (Word and PWT_Mask) /= 0;
      PAT : constant Boolean := (Word and PAT_Mask (Level)) /= 0;

      --  REVIEW: how to handle invalid combinations?
      Table : constant array (Word64 range 0 .. 7) of Caching_Type
        := (WB, WT, WC, UC, WP, others => WB);
   begin
      return Table (Inject (PAT) * 4 + Inject (PCD) * 2 + Inject (PWT));
   end To_Caching;

   -----------------------------------------------------------------------------

   function To_Entry (Word : Word64; Level : Page_Level_Type) return Entry_Type
   is
      PTE_Address : constant Physical_Address_Type
        := Physical_Address_Type (Word and Address_Mask);

      Present    : constant Boolean      := (Word and Present_Mask) /= 0;
      Readable   : constant Boolean      := (Word and US_Mask) /= 0;
      Writable   : constant Boolean      := (Word and RW_Mask) /= 0;
      Executable : constant Boolean      := (Word and XD_Mask) /= 1;
      Active     : constant Boolean      := (Word and Active_Mask) /= 0;
      Caching    : constant Caching_Type := To_Caching (Word  => Word,
                                                        Level => Level);
   begin
      pragma Assert ((Word and Address_Mask) mod 4096 = 0);
      pragma Assert (Aligned_Page (PTE_Address));

      if Level = 1 then
         return Entry_Type'
           (Maps_Page  => True,
            Level      => Level,
            Present    => Present,
            Readable   => Readable,
            Writable   => Writable,
            Executable => Executable,
            Active     => Active,
            Caching    => Caching,
            Address    => PTE_Address);
      else
         return Entry_Type'
           (Maps_Page  => False,
            Level      => Level,
            Present    => Present,
            Readable   => Readable,
            Writable   => Writable,
            Executable => Executable,
            Active     => Active,
            Address    => PTE_Address);
      end if;
   end To_Entry;

end Tau0.Types.Page_Table.IA32e;
