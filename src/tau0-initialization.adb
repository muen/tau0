--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Initialization
with Refined_State => (State => Initialized_Flag)
is

   Initialized_Flag : Boolean := False;

   -----------------------------------------------------------------------------

   procedure Initialize (Success : out Boolean)
   is
   begin
      --  FIXME: prove preconditions
      Data.Memory.Initialize (Success => Success);

      if not Success then
         return;
      end if;

      Initialized_Flag := True;
   end Initialize;

   -----------------------------------------------------------------------------

   function Initialized return Boolean
   is (Initialized_Flag);

end Tau0.Initialization;
