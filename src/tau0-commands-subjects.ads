--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.IO_APICs;
with Tau0.Data.Memory.Interrupt_Remapping;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Memory_Region_Table;
with Tau0.Data.Processors;
with Tau0.Data.Roots;
with Tau0.Types.Command;
with Tau0.Types.Device;
with Tau0.Types.IO_Bitmap;
with Tau0.Types.MSR_Bitmap;
with Tau0.Types.Root;

package Tau0.Commands.Subjects
is

   procedure Create_Subject
     (Subject    :     Types.Root_Range;
      Paging     :     Types.Root.Subject_Paging_Type;
      IO_Bitmap  :     Types.Physical_Address_Type;
      MSR_Bitmap :     Types.Physical_Address_Type;
      CPU        :     Types.CPU_Range;
      Status     : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State),
                Input    => (Tau0.State,
                             Data.Processors.State),
                In_Out   => (Data.Roots.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State)),
     Pre    => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Activate_Subject
     (Subject :     Types.Root_Range;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Typization_State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Tau0.State,
                             Data.Devices.State,
                             Data.IO_APICs.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory.Interrupt_Remapping.State),
                In_Out   => (Data.Roots.State,
                             Data.Memory.Memory_State)),
     Pre    => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Attach_Memory_Region
     (Subject          :     Types.Root_Range;
      Region           :     Types.Root_Range;
      Use_Base_Address :     Boolean;
      Base_Address     :     Types.Virtual_Address_Type := 0;
      Offset           :     Types.Page_Count_Type;
      Length           :     Types.Page_Count_Type;
      Index            :     Types.Memory_Region_Table_Range;
      Writable         :     Boolean;
      Executable       :     Boolean;
      Status           : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => (Data.Memory_Region_Table.State, Data.Roots.State)),
     Pre    => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Lock_Subject
     (Subject :     Types.Root_Range;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => Tau0.State,
                In_Out   => Data.Roots.State),
     Pre    => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Assign_Device
     (Subject :     Types.Root_Range;
      Device  :     Types.Device.Device_Range;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Tau0.State, Data.Devices.State),
                In_Out   => Data.Roots.State),
     Pre    => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Assign_IRQ
     (Subject   :     Types.Root_Range;
      Device    :     Types.Device.Device_Range;
      Interrupt :     Types.Device.HW_IRQ_Type;
      Status    : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                Input    => (Tau0.State, Data.Devices.State),
                In_Out   => Data.Roots.State),
     Pre    => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant,
     Post   => Data.Roots.Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant;

   procedure Set_IO_Port_Range
     (Subject :     Types.Root_Range;
      Device  :     Types.Device.Device_Range;
      From    :     Types.IO_Port_Type;
      To      :     Types.IO_Port_Type;
      Mode    :     Types.IO_Bitmap.Mode_Type;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                Input    => (Tau0.State,
                             Data.Roots.State,
                             Data.Devices.State),
                In_Out   => Data.Memory.Memory_State),
     Pre    => Data.Roots.Invariant and Data.Memory.Invariant,
     Post   => Data.Roots.Invariant and Data.Memory.Invariant;

   procedure Set_MSR_Range
     (Subject :     Types.Root_Range;
      From    :     Types.MSR_Bitmap.MSR_Range;
      To      :     Types.MSR_Bitmap.MSR_Range;
      Mode    :     Types.MSR_Bitmap.Mode_Type;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                Input    => (Tau0.State,
                             Data.Roots.State),
                In_Out   => Data.Memory.Memory_State),
     Pre    => Data.Roots.Invariant and Data.Memory.Invariant,
     Post   => Data.Roots.Invariant and Data.Memory.Invariant;

end Tau0.Commands.Subjects;
