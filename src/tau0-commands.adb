--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Directories;
with Tau0.Command_Line;
with Tau0.Types.Root;
with Tau0.Types.Typization;

package body Tau0.Commands
is

   package TC renames Types.Command;
   package TT renames Types.Typization;

   -----------------------------------------------------------------------------

   procedure Clear_Page
     (Page   :     Types.Tau0_Address_Type;
      Status : out Types.Command.Status_Type)
   is
   begin
      if not Types.Aligned_Page (Address => Page) then
         Status := TC.Address_Invalid;
      elsif
        not (Data.Memory.Get_Typization (Page) in TT.Undefined | TT.Zeroed)
      then
         Status := TC.Typization_Violation;
      elsif Data.Memory.Content_Regions_Full then
         Status := TC.Content_Regions_Full;
      else
         Data.Memory.Clear_Page (Page => Page);

         Status := TC.Success;
      end if;
   end Clear_Page;

   -----------------------------------------------------------------------------

   procedure Write_Image_Commands
     (Entry_Point :     Types.Tau0_Address_Type;
      Status      : out Types.Command.Status_Type)
   is
      use type Types.Root.State_Type;

      pragma Warnings (GNATProve, Off,
                       "no Global contract available for ""Compose""",
                       Reason => "Compose does not access any global state");

      Image_Name : constant String := Command_Line.Get_Image_Filename & ".cmds";
      Image_Path : constant String := Ada.Directories.Compose
        (Containing_Directory => Command_Line.Get_Output_Directory,
         Name                 => Image_Name);

      pragma Warnings (GNATProve, On,
                       "no Global contract available for ""Compose""");
   begin
      --  TODO: We would like to state the following condition as a precondition
      --  of Data.Memory.Write_Image, but cannot due to "with"-circularity.
      --  Fix that.
      if not (for all Root in Types.Root_Range =>
                 not Data.Roots.Used (Root) or else
              Data.Roots.State (Root) = Types.Root.Active)
      then
         Status := Types.Command.Some_Root_Inactive;
      else
         Data.Memory.Write_Image_Commands (Filename    => Image_Path,
                                           Entry_Point => Entry_Point);

         Status := TC.Success;
      end if;
   end Write_Image_Commands;

end Tau0.Commands;
