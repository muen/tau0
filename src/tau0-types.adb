--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Types
is

   -----------------------------------------------------------------------------

   function "and" (L, R : Virtual_Address_Type) return Virtual_Address_Type
   is
      WAnd : constant Word64 := Word64 (L) and Word64 (R);
   begin
      pragma Assert (WAnd <= Word64 (Virtual_Address_Type'Last));

      return Virtual_Address_Type (WAnd);
   end "and";

   -----------------------------------------------------------------------------

   function "or" (L, R : Virtual_Address_Type) return Virtual_Address_Type
   is
      WOr : constant Word64 := Word64 (L) or Word64 (R);
   begin
      pragma Assert (WOr <= Word64 (Virtual_Address_Type'Last));

      return Virtual_Address_Type (WOr);
   end "or";

end Tau0.Types;
