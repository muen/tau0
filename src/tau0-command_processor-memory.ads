--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Types.Command;

package Tau0.Command_Processor.Memory
is

   use type Types.Command.Command_ID_Type;

   -----------------------------------------------------------------------------

   procedure Clear_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
     Pre  => Invariants and Command.ID = Types.Command.Clear_Page,
     Post => Invariants;

   procedure Write_Image_Commands
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   with
      Pre  => Invariants and Command.ID = Types.Command.Write_Image_Commands,
      Post => Invariants;

end Tau0.Command_Processor.Memory;
