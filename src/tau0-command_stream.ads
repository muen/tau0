--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

--  Interface for the reception of commands.  In static Tau0 mode, we generate
--  the command stream from an XML input file, while in dynamic mode the
--  commands are sent over an SHM channel.
package Tau0.Command_Stream
with
  Abstract_State => State,
  Initializes    => State
is

   --  Process entire command stream.
   procedure Process
   with
      Global => (In_Out => State);

end Tau0.Command_Stream;
