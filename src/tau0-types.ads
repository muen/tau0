--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;
with Tau0.Mode;

package Tau0.Types
with Abstract_State => null
is

   type Bit    is mod 2;
   type Byte   is mod 2 ** 8;
   type Word16 is mod 2 ** 16;
   type Word32 is mod 2 ** 32;
   type Word64 is mod 2 ** 64;
   type Word128 is array (Natural range 0 .. 1) of Word64
   with Component_Size => 64;

   subtype Null_Bit is Bit range 0 .. 0;

   -----------------------------------------------------------------------------
   --  Addresses
   -----------------------------------------------------------------------------

   Bits_PTE_Address : constant := Config.Bits_Physical_Address - 12;

   --  Represent virtual addresses (i.e. before page translation)
   type Virtual_Address_Type is range 0 .. 2 ** Config.Bits_Virtual_Address - 1;

   function "not" (A : Virtual_Address_Type) return Virtual_Address_Type
   is (Virtual_Address_Type (Word64 (Virtual_Address_Type'Last) and (not (Word64 (A)))));

   function "and" (L, R : Virtual_Address_Type) return Virtual_Address_Type
   with Post => (Word64 ("and"'Result) = (Word64 (L) and Word64 (R)));

   function "or" (L, R : Virtual_Address_Type) return Virtual_Address_Type
   with Post => (Word64 ("or"'Result) = (Word64 (L) or Word64 (R)));

   --  Represent physical and Tau0-private addresses.  The latter are used in
   --  static Tau0 to store values which are not written to the system image.
   --  Virtual addresses can be translated to Tau0 addresses using package
   --  Data.Memory.Paging.
   type Tau0_Address_Type is new Virtual_Address_Type
     range 0 .. 2 ** Config.Bits_Tau0_Address - 1;
   type PTE_Address_Type is new Tau0_Address_Type
     range 0 .. 2 ** Bits_PTE_Address - 1;

   type Page_Count_Type is
     range 0 .. 2 ** (Config.Bits_Virtual_Address - 12) - 1;

   subtype Tau0_Page_Count_Type is Page_Count_Type
     range 0 .. 2 ** (Config.Bits_Tau0_Address - 12) - 1;

   subtype Physical_Page_Count_Type is Page_Count_Type
     range 0 .. 2 ** (Config.Bits_Physical_Address - 12) - 1;

   --  Represent physical addresses
   subtype Physical_Address_Type is Tau0_Address_Type
     range 0 .. 2 ** Config.Bits_Physical_Address - 1;

   First_Tau0_Private_Address : constant Tau0_Address_Type
     := (case Mode.Tau0_Mode is
         when Mode.Static  => Physical_Address_Type'Last + 1,
         when Mode.Dynamic => Physical_Address_Type'First);

   Last_Tau0_Private_Address : constant Tau0_Address_Type
     := (case Mode.Tau0_Mode is
         when Mode.Static  =>
            First_Tau0_Private_Address + 2 ** Config.Bits_Tau0_Private_Address - 1,
         when Mode.Dynamic =>
            Physical_Address_Type'Last);

   --  Represent Tau0-private addresses (see above)
   subtype Tau0_Private_Address_Type is Tau0_Address_Type
     range First_Tau0_Private_Address .. Last_Tau0_Private_Address;

   function Aligned_64 (Address : Tau0_Address_Type) return Boolean
   is (Address mod 8 = 0);

   function Aligned_64 (Address : Virtual_Address_Type) return Boolean
   is (Address mod 8 = 0);

   function Aligned_128 (Address : Tau0_Address_Type) return Boolean
   is (Address mod 16 = 0);

   function Aligned_Page (Address : Tau0_Address_Type) return Boolean
   is (Address mod Config.Page_Size = 0);

   function Aligned_Page (Address : Virtual_Address_Type) return Boolean
   is (Address mod Config.Page_Size = 0);

   function To_Physical_Address
     (Address : PTE_Address_Type)
      return Physical_Address_Type
   is (Tau0_Address_Type (Address) * 2 ** 12);

   function From_Physical_Address
     (Address : Physical_Address_Type)
      return PTE_Address_Type
   is (PTE_Address_Type (Address / 2 ** 12))
   with Pre => Aligned_Page (Address);

   function To_Page_Count (Address : Virtual_Address_Type) return Page_Count_Type
   is (Page_Count_Type (Address / 2 ** 12));

   function From_Page_Count (Count : Page_Count_Type) return Virtual_Address_Type
   is (Virtual_Address_Type (Count * 2 ** 12));

   function To_Page_Count (Address : Tau0_Address_Type) return Tau0_Page_Count_Type
   is (Page_Count_Type (Address / 2 ** 12));

   function From_Page_Count (Count : Tau0_Page_Count_Type) return Tau0_Address_Type
   is (Tau0_Address_Type (Count * 2 ** 12));

   function Successor_Page (Address : Tau0_Address_Type) return Tau0_Address_Type
   is (Address + Config.Page_Size)
   with
     Pre => Types.Aligned_Page (Address) and
            Address + Config.Page_Size <= Tau0_Address_Type'Last;

   -----------------------------------------------------------------------------
   --  Page levels
   -----------------------------------------------------------------------------

   --  General concept:
   --  k        : Top level entry
   --  1 .. k-1 : Page table entry
   --  0        : Memory
   --  For EPT, PT and VTD_PT: k = 5; MPT: k = 1 .. 5

   subtype Level_Type         is Natural            range 0 .. 5;
   subtype Nonleaf_Level_Type is Level_Type         range 1 .. 5;
   subtype Page_Level_Type    is Nonleaf_Level_Type range 1 .. 4;

   -----------------------------------------------------------------------------
   --  Caching
   -----------------------------------------------------------------------------

   type Opt_Caching_Type is range 0 .. 5;
   subtype Caching_Type is Opt_Caching_Type
     range Opt_Caching_Type'First .. Opt_Caching_Type'Last - 1;

   UC : constant Caching_Type := 0;
   WC : constant Caching_Type := 1;
   WT : constant Caching_Type := 2;
   WB : constant Caching_Type := 3;
   WP : constant Caching_Type := 4;

   Ignore : constant Opt_Caching_Type := Opt_Caching_Type'Last;

   -----------------------------------------------------------------------------
   --  Page arrays
   -----------------------------------------------------------------------------

   --  A page array stores a page as an array of Word64s.
   subtype Page_Array_Range is Natural
     range 1 .. Config.Page_Size / (Word64'Size / 8);

   type Page_Array is array (Page_Array_Range) of Word64
   with Component_Size => Word64'Size;

   -----------------------------------------------------------------------------

   type Root_Range is range 0 .. Config.Max_Roots - 1;
   type Memory_Region_Table_Range is range 0 .. Config.Size_MR_Table - 1;

   type CPU_Range is range 0 .. Config.Max_CPUs - 1;

   type APIC_ID_Type is range 0 .. 2 ** 32 - 1;

   type Source_ID_Type is range 0 .. 2 ** 16 - 1;

   -----------------------------------------------------------------------------
   --  IO ports
   -----------------------------------------------------------------------------

   type IO_Port_Type is range 0 .. 2 ** 16 - 1;

private

   --  Injection of Booleans into Word64
   function Inject (B : Boolean) return Word64;

   -----------------------------------------------------------------------------

   function Inject (B : Boolean) return Word64
   is (if B then 1 else 0);

end Tau0.Types;
