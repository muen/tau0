--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Debug.Command_ID;
with Tau0.Command_Processor.Device_Domains;
with Tau0.Command_Processor.Devices;
with Tau0.Command_Processor.Memory;
with Tau0.Command_Processor.Memory_Regions;
with Tau0.Command_Processor.Kernels;
with Tau0.Command_Processor.Setup;
with Tau0.Command_Processor.Subjects;
with Tau0.Command_Stream;

package body Tau0.Command_Processor
is

   -----------------------------------------------------------------------------

   procedure Process
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      pragma Debug (Debug.Put ("Processing command """));
      pragma Debug (Debug.Command_ID.Put (Command.ID));
      pragma Debug (Debug.Put (""" with sequence number "));
      pragma Debug (Debug.Put (Word      => Command.Sequence_Number,
                               Base      => 10,
                               Justified => False));
      pragma Debug (Debug.New_Line);

      case Command.ID is
         when Types.Command.Clear_Page =>
            Memory.Clear_Page (Command => Command, Status => Status);
         when Types.Command.Write_Image_Commands =>
            Memory.Write_Image_Commands (Command => Command, Status => Status);
         when Types.Command.Activate_Tau0 =>
            Setup.Activate_Tau0 (Command => Command, Status => Status);
         when Types.Command.Create_VTd_Context_Table =>
            Setup.Create_VTd_Context_Table (Command => Command, Status => Status);
         when Types.Command.Create_VTd_IRQ_Remap_Table =>
            Setup.Create_VTd_IRQ_Remap_Table (Command => Command, Status => Status);
         when Types.Command.Create_VTd_Root_Table =>
            Setup.Create_VTd_Root_Table (Command => Command, Status => Status);
         when Types.Command.Add_Memory_Block =>
            Setup.Add_Memory_Block (Command => Command, Status => Status);
         when Types.Command.Add_Processor =>
            Setup.Add_Processor (Command => Command, Status => Status);
         when Types.Command.Add_Ioapic =>
            Setup.Add_Ioapic (Command => Command, Status => Status);
         when Types.Command.Create_Memory_Region =>
            Memory_Regions.Create_Memory_Region (Command => Command, Status => Status);
         when Types.Command.Lock_Memory_Region =>
            Memory_Regions.Lock_Memory_Region (Command => Command, Status => Status);
         when Types.Command.Activate_Memory_Region =>
            Memory_Regions.Activate_Memory_Region (Command => Command, Status => Status);
         when Types.Command.Create_Page_Table_MR =>
            Memory_Regions.Create_Page_Table (Command => Command, Status => Status);
         when Types.Command.Append_Page_MR =>
            Memory_Regions.Append_Page (Command => Command, Status => Status);
         when Types.Command.Append_Vacuous_Page_MR =>
            Memory_Regions.Append_Vacuous_Page (Command => Command, Status => Status);
         when Types.Command.Activate_Page_MR =>
            Memory_Regions.Activate_Page (Command => Command, Status => Status);
         when Types.Command.Activate_Page_Table_MR =>
            Memory_Regions.Activate_Page_Table (Command => Command, Status => Status);
         when Types.Command.Create_Subject =>
            Subjects.Create_Subject (Command => Command, Status => Status);
         when Types.Command.Lock_Subject =>
            Subjects.Lock_Subject (Command => Command, Status => Status);
         when Types.Command.Activate_Subject =>
            Subjects.Activate_Subject (Command => Command, Status => Status);
         when Types.Command.Attach_Memory_Region_Subject =>
            Subjects.Attach_Memory_Region_Subject (Command => Command, Status => Status);
         when Types.Command.Create_Page_Table_Subject =>
            Subjects.Create_Page_Table (Command => Command, Status => Status);
         when Types.Command.Activate_Page_Table_Subject =>
            Subjects.Activate_Page_Table (Command => Command, Status => Status);
         when Types.Command.Map_Device_Page_Subject =>
            Subjects.Map_Device_Page (Command => Command, Status => Status);
         when Types.Command.Map_Page_Subject =>
            Subjects.Map_Page (Command => Command, Status => Status);
         when Types.Command.Assign_Device_Subject =>
            Subjects.Assign_Device_Subject (Command => Command, Status => Status);
         when Types.Command.Assign_IRQ_Subject =>
            Subjects.Assign_IRQ_Subject (Command => Command, Status => Status);
         when Types.Command.Set_IO_Port_Range =>
            Subjects.Set_IO_Port_Range (Command => Command, Status => Status);
         when Types.Command.Set_MSR_Range =>
            Subjects.Set_MSR_Range (Command => Command, Status => Status);
         when Types.Command.Create_Device_Domain =>
            Device_Domains.Create_Device_Domain (Command => Command, Status => Status);
         when Types.Command.Add_Device_To_Device_Domain =>
            Device_Domains.Add_Device (Command => Command, Status => Status);
         when Types.Command.Lock_Device_Domain =>
            Device_Domains.Lock_Device_Domain (Command => Command, Status => Status);
         when Types.Command.Activate_Device_Domain =>
            Device_Domains.Activate_Device_Domain (Command => Command, Status => Status);
         when Types.Command.Attach_Memory_Region_Domain =>
            Device_Domains.Attach_Memory_Region_Domain (Command => Command,
                                                        Status  => Status);
         when Types.Command.Create_Page_Table_Device_Domain =>
            Device_Domains.Create_Page_Table_Device_Domain (Command => Command,
                                                            Status  => Status);
         when Types.Command.Activate_Page_Table_Device_Domain =>
            Device_Domains.Activate_Page_Table_Device_Domain (Command => Command,
                                                              Status  => Status);
         when Types.Command.Map_Page_Device_Domain =>
            Device_Domains.Map_Page_Device_Domain (Command => Command,
                                                   Status  => Status);
         when Types.Command.Create_Kernel =>
            Kernels.Create_Kernel (Command => Command, Status => Status);
         when Types.Command.Lock_Kernel =>
            Kernels.Lock_Kernel (Command => Command, Status => Status);
         when Types.Command.Activate_Kernel =>
            Kernels.Activate_Kernel (Command => Command, Status => Status);
         when Types.Command.Attach_Memory_Region_Kernel =>
            Kernels.Attach_Memory_Region_Kernel (Command => Command, Status => Status);
         when Types.Command.Create_Page_Table_Kernel =>
            Kernels.Create_Page_Table_Kernel (Command => Command, Status => Status);
         when Types.Command.Activate_Page_Table_Kernel =>
            Kernels.Activate_Page_Table_Kernel (Command => Command, Status => Status);
         when Types.Command.Map_Page_Kernel =>
            Kernels.Map_Page_Kernel (Command => Command, Status => Status);
         when Types.Command.Assign_Device_Kernel =>
            Kernels.Assign_Device_Kernel (Command => Command, Status => Status);
         when Types.Command.Map_Device_Page_Kernel =>
            Kernels.Map_Device_Page (Command => Command, Status => Status);

         when Types.Command.Create_Legacy_Device =>
            Devices.Create_Legacy_Device (Command => Command, Status => Status);
         when Types.Command.Create_PCI_Device =>
            Devices.Create_PCI_Device (Command => Command, Status => Status);
         when Types.Command.Add_IO_Port_Range_Device =>
            Devices.Add_IO_Port_Range (Command => Command, Status => Status);
         when Types.Command.Add_IRQ_Device =>
            Devices.Add_IRQ (Command => Command, Status => Status);
         when Types.Command.Add_Memory_Device =>
            Devices.Add_Memory (Command => Command, Status => Status);
         when Types.Command.Activate_Device =>
            Devices.Activate (Command => Command, Status => Status);

         when others =>
            Status := Types.Command.Unknown_Command;
      end case;
   end Process;

   -----------------------------------------------------------------------------

   procedure Process_Command_Stream
   is separate;

end Tau0.Command_Processor;
