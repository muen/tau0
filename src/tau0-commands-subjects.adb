--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Roots.Writers.Subjects;
with Tau0.Types.Typization;

package body Tau0.Commands.Subjects
is

   package R  renames Data.Roots;
   package RW renames Data.Roots.Writers;

   use type Types.Device.State_Type;
   use type Types.Root.Kind_Type;
   use type Types.Root.State_Type;

   -----------------------------------------------------------------------------

   procedure Activate_Subject
     (Subject :     Types.Root_Range;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Subject) /= Types.Root.Subject then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Locked then
         Status := Types.Command.Subject_Not_Locked;
      elsif
        R.PTP_Present (Root => Subject) and
        not R.PTP_Active (Root => Subject)
      then
         Status := Types.Command.Subject_PTP_Inactive;
      elsif R.Assigned_IRQs (Root => Subject) > 0 and
            not Data.Memory.Interrupt_Remapping.Table_Present
      then
         Status := Types.Command.VTd_IRQ_Remap_Table_Not_Present;
      elsif not Data.IO_APICs.Is_Set and then
        (for all Device in Types.Device.Device_Range =>
           (Data.Roots.Device_Assigned (Subject, Device)
            and then Data.Devices.Uses_IOAPIC (Device)))
      then
         Status := Types.Command.IOAPIC_ID_Not_Set;
      else
         RW.Activate (Root => Subject);
         Status := Types.Command.Success;
      end if;
   end Activate_Subject;

   -----------------------------------------------------------------------------

   procedure Assign_Device
     (Subject :     Types.Root_Range;
      Device  :     Types.Device.Device_Range;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Subject) /= Types.Root.Subject then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Setup then
         Status := Types.Command.Subject_Not_Setup;
      elsif R.Device_Table_Full (Root => Subject) then
         Status := Types.Command.Subject_Device_Table_Full;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Active then
         Status := Types.Command.Device_Inactive;
      elsif not R.Device_Assignable (Root => Subject, Device => Device) then
         Status := Types.Command.Subject_Device_Unassignable;
      else
         RW.Assign_Device (Root => Subject, Device => Device);
         Status := Types.Command.Success;
      end if;
   end Assign_Device;

   -----------------------------------------------------------------------------

   procedure Assign_IRQ
     (Subject   :     Types.Root_Range;
      Device    :     Types.Device.Device_Range;
      Interrupt :     Types.Device.HW_IRQ_Type;
      Status    : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Subject) /= Types.Root.Subject then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Setup then
         Status := Types.Command.Subject_Not_Setup;
      elsif R.IRQ_Table_Full (Root => Subject) then
         Status := Types.Command.Subject_IRQ_Table_Full;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Active then
         Status := Types.Command.Device_Inactive;
      elsif not R.IRQ_Assignable (Root   => Subject,
                                  Device => Device,
                                  IRQ    => Interrupt)
      then
         Status := Types.Command.Subject_IRQ_Unassignable;
      else
         RW.Subjects.Assign_IRQ
           (Root      => Subject,
            Device    => Device,
            Interrupt => Interrupt);

         Status := Types.Command.Success;
      end if;
   end Assign_IRQ;

   -----------------------------------------------------------------------------

   procedure Attach_Memory_Region
     (Subject          :     Types.Root_Range;
      Region           :     Types.Root_Range;
      Use_Base_Address :     Boolean;
      Base_Address     :     Types.Virtual_Address_Type := 0;
      Offset           :     Types.Page_Count_Type;
      Length           :     Types.Page_Count_Type;
      Index            :     Types.Memory_Region_Table_Range;
      Writable         :     Boolean;
      Executable       :     Boolean;
      Status           : out Types.Command.Status_Type)
   is
      package M renames Data.Memory_Region_Table;

      use type Types.Root.Counter_Type;
      use type Types.Page_Count_Type;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif
        R.Kind (Root => Subject) /= Types.Root.Subject or
        R.Kind (Root => Region) /= Types.Root.Memory_Region
      then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Setup then
         Status := Types.Command.Subject_Not_Setup;
      elsif Data.Roots.State (Region) /= Types.Root.Active then
         Status := Types.Command.Memory_Region_Inactive;
      elsif M.Used (Index => Index) then
         Status := Types.Command.MR_Table_Index_Invalid;
      elsif not Types.Aligned_Page (Base_Address) then
         Status := Types.Command.Address_Invalid;
      elsif R.Reference_Count (Region) = Types.Root.Counter_Type'Last then
         Status := Types.Command.Memory_Region_Counter_Full;
      elsif R.Attached_Memory_Regions (Subject) = Natural'Last then
         Status := Types.Command.Attached_Region_Counter_Full;
      elsif
        Offset + Length > Data.Roots.Length (Root => Region)
      then
         Status := Types.Command.Memory_Region_Out_Of_Bounds;
      else
         M.Add_Region
           (Owner            => Subject,
            Region           => Region,
            Use_Base_Address => Use_Base_Address,
            Base_Address     => Base_Address,
            Offset           => Offset,
            Length           => Length,
            Index            => Index,
            Writable         => Writable,
            Executable       => Executable);

         Status := Types.Command.Success;
      end if;
   end Attach_Memory_Region;

   -----------------------------------------------------------------------------

   procedure Create_Subject
     (Subject    :     Types.Root_Range;
      Paging     :     Types.Root.Subject_Paging_Type;
      IO_Bitmap  :     Types.Physical_Address_Type;
      MSR_Bitmap :     Types.Physical_Address_Type;
      CPU        :     Types.CPU_Range;
      Status     : out Types.Command.Status_Type)
   is
      use type Types.Typization.Typization_Type;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif not Data.Processors.Present (ID => CPU) then
         Status := Types.Command.CPU_Invalid;
      elsif R.Used (Root => Subject) then
         Status := Types.Command.Subject_Used;
      elsif
        not Types.Aligned_Page (MSR_Bitmap) or else
        not Types.Aligned_Page (IO_Bitmap) or else
        not (Types.Successor_Page (IO_Bitmap) in Types.Physical_Address_Type) or else
        MSR_Bitmap in IO_Bitmap | Types.Successor_Page (IO_Bitmap)
      then
         Status := Types.Command.Address_Invalid;
      elsif
        Data.Memory.Get_Typization (MSR_Bitmap) /= Types.Typization.Zeroed or
        Data.Memory.Get_Typization (IO_Bitmap) /= Types.Typization.Zeroed or
        Data.Memory.Get_Typization (Types.Successor_Page (IO_Bitmap))
          /= Types.Typization.Zeroed
      then
         Status := Types.Command.Typization_Violation;
      else
         RW.Subjects.Create_Subject
           (Root       => Subject,
            Paging     => Paging,
            MSR_Bitmap => MSR_Bitmap,
            IO_Bitmap  => IO_Bitmap,
            CPU        => CPU);

         Status := Types.Command.Success;
      end if;
   end Create_Subject;

   -----------------------------------------------------------------------------

   procedure Lock_Subject
     (Subject :     Types.Root_Range;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Subject) /= Types.Root.Subject then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Setup then
         Status := Types.Command.Subject_Not_Setup;
      else
         R.Writers.Lock (Root => Subject);
         Status := Types.Command.Success;
      end if;
   end Lock_Subject;

   -----------------------------------------------------------------------------

   procedure Set_IO_Port_Range
     (Subject :     Types.Root_Range;
      Device  :     Types.Device.Device_Range;
      From    :     Types.IO_Port_Type;
      To      :     Types.IO_Port_Type;
      Mode    :     Types.IO_Bitmap.Mode_Type;
      Status  : out Types.Command.Status_Type)
   is
      use type Types.IO_Port_Type;
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Subject) /= Types.Root.Subject then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Setup then
         Status := Types.Command.Subject_Not_Setup;
      elsif To < From then
         Status := Types.Command.Invalid_Parameters;
      elsif Data.Devices.State (Device => Device) /= Types.Device.Active then
         Status := Types.Command.Device_Inactive;
      elsif not R.IO_Port_Range_Assignable
        (Root   => Subject,
         Device => Device,
         From   => From,
         To     => To)
      then
         Status := Types.Command.Subject_IO_Range_Invalid;
      else
         R.Writers.Subjects.Set_IO_Port_Range
           (Root   => Subject,
            Device => Device,
            From   => From,
            To     => To,
            Mode   => Mode);

         Status := Types.Command.Success;
      end if;
   end Set_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Set_MSR_Range
     (Subject :     Types.Root_Range;
      From    :     Types.MSR_Bitmap.MSR_Range;
      To      :     Types.MSR_Bitmap.MSR_Range;
      Mode    :     Types.MSR_Bitmap.Mode_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if Tau0_State /= Running then
         Status := Types.Command.Tau0_Mode_Invalid;
      elsif R.Kind (Root => Subject) /= Types.Root.Subject then
         Status := Types.Command.Root_Object_Kind_Invalid;
      elsif R.State (Root => Subject) /= Types.Root.Setup then
         Status := Types.Command.Subject_Not_Setup;
      elsif not Types.MSR_Bitmap.Same_Subrange (From, To) then
         Status := Types.Command.Subject_MSR_Range_Invalid;
      else
         R.Writers.Subjects.Set_MSR_Range
           (Root => Subject,
            From => From,
            To   => To,
            Mode => Mode);

         Status := Types.Command.Success;
      end if;
   end Set_MSR_Range;

end Tau0.Commands.Subjects;
