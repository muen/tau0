--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.VTd_IOMMU.Writers;

package body Tau0.Data.Roots.Writers.Device_Domains
is

   -----------------------------------------------------------------------------

   procedure Activate (Root : Types.Root_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      --  Write attached devices into VT-d tables
      for I in Roots (Root).Domain_Devices loop
         pragma Loop_Invariant (Invariant);
         pragma Loop_Invariant (Data.Memory.Invariant);

         pragma Loop_Variant
           (Increases => Types.Root.Domain_Device_Table.To_Natural (I));

         declare
            Device : constant Types.Device.Device_Range
              := Types.Root.Domain_Device_Table.Element
                   (Table  => Roots (Root).Domain_Devices,
                    Cursor => I);

            pragma Assert
              (Types.Root.Domain_Device_Table.Member
                 (Table => Roots (Root).Domain_Devices,
                  Elem  => Device));
            --  pragma Assert (Data.Devices.State (Device) = Types.Device.Active);
            --  pragma Assert (Data.Devices.Is_PCI (Device));

            Bus : constant Types.Device.PCI_Bus_Range
              := Data.Devices.PCI_Bus (Device);
            Dev : constant Types.Device.PCI_Device_Range
              := Data.Devices.PCI_Dev (Device);
            Func : constant Types.Device.PCI_Function_Range
              := Data.Devices.PCI_Func (Device);
         begin
            --  --  FIXME: Prove
            --  pragma Assert (Data.Memory.VTd_IOMMU.Root_Table_Present);
            --  -- establish by Roots invariant
            --  pragma Assert (Data.Memory.VTd_IOMMU.Context_Table_Present (Bus));
            --  -- export from VTd_IOMMU invariant
            --  pragma Assert
            --    (Data.Memory.Get_Typization
            --       (Data.Memory.VTd_IOMMU.Context_Table_Address (Bus))
            --       = Types.Typization.System);

            --  consequence of uniqueness of device assignment --
            --  establish by Roots invariant
            pragma Assert
              (not Data.Memory.VTd_IOMMU.Context_Table_Entry_Used
                (Bus  => Bus,
                 Dev  => Dev,
                 Func => Func));

            Data.Memory.VTd_IOMMU.Writers.Add_Device
              (Domain => Root,
               Device => Device);
         end;

         --  TODO: prove that if relevant parts of memory are unmodified, then
         --  the invariant is preserved.
         pragma Assert (Invariant_Devices_Device_Domain (Roots,
                                                         Memory.Get_Memory_Model));
      end loop;

      Roots (Root) := Types.Root.Root_Type'
        (State            => Types.Root.Active,
         Is_Memory_Region => False,
         Kind             => Types.Root.Device_Domain,
         PTP              => Roots (Root).PTP,
         PTP_Present      => Roots (Root).PTP_Present,
         PTP_Active       => Roots (Root).PTP_Active,
         Attached_Tables  => Roots (Root).Attached_Tables,
         Domain_Devices   => Roots (Root).Domain_Devices,
         Domain_Level     => Roots (Root).Domain_Level,
         Domain_ID        => Roots (Root).Domain_ID);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Activate;

   -----------------------------------------------------------------------------

   procedure Create_Device_Domain
     (Root  : Types.Root_Range;
      Level : Types.Nonleaf_Level_Type;
      ID    : Types.VTd_Tables.Domain_Identifier_Type)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Roots (Root) := Types.Root.Root_Type'
        (State            => Types.Root.Setup,
         Is_Memory_Region => False,
         Kind             => Types.Root.Device_Domain,
         PTP_Present      => False,
         PTP_Active       => False,
         PTP              => 0,
         Attached_Tables  => 0,
         Domain_ID        => ID,
         Domain_Devices   => Types.Root.Domain_Device_Table.Empty_Table,
         Domain_Level     => Level);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
      Lemma_Domain_ID (Old_Roots  => Old_Roots,
                       New_Roots  => Roots,
                       New_Domain => Root,
                       ID         => ID);
   end Create_Device_Domain;

   -----------------------------------------------------------------------------

   procedure Add_Device
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range)
   is
      Old_Roots : constant Types.Root.Root_Array := Roots with Ghost;
   begin
      Types.Root.Domain_Device_Table.Push
        (Table => Roots (Root).Domain_Devices,
         Elem  => Device);

      Lemma_CPUs (Old_Roots => Old_Roots, New_Roots => Roots, CPUs => CPUs);
      Memory_Region_Table.Lemma_Invariant (Old_Roots => Old_Roots,
                                           New_Roots => Roots);
   end Add_Device;

   -----------------------------------------------------------------------------

   procedure Lemma_Domain_ID
     (Old_Roots  : Types.Root.Root_Array;
      New_Roots  : Types.Root.Root_Array;
      New_Domain : Types.Root_Range;
      ID         : Types.VTd_Tables.Domain_Identifier_Type)
   is null;

end Tau0.Data.Roots.Writers.Device_Domains;
