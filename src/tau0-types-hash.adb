--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Types.Hash
with SPARK_Mode => Off
is

   -----------------------------------------------------------------------------

   overriding function Digest (Context : Context_Type) return Digest_Type
   is (GNAT.SHA256.Digest (C => GNAT.SHA256.Context (Context)));

   -----------------------------------------------------------------------------

   procedure Update
     (Context : in out Context_Type;
      Input   :        Types.Page_Array)
   is
      --  HACK!  How to do this properly (while avoiding a copy)?
      Input_As_Str : constant String (1 .. 4096)
      with
        Address => Input'Address,
        Import;
   begin
      GNAT.SHA256.Update
        (C     => GNAT.SHA256.Context (Context),
         Input => Input_As_Str);
   end Update;

end Tau0.Types.Hash;
