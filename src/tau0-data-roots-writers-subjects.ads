--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.IO_APICs;
with Tau0.Data.Memory_Region_Table;
with Tau0.Types.IO_Bitmap;
with Tau0.Types.MSR_Bitmap;
with Tau0.Types.Typization;

--  FIXME: Invariant depends on Memory_State, and thus breaks

package Tau0.Data.Roots.Writers.Subjects
is

   use type Types.Typization.Typization_Type;

   procedure Activate (Root : Types.Root_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Memory.Typization_State,
                             Data.Memory_Region_Table.State),
                Input    => (Data.Devices.State,
                             Data.IO_APICs.State,
                             Data.Memory.Interrupt_Remapping.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                In_Out   => (Data.Memory.Memory_State, State)),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Locked and then
               Kind (Root) = Types.Root.Subject and then
               not Active (Root) and then
               (if Assigned_IRQs (Root) > 0
                then Data.Memory.Interrupt_Remapping.Table_Present) and then
               (if Paging (Root) in Types.Root.Paging_Type
                then PTP_Active (Root)) and then
               (for all Device in Types.Device.Device_Range =>
                (if Data.Roots.Device_Assigned (Root, Device) and
                 Data.Devices.Uses_IOAPIC (Device)
                 then Data.IO_APICs.Is_Set)),
     Post   => Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               --  Update may not modify discriminants
               State (Root) = Types.Root.Active and
               Kind (Root) = Kind (Root)'Old and
               PTP_Present (Root) = PTP_Present (Root)'Old and
               PTP_Active (Root) = PTP_Active (Root)'Old and
               PTP (Root) = PTP (Root)'Old;

   procedure Create_Subject
     (Root       : Types.Root_Range;
      Paging     : Types.Root.Subject_Paging_Type;
      MSR_Bitmap : Types.Physical_Address_Type;
      IO_Bitmap  : Types.Physical_Address_Type;
      CPU        : Types.CPU_Range)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => (State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State)),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Unused and then
               Types.Aligned_Page (MSR_Bitmap) and then
               Types.Aligned_Page (IO_Bitmap) and then
               Types.Successor_Page (IO_Bitmap) in Types.Physical_Address_Type and then
               not (MSR_Bitmap in IO_Bitmap | Types.Successor_Page (IO_Bitmap)) and then
               Data.Memory.Get_Typization_Model (MSR_Bitmap)
                 = Types.Typization.Zeroed and then
               Data.Memory.Get_Typization_Model (IO_Bitmap)
                 = Types.Typization.Zeroed and then
               Data.Memory.Get_Typization_Model (Types.Successor_Page (IO_Bitmap))
                 = Types.Typization.Zeroed and then
               Data.Processors.Present (CPU),
     Post   => Invariant and
               Data.Memory.Invariant and
               Data.Memory_Region_Table.Invariant and
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and
               State (Root) = Types.Root.Setup and
               Kind (Root) = Types.Root.Subject and
               not PTP_Present (Root) and
               not PTP_Active (Root) and
               Data.Memory.Get_Typization (MSR_Bitmap) = Types.Typization.Used and
               Data.Memory.Get_Typization (IO_Bitmap) = Types.Typization.Used and
               Data.Memory.Get_Typization (Types.Successor_Page (IO_Bitmap))
                 = Types.Typization.Used;

   procedure Assign_IRQ
     (Root      : Types.Root_Range;
      Device    : Types.Device.Device_Range;
      Interrupt : Types.Device.HW_IRQ_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Memory_Region_Table.State,
                             Data.Processors.State),
                In_Out   => State),
     Pre    => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Subject and then
               not IRQ_Table_Full (Root) and then
               Data.Devices.State (Device) = Types.Device.Active and then
               IRQ_Assignable (Root, Device, Interrupt),
     --  FIXME: strengthen postcondition -- 'Update for Get_Roots (Root)
     Post   => Invariant and then
               Data.Memory_Region_Table.Invariant and then
               Roots_Unchanged (Get_Roots, Get_Roots'Old, Except => Root) and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Subject and then
               IRQ_Assigned (Root, Device, Interrupt);

   procedure Set_IO_Port_Range
     (Root   : Types.Root_Range;
      Device : Types.Device.Device_Range;
      From   : Types.IO_Port_Type;
      To     : Types.IO_Port_Type;
      Mode   : Types.IO_Bitmap.Mode_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                Input    => State,
                In_Out   => Data.Memory.Memory_State),
     Pre    => Invariant and then
               Data.Memory.Invariant and then
               Tau0_State = Running and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Subject and then
               From <= To and then
               Data.Devices.State (Device) = Types.Device.Active and then
               IO_Port_Range_Assignable (Root, Device, From, To),
     Post   => Invariant and
               Data.Memory.Invariant;

   procedure Set_MSR_Range
     (Root : Types.Root_Range;
      From : Types.MSR_Bitmap.MSR_Range;
      To   : Types.MSR_Bitmap.MSR_Range;
      Mode : Types.MSR_Bitmap.Mode_Type)
   with
     Global => (Proof_In => (Tau0.State,
                             Data.Devices.State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State),
                Input    => State,
                In_Out   => Data.Memory.Memory_State),
     Pre    => Invariant and then
               Tau0_State = Running and then
               Data.Memory.Invariant and then
               State (Root) = Types.Root.Setup and then
               Kind (Root) = Types.Root.Subject and then
               Types.MSR_Bitmap.Same_Subrange (From, To),
     Post   => Invariant and
               Data.Memory.Invariant;

end Tau0.Data.Roots.Writers.Subjects;
