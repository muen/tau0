--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.IO_APICs;
with Tau0.Data.Processors;
with Tau0.Types.Device;

package Tau0.Data.Memory.Interrupt_Remapping
with
  Abstract_State    => State,
  Initializes       => State,
  Initial_Condition => not Table_Present
is

   use type Types.Device.State_Type;

   -----------------------------------------------------------------------------

   function Table_Present return Boolean
   with Global => (Input => State);

   function Table_Address return Types.Physical_Address_Type
   with
     Pre  => Table_Present,
     Post => Types.Aligned_Page (Table_Address'Result);

   function Memory_Only_Table_Changed
     (Old_Model : Memory_Model_Type;
      New_Model : Memory_Model_Type)
      return Boolean
   with
     Ghost,
     Pre => Table_Present;

   function Memory_Only_Table_Changed
     (Old_Model : Memory_Model_Type;
      New_Model : Memory_Model_Type)
      return Boolean
   is (for all Address in Types.Tau0_Address_Type =>
      (if not Page.Within (Address, Table_Address)
       then New_Model (To_Range (Address)) = Old_Model (To_Range (Address))));

   procedure Create_Table (Page : Types.Physical_Address_Type)
   with
     Global => (Proof_In => Data.Memory.Memory_State,
                In_Out   => (State, Data.Memory.Typization_State)),
     Pre    => Data.Memory.Invariant and
               not Table_Present and
               Types.Aligned_Page (Page) and
               Get_Typization (Page) = Types.Typization.Zeroed,
     Post   => Data.Memory.Invariant and
               Table_Present;           --  FIXME extend

   procedure Remap_Interrupt
     (Device    : Types.Device.Device_Range;
      Interrupt : Types.Device.HW_IRQ_Type;
      CPU       : Types.CPU_Range)
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => (State, Devices.State, IO_APICs.State,
                             Processors.State),
                In_Out   => Data.Memory.Memory_State),
     Pre    => Table_Present and then
               Data.Devices.State (Device) = Types.Device.Active and then
               Data.Devices.Owns_IRQ (Device => Device, IRQ => Interrupt) and then
               Data.Processors.Present (CPU) and then
               (if Data.Devices.Uses_IOAPIC (Device) then Data.IO_APICs.Is_Set),
     Post   => Memory_Only_Table_Changed
                 (Old_Model => Get_Memory_Model'Old,
                  New_Model => Get_Memory_Model);

private

   type Trigger_Mode_Type is (Edge, Level);
   type IRQ_Kind_Type is (IRQ_ISA, IRQ_PCI_MSI, IRQ_PCI_LSI);

   function Get_IRQ_Kind
     (Device : Types.Device.Device_Range)
      return IRQ_Kind_Type
   with
     Global => Devices.State,
     Pre    => Data.Devices.Used (Device);

   function Get_Source_ID
     (Device : Types.Device.Device_Range)
     return Types.Source_ID_Type
   with
     Global => (Devices.State, Data.IO_APICs.State),
     Pre    => Data.Devices.Used (Device) and then
               (if Data.Devices.Uses_IOAPIC (Device) then Data.IO_APICs.Is_Set);

   procedure Set_IRTE
     (Src_Vector   : Types.Device.IRQ_Type;
      Dst_Vector   : Types.Device.IRQ_Type;
      Destination  : Types.APIC_ID_Type;
      Source_ID    : Types.Source_ID_Type;
      Trigger_Mode : Trigger_Mode_Type)
   with
     Global => (Proof_In => Data.Memory.Typization_State,
                Input    => State,
                In_Out   => Data.Memory.Memory_State),
     Pre    => Table_Present,
     Post   => Memory_Only_Table_Changed
                 (Old_Model => Get_Memory_Model'Old,
                  New_Model => Get_Memory_Model);

end Tau0.Data.Memory.Interrupt_Remapping;
