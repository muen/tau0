--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands.Kernels.Paging;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Kernels
is

   -----------------------------------------------------------------------------

   procedure Activate_Page_Table_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Activate_Page_Table_Kernel_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Paging.Activate_Page_Table
           (Kernel          =>
              Command.Activate_Page_Table_Kernel_Params.Kernel,
            Level           =>
              Command.Activate_Page_Table_Kernel_Params.Level,
            Virtual_Address =>
              Command.Activate_Page_Table_Kernel_Params.Virtual_Address,
            Status          => Status);
      end if;
   end Activate_Page_Table_Kernel;

   -----------------------------------------------------------------------------

   procedure Activate_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Kernel_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Activate_Kernel
           (Kernel => Command.Activate_Kernel_Params.Kernel,
            Status => Status);
      end if;
   end Activate_Kernel;

   -----------------------------------------------------------------------------

   procedure Assign_Device_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Assign_Device_Kernel_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Assign_Device
           (Kernel => Command.Assign_Device_Kernel_Params.Kernel,
            Device => Command.Assign_Device_Kernel_Params.Device,
            Status => Status);
      end if;
   end Assign_Device_Kernel;

   -----------------------------------------------------------------------------

   procedure Attach_Memory_Region_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Attach_Memory_Region_Kernel_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Attach_Memory_Region
           (Kernel           =>
              Command.Attach_Memory_Region_Kernel_Params.Kernel,
            Region           =>
              Command.Attach_Memory_Region_Kernel_Params.Region,
            Use_Base_Address =>
              Command.Attach_Memory_Region_Kernel_Params.Use_Base_Address,
            Base_Address     =>
              Command.Attach_Memory_Region_Kernel_Params.Base_Address,
            Offset           =>
              Command.Attach_Memory_Region_Kernel_Params.Offset,
            Length           =>
              Command.Attach_Memory_Region_Kernel_Params.Length,
            Index            =>
              Command.Attach_Memory_Region_Kernel_Params.Index,
            Writable         =>
              Command.Attach_Memory_Region_Kernel_Params.Writable,
            Executable       =>
              Command.Attach_Memory_Region_Kernel_Params.Executable,
            Status           => Status);
      end if;
   end Attach_Memory_Region_Kernel;

   -----------------------------------------------------------------------------

   procedure Create_Page_Table_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Create_Page_Table_Kernel_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         pragma Assert (Data.Memory.Invariant);

         Commands.Kernels.Paging.Create_Page_Table
           (Page            =>
              Command.Create_Page_Table_Kernel_Params.Page,
            Kernel          =>
              Command.Create_Page_Table_Kernel_Params.Kernel,
            Level           =>
              Command.Create_Page_Table_Kernel_Params.Level,
            Virtual_Address =>
              Command.Create_Page_Table_Kernel_Params.Virtual_Address,
            Readable        =>
              Command.Create_Page_Table_Kernel_Params.Readable,
            Writable        =>
              Command.Create_Page_Table_Kernel_Params.Writable,
            Executable      =>
              Command.Create_Page_Table_Kernel_Params.Executable,
            Status          => Status);

         pragma Assert (Data.Memory.Invariant);
      end if;
   end Create_Page_Table_Kernel;

   -----------------------------------------------------------------------------

   procedure Create_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Kernel_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Create_Kernel
           (Kernel  => Command.Create_Kernel_Params.Kernel,
            CPU     => Command.Create_Kernel_Params.CPU,
            Status  => Status);
      end if;
   end Create_Kernel;

   -----------------------------------------------------------------------------

   procedure Lock_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Lock_Kernel_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Lock_Kernel
           (Kernel => Command.Lock_Kernel_Params.Kernel,
            Status => Status);
      end if;
   end Lock_Kernel;

   -----------------------------------------------------------------------------

   procedure Map_Device_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Map_Device_Page_Kernel_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Paging.Map_Device_Page
           (Kernel          =>
              Command.Map_Device_Page_Kernel_Params.Kernel,
            Device          =>
              Command.Map_Device_Page_Kernel_Params.Device,
            Page            =>
              Command.Map_Device_Page_Kernel_Params.Page,
            Virtual_Address =>
              Command.Map_Device_Page_Kernel_Params.Virtual_Address,
            Writable        =>
              Command.Map_Device_Page_Kernel_Params.Writable,
            Executable      =>
              Command.Map_Device_Page_Kernel_Params.Executable,
            Status          => Status);
      end if;
   end Map_Device_Page;

   -----------------------------------------------------------------------------

   procedure Map_Page_Kernel
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Map_Page_Kernel_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Kernels.Paging.Map_Page
           (Kernel          => Command.Map_Page_Kernel_Params.Kernel,
            Virtual_Address => Command.Map_Page_Kernel_Params.Virtual_Address,
            Table_Index     => Command.Map_Page_Kernel_Params.Table_Index,
            Offset          => Command.Map_Page_Kernel_Params.Offset,
            Status          => Status);
      end if;
   end Map_Page_Kernel;

end Tau0.Command_Processor.Kernels;
