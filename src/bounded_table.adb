--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Bounded_Table
is

   -----------------------------------------------------------------------------

   function Element (Table : T; Cursor : Cursor_Type) return Elem_Type
   is (Table.Elems (Index_Type (Cursor)));

   -----------------------------------------------------------------------------

   function Empty_Table return T
   is (T'(Elems => (others => Null_Elem),
          Last  => 0));

   -----------------------------------------------------------------------------

   function First (Table : T) return Cursor_Type
   with Refined_Post => First'Result = Cursor_Type'First
   is
      pragma Unreferenced (Table);
   begin
      return Cursor_Type'First;
   end First;

   -----------------------------------------------------------------------------

   function Has_Element (Table : T; Cursor : Cursor_Type) return Boolean
   is (Cursor < Cursor_Type'Last and then Opt_Index_Type (Cursor) <= Table.Last);

   -----------------------------------------------------------------------------

   function Length (Table : T) return Natural
   is (Natural (Table.Last));

   -----------------------------------------------------------------------------

   function Member (Table : T; Elem : Elem_Type) return Boolean
   is (for some I in Table.Elems'First .. Table.Last =>
       Table.Elems (I) = Elem);

   -----------------------------------------------------------------------------

   function Model (Table : T) return Model_Type
   is (Model_Type (Table.Elems (Table.Elems'First .. Table.Last)));

   -----------------------------------------------------------------------------

   function Next (Table : T; Cursor : Cursor_Type) return Cursor_Type
   is (Cursor + 1);

   -----------------------------------------------------------------------------

   function To_Natural (Cursor : Cursor_Type) return Natural
   is (Natural (Cursor));

   -----------------------------------------------------------------------------

   procedure Pop
     (Table : in out T;
      Elem  :    out Elem_Type)
   is
   begin
      Elem       := Table.Elems (Table.Last);
      Table.Last := Table.Last - 1;
   end Pop;

   -----------------------------------------------------------------------------

   procedure Push
     (Table : in out T;
      Elem  :        Elem_Type)
   is
   begin
      Table.Last               := Table.Last + 1;
      Table.Elems (Table.Last) := Elem;
   end Push;

end Bounded_Table;
