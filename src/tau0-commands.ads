--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory;
with Tau0.Data.Roots;
with Tau0.Types.Command;

limited with Tau0.Command_Line;

package Tau0.Commands
is

   procedure Clear_Page
     (Page   :     Types.Tau0_Address_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (In_Out => (Data.Memory.Typization_State,
                           Data.Memory.Memory_State)),
     Pre    => Data.Memory.Invariant,
     Post   => Data.Memory.Invariant;

   procedure Write_Image_Commands
     (Entry_Point :     Types.Tau0_Address_Type;
      Status      : out Types.Command.Status_Type)
   with
     Global => (Input    => (Command_Line.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Roots.State)),
     Pre    => Data.Memory.Invariant,
     Post   => Data.Memory.Invariant;

end Tau0.Commands;
