--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Memory
with Refined_State => (Init_State       => Init_Flag,
                       Memory_State     => Low_Level.State,
                       Typization_State => Typization.State)
is

   package body Low_Level is separate;
   package body Typization is separate;

   use Types;

   Init_Flag : Boolean := False;

   -----------------------------------------------------------------------------

   procedure Add_Memory_Block
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
   renames Typization.Add_Memory_Block;

   -----------------------------------------------------------------------------

   procedure Add_Device_Block
     (Address : Types.Physical_Address_Type;
      Size    : Types.Physical_Page_Count_Type)
   renames Typization.Add_Device_Block;

   -----------------------------------------------------------------------------

   function Imported_Property
     (Memory      : Memory_Model_Type;
      Typizations : T.Typization_Model_Type)
      return Boolean
   with
     Ghost,
     Global => null,
     Import;

   -----------------------------------------------------------------------------

   procedure Clear_Page (Page : Tau0_Address_Type)
   is
      Zeroes : constant Page_Array := (others => 0);
   begin
      pragma Assert (Accessible (Page));
      Write_Page (Page     => Page,
                  Contents => Zeroes);

      --  FIXME
      Typization.Set (Page, T.Zeroed);

      --  Assumption: Every explicitly zeroed page in physical address space
      --              has content.

      if Page in Types.Physical_Address_Type
        and then not Typization.Has_Content (To_Page_Count (Address => Page))
      then
         Typization.Declare_Content (To_Page_Count (Page));
      end if;
   end Clear_Page;

   -----------------------------------------------------------------------------

   function Initial_Condition return Boolean
   is (for all Addr in Tau0_Address_Type =>
       Typization.Get_Model (Addr) = T.Reserved);

   -----------------------------------------------------------------------------

   procedure Initialize (Success : out Boolean)
   is separate;

   -----------------------------------------------------------------------------

   function Initialized return Boolean
   is (Init_Flag);

   -----------------------------------------------------------------------------

   function Invariant return Boolean
   is (Imported_Property (Memory      => Low_Level.Get_Model,
                          Typizations => Typization.Get_Model));

   -----------------------------------------------------------------------------

   procedure Accessible_Lemma (A, B : Tau0_Address_Type)
   is
   begin
      --  Prove with Isabelle?
      pragma Assert (Accessible (A) = Accessible (B));
   end Accessible_Lemma;

   -----------------------------------------------------------------------------

   procedure Accessible_Lemma (A : Types.Tau0_Address_Type)
   is
   begin
      for Addr in Page.First (A) .. Page.Last (A) loop
         pragma Loop_Invariant
           (for all B in Page.First (A) .. Addr - 1 =>
            Accessible (A) = Accessible (B));
         Accessible_Lemma (A, Addr);
      end loop;
   end Accessible_Lemma;

   -----------------------------------------------------------------------------

   procedure Typization_Lemma
   is
   begin
      Typization.Get_Lemma;
   end Typization_Lemma;

   -----------------------------------------------------------------------------

   procedure Use_Page (Page : Types.Tau0_Address_Type)
   is
   begin
      Typization.Set (Address => Page, Typization => T.Used);
      pragma Assert (Typization.Get_Model (Page) = T.Used);
      --  TODO: Prove with Isabelle!
      pragma Assert (Invariant);
   end Use_Page;

   -----------------------------------------------------------------------------

   procedure Write_Image_Commands
     (Filename    : String;
      Entry_Point : Types.Tau0_Address_Type)
   is
   begin
      Low_Level.Write_Image_Commands (Filename    => Filename,
                                      Entry_Point => Entry_Point);
   end Write_Image_Commands;

   -----------------------------------------------------------------------------

   procedure Write_Page
     (Page     : Types.Tau0_Address_Type;
      Contents : Page_Array)
   is
      Mem_Model : constant Memory_Model_Type := Get_Memory_Model
      with Ghost;

      function Get_Address (Idx : Types.Page_Array_Range) return Tau0_Address_Type
      with
        Global => Page,
        Pre    => Types.Aligned_Page (Page),
        Post   => Types.Aligned_64 (Get_Address'Result) and
                  Tau0.Page.Within (Get_Address'Result, Page);

      function Get_Address (Idx : Types.Page_Array_Range) return Tau0_Address_Type
      is (Page + Tau0_Address_Type (Idx - Contents'First) * 8);

   begin
      for Idx in Contents'Range loop
         pragma Loop_Invariant (Accessible (Page));
         pragma Loop_Invariant
           (for all M in Memory_Range =>
           (if not (M in To_Range (Page) .. To_Range (Tau0.Page.Last (Page)))
            then Get_Memory_Model (M) = Mem_Model (M)));
         pragma Loop_Invariant
           (for all I in Contents'First .. Idx - 1 =>
            Get_Memory_Model (To_Range (Page) + Memory_Range (I - Contents'First))
              = Contents (I));

         Accessible_Lemma (A => Page, B => Get_Address (Idx));
         Low_Level.Set (Address => Get_Address (Idx), Value => Contents (Idx));
      end loop;

      pragma Assert
        (for all M in Memory_Range =>
        (if M in To_Range (Page) .. To_Range (Tau0.Page.Last (Page))
         then Get_Memory_Model (M)
                = Contents (Types.Page_Array_Range'First
                    + Natural (M - To_Range (Page)))
         else Get_Memory_Model (M) = Mem_Model (M)));

      --  FIXME prove with Isabelle
      pragma Assert (Invariant and Accessible (Page));
   end Write_Page;

end Tau0.Data.Memory;
