--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Config;

package body Tau0.Types.Page_Table.MPT
is

   Present_Mask : constant Word64 := 2 ** 0;
   Active_Mask  : constant Word64 := 2 ** 11;
   Page_Mask    : constant Word64 := Config.Page_Size - 1;
   Address_Mask : constant Word64
     := (2 ** Config.Bits_Tau0_Address - 1) and not Page_Mask;

   -----------------------------------------------------------------------------

   function From_Entry (Entr : Entry_Type) return Word64
   is (Inject (Entr.Present) * Present_Mask or
       Inject (Entr.Active) * Active_Mask or
       Word64 (Entr.Address));

   -----------------------------------------------------------------------------

   pragma Warnings (Off, "formal parameter ""Level"" is not referenced",
                    Reason => "Function is parameter to generic package");

   function To_Entry (Word : Word64; Level : Page_Level_Type) return Entry_Type
   is
      PTE_Address : constant Tau0_Address_Type
        := Tau0_Address_Type (Word and Address_Mask);
   begin
      pragma Assert (Aligned_Page (PTE_Address));

      if Level = 1 then
         return Entry_Type'
           (Maps_Page  => True,
            Level      => Level,
            Present    => (Word and Present_Mask) /= 0,
            Readable   => True,
            Writable   => True,
            Executable => True,
            Caching    => UC,  --  Caching does not matter for memory regions
            Active     => (Word and Active_Mask) /= 0,
            Address    => PTE_Address);
      else
         return Entry_Type'
           (Maps_Page  => False,
            Level      => Level,
            Present    => (Word and Present_Mask) /= 0,
            Readable   => True,
            Writable   => True,
            Executable => True,
            Active     => (Word and Active_Mask) /= 0,
            Address    => PTE_Address);
      end if;
   end To_Entry;

   pragma Warnings (On, "formal parameter ""Level"" is not referenced");

end Tau0.Types.Page_Table.MPT;
