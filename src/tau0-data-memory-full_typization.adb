--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Memory.Full_Typization
is

   -----------------------------------------------------------------------------

   procedure Active_Cong
     (Root      : Types.Root_Range;
      Old_Path  : Path_Type;
      New_Path  : Path_Type;
      Path_Last : Path_Index)
   is null;

   -----------------------------------------------------------------------------

   procedure Reachable_Step
     (Root        : Types.Root_Range;
      Path        : Path_Type;
      Destination : Types.Tau0_Address_Type;
      Path_Last   : Path_Index)
   is null;

end Tau0.Data.Memory.Full_Typization;
