--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Devices;
with Tau0.Data.Memory.Interrupt_Remapping;
with Tau0.Data.Memory.VTd_IOMMU;
with Tau0.Data.Processors;
with Tau0.Data.IO_APICs;
with Tau0.Types.Device;

package Tau0.Commands.Setup
is

   procedure Activate_Tau0 (Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State,
                             Data.Roots.State),
                Input    => Data.Devices.State,
                In_Out   => Tau0.State),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant;

   procedure Add_Memory_Block
     (Address :     Types.Physical_Address_Type;
      Size    :     Types.Physical_Page_Count_Type;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => Data.Memory.Memory_State,
                Input    => Tau0.State,
                In_Out   => Data.Memory.Typization_State),
     Pre    => Data.Memory.Invariant,
     Post   => Data.Memory.Invariant;

   procedure Add_Processor
     (ID      :     Types.CPU_Range;
      APIC_ID :     Types.APIC_ID_Type;
      Status  : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State, Data.Roots.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State),
                Input    => Tau0.State,
                In_Out   => Data.Processors.State),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant;

   procedure Add_Ioapic
     (Source_ID :     Types.Source_ID_Type;
      Status    : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Memory.Memory_State,
                             Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State,
                             Data.Processors.State, Data.Roots.State),
                Input    => Tau0.State,
                In_Out   => Data.IO_APICs.State),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant;

   procedure Create_VTd_Root_Table
     (Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => Data.Memory.Memory_State,
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Typization_State,
                             Data.Memory.VTd_IOMMU.State)),
     Pre    => Data.Memory.Invariant,
     Post   => Data.Memory.Invariant;

   procedure Create_VTd_Context_Table
     (Bus    :     Types.Device.PCI_Bus_Range;
      Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => (Data.Devices.State,
                             Data.Roots.State,
                             Data.Processors.State),
                Input    => (Tau0.State, Data.Memory.VTd_IOMMU.State),
                In_Out   => (Data.Memory.Typization_State,
                             Data.Memory.Memory_State)),
     Pre    => Data.Memory.Invariant and
               Data.Roots.Invariant,
     Post   => Data.Memory.Invariant and
               Data.Roots.Invariant;

   procedure Create_VTd_IRQ_Remap_Table
     (Page   :     Types.Physical_Address_Type;
      Status : out Types.Command.Status_Type)
   with
     Global => (Proof_In => Data.Memory.Memory_State,
                Input    => Tau0.State,
                In_Out   => (Data.Memory.Typization_State,
                             Data.Memory.Interrupt_Remapping.State)),
     Pre    => Data.Memory.Invariant,
     Post   => Data.Memory.Invariant;

end Tau0.Commands.Setup;
