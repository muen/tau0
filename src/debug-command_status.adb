--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Ada.Text_IO;

package body Debug.Command_Status
with SPARK_Mode => Off                  --  FIXME?
is

   use Tau0.Types.Command;

   subtype Status_Name is String (1 .. 33);

   type Name_Table_Type is array (Status_Type) of Status_Name;

   --  OPTIMIZE: size
   Names : constant Name_Table_Type
     := (Success                           => "Success                          ",
         Address_Invalid                   => "Address_Invalid                  ",
         Level_Invalid                     => "Level_Invalid                    ",
         VTd_Tables_Invalid                => "VTd_Tables_Invalid               ",
         Page_Translation_Error            => "Page_Translation_Error           ",
         Typization_Violation              => "Typization_Violation             ",
         Root_Object_Kind_Invalid          => "Root_Object_Kind_Invalid         ",
         Invalid_Parameters                => "Invalid_Parameters               ",
         Immediate_Command_Expected        => "Immediate_Command_Expected       ",
         Immediate_Mode_Violation          => "Immediate_Mode_Violation         ",
         Attached_Region_Counter_Full      => "Attached_Region_Counter_Full     ",
         VTd_Root_Table_Present            => "VTd_Root_Table_Present           ",
         VTd_Root_Table_Not_Present        => "VTd_Root_Table_Not_Present       ",
         VTd_Context_Table_Present         => "VTd_Context_Table_Present        ",
         VTd_Context_Table_Not_Present     => "VTd_Context_Table_Not_Present    ",
         VTd_IRQ_Remap_Table_Present       => "VTd_IRQ_Remap_Table_Present      ",
         VTd_IRQ_Remap_Table_Not_Present   => "VTd_IRQ_Remap_Table_Not_Present  ",
         Device_Memory_Invalid             => "Device_Memory_Invalid            ",
         Tau0_Mode_Invalid                 => "Tau0_Mode_Invalid                ",
         Reserved_Memory_Set_Full          => "Reserved_Memory_Set_Full         ",
         Setup_Phase_Incomplete            => "Setup_Phase_Incomplete           ",
         CPU_Invalid                       => "CPU_Invalid                      ",
         Memory_Blocks_Full                => "Memory_Blocks_Full               ",
         Content_Regions_Full              => "Content_Regions_Full             ",
         Some_Root_Inactive                => "Some_Root_Inactive               ",
         Processor_Present                 => "Processor_Present                ",
         APIC_ID_Used                      => "APIC_ID_Used                     ",
         IOAPIC_ID_Not_Set                 => "IOAPIC_ID_Not_Set                ",
         MR_Table_Index_Invalid            => "MR_Table_Index_Invalid           ",
         MR_Table_Mapped_Pages_Error       => "MR_Table_Mapped_Pages_Error      ",
         Subject_Not_Locked                => "Subject_Not_Locked               ",
         Subject_Not_Unlocked              => "Subject_Not_Unlocked             ",
         Subject_Not_Setup                 => "Subject_Not_Setup                ",
         Subject_Used                      => "Subject_Used                     ",
         Subject_Invalid                   => "Subject_Invalid                  ",
         Subject_Paging_Error              => "Subject_Paging_Error             ",
         Subject_Active                    => "Subject_Active                   ",
         Subject_Inactive                  => "Subject_Inactive                 ",
         Subject_PTP_Active                => "Subject_PTP_Active               ",
         Subject_PTP_Inactive              => "Subject_PTP_Inactive             ",
         Subject_PTP_Present               => "Subject_PTP_Present              ",
         Subject_PTP_Not_Present           => "Subject_PTP_Not_Present          ",
         Subject_Mapped_Region_Full        => "Subject_Mapped_Region_Full       ",
         Subject_Mapped_Region_Empty       => "Subject_Mapped_Region_Empty      ",
         Subject_MSR_Range_Invalid         => "Subject_MSR_Range_Invalid        ",
         Subject_IO_Range_Invalid          => "Subject_IO_Range_Invalid         ",
         Subject_Device_Table_Full         => "Subject_Device_Table_Full        ",
         Subject_Device_Unassignable       => "Subject_Device_Unassignable      ",
         Subject_IRQ_Table_Full            => "Subject_IRQ_Table_Full           ",
         Subject_IRQ_Unassignable          => "Subject_IRQ_Unassignable         ",
         Subject_Device_Not_Assigned       => "Subject_Device_Not_Assigned      ",
         Device_Domain_Not_Locked          => "Device_Domain_Not_Locked         ",
         Device_Domain_Not_Unlocked        => "Device_Domain_Not_Unlocked       ",
         Device_Domain_Not_Setup           => "Device_Domain_Not_Setup          ",
         Device_Domain_Used                => "Device_Domain_Used               ",
         Device_Domain_Invalid             => "Device_Domain_Invalid            ",
         Device_Domain_Paging_Error        => "Device_Domain_Paging_Error       ",
         Device_Domain_Active              => "Device_Domain_Active             ",
         Device_Domain_Inactive            => "Device_Domain_Inactive           ",
         Device_Domain_PTP_Active          => "Device_Domain_PTP_Active         ",
         Device_Domain_PTP_Inactive        => "Device_Domain_PTP_Inactive       ",
         Device_Domain_PTP_Present         => "Device_Domain_PTP_Present        ",
         Device_Domain_PTP_Not_Present     => "Device_Domain_PTP_Not_Present    ",
         Device_Domain_Mapped_Region_Full  => "Device_Domain_Mapped_Region_Full ",
         Device_Domain_Mapped_Region_Empty => "Device_Domain_Mapped_Region_Empty",
         Device_Included                   => "Device_Included                  ",
         Device_Domain_Table_Full          => "Device_Domain_Table_Full         ",
         Device_Domain_ID_Used             => "Device_Domain_ID_Used            ",
         Memory_Region_Inactive            => "Memory_Region_Inactive           ",
         Memory_Region_Not_Locked          => "Memory_Region_Not_Locked         ",
         Memory_Region_Not_Unlocked        => "Memory_Region_Not_Unlocked       ",
         Memory_Region_Not_Setup           => "Memory_Region_Not_Setup          ",
         Memory_Region_Used                => "Memory_Region_Used               ",
         Memory_Region_Hash_Invalid        => "Memory_Region_Hash_Invalid       ",
         Memory_Region_MPTP_Present        => "Memory_Region_MPTP_Present       ",
         Memory_Region_MPTP_Not_Present    => "Memory_Region_MPTP_Not_Present   ",
         Memory_Region_MPTP_Active         => "Memory_Region_MPTP_Active        ",
         Memory_Region_MPTP_Inactive       => "Memory_Region_MPTP_Inactive      ",
         Memory_Region_Counter_Empty       => "Memory_Region_Counter_Empty      ",
         Memory_Region_Counter_Full        => "Memory_Region_Counter_Full       ",
         Memory_Region_Out_Of_Bounds       => "Memory_Region_Out_Of_Bounds      ",
         Memory_Region_Referenced          => "Memory_Region_Referenced         ",
         Memory_Region_Length_Too_Large    => "Memory_Region_Length_Too_Large   ",
         Memory_Region_Empty               => "Memory_Region_Empty              ",
         Kernel_Not_Locked                 => "Kernel_Not_Locked                ",
         Kernel_Not_Unlocked               => "Kernel_Not_Unlocked              ",
         Kernel_Not_Setup                  => "Kernel_Not_Setup                 ",
         Kernel_Used                       => "Kernel_Used                      ",
         Kernel_Invalid                    => "Kernel_Invalid                   ",
         Kernel_Paging_Error               => "Kernel_Paging_Error              ",
         Kernel_Active                     => "Kernel_Active                    ",
         Kernel_Inactive                   => "Kernel_Inactive                  ",
         Kernel_PTP_Active                 => "Kernel_PTP_Active                ",
         Kernel_PTP_Inactive               => "Kernel_PTP_Inactive              ",
         Kernel_PTP_Present                => "Kernel_PTP_Present               ",
         Kernel_PTP_Not_Present            => "Kernel_PTP_Not_Present           ",
         Kernel_Mapped_Region_Full         => "Kernel_Mapped_Region_Full        ",
         Kernel_Mapped_Region_Empty        => "Kernel_Mapped_Region_Empty       ",
         Kernel_CPU_Used                   => "Kernel_CPU_Used                  ",
         Kernel_Device_Table_Full          => "Kernel_Device_Table_Full         ",
         Kernel_Device_Unassignable        => "Kernel_Device_Unassignable       ",
         Kernel_Device_Not_Assigned        => "Kernel_Device_Not_Assigned       ",
         Device_Used                       => "Device_Used                      ",
         Device_Not_Setup                  => "Device_Not_Setup                 ",
         Device_BDF_Used                   => "Device_BDF_Used                  ",
         Device_IO_Port_Set_Full           => "Device_IO_Port_Set_Full          ",
         Device_IRQ_Table_Full             => "Device_IRQ_Table_Full            ",
         Device_Memory_Table_Full          => "Device_Memory_Table_Full         ",
         Device_Not_PCI                    => "Device_Not_PCI                   ",
         Device_IO_Port_Used               => "Device_IO_Port_Used              ",
         Device_IRQ_Used                   => "Device_IRQ_Used                  ",
         Device_Memory_Used                => "Device_Memory_Used               ",
         Unknown_Command                   => "Unknown_Command                  ",

         others                            => "STATUS_CODE_UNKNOWN              ");

   -----------------------------------------------------------------------------

   procedure Put
     (Status : Status_Type;
      Level  : Level_Type := Info)
   is
      package Status_IO is new Ada.Text_IO.Modular_IO (Num => Status_Type);
   begin
      if Level >= Get_Current_Level then
         declare
            Name : constant String := Trim (Source => Names (Status),
                                            Side   => Right);
            Num  : String (1 .. 8) := (others => ' ');
         begin
            --  How to force leading zeroes?
            Status_IO.Put (Item => Status, To => Num, Base => 16);
            Put (Item => Name, Level => Level);
            Put (Item  => " (" & Trim (Source => Num, Side => Left) & ")",
                 Level => Level);
         end;
      end if;
   end Put;

end Debug.Command_Status;
