--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Types.MSR_Bitmap
is

   type MSR_Base_Range is range 0 .. 16#c0001fff#;

   subtype MSR_Low_Range  is MSR_Base_Range range     16#0000# ..     16#1fff#;
   subtype MSR_High_Range is MSR_Base_Range range 16#c0000000# .. 16#c0001fff#;

   subtype MSR_Range is MSR_Base_Range
   with Static_Predicate => MSR_Range in MSR_Low_Range | MSR_High_Range;

   function Same_Subrange (L, R : MSR_Range) return Boolean
   is ((L in MSR_Low_Range and R in MSR_Low_Range) or
       (L in MSR_High_Range and R in MSR_High_Range));

   type Mode_Type is range 0 .. 3;

   Denied : constant Mode_Type := 0;
   R      : constant Mode_Type := 1;
   W      : constant Mode_Type := 2;
   RW     : constant Mode_Type := 3;

end Tau0.Types.MSR_Bitmap;
