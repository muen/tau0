--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package body Tau0.Data.Memory.Bitmaps
is

   -----------------------------------------------------------------------------

   procedure Set_IO_Port_Range
     (IO_Bitmap : Types.Physical_Address_Type;
      From      : Types.IO_Port_Type;
      To        : Types.IO_Port_Type;
      Mode      : Types.IO_Bitmap.Mode_Type)
   is
      --  Cf. Intel Software Developer's Manual, Vol. 3C, sec. 24.6.4.

      use type Types.IO_Port_Type;

      Word64_Bits  : constant := Types.Word64'Size;
      Word64_Bytes : constant := Word64_Bits / 8;

      subtype Bit_Range    is Natural range 0 .. Word64_Bits - 1;
      subtype Word64_Index is Natural   --  Index Word64s within two consecutive pages
        range 0 .. (2 * Config.Page_Size / Word64_Bytes) - 1;

      IO_Bitmap_High : constant Types.Physical_Address_Type
        := Types.Successor_Page (IO_Bitmap) with Ghost;
      Old_Model      : constant Memory_Model_Type := Get_Memory_Model with Ghost;

      From_Word64 : constant Word64_Index := Natural (From) / Word64_Bits;
      To_Word64   : constant Word64_Index := Natural (To) / Word64_Bits;

      --------------------------------------------------------------------------

      --  Determine address in page by index.  Using a function helps GNATprove.
      function Get_Address (Index : Word64_Index) return Types.Physical_Address_Type
      with
        --  FIXME: GNATprove bug: Typization_State vs Typization.State
        --  Global => (Proof_In => (IO_Bitmap_High, Typization.State),
        --             Input    => IO_Bitmap),
        Pre    => Types.Aligned_Page (IO_Bitmap) and then
                  IO_Bitmap + Config.Page_Size in Types.Physical_Address_Type and then
                  Accessible (IO_Bitmap) and then
                  Accessible (Types.Successor_Page (IO_Bitmap)),
        Post   => Types.Aligned_64 (Get_Address'Result) and
                  (Page.Within (Get_Address'Result, IO_Bitmap) or
                   Page.Within (Get_Address'Result, IO_Bitmap_High)) and
                  Accessible (Get_Address'Result);

      --  Compute bitmask for word at Index
      function Get_Bitmask (Index : Word64_Index) return Types.Word64
      with Pre => Index in From_Word64 .. To_Word64;

      --------------------------------------------------------------------------

      function Get_Address (Index : Word64_Index) return Types.Physical_Address_Type
      is
         Result : constant Types.Physical_Address_Type
           := IO_Bitmap + Types.Physical_Address_Type (Index * Word64_Bytes);
      begin
         Accessible_Lemma (IO_Bitmap);
         Accessible_Lemma (IO_Bitmap_High);

         return Result;
      end Get_Address;

      --------------------------------------------------------------------------

      function Get_Bitmask (Index : Word64_Index) return Types.Word64
      is
         From_Bit : constant Bit_Range
           := (if Index = From_Word64
               then Bit_Range (From mod Word64_Bits)
               else Bit_Range'First);
         To_Bit   : constant Bit_Range
           := (if Index = To_Word64
               then Bit_Range (To mod Word64_Bits)
               else Bit_Range'Last);
      begin
         return 2 ** (To_Bit + 1) - 2 ** From_Bit;
      end Get_Bitmask;

   begin
      --  Prevent overflow of bitmask
      if To < From then
         return;
      end if;

      --  Iterate over words in pages
      for Current in From_Word64 .. To_Word64 loop
         pragma Loop_Invariant (Invariant);
         pragma Loop_Invariant
           (for all M in Memory_Range =>
           (if M < To_Range (IO_Bitmap) or
               M >= To_Range (IO_Bitmap + 2 * Config.Page_Size)
            then Get_Memory_Model (M) = Old_Model (M)));

         declare
            Word_Address : constant Types.Physical_Address_Type
              := Get_Address (Current);
            Word         : Types.Word64;
         begin
            Word := Low_Level.Get (Address => Word_Address);

            begin
               case Mode is
                  --  Bitmaps follow inverted logic.
                  when Types.IO_Bitmap.Allowed =>
                     Low_Level.Set (Address => Word_Address,
                                    Value   => Word and not Get_Bitmask (Current));
                  when Types.IO_Bitmap.Denied =>
                     Low_Level.Set (Address => Word_Address,
                                    Value   => Word or Get_Bitmask (Current));
               end case;
            end;

            --  FIXME: prove with Isabelle
            pragma Assert (Invariant);
         end;
      end loop;
   end Set_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Set_MSR_Range
     (MSR_Bitmap : Types.Physical_Address_Type;
      From       : Types.MSR_Bitmap.MSR_Range;
      To         : Types.MSR_Bitmap.MSR_Range;
      Mode       : Types.MSR_Bitmap.Mode_Type)
   is
      --   MSR bitmap
      --     Low Read    High Read   Low Write   High Write
      --   |-----------|-----------|-----------|-----------|
      --   0           128         256         384         512 Word64_Offset
      --         (High_Offset) (Write_Offset)

      --  Cf. also Intel Software Developer's Manual, Vol. 3C, sec. 24.6.9.

      use Types.MSR_Bitmap;

      Word64_Bits  : constant := Types.Word64'Size;
      Word64_Bytes : constant := Word64_Bits / 8;
      High_Offset  : constant := 128;
      Write_Offset : constant := 256;

      Word      : Types.Word64;
      Old_Model : constant Memory_Model_Type := Get_Memory_Model with Ghost;

      subtype Bit_Range    is Natural range 0 .. Word64_Bits - 1;
      subtype Word64_Index is Natural   --  Index Word64s within half of page
        range 0 .. (Config.Page_Size / (2 * Word64_Bytes)) - 1;

      type RW_Type is (Read, Write);

      function To_Word64_Index (MSR : MSR_Range) return Word64_Index
      is (Word64_Index
            (if MSR in MSR_Low_Range then MSR / Word64_Bits
             else High_Offset + (MSR - MSR_High_Range'First) / Word64_Bits));

      From_Word64 : constant Word64_Index := To_Word64_Index (From);
      To_Word64   : constant Word64_Index := To_Word64_Index (To);

      --------------------------------------------------------------------------

      --  Determine address by index and read/write mode
      function Get_Address
        (Index : Word64_Index;
         Mode  : RW_Type)
         return Types.Physical_Address_Type
      with
        --  FIXME: GNATprove bug: Typization_State vs Typization.State
        --  Global => (Proof_In => Typization.State,
        --             Input    => MSR_Bitmap),
        Pre    => Types.Aligned_Page (MSR_Bitmap) and
                  Accessible (MSR_Bitmap),
        Post   => Types.Aligned_64 (Get_Address'Result) and
                  Page.Within (Get_Address'Result, MSR_Bitmap) and
                  Accessible (Get_Address'Result);

      --  Compute bitmask for word at Index
      function Get_Bitmask (Index : Word64_Index) return Types.Word64
      with Pre => Index in From_Word64 .. To_Word64;

      --------------------------------------------------------------------------

      function Get_Address
        (Index : Word64_Index;
         Mode  : RW_Type)
         return Types.Physical_Address_Type
      is
         RW_Index : constant Natural
           := (if Mode = Read then Index else Index + Write_Offset);
         Offset : constant Types.Physical_Address_Type
           := Word64_Bytes * Types.Physical_Address_Type (RW_Index);
         Address : constant Types.Physical_Address_Type
           := MSR_Bitmap + Offset;
      begin
         Accessible_Lemma (MSR_Bitmap);

         return Address;
      end Get_Address;

      --------------------------------------------------------------------------

      function Get_Bitmask (Index : Word64_Index) return Types.Word64
      is
         From_Bit : constant Bit_Range
           := (if Index = From_Word64
               then Bit_Range (From mod Word64_Bits)
               else Bit_Range'First);
         To_Bit   : constant Bit_Range
           := (if Index = To_Word64
               then Bit_Range (To mod Word64_Bits)
               else Bit_Range'Last);
      begin
         return 2 ** (To_Bit + 1) - 2 ** From_Bit;
      end Get_Bitmask;

   begin
      if To < From then
         return;
      end if;

      for Current in From_Word64 .. To_Word64 loop
         pragma Loop_Invariant (Invariant);
         pragma Loop_Invariant
           (for all M in Memory_Range =>
             (if M < To_Range (MSR_Bitmap) or
                 M >= To_Range (MSR_Bitmap + Config.Page_Size)
              then Get_Memory_Model (M) = Old_Model (M)));

         Set_Read_Bitmap :
         declare
            Read_Address  : constant Types.Physical_Address_Type
              := Get_Address (Index => Current, Mode => Read);
         begin
            Word := Low_Level.Get (Address => Read_Address);

            case Mode is
               --  Bitmaps follow inverted logic.
               when R | RW =>
                  Low_Level.Set (Address => Read_Address,
                                 Value   => Word and not Get_Bitmask (Current));
               when others =>
                  Low_Level.Set (Address => Read_Address,
                                 Value   => Word or Get_Bitmask (Current));
            end case;
         end Set_Read_Bitmap;

         pragma Assert
           (for all M in Memory_Range =>
             (if M < To_Range (MSR_Bitmap) or
                 M >= To_Range (MSR_Bitmap + Config.Page_Size)
              then Get_Memory_Model (M) = Old_Model (M)));

         Set_Write_Bitmap :
         declare
            Write_Address  : constant Types.Physical_Address_Type
              := Get_Address (Index => Current, Mode => Write);
         begin
            Word := Low_Level.Get (Address => Write_Address);

            case Mode is
               when W | RW =>
                  Low_Level.Set (Address => Write_Address,
                                 Value   => Word and not Get_Bitmask (Current));
               when others =>
                  Low_Level.Set (Address => Write_Address,
                                 Value   => Word or Get_Bitmask (Current));
            end case;
         end Set_Write_Bitmap;

         pragma Assert (Invariant);
         pragma Assert
           (for all M in Memory_Range =>
             (if M < To_Range (MSR_Bitmap) or
                 M >= To_Range (MSR_Bitmap + Config.Page_Size)
              then Get_Memory_Model (M) = Old_Model (M)));
      end loop;
   end Set_MSR_Range;

end Tau0.Data.Memory.Bitmaps;
