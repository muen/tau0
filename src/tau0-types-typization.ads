--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Types.Typization
with Abstract_State => null
is

   type Typization_Type is (Reserved, Undefined, Zeroed, Used, Device_Page);

   --  Specifies valid typization transitions
   function Transition_Valid (Before, After : Typization_Type) return Boolean
   with Ghost;

   function Transition_Valid (Before, After : Typization_Type) return Boolean
   is ((Before = Undefined and After = Zeroed) or
       (Before = Undefined and After = Reserved) or
       (Before = Zeroed    and After = Zeroed) or
       (Before = Undefined and After = Used) or
       (Before = Zeroed    and After = Used) or
       (Before = Used      and After = Undefined));

   --  Used in Data.Memory.Typization as a model of the current page typization.
   --  In the model, every address is assigned a typization for convenience, but
   --  note that for two addresses with the same page, their typizations are
   --  equal.
   type Typization_Model_Type is
     array (Types.Tau0_Address_Type) of Typization_Type
   with Ghost;

   -----------------------------------------------------------------------------

   type Full_Typization_Type is
     (Reserved, Undefined, Zeroed, Device_Page,   --  as in Typization_Type
      EPT4,      EPT3,      EPT2,      EPT1,      --  \
      IA32e_PT4, IA32e_PT3, IA32e_PT2, IA32e_PT1, --   |
      MPT4,      MPT3,      MPT2,      MPT1,      --   |
      VTd_PT4,   VTd_PT3,   VTd_PT2,   VTd_PT1,   --   |
      Tau0_PT4,  Tau0_PT3,  Tau0_PT2,  Tau0_PT1,  --   |
      MR_Page,                                    --   |- Used
      IO_Bitmap_Low, IO_Bitmap_High,              --   |
      MSR_Bitmap,                                 --   |
      VTd_Root_Table, VTd_Context_Table,          --   |
      VTd_IR_Table)                               --  /
   with Ghost;

   subtype Unused_Typization_Type is Full_Typization_Type
     range Reserved .. Zeroed
   with Ghost;

   subtype Used_Typization_Type is Full_Typization_Type
     range EPT4 .. VTd_IR_Table
   with Ghost;

   subtype Paging_Typization_Type is Full_Typization_Type
     range Device_Page .. MR_Page
   with Ghost;

   subtype Subject_Bitmap_Type is Full_Typization_Type
     range IO_Bitmap_Low .. MSR_Bitmap
   with Ghost;

   function Is_Page_Table (T : Full_Typization_Type) return Boolean
   is (T in EPT4 .. Tau0_PT1)
   with Ghost;

   function Is_Subject_Page_Table (T : Full_Typization_Type) return Boolean
   is (T in EPT4 .. IA32e_PT1)
   with Ghost;

   function Is_EPT (T : Full_Typization_Type) return Boolean
   is (T in EPT4 .. EPT1)
   with Ghost;

   function Is_IA32e_PT (T : Full_Typization_Type) return Boolean
   is (T in IA32e_PT4 .. IA32e_PT1)
   with Ghost;

   function Is_MPT (T : Full_Typization_Type) return Boolean
   is (T in MPT4 .. MPT1)
   with Ghost;

   function Is_VTd_PT (T : Full_Typization_Type) return Boolean
   is (T in VTd_PT4 .. VTd_PT1)
   with Ghost;

   function Is_Tau0_PT (T : Full_Typization_Type) return Boolean
   is (T in Tau0_PT4 .. Tau0_PT1)
   with Ghost;

   function Level (T : Paging_Typization_Type) return Types.Level_Type
   is (case T is
       when EPT4 | IA32e_PT4 | MPT4 | VTd_PT4 | Tau0_PT4 => 4,
       when EPT3 | IA32e_PT3 | MPT3 | VTd_PT3 | Tau0_PT3 => 3,
       when EPT2 | IA32e_PT2 | MPT2 | VTd_PT2 | Tau0_PT2 => 2,
       when EPT1 | IA32e_PT1 | MPT1 | VTd_PT1 | Tau0_PT1 => 1,
       when MR_Page | Device_Page                        => 0)
   with Ghost;

end Tau0.Types.Typization;
