--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Data.Memory.Full_Typization;

package body Tau0.Data.Memory.VTd_IOMMU
with Refined_State => (State => (Root_Table_Present_Flag, Root_Table_Pointer))
is

   -----------------------------------------------------------------------------

   function Get_Context_Table_Entry
     (Bus  : Types.Device.PCI_Bus_Range;
      Dev  : Types.Device.PCI_Device_Range;
      Func : Types.Device.PCI_Function_Range)
      return Context_Entry_Type
   is
      use type Types.Word128;

      Base_Address  : constant Types.Physical_Address_Type
        := Context_Table_Address (Bus => Bus);
      Entry_Address : constant Types.Physical_Address_Type
        := Base_Address + Context_Table_Offset (Dev => Dev, Func => Func);
      Word          : Types.Word128;
   begin
      Full_Typization.VTd_Context_Table_Lemma
        (Address => Entry_Address,
         Bus     => Bus);
      Full_Typization.VTd_Context_Table_Lemma
        (Address => Entry_Address + 8,
         Bus     => Bus);

      Word := Low_Level.Get (Entry_Address);

      return From_Word128 (Word);
   end Get_Context_Table_Entry;

   -----------------------------------------------------------------------------

   function Get_Root_Table_Entry
     (Bus : Types.Device.PCI_Bus_Range)
      return Root_Entry_Type
   is
      Entry_Address : constant Types.Physical_Address_Type
        := Root_Table_Address + Root_Table_Offset (Bus => Bus);
   begin
      Full_Typization.VTd_Root_Table_Lemma (Address => Entry_Address);
      Accessible_Lemma (Root_Table_Address);

      return From_Word128 (Low_Level.Get (Entry_Address));
   end Get_Root_Table_Entry;

   -----------------------------------------------------------------------------

   procedure Root_Table_Entry_Lemma
     (Old_Model : Memory_Model_Type;
      New_Model : Memory_Model_Type)
   is
   begin
      null;
   end Root_Table_Entry_Lemma;

   -----------------------------------------------------------------------------

   function Root_Table_Model return Types.VTd_Tables.Root_Table_Model_Type
   is (Root_Table_Model_Using (Get_Memory_Model));

   -----------------------------------------------------------------------------

   function Root_Table_Model_Using
     (Model : Memory_Model_Type)
      return Types.VTd_Tables.Root_Table_Model_Type
   is
      Result : Types.VTd_Tables.Root_Table_Model_Type
        := (others => Types.VTd_Tables.Root_Entry_Type'(Present => False));
   begin
      for Bus in Types.Device.PCI_Bus_Range loop
         pragma Loop_Invariant
           (for all B in Types.Device.PCI_Bus_Range =>
           (if B < Bus
            then Result (B) = Model_Root_Table_Entry_Using
                   (Model => Model,
                    Bus   => B)));

         Result (Bus) := Model_Root_Table_Entry_Using
           (Model => Model,
            Bus   => Bus);
      end loop;

      return Result;
   end Root_Table_Model_Using;

end Tau0.Data.Memory.VTd_IOMMU;
