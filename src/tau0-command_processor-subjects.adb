--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

with Tau0.Commands.Subjects.Paging;
with Tau0.Types.Parameters;

package body Tau0.Command_Processor.Subjects
is

   -----------------------------------------------------------------------------

   procedure Activate_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Page_Table_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Paging.Activate_Page_Table
           (Subject         =>
              Command.Activate_Page_Table_Subject_Params.Subject,
            Level           =>
              Command.Activate_Page_Table_Subject_Params.Level,
            Virtual_Address =>
              Command.Activate_Page_Table_Subject_Params.Virtual_Address,
            Status          => Status);
      end if;
   end Activate_Page_Table;

   -----------------------------------------------------------------------------

   procedure Activate_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Activate_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Activate_Subject
           (Subject => Command.Activate_Subject_Params.Subject,
            Status  => Status);
      end if;
   end Activate_Subject;

   -----------------------------------------------------------------------------

   procedure Assign_Device_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Assign_Device_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Assign_Device
           (Subject => Command.Assign_Device_Subject_Params.Subject,
            Device  => Command.Assign_Device_Subject_Params.Device,
            Status  => Status);
      end if;
   end Assign_Device_Subject;

   -----------------------------------------------------------------------------

   procedure Assign_IRQ_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Assign_IRQ_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Assign_IRQ
           (Subject   => Command.Assign_IRQ_Subject_Params.Subject,
            Device    => Command.Assign_IRQ_Subject_Params.Device,
            Interrupt => Command.Assign_IRQ_Subject_Params.IRQ,
            Status    => Status);
      end if;
   end Assign_IRQ_Subject;

   -----------------------------------------------------------------------------

   procedure Attach_Memory_Region_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Attach_Memory_Region_Subject_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Attach_Memory_Region
           (Subject          =>
              Command.Attach_Memory_Region_Subject_Params.Subject,
            Region           =>
              Command.Attach_Memory_Region_Subject_Params.Region,
            Use_Base_Address =>
              Command.Attach_Memory_Region_Subject_Params.Use_Base_Address,
            Base_Address     =>
              Command.Attach_Memory_Region_Subject_Params.Base_Address,
            Offset           =>
              Command.Attach_Memory_Region_Subject_Params.Offset,
            Length           =>
              Command.Attach_Memory_Region_Subject_Params.Length,
            Index            =>
              Command.Attach_Memory_Region_Subject_Params.Index,
            Writable         =>
              Command.Attach_Memory_Region_Subject_Params.Writable,
            Executable       =>
              Command.Attach_Memory_Region_Subject_Params.Executable,
            Status           => Status);
      end if;
   end Attach_Memory_Region_Subject;

   -----------------------------------------------------------------------------

   procedure Create_Page_Table
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Create_Page_Table_Subject_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Paging.Create_Page_Table
           (Page            =>
              Command.Create_Page_Table_Subject_Params.Page,
            Subject         =>
              Command.Create_Page_Table_Subject_Params.Subject,
            Level           =>
              Command.Create_Page_Table_Subject_Params.Level,
            Virtual_Address =>
              Command.Create_Page_Table_Subject_Params.Virtual_Address,
            Readable        =>
              Command.Create_Page_Table_Subject_Params.Readable,
            Writable        =>
              Command.Create_Page_Table_Subject_Params.Writable,
            Executable      =>
              Command.Create_Page_Table_Subject_Params.Executable,
            Status          => Status);
      end if;
   end Create_Page_Table;

   -----------------------------------------------------------------------------

   procedure Create_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Create_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Create_Subject
           (Subject    => Command.Create_Subject_Params.Subject,
            Paging     => Command.Create_Subject_Params.Paging,
            IO_Bitmap  => Command.Create_Subject_Params.IO_Bitmap,
            MSR_Bitmap => Command.Create_Subject_Params.MSR_Bitmap,
            CPU        => Command.Create_Subject_Params.CPU,
            Status     => Status);
      end if;
   end Create_Subject;

   -----------------------------------------------------------------------------

   procedure Lock_Subject
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Lock_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Lock_Subject
           (Subject => Command.Lock_Subject_Params.Subject,
            Status  => Status);
      end if;
   end Lock_Subject;

   -----------------------------------------------------------------------------

   procedure Map_Device_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if
        not Types.Parameters.Valid (Command.Map_Device_Page_Subject_Params)
      then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Paging.Map_Device_Page
           (Subject         =>
              Command.Map_Device_Page_Subject_Params.Subject,
            Device          =>
              Command.Map_Device_Page_Subject_Params.Device,
            Page            =>
              Command.Map_Device_Page_Subject_Params.Page,
            Virtual_Address =>
              Command.Map_Device_Page_Subject_Params.Virtual_Address,
            Writable        =>
              Command.Map_Device_Page_Subject_Params.Writable,
            Executable      =>
              Command.Map_Device_Page_Subject_Params.Executable,
            Status          => Status);
      end if;
   end Map_Device_Page;

   -----------------------------------------------------------------------------

   procedure Map_Page
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Map_Page_Subject_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Paging.Map_Page
           (Subject         => Command.Map_Page_Subject_Params.Subject,
            Virtual_Address => Command.Map_Page_Subject_Params.Virtual_Address,
            Table_Index     => Command.Map_Page_Subject_Params.Table_Index,
            Offset          => Command.Map_Page_Subject_Params.Offset,
            Status          => Status);
      end if;
   end Map_Page;

   -----------------------------------------------------------------------------

   procedure Set_IO_Port_Range
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Set_IO_Port_Range_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Set_IO_Port_Range
           (Subject => Command.Set_IO_Port_Range_Params.Subject,
            Device  => Command.Set_IO_Port_Range_Params.Device,
            From    => Command.Set_IO_Port_Range_Params.From,
            To      => Command.Set_IO_Port_Range_Params.To,
            Mode    => Command.Set_IO_Port_Range_Params.Mode,
            Status  => Status);
      end if;
   end Set_IO_Port_Range;

   -----------------------------------------------------------------------------

   procedure Set_MSR_Range
     (Command :     Types.Command.Command_Type;
      Status  : out Types.Command.Status_Type)
   is
   begin
      if not Types.Parameters.Valid (Command.Set_MSR_Range_Params) then
         Status := Types.Command.Invalid_Parameters;
      else
         Commands.Subjects.Set_MSR_Range
           (Subject => Command.Set_MSR_Range_Params.Subject,
            From    => Command.Set_MSR_Range_Params.From,
            To      => Command.Set_MSR_Range_Params.To,
            Mode    => Command.Set_MSR_Range_Params.Mode,
            Status  => Status);
      end if;
   end Set_MSR_Range;

end Tau0.Command_Processor.Subjects;
