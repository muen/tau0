--
--  Copyright (C) 2019  secunet Security Networks AG
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--

package Tau0.Config
with
  Initial_Condition =>
     -- Physical Addresses must be mapped within virtual memory
     Bits_Physical_Address <= Bits_Virtual_Address - 1 and
     Tau0_Private_Memory_Size <= 2 ** Bits_Tau0_Private_Address and
     --  Prevent overlap of typization and memory array.
     Typization_Base + 2 ** (Bits_Tau0_Address - 12) < Memory_Map_Base
is

   Max_Phys_Address          : constant := 46;
   Page_Size                 : constant := 4096; --  TODO: move to Constants
   Bits_Virtual_Address      : constant := 48;
   Bits_Tau0_Address         : constant := 47;
   Bits_Physical_Address     : constant := Max_Phys_Address;
   Bits_Tau0_Private_Address : constant := 32;

   --  Actual memory sizes.
   Tau0_Private_Memory_Size : constant := 2 ** Bits_Tau0_Private_Address;

   --  Address at which system memory is mapped into Tau0's address space
   Memory_Map_Base : constant := 2 ** 45;
   --  Address at which typization array is mapped into Tau0's address space
   Typization_Base : constant := 2 ** 44;

   Max_CPUs                : constant := 32;
   Max_Roots               : constant := 4096;
   Max_Devices             : constant := 32;
   Max_Devices_Per_Subject : constant := 16;
   Max_Devices_Per_Kernel  : constant := 32;
   Max_Devices_Per_Domain  : constant := 8;
   Max_IRQs_Per_Subject    : constant := 128;
   Size_MR_Table           : constant := 8192;

   Max_IO_Port_Intervals_Per_Device : constant := 16;
   Max_Memory_Regions_Per_Device    : constant := 64;
   Max_IRQs_Per_Device              : constant := 36;

end Tau0.Config;
