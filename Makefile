include ../../Makeconf

DUMMY := $(shell mkdir -p $(GEN_DIR))

SPARK2014_WARNINGS ?= continue

SPECS  = $(shell find src/ -name '*.ads')
BODIES = $(shell find src/ -name '*.adb')

BUILD_GPR = tau0.gpr
PROOF_GPR = tau0_proof.gpr

GEN_DIR = generated

SCHEMA_FILE  = schema/$*.xsd
XSD_FILES    = $(shell find schema/ -name '*.xsd')
SCHEMA_SPECS = $(GEN_DIR)/tau0-command_stream_schema.ads

SPARK2014_OPTS  = $(filter-out --warnings=%,$(SPARK_OPTS))
SPARK2014_OPTS += \
	--warnings=$(SPARK2014_WARNINGS) \
	--assumptions \
	--no-loop-unrolling \
	--no-axiom-guard \
	--info

BUILD = bin/tau0_main
PROVE = obj/gnatprove/gnatprove.out

DESTDIR ?= /usr/local/bin

all: $(BUILD)

$(BUILD): $(SPECS) $(BODIES) $(SCHEMA_SPECS)
	@$(E) tau0 Build "gprbuild -v $(BUILD_OPTS) -P $(BUILD_GPR)"

proof: $(PROVE)

# FIXME: --no-axiom-guard is workaround for [RC12-022]
$(PROVE): $(SPECS) $(BODIES) $(SCHEMA_SPECS)
	@$(E) tau0 Proof "gnatprove $(SPARK2014_OPTS) -P $(PROOF_GPR)"
	@touch $@

$(GEN_DIR)/tau0-%_schema.ads: $(XSD_FILES) $(RESOLVE_XSL)
	@$(E) tau0 "XSLT $*" \
		"xsltproc $(RESOLVE_XSL) $(SCHEMA_FILE) > $(GEN_DIR)/$*.xsd"
	@$(E) tau0 "Xml2Ada $*" \
		"$(XML2ADA) Tau0.$*_schema $(GEN_DIR)/$*.xsd $@"

install: $(BUILD)
	install -d $(DESTDIR)
	install $< $(DESTDIR)

clean:
	rm -rf bin obj generated
